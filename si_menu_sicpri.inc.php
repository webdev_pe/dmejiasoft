<h3><a href="#">CAJA</a></h3>
<div style="padding:5px;">
  <ul id="menu_sicpri">
      <li class="menu_li"><span class="ui-icon ui-icon-plus"></span><a href="sicpri/00_caja/01_registrar.php">Registrar Caja</a></li>
      <!--li class="menu_li"><span class="ui-icon ui-icon-pencil"></span><a href="sicpri/00_caja/02_modificar.php">Modificar Caja</a></li>
      <li class="menu_li"><span class="ui-icon ui-icon-trash"></span><a href="sicpri/00_caja/03_eliminar.php">Eliminar Caja</a></li>
      <li class="menu_li"><span class="ui-icon ui-icon-clipboard"></span><a href="sicpri/00_caja/05_reportes.php">Generar Reporte</a></li-->
      <li class="menu_li"><span class="ui-icon ui-icon-folder-open"></span><a href="sicpri/00_caja/06_historial.php">Historial de Caja</a></li> 
      <li class="menu_li"><span class="ui-icon ui-icon-folder-open"></span><a href="sicpri/00_caja/07_asignar.php">Asignar Monto a Operador</a></li>             
  </ul>
</div>

<h3><a href="#">COBRANZAS</a></h3>
<div style="padding:5px;">
  <ul id="menu_sicpri">
      <li class="menu_li"><span class="ui-icon ui-icon-plus"></span><a href="sicpri/01_cobranzas/01_registrar.php">Registrar Cobranza</a></li>
      <li class="menu_li"><span class="ui-icon ui-icon-clipboard"></span><a href="sicpri/01_cobranzas/05_reportes.php">Generar Reporte</a></li>    
      <!--li class="menu_li"><span class="ui-icon ui-icon-pencil"></span><a href="sicpri/01_cobranzas/02_modificar.php">Modificar Cobranza</a></li>
      <li class="menu_li"><span class="ui-icon ui-icon-trash"></span><a href="sicpri/01_cobranzas/03_eliminar.php">Eliminar Cobranza</a></li>
      <li class="menu_li"><span class="ui-icon ui-icon-search"></span><a href="sicpri/01_cobranzas/04_buscar.php">Buscar Cobranza</a></li>
      <li class="menu_li"><span class="ui-icon ui-icon-clipboard"></span><a href="sicpri/01_cobranzas/05_reportes.php">Generar Reporte</a></li>
      <li class="menu_li"><span class="ui-icon ui-icon-document"></span><a href="sicpri/01_cobranzas/06_documentacion.php">Terminar Cobranza</a></li-->
      <li class="menu_li"><span class="ui-icon ui-icon-folder-open"></span><a href="sicpri/01_cobranzas/06_historial.php">Historial de Cobranzas</a></li> 
      <!--li class="menu_li"><span class="ui-icon ui-icon-folder-open"></span><a href="sicpri/01_cobranzas/07_reportes.php">Reporte de Cobranzas</a></li>  
      <li class="menu_li"><span class="ui-icon ui-icon-folder-open"></span><a href="sicpri/01_cobranzas/08_reportes_hoy.php">Reporte de Cobranzas del D&iacute;a</a></li>  
      <li class="menu_li"><span class="ui-icon ui-icon-folder-open"></span><a href="sicpri/01_cobranzas/09_mixto_cp.php">Reporte Mixto General</a></li>       
      <li class="menu_li"><span class="ui-icon ui-icon-clipboard"></span><a href="sicpri/01_cobranzas/10_hoja.php">Formato hoja de cobro</a></li-->             
  </ul>
</div>

<h3><a href="#">CR&Eacute;DITOS</a></h3>
<div style="padding:5px;">
  <ul id="menu_sicpri">
      <li class="menu_li"><span class="ui-icon ui-icon-plus"></span><a href="sicpri/02_prestamos/01_registrar.php">Registrar Cr&eacute;dito</a></li>
      <li class="menu_li"><span class="ui-icon ui-icon-pencil"></span><a href="sicpri/02_prestamos/02_modificar.php">Modificar Cr&eacute;dito</a></li>
      <li class="menu_li"><span class="ui-icon ui-icon-trash"></span><a href="sicpri/02_prestamos/03_eliminar.php">Eliminar Cr&eacute;dito</a></li>
      <!--li class="menu_li"><span class="ui-icon ui-icon-search"></span><a href="sicpri/02_prestamos/04_buscar.php">Buscar Pr&eacute;stamo</a></li-->
      <?php if($_SESSION['p1']=="1"){?>
      <li class="menu_li"><span class="ui-icon ui-icon-clipboard"></span><a href="sicpri/02_prestamos/05_reportes.php">Generar Reporte de Cr&eacute;ditos</a></li>
      <?php }?>
      <?php if($_SESSION['p2']=="1"){?>
      <li class="menu_li"><span class="ui-icon ui-icon-folder-open"></span><a href="sicpri/02_prestamos/06_historial.php">Historial de Cr&eacute;ditos</a></li>
      <?php }?>
      <?php if($_SESSION['p3']=="1"){?>
      <li class="menu_li"><span class="ui-icon ui-icon-folder-open"></span><a href="sicpri/02_prestamos/09_vigentes.php">Cr&eacute;ditos Vigentes</a></li>
      <?php }?>
      <?php if($_SESSION['p4']=="1"){?>
      <li class="menu_li"><span class="ui-icon ui-icon-folder-open"></span><a href="sicpri/02_prestamos/07_cancelados.php">Cr&eacute;ditos Cancelados</a></li>
      <?php }?>
      <?php if($_SESSION['p5']=="1"){?>
      <li class="menu_li"><span class="ui-icon ui-icon-folder-open"></span><a href="sicpri/02_prestamos/08_hoy.php">Cr&eacute;ditos Vencidos</a></li>
      <?php }?>
      <?php if($_SESSION['p6']=="1"){?>
      <li class="menu_li"><span class="ui-icon ui-icon-folder-open"></span><a href="sicpri/02_prestamos/11_morosos.php">Cr&eacute;ditos Morosos</a></li>
      <?php }?>
  </ul>
</div>

<h3><a href="#">CLIENTES</a></h3>
<div style="padding:5px;">
  <ul id="menu_sicpri">
      <li class="menu_li"><span class="ui-icon ui-icon-plus"></span><a href="sicpri/03_clientes/01_registrar.php">Registrar Cliente</a></li>
      <li class="menu_li"><span class="ui-icon ui-icon-pencil"></span><a href="sicpri/03_clientes/02_modificar.php">Modificar Cliente</a></li>
      <li class="menu_li"><span class="ui-icon ui-icon-trash"></span><a href="sicpri/03_clientes/03_eliminar.php">Eliminar Cliente</a></li>
      <!--li class="menu_li"><span class="ui-icon ui-icon-search"></span><a href="sicpri/03_clientes/04_buscar.php">Buscar Cliente</a></li-->
      <li class="menu_li"><span class="ui-icon ui-icon-clipboard"></span><a href="sicpri/03_clientes/05_reportes.php">Generar Reporte</a></li>
      <li class="menu_li"><span class="ui-icon ui-icon-folder-open"></span><a href="sicpri/03_clientes/06_historial.php">Historial de Clientes</a></li>
  </ul>
</div>

<h3><a href="#">ZONAS</a></h3>
<div style="padding:5px;">
  <ul id="menu_sicpri">
      <li class="menu_li"><span class="ui-icon ui-icon-plus"></span><a href="sicpri/04_zonas/01_registrar.php">Registrar Zona</a></li>
      <li class="menu_li"><span class="ui-icon ui-icon-pencil"></span><a href="sicpri/04_zonas/02_modificar.php">Modificar Zona</a></li>
      <li class="menu_li"><span class="ui-icon ui-icon-trash"></span><a href="sicpri/04_zonas/03_eliminar.php">Eliminar Zona</a></li>
      <!--li class="menu_li"><span class="ui-icon ui-icon-search"></span><a href="sicpri/04_zonas/04_buscar.php">Buscar Zona</a></li-->
      <li class="menu_li"><span class="ui-icon ui-icon-clipboard"></span><a href="sicpri/04_zonas/05_reportes.php">Generar Reporte</a></li>
      <li class="menu_li"><span class="ui-icon ui-icon-folder-open"></span><a href="sicpri/04_zonas/06_historial.php">Historial de Zonas</a></li>
  </ul>
</div>

<h3><a href="#">GALER&Iacute;AS</a></h3>
<div style="padding:5px;">
  <ul id="menu_sicpri">
      <li class="menu_li"><span class="ui-icon ui-icon-plus"></span><a href="sicpri/05_galerias/01_registrar.php">Registrar Galer&iacute;a</a></li>
      <li class="menu_li"><span class="ui-icon ui-icon-pencil"></span><a href="sicpri/05_galerias/02_modificar.php">Modificar Galer&iacute;a</a></li>
      <li class="menu_li"><span class="ui-icon ui-icon-trash"></span><a href="sicpri/05_galerias/03_eliminar.php">Eliminar Galer&iacute;a</a></li>
      <!--li class="menu_li"><span class="ui-icon ui-icon-search"></span><a href="sicpri/05_galerias/04_buscar.php">Buscar Galer&iacute;a</a></li-->
      <li class="menu_li"><span class="ui-icon ui-icon-clipboard"></span><a href="sicpri/05_galerias/05_reportes.php">Generar Reporte</a></li>
      <li class="menu_li"><span class="ui-icon ui-icon-folder-open"></span><a href="sicpri/05_galerias/06_historial.php">Historial de Galer&iacute;as</a></li>
  </ul>
</div>

<!--h3><a href="#">MOTIVOS</a></h3>
<div style="padding:5px;">
  <ul id="menu_sicpri">
      <li class="menu_li"><span class="ui-icon ui-icon-plus"></span><a href="sicpri/06_motivos/01_registrar.php">Registrar Motivo</a></li>
      <li class="menu_li"><span class="ui-icon ui-icon-pencil"></span><a href="sicpri/06_motivos/02_modificar.php">Modificar Motivo</a></li>
      <li class="menu_li"><span class="ui-icon ui-icon-trash"></span><a href="sicpri/06_motivos/03_eliminar.php">Eliminar Motivo</a></li>
      <li class="menu_li"><span class="ui-icon ui-icon-search"></span><a href="sicpri/06_motivos/04_buscar.php">Buscar Motivo</a></li>
      <li class="menu_li"><span class="ui-icon ui-icon-clipboard"></span><a href="sicpri/06_motivos/05_reportes.php">Generar Reporte</a></li>
      <li class="menu_li"><span class="ui-icon ui-icon-folder-open"></span><a href="sicpri/06_motivos/07_historial.php">Historial de Motivos</a></li>
  </ul>
</div-->

<h3><a href="#">USUARIOS</a></h3>
      
<div style="padding:5px;">
  <ul id="menu_sicpri">
  	  <?php if($_SESSION['p7']=="1"){?>
      <li class="menu_li"><span class="ui-icon ui-icon-plus"></span><a href="sicpri/12_usuarios/01_registrar.php">Registrar Usuario</a></li>
      <li class="menu_li"><span class="ui-icon ui-icon-pencil"></span><a href="sicpri/12_usuarios/02_modificar.php">Modificar Usuario</a></li>
      <li class="menu_li"><span class="ui-icon ui-icon-trash"></span><a href="sicpri/12_usuarios/03_eliminar.php">Eliminar Usuario</a></li>
      <!--li class="menu_li"><span class="ui-icon ui-icon-search"></span><a href="sicpri/12_usuarios/04_buscar.php">Buscar Usuario</a></li-->
      <li class="menu_li"><span class="ui-icon ui-icon-clipboard"></span><a href="sicpri/12_usuarios/05_reportes.php">Generar Reporte</a></li>
      <li class="menu_li"><span class="ui-icon ui-icon-folder-open"></span><a href="sicpri/12_usuarios/06_historial.php">Historial de Todos los Usuarios</a></li>
      <?php }?>
      <li class="menu_li"><span class="ui-icon ui-icon-folder-open"></span><a href="sicpri/12_usuarios/07_historial.php">Historial de Operadores</a></li>
      <?php if($_SESSION['p7']=="1"){?>
      <li class="menu_li"><span class="ui-icon ui-icon-folder-open"></span><a href="sicpri/12_usuarios/08_historial.php">Historial de Administradores</a></li>      <?php }?>      
  </ul>
      
</div>