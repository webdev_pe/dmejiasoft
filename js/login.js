$(document).ready(function(){ 
	var ch=0;
	$("#chk").click(function(){
		if(ch++ % 2 == 0){ $(this).css({"background":"url(images/login/chk_activo.png)"}); $("#rmbr").val("1");
		}else{ $(this).css({"background":"url(images/login/chk_desactivo.png)"}); $("#rmbr").val("0");}
	});
	
	$("#tag_recordar").click(function(){ $("#chk").trigger("click"); });
	
	$("#ingresar").click(function(){
		var u=$("#user").val();
		var p=$("#pswd").val();
		var r=$("#rmbr").val();		
   		//alert(u+"-"+p+"-"+r);
		$.post("si_login.php", {u:u,p:p,r:r}, function(data){ //alert("json:"+data.std);
			if(data.std=="0"){
				$("#msj").removeClass().addClass("ui-state-error ui-corner-all").html(data.msj).prepend("<span class='ui-icon ui-icon-alert'></span>").wrap("<div class='ui-widget'></div>");
			}else if(data.std=="1"){
				location.replace("main.php");
			}
		},"json");		
	});
	
	$("#user").focus(function(){ $(this).val(""); });
	$("#pswd").focus(function(){ $(this).val(""); });
	
	$("#user").keypress(function(e){ if(e.keyCode==13) $("#pswd").focus(); });
	$("#pswd").keypress(function(e){ if(e.keyCode==13) $("#ingresar").trigger("click"); });	
	
});