// JavaScript Document
var imgOrder = '';
$(function() {
  $("#sortable").sortable({
    update: function(event, ui) {
      imgOrder = $("#sortable").sortable('toArray').toString();
	  $.post("_temp_fotos.php", {opt:"a", imgOrder:imgOrder});
    }
  });
  $("#sortable").disableSelection();
});

$(document).ready(function(){
  /*$("#update_fotos").click(function(){
	//alert(imgOrder);
	$.post("_temp_fotos.php", {opt:"a", imgOrder:imgOrder});
  });*/
  $("#sortable li").each(function(){
	$(this).dblclick(function(){
	  if(confirm("Esta seguro de eliminar la foto?")){
		var img=$(this).attr("id");
		$(this).remove();
		$.post("_temp_fotos.php", {opt:"e", img:img});		  
	  }
	});
  });
  
  $("input[name='foto_vista']:radio").click(function(){	  
		var img=$(this).val();
		$.post("_temp_fotos.php", {opt:"p", img:img});
  });  
});	