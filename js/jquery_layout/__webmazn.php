<!DOCTYPE html> 
<HTML> 
<HEAD>
    <META http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <TITLE>SANSON INTERNATIONAL - GRUPO INMOBILIARIO</TITLE>
	<LINK rel="stylesheet" type="text/css" href="../js/jquery-ui-1.8.16.custom/development-bundle/themes/custom-theme/jquery.ui.all.css">
	<LINK rel="stylesheet" type="text/css" href="../js/jquery_layout/layout.css">
    
    <SCRIPT type="text/javascript" src="../js/jquery_layout/jquery-latest.js"></SCRIPT>
    <SCRIPT type="text/javascript" src="../js/jquery_layout/jquery-ui-latest.js"></SCRIPT>
    <SCRIPT type="text/javascript" src="../js/jquery_layout/jquery.layout-latest.js"></SCRIPT>
    <SCRIPT type="text/javascript" src="../js/jquery_layout/jquery.layout.resizeTabLayout.min-1.1.js"></SCRIPT>
    <SCRIPT type="text/javascript" src="../js/jquery_layout/jquery.layout.resizePaneAccordions.min-1.0.js"></SCRIPT>
    <SCRIPT type="text/javascript" src="../js/jquery_layout/themeswitchertool.js"></SCRIPT> 
    <SCRIPT type="text/javascript" src="../js/jquery_layout/debug.js"></SCRIPT>
    <SCRIPT type="text/javascript" src="../js/jquery_layout/layout.js"></SCRIPT> 
 
	<link type="text/css" href="../js/jquery-ui-1.8.16.custom/css/custom-theme/jquery-ui-1.8.16.custom.css" rel="stylesheet" />	
	<script type="text/javascript" src="../js/jquery-ui-1.8.16.custom/js/jquery-ui-1.8.16.custom.min.js"></script>
	<script type="text/javascript" src="../js/jquery-ui-1.8.16.custom/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>    
    
	<script src="../js/ckeditor/ckeditor.js" type="text/javascript"></script>
	<script src="../js/ckeditor/_samples/sample.js" type="text/javascript"></script>
	<link href="../js/ckeditor/_samples/sample.css" rel="stylesheet" type="text/css" />    

	<script type="text/javascript"  src="../js/jquery_datatable/js/jquery.dataTables.js"></script>
    <script type="text/javascript"  src="../js/jquery_datatable/js/jquery.dataTables.columnFilter.js"></script>
    <script type="text/javascript"  src="../js/datatable/js/jquery-ui.js"></script>
    <!--link rel="stylesheet" type="text/css" href="jquery-ui-1.8.16.custom/css/custom-theme/jquery-ui-1.8.16.custom.css" media="screen" /-->
    <link rel="stylesheet" type="text/css" href="../js/jquery_datatable/css/demo_table_jui.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="../js/jquery_datatable/css/demo_table.css" media="screen" />

	<script type="text/javascript" src="../js/custom.js"></script>
	<LINK rel="stylesheet" type="text/css" href="../custom.css">    
</HEAD> 
<BODY class="custom"> 
<DIV id="outer-north">
    <!--DIV class="buttons">
        <BUTTON onClick="toggleCustomTheme()">Toggle Custom Theme</BUTTON>
        <BUTTON onClick="removeUITheme(); resizePageLayout()">Remove UI Theme</BUTTON>
        <BUTTON onClick="resizePageLayout()">Resize Layout</BUTTON>
    </DIV-->
    SANSON INTERNATIONAL - GRUPO INMOBILIARIO
</DIV>

<DIV id="page-loading">Cargando...</DIV>

<DIV id="outer-south" class="hidden"><div id="footer">SIMUS - POWERED BY: SANTEC&reg;</div></DIV>

<DIV id="outer-center" class="hidden">

    <UL id="tabbuttons" class="hidden">
        <LI class="tab1"><A href="#tab1">P&Aacute;GINA WEB - ISANSON</A></LI>
        <LI class="tab2"><A href="#tab2">SIMUS - ISANSON</A></LI>
        <LI class="tab3"><A href="#tab3">SOPORTE T&Eacute;CNICO</A></LI>
    </UL>

    <DIV id="tabpanels">

        <DIV id="tab1" class="tab-panel ui-tabs-hide">
            <DIV class="ui-layout-north ui-widget">
                <DIV class="toolbar ui-widget-content ui-state-active">
                    Herramientas de mantenimiento para la p&aacute;gina web.
                </DIV>
            </DIV>
            <DIV class="ui-layout-south ui-widget">
                <DIV class="toolbar ui-widget-content ui-state-default">
                    Hoy: <?=date("d-m-Y")?>
                </DIV>
            </DIV>
            <DIV class="ui-layout-center">
                <DIV class="ui-widget-header ui-corner-top">VISTA DEL CONTENIDO</DIV>
                <DIV id="notes" class="ui-widget-content">
                    <div id="contenido_web">

                    </div>
                </DIV>
                <DIV class="ui-widget-footer ui-widget-header ui-corner-bottom">www.isanson.com</DIV>
            </DIV>
            <DIV class="ui-layout-west">
<DIV class="ui-layout-north">
    <DIV class="ui-widget-header ui-corner-top">MENU QUIENES SOMOS</DIV>
    <DIV class="ui-widget-content">
    	<ul id="menu">
        <li><a href="../si_quienes_somos.php">Quienes somos</a></li>
        <li><a href="../si_nuestros_directores.php">Nuestro directores</a></li>
        <li><a href="../si_nuestro_equipo.php">Nuestro equipo</a></li>
        <li><a href="../si_nuestros_clientes.php">Nuestros clientes</a></li>
        </ul>
    </DIV>
    <DIV class="ui-widget-footer ui-widget-header ui-corner-bottom">Opciones Men&uacute; Quienes Somos</DIV>
</DIV>
<DIV class="ui-layout-center">
    <DIV class="ui-widget-header ui-corner-top">OTROS MENUS</DIV>
    <DIV class="ui-widget-content">
    	<ul id="menu">
        <li><a href="../si_referencias.php">Referencias</a></li>
        <li><a href="../si_trabaja_sup.php">Trabaja bloque sup.</a></li>
            <li><a href="../si_trabaja_inf.php">Trabaja bloque inf.</a></li>
        <li><a href="../si_servicios.php">Servicios</a></li>
        <li><a href="../si_inversiones.php">Inversiones</a></li>
        </ul>    
    </DIV>
    <DIV class="ui-widget-footer ui-widget-header ui-corner-bottom">Opciones p&aacute;ginas</DIV>
</DIV>
<DIV class="ui-layout-south">
    <DIV class="ui-widget-header ui-corner-top">NOTICIAS Y M&Aacute;S</DIV>
    <DIV class="ui-widget-content">
        <ul id="adicional">
        <li><a href="../si_agregar_noticias.php">Agregar Noticas</a></li>
        <li><a href="../si_todas_noticias.php">Todas Noticas</a></li>
        </ul>    
    </DIV>
    <DIV class="ui-widget-footer ui-widget-header ui-corner-bottom">Otras Men&uacute; Noticias</DIV>
</DIV>
            </DIV>
            <DIV class="ui-layout-east">
<DIV class="ui-layout-center">
    <DIV class="ui-widget-header ui-corner-top">ENLACES</DIV>
    <DIV class="ui-widget-content">
    	<ul id="links">
        <li><a href="http://www.isanson.com/" target="_blank">Sanson International</a></li>
        <li><a href="http://www.mlsdelperu.com/" target="_blank">MLS DEL PER&uacute;</a></li>
        <li><a href="http://urbania.pe/" target="_blank">URBANIA</a></li>
        <li><a href="http://www.google.com.pe/" target="_blank">Google</a></li>
        <li><a href="http://es.yahoo.com/" target="_blank">Yahoo</a></li>
        <li><a href="http://www.bing.com/" target="_blank">Bing</a></li>
        <li><a href="http://www.hotmail.com/" target="_blank">Hotmail</a></li>
        <li><a href="http://elcomercio.pe/" target="_blank">El Comercio</a></li>
        <li><a href="http://gestion.pe/" target="_blank">Gesti&oacute;n</a></li>
        <li><a href="http://www.facebook.com/" target="_blank">Facebook</a></li>
        <li><a href="http://www.twitter.com/" target="_blank">Twitter</a></li>
        <li><a href="http://www.youtube.com/" target="_blank">YouTube</a></li>
        <li><a href="http://www.linkedin.com/" target="_blank">LinkedIn</a></li>
        </ul>
    </DIV>
    <DIV class="ui-widget-footer ui-widget-header ui-corner-bottom">Enlaces r&aacute;pidos</DIV>
</DIV>
<DIV class="ui-layout-south">
    <DIV class="ui-widget-header ui-corner-top">Calendario</DIV>
    <DIV class="ui-widget-content">
        <DIV id="datepicker" style="overflow:hidden;"></DIV>
    </DIV>
    <DIV class="ui-widget-footer ui-widget-header ui-corner-bottom"><?=date("Y")?></DIV>
</DIV>
            </DIV>
        </DIV><!-- /#tab1 -->

        <DIV id="tab2" class="tab-panel ui-tabs-hide">
            <DIV class="ui-layout-north ui-widget">
                <DIV class="toolbar ui-widget-content ui-state-active">
                    Sanson International Modelo Unificado de Software</DIV>
            </DIV>
            <DIV class="ui-layout-south ui-widget">
                <DIV class="toolbar ui-widget-content ui-state-default">
                    SIMUS
                </DIV>
            </DIV>
            <DIV class="ui-layout-center">
                <DIV class="ui-widget-header ui-corner-top">Center-Center</DIV>
                <DIV class="ui-widget-content container">
<DIV class="accordion">

        <H3><A href="#">Section 1</A></H3>
        <DIV>
            <P>Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. 
                Integer ut neque. Vivamus nisi metus, molestie vel, gravida in, condimentum sit amet, nunc.</P>
            <P>Nam a nibh. Donec suscipit eros. Nam mi. Proin viverra leo ut odio. Curabitur malesuada. 
                Vestibulum a velit eu ante scelerisque vulputate.</P>
        </DIV>

        <H3><A href="#">Section 2</A></H3>
        <DIV>
            <P style="font-weight: bold;">Sed Non Urna</P>
            <P>Donec et ante. Phasellus eu ligula. Vestibulum sit amet purus.
                Vivamus hendrerit, dolor at aliquet laoreet, mauris turpis porttitor velit,
                faucibus interdum tellus libero ac justo.</P>
            <P>Vivamus non quam. In suscipit faucibus urna.</P>
        </DIV>

        <H3><A href="#">Section 3</A></H3>
        <DIV>
            <P>Nam enim risus, molestie et, porta ac, aliquam ac, risus. Quisque lobortis.
                Phasellus pellentesque purus in massa. Aenean in pede.</P>
            <P>Phasellus ac libero ac tellus pellentesque semper. Sed ac felis. Sed commodo, 
                magna quis lacinia ornare, quam ante aliquam nisi, eu iaculis leo purus venenatis dui.</P>
            <UL>
                <LI>List item one</LI>
                <LI>List item two</LI>
                <LI>List item three</LI>
                <LI>Another Item</LI>
                <LI>Another Item</LI>
                <LI>Another Item</LI>
                <LI>Another Item</LI>
                <LI>Another Item</LI>
                <LI>Another Item</LI>
                <LI>Another Item</LI>
                <LI>Another Item</LI>
                <LI>Another Item</LI>
                <LI>Another Item</LI>
                <LI>Another Item</LI>
                <LI>Another Item</LI>
                <LI>Another Item</LI>
                <LI>Another Item</LI>
                <LI>Another Item</LI>
                <LI>Another Item</LI>
                <LI>Another Item</LI>
                <LI>Another Item</LI>
                <LI>Another Item</LI>
                <LI>Another Item</LI>
                <LI>Another Item</LI>
                <LI>Another Item</LI>
                <LI>Another Item</LI>
                <LI>Another Item</LI>
                <LI>Another Item</LI>
                <LI>Another Item</LI>
            </UL>
        </DIV>

        <H3><A href="#">Section 4</A></H3>
        <DIV>
            <P>Cras dictum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames 
                ac turpis egestas.</P>
            <P>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
                Aenean lacinia mauris vel est.</P>
            <P>Suspendisse eu nisl. Nullam ut libero. Integer dignissim consequat lectus.
                Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</P>
        </DIV>

</DIV>
                </DIV>
                <DIV class="ui-widget-footer ui-widget-header ui-corner-bottom">Center-Center Footer</DIV>
            </DIV>
            <DIV class="ui-layout-west">
<DIV class="accordion">

        <H3><A href="#">PROPIEDADES</A></H3>
        <DIV>
	        <ul id="menu">
            	<li>Ingresar Propiedad</li>
                <li>Modificar Propiedad</li>
                <li>Eliminar Propiedad</li>
                <li>Buscar Propiedad</li>
                <li>Generar Reporte</li>
                <li>Propiedades Vendidas</li>
                <li>Propiedades Alquiladas</li>
                <li>Residencial</li>
                <li>Comercial</li>
                <li>Industrial</li>
                <li>Terreno</li>
                <li>Proyecto</li>
            </ul>
        </DIV>

        <H3><A href="#">PROPIETARIOS</A></H3>
        <DIV>
	        <ul id="menu">
            	<li>Ingresar Propietario</li>
                <li>Modificar Propietario</li>
                <li>Eliminar Propietario</li>
                <li>Buscar Propietario</li>
                <li>Generar Reporte</li>
                <li>Documentaci&oacute;n de Propietarios</li>
            </ul>
        </DIV>

        <H3><A href="#">VENDEDORES</A></H3>
        <DIV>
        </DIV>

        <H3><A href="#">COMPRADORES</A></H3>
        <DIV>
        </DIV>

        <H3><A href="#">CONTRATOS</A></H3>
        <DIV>
        </DIV>
        
        <H3><A href="#">LLAMADAS</A></H3>
        <DIV>
        </DIV>
        
        <H3><A href="#">REQUERIMIENTOS</A></H3>
        <DIV>
        </DIV>
        
        <H3><A href="#">CITAS</A></H3>
        <DIV>
        </DIV>
        
        <H3><A href="#">VISITAS</A></H3>
        <DIV>
        </DIV>

        <H3><A href="#">ZONAS</A></H3>
        <DIV>
        </DIV>
                
        <H3><A href="#">CONSTRUCTORAS</A></H3>
        <DIV>
        </DIV>
        
        <H3><A href="#">USUARIOS SIMUS</A></H3>
        <DIV>
        </DIV>
</DIV>
<!--
<DIV class="ui-layout-north">
    <DIV class="ui-widget-header ui-corner-top">West-North</DIV>
    <DIV class="ui-widget-content">
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
    </DIV>
    <DIV class="ui-widget-footer ui-widget-header ui-corner-bottom">West-North Footer</DIV>
</DIV>
<DIV class="ui-layout-center">
    <DIV class="ui-widget-header ui-corner-top">West-Center</DIV>
    <DIV class="ui-widget-content">
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
    </DIV>
    <DIV class="ui-widget-footer ui-widget-header ui-corner-bottom">West-Center Footer</DIV>
</DIV>
<DIV class="ui-layout-south">
    <DIV class="ui-widget-header ui-corner-top">West-South</DIV>
    <DIV class="ui-widget-content">
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
    </DIV>
    <DIV class="ui-widget-footer ui-widget-header ui-corner-bottom">West-South Footer</DIV>
</DIV>
-->
            </DIV>
            <DIV class="ui-layout-east">
<DIV class="ui-layout-center">
    <DIV class="ui-widget-header ui-corner-top">East-Center</DIV>
    <DIV class="ui-widget-content">
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
    </DIV>
    <DIV class="ui-widget-footer ui-widget-header ui-corner-bottom">East-Center Footer</DIV>
</DIV>
<DIV class="ui-layout-south">
    <DIV class="ui-widget-header ui-corner-top">East-South</DIV>
    <DIV class="ui-widget-content">
        <DIV><A href="#">Another Item</A></DIV>
        <DIV><A href="#">Another Item</A></DIV>
        <DIV><A href="#">Another Item</A></DIV>
        <DIV><A href="#">Another Item</A></DIV>
        <DIV><A href="#">Another Item</A></DIV>
        <DIV><A href="#">Another Item</A></DIV>
    </DIV>
    <DIV class="ui-widget-footer ui-widget-header ui-corner-bottom">East-South Footer</DIV>
</DIV>
            </DIV>
        </DIV><!-- /#tab2 -->

        <DIV id="tab3" class="tab-panel ui-tabs-hide">
            <DIV class="ui-layout-north ui-widget">
                <DIV class="toolbar ui-widget-content ui-state-active">
                    Toolbar - tab3
                </DIV>
            </DIV>
            <DIV class="ui-layout-south ui-widget">
                <DIV class="toolbar ui-widget-content ui-state-default">
                    Statusbar - tab3
                </DIV>
            </DIV>
            <DIV id="innerTabs" class="ui-layout-center container tabs">
                <DIV class="ui-widget-header ui-corner-top"> Center -Center </DIV>
                <UL>
                    <LI class="tab1"><A href="#simpleTab1">Tab #1</A></LI>
                    <LI class="tab2"><A href="#simpleTab2">Tab #2</A></LI>
                    <LI class="tab3"><A href="#simpleTab3">Tab #3</A></LI>
                </UL>
                <DIV class="ui-widget-content" style="border-top: 0;">

                    <DIV id="simpleTab1" class="container" style="height: 100%;">
                        <DIV class="accordion">
                
                            <H3><A href="#">Section 1</A></H3>
                            <DIV>
                                <P>Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. 
                                    Integer ut neque. Vivamus nisi metus, molestie vel, gravida in, condimentum sit amet, nunc.</P>
                                <P>Nam a nibh. Donec suscipit eros. Nam mi. Proin viverra leo ut odio. Curabitur malesuada. 
                                    Vestibulum a velit eu ante scelerisque vulputate.</P>
                            </DIV>
                    
                            <H3><A href="#">Section 2</A></H3>
                            <DIV>
                                <P style="font-weight: bold;">Sed Non Urna</P>
                                <P>Donec et ante. Phasellus eu ligula. Vestibulum sit amet purus.
                                    Vivamus hendrerit, dolor at aliquet laoreet, mauris turpis porttitor velit,
                                    faucibus interdum tellus libero ac justo.</P>
                                <P>Vivamus non quam. In suscipit faucibus urna.</P>
                            </DIV>
                    
                            <H3><A href="#">Section 3</A></H3>
                            <DIV>
                                <P>Nam enim risus, molestie et, porta ac, aliquam ac, risus. Quisque lobortis.
                                    Phasellus pellentesque purus in massa. Aenean in pede.</P>
                                <P>Phasellus ac libero ac tellus pellentesque semper. Sed ac felis. Sed commodo, 
                                    magna quis lacinia ornare, quam ante aliquam nisi, eu iaculis leo purus venenatis dui.</P>
                                <UL>
                                    <LI>List item one</LI>
                                    <LI>List item two</LI>
                                    <LI>List item three</LI>
                                    <LI>Another Item</LI>
                                    <LI>Another Item</LI>
                                    <LI>Another Item</LI>
                                    <LI>Another Item</LI>
                                    <LI>Another Item</LI>
                                    <LI>Another Item</LI>
                                    <LI>Another Item</LI>
                                    <LI>Another Item</LI>
                                    <LI>Another Item</LI>
                                    <LI>Another Item</LI>
                                    <LI>Another Item</LI>
                                    <LI>Another Item</LI>
                                    <LI>Another Item</LI>
                                    <LI>Another Item</LI>
                                    <LI>Another Item</LI>
                                    <LI>Another Item</LI>
                                    <LI>Another Item</LI>
                                    <LI>Another Item</LI>
                                    <LI>Another Item</LI>
                                    <LI>Another Item</LI>
                                    <LI>Another Item</LI>
                                    <LI>Another Item</LI>
                                    <LI>Another Item</LI>
                                    <LI>Another Item</LI>
                                    <LI>Another Item</LI>
                                    <LI>Another Item</LI>
                                </UL>
                            </DIV>
                    
                            <H3><A href="#">Section 4</A></H3>
                            <DIV>
                                <P>Cras dictum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames 
                                    ac turpis egestas.</P>
                                <P>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
                                    Aenean lacinia mauris vel est.</P>
                                <P>Suspendisse eu nisl. Nullam ut libero. Integer dignissim consequat lectus.
                                    Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</P>
                            </DIV>

                        </DIV>
                    </DIV>

                    <DIV id="simpleTab2">
                        Tab #2 Content
                        <P>List of Items</P>
                        <P>List of Items</P>
                        <P>List of Items</P>
                        <P>List of Items</P>
                        <P>List of Items</P>
                        <P>List of Items</P>
                        <P>List of Items</P>
                        <P>List of Items</P>
                        <P>List of Items</P>
                        <P>List of Items</P>
                        <P>List of Items</P>
                        <P>List of Items</P>
                        <P>List of Items</P>
                        <P>List of Items</P>
                        <P>List of Items</P>
                        <P>List of Items</P>
                        <P>List of Items</P>
                        <P>List of Items</P>
                        <P>List of Items</P>
                        <P>List of Items</P>
                        <P>List of Items</P>
                        <P>List of Items</P>
                        <P>List of Items</P>
                        <P>List of Items</P>
                        <P>List of Items</P>
                        <P>List of Items</P>
                        <P>List of Items</P>
                        <P>List of Items</P>
                        <P>List of Items</P>
                        <P>List of Items</P>
                    </DIV>

                    <DIV id="simpleTab3"> Tab #3 Content </DIV>

                </DIV>
                <DIV class="ui-widget-footer ui-widget-header ui-corner-bottom">Center-Center Footer</DIV>
            </DIV>
            <DIV class="ui-layout-west">
<DIV class="ui-layout-north">
    <DIV class="ui-widget-header ui-corner-top">West-North</DIV>
    <DIV class="ui-widget-content">
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
    </DIV>
    <DIV class="ui-widget-footer ui-widget-header ui-corner-bottom">West-North Footer</DIV>
</DIV>
<DIV class="ui-layout-center">
    <DIV class="ui-widget-header ui-corner-top">West-Center</DIV>
    <DIV class="ui-widget-content">
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
    </DIV>
    <DIV class="ui-widget-footer ui-widget-header ui-corner-bottom">West-Center Footer</DIV>
</DIV>
<DIV class="ui-layout-south">
    <DIV class="ui-widget-header ui-corner-top">West-South</DIV>
    <DIV class="ui-widget-content">
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
    </DIV>
    <DIV class="ui-widget-footer ui-widget-header ui-corner-bottom">West-South Footer</DIV>
</DIV>
            </DIV>
            <DIV class="ui-layout-east">
<DIV class="ui-layout-center">
    <DIV class="ui-widget-header ui-corner-top">East-Center</DIV>
    <DIV class="ui-widget-content">
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
        <P>List of Items</P>
    </DIV>
    <DIV class="ui-widget-footer ui-widget-header ui-corner-bottom">East-Center Footer</DIV>
</DIV>
<DIV class="ui-layout-south">
    <DIV class="ui-widget-header ui-corner-top">East-South</DIV>
    <DIV class="ui-widget-content">
        <DIV><A href="#">Another Item</A></DIV>
        <DIV><A href="#">Another Item</A></DIV>
        <DIV><A href="#">Another Item</A></DIV>
        <DIV><A href="#">Another Item</A></DIV>
        <DIV><A href="#">Another Item</A></DIV>
        <DIV><A href="#">Another Item</A></DIV>
    </DIV>
    <DIV class="ui-widget-footer ui-widget-header ui-corner-bottom">East-South Footer</DIV>
</DIV>
            </DIV>
        </DIV><!-- /#tab3 -->

    </DIV><!-- /#tabpanels -->

</DIV><!-- /#outer-center -->

</BODY>
</HTML>