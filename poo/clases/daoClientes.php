<?php
require("getConection.php");
class daoClientes extends getConection{
	
	public function traer_cliente($idu){
		$sql="select cod_cli from si_clientes where id_cli=".$idu."";
		parent::ejecutar_sql(base64_encode($sql));
		$row=parent::cantidad_sql();
		$cel=parent::resultado_sql();
		return ($row>0)?$cel['cod_cli']:"";
	}

	public function crear_codigo(){
		$sql="select cod_cli from si_clientes order by id_cli desc limit 1";
		parent::ejecutar_sql(base64_encode($sql));
		$row=parent::cantidad_sql();
		if($row>0){
			$cel=parent::resultado_sql();
			$ult=$cel['cod_cli'];
			$num=substr($ult,3,4);
			$inc=(int)$num+1;//incrementar
			$cod="CLI".str_pad($inc,4,"0",STR_PAD_LEFT);
		}else{
			$cod="CLI0001";
		}
		return $cod;
	}
	
	public function guardar_clientes($per,$nom,$ape,$dni,$nro,$dir,$tel,$cel,$eml,$mnt,$zon,$gal){
		$cod=$this->crear_codigo();
		$sql="insert into si_clientes (cod_cli, nom_cli, ape_cli, tip_cli, doc_cli, nro_cli, dir_cli, tel_cli, cel_cli, eml_cli, mnt_cli, id_zon, id_gal) 
		values ('$cod', '$nom', '$ape', '$per', '$dni', '$nro', '$dir', '$tel', '$cel', '$eml', $mnt, $zon, $gal)";
		$stm=parent::ejecutar_sql(base64_encode($sql));
		return ($stm)?1:0;
	}
	
	public function modificar_clientes($idu,$per,$nom,$ape,$dni,$nro,$dir,$tel,$cel,$eml,$mnt,$zon,$gal){
		$sql="update si_clientes set
nom_cli='$nom', ape_cli='$ape', tip_cli='$per', doc_cli='$dni', nro_cli='$nro', dir_cli='$dir', tel_cli='$tel', cel_cli='$cel', eml_cli='$eml', mnt_cli=$mnt, id_zon=$zon, id_gal=$gal where id_cli=".$idu;
		$stm=parent::ejecutar_sql(base64_encode($sql));
		return ($stm)?1:0;
	}	
	
	public function eliminar_cliente($idu,$cod){
		if(isset($idu) && !isset($cod)){
			$cod=$this->traer_cliente($idu);
		}
		$sql="delete from si_clientes where cod_cli='".$cod."'";
		$stm=parent::ejecutar_sql(base64_encode($sql));
		return ($stm==true)?1:0;
	}
	
	public function eliminar_clientes($arr){
		$cod=array();
		if(is_array($arr)){
			foreach($arr as $idu){
				$sql="delete from si_clientes where id_cli=".$idu."";
				$stm=parent::ejecutar_sql(base64_encode($sql));
				$afe=parent::afectados_sql();
			}			
		}		
		return ($stm==true)?1:0;
	}	

}
?>