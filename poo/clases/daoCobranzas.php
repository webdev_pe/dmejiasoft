<?php
require("getConection.php");
class daoCobranzas extends getConection{
	
	public function traer_prestamo($idu){
		$sql="select cod_pre from si_prestamos where id_pre=".$idu."";
		parent::ejecutar_sql(base64_encode($sql));
		$row=parent::cantidad_sql();
		$cel=parent::resultado_sql();
		return ($row>0)?$cel['cod_pre']:"";
	}

	public function crear_codigo(){
		$sql="select cod_pre from si_prestamos order by id_pre desc limit 1";
		parent::ejecutar_sql(base64_encode($sql));
		$row=parent::cantidad_sql();
		if($row>0){
			$cel=parent::resultado_sql();
			$ult=$cel['cod_pre'];
			$num=substr($ult,3,4);
			$inc=(int)$num+1;//incrementar
			$cod="PRE".str_pad($inc,4,"0",STR_PAD_LEFT);
		}else{
			$cod="PRE0001";
		}
		return $cod;
	}
	
	public function traer_dias(){
		$sql="insert into si_cobranzas (id_pre,mnt_cob,dias,adicional,fecha,cierre) 
		values ($idp,$mnt,$usu,'$tip',$dia,$int,$cuo,$mnt,'a',curdate(),curtime())";
		$stm=parent::ejecutar_sql(base64_encode($sql));
		return ($stm)?1:0;	
	}	
	
	public function traer_adicional($idp){
		$sql="select adicional from si_cobranzas where id_pre=".$idp." order by id_cob desc limit 1";
		$stm=parent::ejecutar_sql(base64_encode($sql));
		$cel=parent::resultado_sql();
		return $cel['adicional'];		
	}

	public function traer_real($idp,$mto,$mnt,$add,$inr,$fra){
		$sql_c="select real_mnt, real_int from si_cobranzas where id_pre=".$idp." order by id_cob desc";
		$stm_c=parent::ejecutar_sql(base64_encode($sql_c));
		$row_c=parent::cantidad_sql();
		$cel_c=parent::resultado_sql();
		if($row_c>0){
			$rm=$cel_c['real_mnt']-$mnt+$add;
			$ri=($fra==0)?$cel_c['real_int']:(($rm-$add)*$inr)/$mto;
		}else{
			$rm=$mto-$mnt+$add;
			$ri=($fra==0)?$inr:(($rm-$add)*$inr)/$mto;
		}
		$array[0]=$rm;
		$array[1]=$ri;
		return $array;
	}
	
	public function reales($idp){
		$sql="select id_pre, mnt_pre, fra_pre, 
		(select id_cob from si_cobranzas where id_pre=$idp order by id_cob desc limit 1) as 'id_cob',
		(select mnt_cob from si_cobranzas where id_pre=$idp order by id_cob desc limit 1) as 'mnt_cob',
		(select agregar from si_cobranzas where id_pre=$idp order by id_cob desc limit 1) as 'agregar',
		(select cierre from si_cobranzas where id_pre=$idp order by id_cob desc limit 1) as 'cierre',
		(select monto_real from si_cobranzas where id_pre=$idp order by id_cob desc limit 1) as 'monto_real',
		(select fracc_real from si_cobranzas where id_pre=$idp order by id_cob desc limit 1) as 'fracc_real'				
from si_prestamos where id_pre=$idp";
		parent::ejecutar_sql(base64_encode($sql));
		$row=parent::cantidad_sql();	
		$cel=parent::resultado_sql();
		
		$mnt_cob	= $cel['mnt_cob'];
		$agregar	= $cel['agregar'];	
		$cierre		= $cel['cierre'];	
		$monto_real	= $cel['monto_real'];
		
		$reales=array();
		if($cel['id_con']!=""){
			switch($cierre){
				case "pg": 
					$real_mnt=$monto_real-$mnt_cob;
					$real_fra=($real_mnt*$cel['fra_pre'])/$cel['mnt_pre'];
					$reales[0]=$real_mnt;
					$reales[1]=$real_fra;
				break;			
				case "pa":
					$real_mnt=$monto_real+$agregar;
					$reales[0]=$real_mnt;
					$reales[1]=0;
				break;
				case "np":
					$reales[0]=$monto_real;
					$reales[1]=0;					
				break;
			}
		}else{
			$reales[0]=$cel['mnt_pre'];
			$reales[1]=$cel['fra_pre'];
		}
		$reales[2]=$sql;
		return $reales;
	}

	public function guardar_cobranzas_rep($sql){		
		$stm=parent::ejecutar_sql(base64_encode($sql));
		return ($stm)?1:0;	
	}		
	
	public function guardar_cobranzas($idp, $dro, $mnt, $fra, $dto, $add, $cie, $tip, $mto, $inr, $cob, $fch){
		if($tip=="p"){
			$adc=$this->traer_adicional($idp);		
			if($adc>0 && $adc!=""){
				$cta=($adc>0)?$dro+$adc:0;
				if($mnt>=$cta){
					$dias=round(($mnt+$dro-$adc)/$dro);
					$pre=($mnt+($dro-$adc))%$dro;
					if($pre>0){ $adic=$dro-$pre;
					}else{ $adic=$pre; }			
				}else if($mnt<$cta){				
					$dias=round($mnt/$dro);
					$adic=($cta-$mnt)+($dro-($mnt%$dro));
				}
			}else{
				if($mnt>0){
					$dias=round($mnt/$dro);
					$pre=$mnt%$dro;
						if($pre>0){ $adic=$dro-$pre;
						}else{ $adic=$pre; }
				}else{ $dias=0; $adic=$dro; }
			}			
			$tabla1="si_cobranzas";
			$sql="insert into $tabla1 (id_pre,mnt_cob,dias,adicional,fecha,cierre,dispositivo,estado,hora,fecha_reg) 
			values ($idp,$mnt,$dias,$adic,'$fch','$cie','$cob','1',curtime(),curdate())";
			$tabla2="si_cobranzas_rep";
			$sqlX="insert into $tabla2 (id_pre,mnt_cob,dias,adicional,fecha,cierre,dispositivo,estado,hora,fecha_reg) 
			values ($idp,$mnt,$dias,$adic,'$fch','$cie','$cob','1',curtime(),curdate())";			
		}else if($tip=="a"){
			#$a=$this->traer_real($idp,$mto,$mnt,$add,$inr,$fra);
			#$rm=$a[0]; $ri=$a[1];
			$rm=0; $ri=0;
			
			$reales=$this->reales($idp);
			$real_mnt_a=$reales[0];
			$real_fra_a=$reales[1];			
			$real_sql_a=$reales[2];
			$tabla1="si_cobranzas";
			$sql="insert into $tabla1 (id_pre,mnt_cob,interes,agregar,real_mnt,real_int,fecha,cierre,dscto,dispositivo,estado,hora,fecha_reg,monto_real, fracc_real) values ($idp,$mnt,$fra,$add,$rm,$ri,'$fch','$cie',$dto,'$cob','1',curtime(),curdate(),$real_mnt_a,$real_fra_a)";
			
			#$tabla2="si_cobranzas_rep";
			#$sqlX="insert into $tabla2 (id_pre,mnt_cob,interes,agregar,real_mnt,real_int,fecha,cierre,dscto,dispositivo,estado,hora,fecha_reg)
			#values ($idp,$mnt,$fra,$add,$rm,$ri,'$fch','$cie',$dto,'$cob','1',curtime(),curdate())";			
		}

				$sql_ex="select id_pre, fecha, cierre from si_cobranzas where id_pre=$idp and fecha='$fch'";
				parent::ejecutar_sql(base64_encode($sql_ex));				
				$row_ex=parent::cantidad_sql();
				
				if($row_ex>0){
					  if($cie=="pa"){
						  $sqlc="delete from si_cobranzas where id_pre=$idp and fecha='$fch' and cierre='pa'";
						  parent::ejecutar_sql(base64_encode($sqlc)); #break;
					  }else{
						  $sqlc="delete from si_cobranzas where id_pre=$idp and fecha='$fch' and cierre in ('np','pg')";
						  parent::ejecutar_sql(base64_encode($sqlc)); #break;
					  }
					$stm=parent::ejecutar_sql(base64_encode($sql));					
					$this->guardar_cobranzas_rep($sqlX);
					return ($stm)?1:0;																	
				}else{
					$stm=parent::ejecutar_sql(base64_encode($sql));
					
					#$this->guardar_cobranzas_rep($sqlX);
					return ($stm)?1:$real_sql_a;										
				}				
	}

/**************************************************************************************************************************************/
public function traer_mi_cobros($i,$ic,$ip,$tipo){

$get_id=$i;
$get_ic=$ic;
$get_ip=$ip;

session_start();
unset($_SESSION['s']); unset($_SESSION['sh']);

date_default_timezone_set("America/Lima");
#require("clases/getConection.php");
$cn=new getConection();

$dias=-1;
$fecha_ayer=date("Y-m-d", strtotime("$dias days"));
$fecha_hoy=date("Y-m-d");

$sql="
select id_cob, id_pre, mnt_cob, dias, adicional, interes, agregar, fecha, cierre, estado 
from si_cobranzas where id_pre=".$get_id." order by fecha asc";
$cn->ejecutar_sql(base64_encode($sql));
$row=$cn->cantidad_sql();

$cn1=new getConection();
$sql1="select id_pre, tip_pre, mnt_pre, int_pre, dia_pre, fra_pre,
p.id_cli, concat(c.nom_cli,' ',c.ape_cli) as 'nombres', c.cod_cli, p.cod_pre, date_format(fecha,'%d-%m-%Y') as 'fecha', interes_por, est_pre
 from si_prestamos p, si_clientes c
 where p.id_cli=c.id_cli and id_pre=".$get_id;
$cn1->ejecutar_sql(base64_encode($sql1));
$cel1=$cn1->resultado_sql();

if($cel1['tip_pre']=="p"){
	$monto=$cel1['mnt_pre'];
	$diario=$monto/$cel1['dia_pre'];
	$interes=$monto*$cel1['int_pre']/$cel1['dia_pre'];
	$int_tot=$monto*$cel1['int_pre'];
	$diario_a_pagar=$cn->redondeo($diario)+$cn->redondeo($interes);
	$dev_tot=$diario_a_pagar*$cel1['dia_pre'];
}else if($cel1['tip_pre']=="a"){
	$monto=$cn->redondeo($cel1['mnt_pre']);
	$inter=$cn->redondeo($cel1['fra_pre']);
	$dev_tot=$monto;	
}

if($cel1['tip_pre']=="p"){ 
	$i=1;
	while($cel=$cn->resultado_sql()){ 
	$fecha=date("d-m-Y",strtotime($cel['fecha']));
	$total=$cn->redondeo($diario)+$cn->redondeo($interes);	

  	$ult_adc=$cel['adicional'];
	$acum1+=$cel['mnt_cob'];
	$i++;
	}
}else if($cel1['tip_pre']=="a"){ 
	$i=1;
	$ult_int=$cel1['fra_pre'];
	$j=0;	
	$montos=$cel1['mnt_pre'];
	$intere=$cn->redondeo($cel1['fra_pre']);
	$totale=$montos+$intere;
	$array_fechas=array();
	$array_montos=array();
	while($cel=$cn->resultado_sql()){ 
	$fecha=date("d-m-Y",strtotime($cel['fecha']));
	$pago_real=$montos;

		if($cel['estado']=="1"){
			$ultimo+=$cel['mnt_cob'];
			
			if($cel['cierre']=='pa'){ if(!in_array($fecha,$array_fechas)){$array_fechas[$fecha]=$cel['agregar']; }
			}else{ if(!in_array($fecha,$array_montos)){$array_montos[$fecha]=$cel['mnt_cob'];} }
			
			$ayer=date("d-m-Y", strtotime("$fecha -1 days"));
	
			if($ayer!=$_SESSION['s']){
				$ultimo_add1+=$array_fechas[$ayer];
				$ultimo_mnt1+=$array_montos[$ayer];
				$_SESSION['s']=$ayer;
			}else{
				unset($_SESSION['s']);
			}

			$montos=$cel1['mnt_pre']-$ultimo_mnt1+$ultimo_add1; #echo "<br />".$cel1['mnt_pre']."-$ultimo+$ultimo_add1 ======= $montos 1";

		$intere=$cn->redondeo(($montos*$cel1['fra_pre'])/$cel1['mnt_pre']);
		$totale=$montos+$intere;
		$acum1=$ultimo;
		$ult_mnt=$cel['mnt_cob'];
		}else{	
			$ayer=date("d-m-Y", strtotime("$fecha -1 days"));
	
			if($ayer!=$_SESSION['s']){
				$ultimo_add1+=$array_fechas[$ayer];
				$ultimo_mnt1+=$array_montos[$ayer];
				$_SESSION['s']=$ayer;
			}else{
				unset($_SESSION['s']);
			}

			$montos=$cel1['mnt_pre']-$ultimo_mnt1+$ultimo_add1; #echo "<br />".$cel1['mnt_pre']."-$ultimo+$ultimo_add1 ======= $montos 1";		
		}

		if($cel['estado']=="1"){
		$acum_pen+=($cel['agregar']>0)?0:$intere;
		$acum_cob+=$cel['mnt_cob'];
		$acum_int+=$cel['interes'];
		$ppagar=($acum_pen-$acum_int);
			
		}
	$i++;	
	} 

	$pendiente=$ult_add+$pago_real-$ult_mnt;
	
   /*
    $cn->redondeo($ppagar)
    $cn->redondeo($acum1)
    $cn->redondeo($acum_int) 
	*/

		$amort=end($array_montos);
		$amort=prev($array_montos);
		
		$pago_hoy=$montos-$array_montos[$fecha]+$array_fechas[$fecha];
		$inte_hoy=($pago_hoy*$cel1['fra_pre'])/$cel1['mnt_pre'];
		$real_hoy=$inte_hoy+$ppagar;
	/*	
	$cn->redondeo($pago_hoy);		
   $cn->redondeo($inte_hoy);
    $cn->redondeo($real_hoy);  
    */



} 
	if($tipo=="a"){
		if($get_ic=="1"){
			return ($cn->redondeo($acum_int)!="")?$cn->redondeo($acum_int):"-----------";
		}else if($get_ip=="1"){
			return ($cn->redondeo($real_hoy)!="")?$cn->redondeo($real_hoy):"-----------";
		}	
	}else if($tipo=="p"){
		if($get_ic=="1"){
			$interes_entero=$acum1*$cel1['int_pre']/(1+$cel1['int_pre']);
			$interes_ciento=100+$interes_entero;
			return $interes_entero;
		}else if($get_ip=="1"){
			$interes_entero=$acum1*$cel1['int_pre']/(1+$cel1['int_pre']);
			$interes_ciento=100+$interes_entero;
			return $interes_entero;
		}
	}
}



/**************************************************************************************************************************************/
	
}
?>