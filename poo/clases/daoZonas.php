<?php
require("getConection.php");
class daoZonas extends getConection{
	
	public function traer_zona($idu){
		$sql="select cod_zon from si_zonas where id_zon=".$idu."";
		parent::ejecutar_sql(base64_encode($sql));
		$row=parent::cantidad_sql();
		$cel=parent::resultado_sql();
		return ($row>0)?$cel['cod_zon']:"";
	}
	
	public function crear_codigo(){
		$sql="select cod_zon from si_zonas order by id_zon desc limit 1";
		parent::ejecutar_sql(base64_encode($sql));
		$row=parent::cantidad_sql();
		if($row>0){
			$cel=parent::resultado_sql();
			$ult=$cel['cod_zon'];
			$num=substr($ult,3,4);
			$inc=(int)$num+1;//incrementar
			$cod="ZON".str_pad($inc,4,"0",STR_PAD_LEFT);
		}else{
			$cod="ZON0001";
		}
		return $cod;
	}
	
	public function guardar_zonas($nom, $des){
		$cod=$this->crear_codigo();
		$sql="insert into si_zonas (cod_zon, nom_zon, des_zon) values ('$cod', '$nom', '$des')";
		$stm=parent::ejecutar_sql(base64_encode($sql));
		$idg=parent::id_generado_sql();
		return ($idg)?1:0;
	}
	
	public function modificar_zonas($idu, $nom, $des){
		$cod=$this->traer_zona($idu);
		$sql="update si_zonas set nom_zon='$nom', des_zon='$des' where id_zon=".$idu;
		$stm=parent::ejecutar_sql(base64_encode($sql));
		return ($stm==true)?1:0;
	}	
	
	public function eliminar_zona($idu,$cod){
		$c=($idu!="" && $cod=="")?$this->traer_zona($idu):$cod;
		$sql="delete from si_zonas where cod_zon='".$c."'";
		$stm=parent::ejecutar_sql(base64_encode($sql));
		return ($stm==true)?1:0;
	}
	
	public function eliminar_zonas($arr){
		$cod=array();
		if(is_array($arr)){
			foreach($arr as $idu){
				array_push($cod,$this->traer_zona($idu));
				$sql="delete from si_zonas where id_zon=".$idu."";
				$stm=parent::ejecutar_sql(base64_encode($sql));		
				$afe=parent::afectados_sql();				
			}			
		}
		return ($stm==true)?1:0;
	}	
}
?>