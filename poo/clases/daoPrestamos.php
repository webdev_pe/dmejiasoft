<?php
require("getConection.php");
class daoPrestamos extends getConection{
	
	public function traer_prestamo($idu){
		$sql="select cod_pre from si_prestamos where id_pre=".$idu."";
		parent::ejecutar_sql(base64_encode($sql));
		$row=parent::cantidad_sql();
		$cel=parent::resultado_sql();
		return ($row>0)?$cel['cod_pre']:"";
	}

	public function crear_codigo(){
		$sql="select cod_pre from si_prestamos order by id_pre desc limit 1";
		parent::ejecutar_sql(base64_encode($sql));
		$row=parent::cantidad_sql();
		if($row>0){
			$cel=parent::resultado_sql();
			$ult=$cel['cod_pre'];
			$num=substr($ult,3,4);
			$inc=(int)$num+1;//incrementar
			$cod="CRE".str_pad($inc,4,"0",STR_PAD_LEFT);
		}else{
			$cod="CRE0001";
		}
		return $cod;
	}
	
	public function guardar_prestamos($cli,$usu,$tip,$est,$dia,$int,$cuo,$mnt,$inp,$pre,$fch){
		$cod=$this->crear_codigo();
		$sql="insert into si_prestamos (cod_pre,id_cli,id_usu,tip_pre,dia_pre,int_pre,fra_pre,mnt_pre,est_pre,fecha,hora,interes_por,dispositivo,moroso,fecha_reg) 
		values ('$cod',$cli,$usu,'$tip',$dia,$int,$cuo,$mnt,'a','$fch',curtime(),$inp,'$pre','$est',curdate())";
		$stm=parent::ejecutar_sql(base64_encode($sql));
		
		$idp=parent::id_generado_sql();
		if($tip=="a"){
			/*require("daoCobranzas.php");
			$cb=new daoCobranzas();*/			
			$fch=strtotime(date("Y-m-d",strtotime("$fch +1 days")));
			$hoy=strtotime(date("Y-m-d"));
				$dro=0;
				$mnt=0;
				$fra=0;
				$dto=0;
				$add=0;
				$cie="np";
				$tip='a';
				$mto=0;
				$inr=0;
				$cob="antiguo";			
			for($i=$fch;$i<$hoy;$i+=86400){
				$sqlc="insert into si_cobranzas (id_pre,mnt_cob,interes,agregar,real_mnt,real_int,fecha,cierre,dscto,dispositivo,estado,hora,fecha_reg)
				values ($idp,$mnt,$fra,$add,$mto,$inr,'".date("Y-m-d",$i)."','$cie',$dto,'$cob','1',curtime(),curdate())";
				parent::ejecutar_sql(base64_encode($sqlc));
				#$cb->guardar_cobranzas($idp, $dro, $mnt, $fra, $dto, $add, $cie, $tip, $mto, $inr, $cob, "'".date("Y-m-d",$i)."'");
				#echo date("Y-m-d",$i)."<br/>";
			}
		}
		
		
		return ($stm)?1:0;
	}
	
	public function modificar_prestamos($idu,$idp,$cli,$usu,$tip,$est,$dia,$int,$cuo,$mnt,$inp){
		$sql="update si_prestamos set id_usu=$usu,tip_pre='$tip',moroso='$est',dia_pre=$dia,int_pre=$int,fra_pre=$cuo,mnt_pre=$mnt,interes_por=$inp where id_pre=".$idp;
		$stm=parent::ejecutar_sql(base64_encode($sql));
		return ($stm)?1:0;
	}	

	public function eliminar_prestamo($idu,$cod){
		if(isset($idu) && !isset($cod)){
			$cod=$this->traer_prestamo($idu);
		}
		$sql="delete from si_prestamos where cod_pre='".$cod."'";
		$stm=parent::ejecutar_sql(base64_encode($sql));
		return ($stm==true)?1:0;
	}
	
	public function eliminar_prestamos($arr){
		$cod=array();
		if(is_array($arr)){
			foreach($arr as $idu){
				$sql="delete from si_prestamos where id_pre=".$idu."";
				$stm=parent::ejecutar_sql(base64_encode($sql));
				$afe=parent::afectados_sql();
			}			
		}		
		return ($stm==true)?1:0;
	}	
}
?>