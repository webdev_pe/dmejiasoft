<?php
require("getConection.php");
class daoGalerias extends getConection{
	
	public function traer_galeria($idu){
		$sql="select cod_gal from si_galerias where id_gal=".$idu."";
		parent::ejecutar_sql(base64_encode($sql));
		$row=parent::cantidad_sql();
		$cel=parent::resultado_sql();
		return ($row>0)?$cel['cod_gal']:"";
	}
	
	public function crear_codigo(){
		$sql="select cod_gal from si_galerias order by id_gal desc limit 1";
		parent::ejecutar_sql(base64_encode($sql));
		$row=parent::cantidad_sql();
		if($row>0){
			$cel=parent::resultado_sql();
			$ult=$cel['cod_gal'];
			$num=substr($ult,3,4);
			$inc=(int)$num+1;//incrementar
			$cod="GAL".str_pad($inc,4,"0",STR_PAD_LEFT);
		}else{
			$cod="GAL0001";
		}
		return $cod;
	}
	
	public function guardar_galerias($nom, $dir, $zon){
		$cod=$this->crear_codigo();
		$sql="insert into si_galerias (id_zon, cod_gal, nom_gal, dir_gal) values ($zon, '$cod', '$nom', '$dir')";
		$stm=parent::ejecutar_sql(base64_encode($sql));
		$idg=parent::id_generado_sql();
		return ($idg)?1:0;
	}
	
	public function modificar_galerias($idu, $nom, $dir, $zon){
		$cod=$this->traer_galeria($idu);
		$sql="update si_galerias set id_zon=$zon, nom_gal='$nom', dir_gal='$dir' where id_gal=".$idu;
		$stm=parent::ejecutar_sql(base64_encode($sql));
		return ($stm==true)?1:0;
	}	
	
	public function eliminar_galeria($idu,$cod){
		$c=($idu!="" && $cod=="")?$this->traer_galeria($idu):$cod;
		$sql="delete from si_galerias where cod_gal='".$c."'";
		$stm=parent::ejecutar_sql(base64_encode($sql));
		return ($stm==true)?1:0;
	}
	
	public function eliminar_galerias($arr){
		$cod=array();
		if(is_array($arr)){
			foreach($arr as $idu){
				array_push($cod,$this->traer_galeria($idu));
				$sql="delete from si_galerias where id_gal=".$idu."";
				$stm=parent::ejecutar_sql(base64_encode($sql));		
				$afe=parent::afectados_sql();				
			}			
		}
		return ($stm==true)?1:0;
	}	
}
?>