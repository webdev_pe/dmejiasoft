<?php
require("getConection.php");
class daoUsuarios extends getConection{
	
	public function traer_usuario($idu){
		$sql="select cod_usu from si_usuarios where id_usu=".$idu."";
		parent::ejecutar_sql(base64_encode($sql));
		$row=parent::cantidad_sql();
		$cel=parent::resultado_sql();
		return ($row>0)?$cel['cod_usu']:"";
	}
	
	public function validar_usuario($usu){
		$sql="select user from si_usuarios where user='".$usu."'";
		parent::ejecutar_sql(base64_encode($sql));
		$row=parent::cantidad_sql();	
		return ($row>0)?1:0;
	}

	public function crear_codigo(){
		$sql="select cod_usu from si_usuarios order by id_usu desc limit 1";
		parent::ejecutar_sql(base64_encode($sql));
		$row=parent::cantidad_sql();
		if($row>0){
			$cel=parent::resultado_sql();
			$ult=$cel['cod_usu'];
			$num=substr($ult,1,4);
			$inc=(int)$num+1;//incrementar
			$cod="USU".str_pad($inc,4,"0",STR_PAD_LEFT);
		}else{
			$cod="USU0001";
		}
		return $cod;
	}
	
	public function guardar_usuarios($nom, $ape, $tdc, $ndc, $usu, $pas, $dir, $eml, $tel, $cla, $mov, $nex, $rpc, $rpm, $niv, $zon, $p1, $p2, $p3, $p4, $p5, $p6, $p7, $d1, $d2, $d3, $d4, $d5, $d6, $d7, $ds, $hs){
		$cod=$this->crear_codigo();
		$sql="insert into si_usuarios 
		(cod_usu, nom_usu, ape_usu, tipo_dni, num_dni, user, pswd, dir_usu, eml_usu, tlf_usu, cla_usu, mov_usu, nex_usu, rpc_usu, rpm_usu, nivel, id_zon,
		p1,p2,p3,p4,p5,p6,p7,d1,d2,d3,d4,d5,d6,d7,ds,hs)
		values
		('$cod', '$nom', '$ape', $tdc, '$ndc', '$usu', '$pas', '$dir', '$eml', '$tel', '$cla', '$mov', '$nex', '$rpc', '$rpm', '$niv', $zon,
		'$p1', '$p2', '$p3', '$p4', '$p5', '$p6', '$p7', '$d1','$d2','$d3','$d4','$d5','$d6','$d7','$ds','$hs')";
		$stm=parent::ejecutar_sql(base64_encode($sql));
		return ($stm)?1:$sql;
	}
	
	public function modificar_usuarios($idu, $nom, $ape, $tdc, $ndc, $usu, $pas, $dir, $eml, $tel, $cla, $mov, $nex, $rpc, $rpm, $niv, $zon, $p1, $p2, $p3, $p4, $p5, $p6, $p7, $d1, $d2, $d3, $d4, $d5, $d6, $d7, $ds, $hs){
		$sql="update si_usuarios set
		nom_usu='$nom', ape_usu='$ape', tipo_dni=$tdc, num_dni='$ndc', 
		pswd='$pas', dir_usu='$dir', eml_usu='$eml', 
		tlf_usu='$tel', cla_usu='$cla', mov_usu='$mov', nex_usu='$nex', 
		rpc_usu='$rpc', rpm_usu='$rpm', 
		nivel='$niv', id_zon=$zon,
		p1='$p1', p2='$p2', p3='$p3', p4='$p4', p5='$p5', p6='$p6', p7='$p7',
		d1='$d1', d2='$d2', d3='$d3', d4='$d4', d5='$d5', d6='$d6', d7='$d7', ds='$ds', hs='$hs'
		where id_usu=".$idu;
		$stm=parent::ejecutar_sql(base64_encode($sql));
		return ($stm)?1:0;
	}	
	
	public function eliminar_usuario($idu,$cod){
		if(isset($idu) && !isset($cod)){
			$cod=$this->traer_usuario($idu);
		}
		$sql="delete from si_usuarios where cod_usu='".$cod."'";
		$stm=parent::ejecutar_sql(base64_encode($sql));
		return ($stm==true)?1:0;
	}
	
	public function eliminar_usuarios($arr){
		$cod=array();
		if(is_array($arr)){
			foreach($arr as $idu){
				$sql="delete from si_usuarios where id_usu=".$idu."";
				$stm=parent::ejecutar_sql(base64_encode($sql));
				$afe=parent::afectados_sql();
			}			
		}		
		return ($stm==true)?1:0;
	}	

}
?>