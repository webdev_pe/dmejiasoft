<?php
require_once("getConfiguration.php");
class getConection{
	
	public $host=HOST;
	public $user=USER;
	public $pswd=PSWD;
	public $base=BASE;
	public $cone;
	
	public $stmt;
	
	public function __construct(){
		/*$this->host="localhost";
		$this->user="root";
		$this->pswd="root";
		$this->base="sanson_int";*/
		$this->cone=@mysqli_connect($this->host,$this->user,$this->pswd,$this->base)or die("No se conecto a la BD");
		mysqli_query($this->cone, "SET NAMES 'utf8'");
	}
	
	public function conectar_sql(){
		return $this->cone;
	}
	
	public function ejecutar_sql($sql){
		$this->stmt=mysqli_query($this->cone,base64_decode($sql));		
		return $this->stmt;
	}
	
	public function id_generado_sql(){
		return mysqli_insert_id($this->cone);		
	}	
	
	public function cantidad_sql(){
		return mysqli_num_rows($this->stmt);
	}
	
	public function afectados_sql(){
		return mysqli_affected_rows($this->cone);
	}	
	
	public function resultado_sql(){
		return mysqli_fetch_assoc($this->stmt);
	}
	
	public function limpiar_sql(){
		return mysqli_free_result($this->stmt);	
	}

	public function cerrar_sql(){
		return mysqli_close($this->cone);
	}

	public function redondeo($n){
		$num1=round($n);
		$num2=round($n,2);
		$num3=($num2-$num1)*100;
		$num4=round($num3/10);
		$num5=$num1+($num4/10);
		return number_format($num2,2,'.',',');
	}
}

		/*$n=8.75;
		$num1=round($n); echo $num1."<br />";
		$num2=round($n,2); echo $num2."<br />";
		$num3=($num2-$num1)*100; echo $num3."<br />";
		$num4=round($num3/10); echo $num4."<br />";
		$num5=$num1+($num4/10); echo $num5."<br />";*/
		#return $num5;
		#return number_format($num5,2,'.',',');

/*
DELIMITER $$
CREATE PROCEDURE sp_prp(
       IN ids int(10)
       #,OUT total INT
		 )
    BEGIN
       SELECT id_sitio, nom_sitio
       #INTO total
       FROM sanson_sitio
       WHERE id_sitio = ids;
    END$$
 DELIMITER ;
 
#call sp_sss(5)
*/
?>