<?php
require ("getConection.php");
class getPeruUbicacion extends getConection{
	public function traer_departamentos(){
		parent::ejecutar_sql(base64_encode("select id_dep, nom_dep from si_departamentos"));
		$row=parent::cantidad_sql();
		if($row>0){
			$options="<option value='nn' selected='selected'>[-Seleccione-]</option>";
			while($cel=parent::resultado_sql()){
				$options.="<option value='".$cel['id_dep']."'>".$cel['nom_dep']."</option>";
			}
		}
		parent::limpiar_sql();
		parent::cerrar_sql();
		return $options;
	}
	
	public function traer_provincias($dep){
		parent::ejecutar_sql(base64_encode("select id_prv, nom_prv from si_provincias where id_dep=".$dep));
		$row=parent::cantidad_sql();;
		if($row>0){
			$options="<option value='nn' selected='selected'>[-Seleccione-]</option>";
			while($cel=parent::resultado_sql()){
				$options.="<option value='".$cel['id_prv']."'>".$cel['nom_prv']."</option>";
			}
		}
		parent::limpiar_sql();
		parent::cerrar_sql();
		return $options;
	}	
	
	public function traer_distritos($dep,$prv){
		parent::ejecutar_sql(base64_encode("select id_dst, nom_dst from si_distritos where id_dep=".$dep." and id_prv=".$prv));
		$row=parent::cantidad_sql();;
		if($row>0){
			$options="<option value='nn' selected='selected'>[-Seleccione-]</option>";
			while($cel=parent::resultado_sql()){
				$options.="<option value='".$cel['id_dst']."'>".$cel['nom_dst']."</option>";
			}
		}
		parent::limpiar_sql();
		parent::cerrar_sql();
		return $options;
	}	
	
	
	public function traer_departamento_cod($dep){
		parent::ejecutar_sql(base64_encode("select id_dep, nom_dep from si_departamentos where id_dep=".$dep));
		$cel=parent::resultado_sql();
		$row=parent::cantidad_sql();
		if($row>0){
			$options="<option value='".$cel['id_dep']."' selected='selected'>".$cel['nom_dep']."</option>";
		}		
		parent::ejecutar_sql(base64_encode("select id_dep, nom_dep from si_departamentos where id_dep<>".$dep));
		$row=parent::cantidad_sql();
		if($row>0){
			while($cel=parent::resultado_sql()){
				$options.="<option value='".$cel['id_dep']."'>".$cel['nom_dep']."</option>";
			}
		}
		parent::limpiar_sql();
		parent::cerrar_sql();		
		return $options;
	}		
	
	public function traer_provincia_cod($dep,$prv){
		parent::ejecutar_sql(base64_encode("select id_prv, nom_prv from si_provincias where id_dep=".$dep." and id_prv=".$prv));
		$cel=parent::resultado_sql();
		$row=parent::cantidad_sql();
		if($row>0){
			$options="<option value='".$cel['id_prv']."' selected='selected'>".$cel['nom_prv']."</option>";
		}		
		parent::ejecutar_sql(base64_encode("select id_prv, nom_prv from si_provincias where id_dep=".$dep." and id_prv<>".$prv));
		$row=parent::cantidad_sql();
		if($row>0){
			while($cel=parent::resultado_sql()){
				$options.="<option value='".$cel['id_prv']."'>".$cel['nom_prv']."</option>";
			}
		}
		parent::limpiar_sql();
		parent::cerrar_sql();		
		return $options;
	}	
	
	public function traer_distrito_cod($dep,$prv,$dst){
		parent::ejecutar_sql(base64_encode("select id_dst, nom_dst from si_distritos where id_dep=".$dep." and id_prv=".$prv." and id_dst=".$dst));
		$cel=parent::resultado_sql();
		$row=parent::cantidad_sql();
		if($row>0){
			$options="<option value='".$cel['id_dst']."' selected='selected'>".$cel['nom_dst']."</option>";
		}		
		parent::ejecutar_sql(base64_encode("select id_dst, nom_dst from si_distritos where id_dep=".$dep." and id_prv=".$prv." and id_dst<>".$dst));
		$row=parent::cantidad_sql();
		if($row>0){
			while($cel=parent::resultado_sql()){
				$options.="<option value='".$cel['id_dst']."'>".$cel['nom_dst']."</option>";
			}
		}
		parent::limpiar_sql();
		parent::cerrar_sql();		
		return $options;
	}		
}
?>