<?php
session_start();
require_once("poo/clases/getConfiguration.php");
if(isset($_SESSION['sicpri_idu'])){
	ob_start("ob_gzhandler");
?>
<!DOCTYPE html> 
<html> 
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<META Http-Equiv="Cache-Control" Content="no-cache">
	<META Http-Equiv="Pragma" Content="no-cache">
	<META Http-Equiv="Expires" Content="0">

<title>SIPC - Sistema Integral de Cr&eacute;ditos y Cobranzas de Mercader&iacute;a Valorizada</title>
    <link href="images/favicon.png" rel="shortcut icon" />
	<LINK rel="stylesheet" type="text/css" href="js/jquery-ui-1.8.18.custom/development-bundle/themes/custom-theme/jquery.ui.all.css">
	<LINK rel="stylesheet" type="text/css" href="js/jquery_layout/layout.css">

    <script type="text/javascript" src="js/jquery_layout/jquery-latest.js"></script>
    <script type="text/javascript" src="js/jquery_layout/jquery-ui-latest.js"></script>
    <script type="text/javascript" src="js/jquery_layout/jquery.layout-latest.js"></script>
    <script type="text/javascript" src="js/jquery_layout/jquery.layout.resizeTabLayout.min-1.1.js"></script>
    <script type="text/javascript" src="js/jquery_layout/jquery.layout.resizePaneAccordions.min-1.0.js"></script>
    <script type="text/javascript" src="js/jquery_layout/debug.js"></script>
    <script type="text/javascript" src="js/jquery_layout/layout.js"></script> 
 
	<link type="text/css" href="js/jquery-ui-1.8.18.custom/css/custom-theme/jquery-ui-1.8.18.custom.css" rel="stylesheet" />	
	<script type="text/javascript" src="js/jquery-ui-1.8.18.custom/js/jquery-ui-1.8.18.custom.min.js"></script>
	<script type="text/javascript" src="js/jquery-ui-1.8.18.custom/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>    
	<script type="text/javascript" src="js/jquery-ui-1.8.18.custom/development-bundle/ui/jquery.ui.core.js"></script>
	<script type="text/javascript" src="js/jquery-ui-1.8.18.custom/development-bundle/ui/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="js/jquery-ui-1.8.18.custom/development-bundle/ui/jquery.ui.button.js"></script>    
	<script type="text/javascript" src="js/jquery-ui-1.8.18.custom/development-bundle/ui/jquery.ui.position.js"></script>
	<script type="text/javascript" src="js/jquery-ui-1.8.18.custom/development-bundle/ui/jquery.ui.autocomplete.js"></script>
	<link type="text/css" href="js/jquery-ui-1.8.18.custom/development-bundle/themes/base/jquery.ui.selectmenu.css" rel="stylesheet" />
	<script type="text/javascript" src="js/jquery-ui-1.8.18.custom/development-bundle/ui/jquery.ui.selectmenu.js"></script> 
    
	<!--script src="js/ckeditor/ckeditor.js" type="text/javascript"></script-->
	<!--script src="js/ckeditor/adapters/jquery.js" type="text/javascript"></script-->
	<!--script src="js/ckeditor/_samples/sample.js" type="text/javascript"></script-->
	<!--link href="js/ckeditor/_samples/sample.css" rel="stylesheet" type="text/css" /-->    

    <script type="text/javascript" charset="utf-8" src="js/jquery_datatable/js/jquery-ui.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/jquery_datatable/js/jquery.dataTables.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/jquery_datatable/js/jquery.dataTables.columnFilter.js"></script>    
    <!--link rel="stylesheet" type="text/css" href="jquery-ui-1.8.18.custom/css/custom-theme/jquery-ui-1.8.18.custom.css" media="screen" /-->
    <link rel="stylesheet" type="text/css" href="js/jquery_datatable/css/demo_table_jui.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="js/jquery_datatable/css/demo_table.css" media="screen" /> 
    <!--link rel="stylesheet" type="text/css" href="js/jquery_datatable/media/css/TableTools_JUI.css" media="screen" />     
    <script type="text/javascript" charset="utf-8" src="js/jquery_datatable/media/js/ZeroClipboard.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/jquery_datatable/media/js/TableTools.js"></script-->
    
	<script type="text/javascript" src="main.js"></script>
	<link rel="stylesheet" type="text/css" href="main.css" />
</head> 
<body> <!--  <body class="custom"> -->
<div id="outer-north">
	<div class="ui-widget-header ui-corner-all ui-state-active toolbar_user">
		<div class="nom_user"><?=$_SESSION['sicpri_cod']?> - <?=ucwords($_SESSION['sicpri_nom'])." ".ucwords($_SESSION['sicpri_ape'])?> (<?=$_SESSION['sicpri_nivel']?>)</div>
		<button id="salir" class="btn salir"><span class="ui-icon ui-icon-power"></span>Cerrar sesi&oacute;n</button>
    </div>
    <!--div class="buttons" >
        <BUTTON onClick="toggleCustomTheme()">Toggle Custom Theme</BUTTON>
        <BUTTON onClick="removeUITheme(); resizePageLayout()">Remove UI Theme</BUTTON>
        <BUTTON onClick="resizePageLayout()">Resize Layout</BUTTON>
    </div-->
    <?=strtoupper(EMPRESA)?><br/>
</div>

<DIV id="page-loading">Cargando...</DIV>



<DIV id="outer-center" class="hidden">

    <UL id="tabbuttons" class="hidden">
        <LI class="tab1"><A href="#tab1">Sistema Integral de Pr&eacute;stamos y Cobranzas. </A></LI>
    </UL>
    
    <DIV id="tabpanels">

        <DIV id="tab1" class="tab-panel ui-tabs-hide">
            <!--DIV class="ui-layout-north ui-widget">
                <DIV class="toolbar ui-widget-content ui-state-active"></DIV>
            </DIV>
            <DIV class="ui-layout-south ui-widget">
                <DIV class="toolbar ui-widget-content ui-state-default"> </DIV>
            </DIV-->
            <DIV class="ui-layout-center">
                <DIV class="ui-widget-header ui-corner-top" id="sicpri_tit">BIENVENIDO</DIV>
                <DIV class="ui-widget-content"><!--class="ui-widget-content container"-->
                    <div id="contenido_sicpri">
						<div id="main_header_fnd"></div><div id="main_footer_fnd"></div>
                    </div>
                </DIV>
                <!--DIV class="ui-widget-footer ui-widget-header ui-corner-bottom">
                <span id="fecha"></span> - <span id="hora"></span>
                </DIV-->
            </DIV>
            <DIV class="ui-layout-west">
			<DIV class="accordion"><?php include("si_menu_sicpri.inc.php");?></DIV>

            </DIV>
            <!--DIV class="ui-layout-east">
                <DIV class="ui-layout-center">
                    <DIV class="ui-widget-header ui-corner-top">Reportes</DIV>
                    <DIV class="ui-widget-content" id="google_tool_map" style="overflow:hidden;">                    
                    </DIV>
                    <DIV class="ui-widget-footer ui-widget-header ui-corner-bottom">Acceso directo a reportes</DIV>
                </DIV>
                <DIV class="ui-layout-south">
                    <DIV class="ui-widget-header ui-corner-top">Calendario</DIV>
                    <DIV class="ui-widget-content">
                    </DIV>
                    <DIV class="ui-widget-footer ui-widget-header ui-corner-bottom">Hoy: <?=date("d-m-Y")?></DIV>
                </DIV>
            </DIV-->
        </DIV><!-- /#tab1 -->

    </DIV><!-- /#tabpanels -->

</DIV><!-- /#outer-center -->
</body>
</html>
<?php
	ob_end_flush();
}else{
	session_destroy();
	$m=base64_encode(urlencode("<strong>Alerta:</strong> Usuario no autorizado."));
	$c=base64_encode(urlencode("error"));	
	header("Location: index.php?m=$m&c=$c");
}
?>