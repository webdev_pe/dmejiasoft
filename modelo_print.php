<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sin título</title>
<style type="text/css" media="screen">
body{ margin:0px; padding:0px; color:#666666; font-size:11px; font-family:Verdana;}
#a4{ position:relative; width:780px; height:1000px; margin:0 auto; margin-top:10px; margin-bottom:10px; background:#ffffff; border:1px solid #ccc;}

#a4_cab{ position:absolute; width:760px; height:150px; border:1px solid #ccc; top:10px; left:10px;}
#a4_cab_logo{ position:absolute; width:85px; height:122px; top:14px; left:10px;}
#a4_cab_desc{ position:absolute; width:645px; height:122px; top:14px; left:105px;}
#a4_cab_desc_tit{ position:absolute; width:645px; height:30px; top:0px; left:0px; font-size:12px; font-weight:bold; text-align:center;}
#a4_cab_desc_cnt{ position:absolute; width:625px; height:72px; top:40px; left:10px; font-size:11px; font-weight:normal; line-height:20px;}

#a4_cue{ position:absolute; width:760px; height:760px; border:1px solid #ccc; top:170px; left:10px;}
#a4_cue_img{ position:absolute;	width:365px; height:365px; border:1px solid #ccc; top:10px; left:10px;}
#a4_cue_b01{ position:absolute;	width:365px; height:365px; border:1px solid #ccc; top:10px; right:10px; line-height:20px;}

#a4_pie{ position:absolute; width:760px; height:50px;  border:1px solid #ccc; bottom:10px; left:10px;}
</style>
<style type="text/css" media="print">
#a4{ position:relative; width:780px; height:1000px; margin:0 auto; margin-top:10px; margin-bottom:10px; background:#ffffff; border:1px solid #ccc; page-break-after:always;}
#a4_cab{ position:absolute; width:760px; height:150px; border:1px solid #ccc; top:10px; left:10px;}
#a4_cue{ position:absolute; width:760px; height:760px; border:1px solid #ccc; top:170px; left:10px;}
#a4_pie{ position:absolute; width:760px; height:50px;  border:1px solid #ccc; bottom:10px; left:10px;}
</style>
</head>
<body>
<?php $i=1; while($i<10){ //10 paginas ?>
<div id="a4">
	<div id="a4_cab">
    	<div id="a4_cab_logo"><img src="images/login/logo.png" width="85" height="122" /></div>
        <div id="a4_cab_desc">
	        <div id="a4_cab_desc_tit">SISTEMA INTEGRAL DE CONTROL Y PROCESOS DE RECAUDACI&Oacute;N DE INMUEBLES<br />[DETALLE DE PROPIEDADES]</div>
   	        <div id="a4_cab_desc_cnt">
                <table width="100%" border="0" cellspacing="2" cellpadding="2">
                  <tr>
                    <td width="40%"><strong>Usuario:</strong> Maycol Zambrano Nu&ntilde;ez</td>
                    <td width="60%"><strong>Tel&eacute;fonos:</strong> 99456895</td>
                  </tr>
                  <tr>
                    <td><strong>C&oacute;digo:</strong> USU0001</td>
                    <td><strong>Email:</strong> webmaster@sicpri.com</td>
                  </tr>
                  <tr>
                    <td><strong>Fecha:</strong> <?=date("d-m-Y")?></td>
                    <td>&nbsp;</td>
                  </tr>
                </table>
            </div>
        </div>
    </div>
	<div id="a4_cue">
	    <div id="a4_cue_img">
        <img src="poo/timthumb/timthumb.php?src=/images/IMG_7007.jpg&h=365&w=365&zc=1&a=c&q=100" width="365" height="365" />
        </div>
	    <div id="a4_cue_b01">
            <table width="100%" border="0" cellspacing="2" cellpadding="2">
              <tr>
                <td width="50%"><strong>C&oacute;digo:</strong> PRP0001</td>
                <td width="50%"><strong>Estado:</strong> Bajo Contrato</td>
              </tr>
              <tr>
                <td colspan="2"><strong>Tipo:</strong> RESIDENCIAL - COMERCIAL</td>
              </tr>
              <tr>
                <td colspan="2"><strong>Sub-Tipo:</strong> CASA - OFICINA</td>
              </tr>              
              <tr>
                <td colspan="2"><strong>Ubicaci&oacute;n:</strong></td>
              </tr>
            </table>        
        </div>
  </div>    
  <div id="a4_pie"><?=$i?></div>
</div>
<?php $i++; } ?>
</body>
</html>
