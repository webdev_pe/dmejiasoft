<?php session_start();?>
<!DOCTYPE html> 
<html class="ui-mobile-rendering"> 
	<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
	<title>SIPC - M&Oacute;DULOS</title> 
	<link rel="stylesheet"  href="../demos/css/themes/default/jquery.mobile-1.1.0.css" />
	<link rel="stylesheet" href="../demos/docs/_assets/css/jqm-docs.css" />
	<script src="../demos/js/jquery.js"></script>
	<script src="../demos/docs/_assets/js/jqm-docs.js"></script>
	<script src="../demos/js/jquery.mobile-1.1.0.js"></script>
</head> 
<body> 
<div data-role="page" class="type-interior">

	<div data-role="header" data-theme="f">
		<h1>M&oacute;dulos SIPC</h1>
		<a href="../index.php" data-icon="home" data-iconpos="notext" data-direction="reverse" class="ui-btn-right jqm-home">Inicio</a>
	</div><!-- /header -->

	<div data-role="content">

		<div class="content-primary">
        <p><!--strong>Acceso interactivo:</strong><br/-->
        <strong>Bienvenido</strong>: <?=strtoupper($_SESSION['nomUsu'])?><!--br/><strong>C&oacute;digo</strong>: <?=$_SESSION['codUsu']?> --></p>
        <br/>

		<script type='text/javascript'>//<![CDATA[ 
        $(document).ready(function(){
			$("#preBtn").click(function(){
				var i=$("#idc").val();
				var c=$("#cli").val();
				//if(i=="" || c==""){alert("Ingrese un dato para la busqueda de los creditos de su cliente.");
				//}else{
					$.mobile.showPageLoadingMsg("b", "Enviando datos, espere un momento por favor...", true);
					$.post("funciones/traer_prestamos.php",{c:c,i:i},function(data){ //alert(data);
						if(data=="1"){
							$.mobile.hidePageLoadingMsg();
							$.mobile.changePage("detalle.php?c="+c+"&i="+i,{transition:"flip"});
						}else{ 
							$.mobile.hidePageLoadingMsg();
							$("#btnPrestamos").empty().html(data);
						}
					});
				//}
			});
			$("#prePst").click(function(){
				var i=$("#idc").val();
				var c=$("#cli").val();
				if(i=="" || c==""){alert("Ingrese un dato para la busqueda de los creditos de su cliente.");
				}else{
					$.mobile.showPageLoadingMsg("b", "Enviando datos, espere un momento por favor...", true);
					$.post("funciones/comprobar_cliente.php",{c:c},function(data){ //alert(data);
						if(data=="1"){
							$.mobile.hidePageLoadingMsg();
							$.mobile.changePage("prestamo.php?c="+c+"&d=i",{transition:"flip"});
						}else{ 
							$.mobile.hidePageLoadingMsg();
							$("#btnPrestamos").empty().html(data);
						}
					});
				}
			});	
			$("#preCja").click(function(){
				$.mobile.changePage("caja.php",{transition:"flip"});
			});

			$("#viewCred").click(function(){
				var i=$("#idc").val();
				var c=$("#cod").val();				
				if(i=="" && c==""){alert("Complete un campo como minimo para ver los detalles del prestamo.");}
				else{ $.mobile.changePage("visualizar.php?i="+i+"&c="+c,{transition:"flip"}); }
			});			
        });//]]>  
        </script>        

        <div data-role="fieldcontain">
        <!--label for="name">ID de cr&eacute;dito:</label><br/>
        <input type="text" name="idc" id="idc" value="" placeholder="ID Credito" class="required"  /><br/>
        <label for="name">C&oacute;digo de cr&eacute;dito:</label><br/> <?=$_GET['c']?>
        <input type="text" name="cod" id="cod" value="" placeholder="Codigo Credito" class="required"  /><br/-->
        
        <label for="idc">ID de cliente:</label><br/>
        <input type="text" name="idc" id="idc" value="" placeholder="id de cliente" class="required"  /><br/>
        <label for="cli">C&oacute;digo de cliente:</label><br/>
        <input type="text" name="cli" id="cli" value="" placeholder="codigo de cliente" class="required"  />
        </div> 
        <div class="ui-body ui-body-b">
         <button class="btnLogin" type="submit" data-icon='gear' data-iconpos='top' data-theme="a" id="preBtn">Ver cr&eacute;ditos</button>
        </div>            
        <!--div class="ui-body ui-body-b">
         <button class="btnLogin" type="submit" data-icon='check' data-iconpos='top' data-theme="a" id="preBtn">Iniciar Cobranzas</button>
        </div>
        <div class="ui-body ui-body-b">
         <button class="btnLogin" type="submit" data-icon='star' data-iconpos='top' data-theme="b" id="prePst">Nuevo Cr&eacute;dito</button>
        </div>
        <div class="ui-body ui-body-b">
         <button class="btnLogin" type="submit" data-icon='gear' data-iconpos='top' data-theme="c" id="preCja">Solicitar a Caja</button>
        </div-->           
        <script type="text/javascript">
        $(document).ready(function(){
			$("#logout").click(function(){
				$.post("funciones/logout.php",function(){
					$.mobile.changePage("acceso.php?m=m",{transition:"flip"});	
				});				
			});		
		});
        </script>            
        <button class="btnLogin" type="submit" data-theme="b" data-icon='delete' data-iconpos='left' id="logout">Cerrar sesi&oacute;n</button>

        <br/>
		<div id="btnPrestamos" style="color:#ff0000; font-weight:bold; text-align:center;">
        </div>     
        <br/>
			<!--nav>
				<ul data-role="listview" data-inset="true" data-theme="c" data-dividertheme="b">
					<li data-role="list-divider">M&oacute;dulos</li>
					<li><a href="docs/pages/index.html">Pr&eacute;stamos</a></li>
					<li><a href="docs/toolbars/index.html">Cobranzas</a></li>
					<li><a href="paginas/contacto.php" rel="external">Otros</a></li>
				</ul>
			</nav-->
		</div>

		</div><!-- /content -->

        <div data-role="footer" class="footer-docs" data-theme="c">
                <p>&copy; <?=date("Y")?> SIPC: Sistema Integral de Pr&eacute;stamos y Cobranzas</p>
        </div>

						

	</div><!-- /content -->
	
</div><!-- /page -->
</body>
</html>
