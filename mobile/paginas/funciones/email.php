<!DOCTYPE html> 
<html class="ui-mobile-rendering"> 
	<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
	<title>SIPC - Confirmaci&oacute;n de envio</title> 
	<link rel="stylesheet"  href="../demos/css/themes/default/jquery.mobile-1.1.0.css" />
	<link rel="stylesheet" href="../demos/docs/_assets/css/jqm-docs.css" />
	<script src="../demos/js/jquery.js"></script>
	<script src="../demos/docs/_assets/js/jqm-docs.js"></script>
	<script src="../demos/js/jquery.mobile-1.1.0.js"></script>
</head> 
<body> 

<div data-role="page" class="type-interior">

	<div data-role="header" data-theme="f">
		<h1>Confirmaci&oacute;n de envio</h1>
		<a href="../../index.php" data-icon="home" data-iconpos="notext" data-direction="reverse" class="ui-btn-right jqm-home">Inicio</a>
	</div><!-- /header -->

	<div data-role="content">
		<div class="content-primary">
		<?php
		$nom=$_POST['nom']; $ape=$_POST['ape'];
		$tel=$_POST['tel']; $eml=$_POST['eml'];
		$msj=$_POST['msj'];	
		
		$destinatario = "informes@sipc.com";
		$asunto = utf8_decode("Contacto SIPC Movil.");
		$de = $eml;
		
		$cuerpo =utf8_decode("
		<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
		<strong>CONTACTO SIPC</strong><br/>
		Nombres: $nom <br/>
		Apellidos: $ape <br/>
		Tel&eacute;fono: $tel <br/>
		Email: $eml <br/>
		Mensaje: $msj <hr/>
		".date("d-m-Y")."");
	
		$to      = $destinatario;
		$subject = "$asunto - $de"; 
		$message = $cuerpo;
		$headers = "From: Contacto SIPC Movil < $de >" . "\r\n" .
           "Reply-To:" . "\r\n" .
           "CC:" . "\r\n" .		   
		   "Bcc: maycol_630@hotmail.com" . "\r\n".	   
		   "MIME-Version: 1.0" . "\r\n".
		   "Content-type:text/html;charset=iso-8859-1" . "\r\n".
           "X-Mailer: PHP/" . phpversion();
		   
			if(@mail($to, $subject, $message, $headers)){
				echo "<p>Email enviado correctamente.</p><a href='../contacto.php' data-role='button' data-icon='arrow-l'>Regresar</a>";
			}else{
				echo "<p>Vuelva a intentarlo nuevamente, o envie un email a <a href='mailto:$destinatario?subject=Contacto SIPC&body=Contacto'>$destinatario</a></p><a href='../contacto.php' data-role='button' data-icon='arrow-l'>Regresar</a>";
			}		
		?>
		</div>		
		
		<div class="content-secondary">
			<div data-role="collapsible" data-collapsed="true" data-theme="b" data-content-theme="d">
					<h3>M&aacute;s opciones</h3>
					<ul data-role="listview" data-theme="c" data-dividertheme="d">
						<li data-role="list-divider">Soporte On-Line</li>
						<li><a href="docs/pages/index.html">Ayuda</a></li>
						<li><a href="docs/toolbars/index.html">Documentaci&oacute;n</a></li>
						<li><a href="../../paginas/compatibilidad.php">Compatibilidad M&oacute;vil</a></li>
						<li><a href="../../paginas/contacto.php">Contacto</a></li>					
					</ul>
			</div>
		</div>
				

		</div><!-- /content -->

        <div data-role="footer" class="footer-docs" data-theme="c">
                <p>&copy; <?=date("Y")?> SIPC: Sistema Integral de Pr&eacute;stamos y Cobranzas</p>
        </div>

						

	</div><!-- /content -->
	
</div><!-- /page -->

</body>
</html>