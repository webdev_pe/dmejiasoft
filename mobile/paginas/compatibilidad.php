<!DOCTYPE html> 
<html class="ui-mobile-rendering"> 
	<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
	<title>SIPC - Compatibilidad M&oacute;vil</title> 
	<link rel="stylesheet"  href="../demos/css/themes/default/jquery.mobile-1.1.0.css" />
	<link rel="stylesheet" href="../demos/docs/_assets/css/jqm-docs.css" />
	<script src="../demos/js/jquery.js"></script>
	<script src="../demos/docs/_assets/js/jqm-docs.js"></script>
	<script src="../demos/js/jquery.mobile-1.1.0.js"></script>
</head> 
<body> 
<div data-role="page" class="type-interior">

	<div data-role="header" data-theme="f">
		<h1>Compatibilidad M&oacute;vil</h1>
		<a href="../index.php" data-icon="home" data-iconpos="notext" data-direction="reverse" class="ui-btn-right jqm-home">Inicio</a>
	</div><!-- /header -->

	<div data-role="content">
		
		
		<div class="content-primary">
				<h2 id="platforms">Plataformas que soportan SIPC:</h2>
				<p>Estos son los dispositivos que soportan SIPC, tienen grado A de aprobaci&oacute;n, los cuales son los dispositivos moviles m&aacute;s recomendables para su uso:</p>

				<ul>
					<li><strong>Apple iOS 3.2-5.0</strong> - Tested on the original iPad (4.3 / 5.0), iPad 2 (4.3), original iPhone (3.1), iPhone 3 (3.2), 3GS (4.3), and 4 (4.3 / 5.0)</li>
					<li><strong>Android 2.1-2.3</strong> - Tested on the HTC Incredible (2.2), original Droid (2.2), Nook Color (2.2), HTC Aria (2.1), Google Nexus S (2.3). Functional on 1.5 &amp; 1.6 but performance may be sluggish, tested on Google G1 (1.5)</li>
					<li><strong>Android Honeycomb</strong>- Tested on the Samsung Galaxy Tab 10.1 and Motorola XOOM</li>
					<li><strong>Windows Phone 7-7.5</strong> - Tested on the HTC Surround (7.0) HTC Trophy (7.5), and LG-E900 (7.5)</li>
					<li><strong>Blackberry 6.0</strong> - Tested on the Torch 9800 and Style 9670</li>
					<li><strong>Blackberry 7</strong> - Tested on BlackBerry® Torch 9810</li>
					<li><strong>Blackberry Playbook</strong> - Tested on PlayBook version 1.0.1 / 1.0.5</li>
					<li><strong>Palm WebOS (1.4-2.0)</strong> - Tested on the Palm Pixi (1.4), Pre (1.4), Pre 2 (2.0)</li>
					<li><strong>Palm WebOS 3.0 </strong> - Tested on HP TouchPad</li>
					<li><strong>Firebox Mobile (Beta)</strong> - Tested on Android 2.2</li>
					<li><strong>Opera Mobile 11.0</strong>: Tested on Android 2.2</li>
					<li><strong>Meego 1.2</strong> - Tested on Nokia 950 and N9</li>
					<li><strong>Kindle 3 and Fire</strong>: Tested on the built-in WebKit browser for each</li>
					<li><strong>Chrome <strong>Desktop </strong>11-15</strong> - Tested on OS X 10.6.7 and Windows 7</li>
					<li><strong>Firefox Desktop 4-8</strong> - Tested on OS X 10.6.7 and Windows 7</li>
					<li><strong>Internet Explorer 7-9</strong> - Tested on Windows XP, Vista and 7 (minor CSS issues)</li>
					<li><strong>Opera Desktop 10-11</strong> - Tested on OS X 10.6.7 and Windows 7</li>
					<li><strong>Samsung Bada</strong> - Tested on the device's stock Dolphin Browser</li>
					<li><strong>Android UCWeb</strong> - Tested on Android 2.3</li>
				</ul>
		</div>		
				

		</div><!-- /content -->

        <div data-role="footer" class="footer-docs" data-theme="c">
                <p>&copy; <?=date("Y")?> SIPC: Sistema Integral de Pr&eacute;stamos y Cobranzas</p>
        </div>

						

	</div><!-- /content -->
	
</div><!-- /page -->
</body>
</html>