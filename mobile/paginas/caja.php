<?php
session_start();
require("../../poo/clases/getConection.php");
$cn=new getConection();
$sql="select id_caja, ifnull(sum(inicial),0) as 'caja', 
ifnull((select sum(monto) from si_caja_sol where fecha=curdate()),0) as 'solicitado',
ifnull(sum(inicial),0) - ifnull((select sum(monto) from si_caja_sol where fecha=curdate()),0) as 'total'
from si_caja where fecha=curdate()";
$cn->ejecutar_sql(base64_encode($sql));
$cel=$cn->resultado_sql();
?>   
<!DOCTYPE html> 
<html class="ui-mobile-rendering"> 
	<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
	<title>SIPC - Pr&eacute;stamos</title> 
	<link rel="stylesheet"  href="../demos/css/themes/default/jquery.mobile-1.1.0.css" />
	<link rel="stylesheet" href="../demos/docs/_assets/css/jqm-docs.css" />
	<script src="../demos/js/jquery.js"></script>
	<script src="../demos/docs/_assets/js/jqm-docs.js"></script>
	<script src="../demos/js/jquery.mobile-1.1.0.js"></script>
</head> 
<body> 
<div data-role="page" class="type-interior">

	<div data-role="header" data-theme="f">
		<h1>Nuevo Pr&eacute;stamo</h1>
		<a href="../index.php" data-icon="home" data-iconpos="notext" data-direction="reverse" class="ui-btn-right jqm-home">Inicio</a>
	</div><!-- /header -->

	<div data-role="content">		
		<div class="content-primary">
	  <script type="text/javascript">
      $(document).ready(function(){		
		$("#preCaj").click(function(){
			var mnt=$("#monto").val();	
			var sal=$("#saldo").val();
			if(mnt=="" || mnt<=0){ alert("Ingrese un monto a solicitar."); $("#monto").focus(); }
			else if(mnt>sal){ alert("El monto solicitado debe ser menor o igual al disponible."); $("#monto").focus(); }		
			else{
				$.mobile.showPageLoadingMsg("b", "Guardando, espere un momento porfavor...", true);
				$.post("../../sicpri/00_caja/dao.php",{opt:"c",i:'<?=$_SESSION['idUsu']?>',mnt:mnt,dis:"movil"},function(data){
					if(data==1){ 
						alert("Solicitud registrada correctamente.");
						$.mobile.hidePageLoadingMsg();
						$.mobile.changePage("modulos.php",{transition:"flip"});
					}else{
						alert("Vuelva a intentarlo por favor.");
						$.mobile.hidePageLoadingMsg();
					}
				});
			}
		});				 
	  });
      </script>
        <strong>Operador</strong>: <?=strtoupper($_SESSION['nomUsu'])?><br/>
		<strong>C&oacute;digo</strong>: <?=$_SESSION['codUsu']?><br/>
        <strong>Disponible en caja</strong>: S/. <?=number_format($cel['total'],2,'.',',')?><br/><br>

        <input type="hidden" id="saldo" value="<?=$cel['total']?>" >
        <div class="norm">
        Monto a solicitar<br />
        <input type="text" id="monto" />
        </div><br>


            <div class="ui-body ui-body-b">
                <button class="btnLogin" type="submit" data-icon='check' data-iconpos='top' 
                    data-theme="b" id="preCaj">Registrar saldo</button>
            </div>  
            
<?php
$cns=new getConection();
$sqls="select date_format(fecha,'%d-%m-%Y') as 'fecha', hora, monto from si_caja_sol where id_usu=".$_SESSION['idUsu']." and fecha=curdate()";
$cns->ejecutar_sql(base64_encode($sqls));
?>  
<table width="100%" border="1" id="tbl_res" style="text-shadow:none; font-size:9px; border-collapse:collapse;">
<thead style="background:#73b242; color:#FFF; text-shadow:none; font-size:9px; border-collapse:collapse;">
  <tr>
    <th>#</th>
    <th>Fecha</th>
    <th>Hora</th>
    <th>Monto</th>
  </tr>
</thead>
<tbody>  
  <?php $i=1; while($cels=$cns->resultado_sql()){ ?>
  <tr>
    <td align="center"><?=$i++?></td>
    <td align="center"><?=$cels['fecha']?></td>
    <td align="center"><?=$cels['hora']?></td>
    <td align="right"><?=number_format($cels['monto'],2,'.',',')?></td>
  </tr>
  <?php $solicitado+=$cels['monto'];}?>
</tbody>
<tfoot>
  <tr>
    <td align="left" colspan="3">Total solicitado:</td>
    <td align="right"><?=number_format($solicitado,2,'.',',')?></td>
  </tr>
</tfoot>
</table>
            
		</div><br/>




		<div class="content-secondary">
        <a href="modulos.php" data-role='button' data-icon='arrow-l' data-iconpos='left' data-theme='a'>Regresar</a>
        </div>
		</div><!-- /content -->

        <div data-role="footer" class="footer-docs" data-theme="c">
                <p>&copy; <?=date("Y")?> SIPC: Sistema Integral de Pr&eacute;stamos y Cobranzas</p>
        </div>

						

	</div><!-- /content -->
	
</div><!-- /page -->
</body>
</html>