<?php
require("../../poo/clases/getConection.php");
$cn=new getConection();
$sql="select id_cob, id_pre, mnt_cob, dias, adicional, interes, fecha, cierre from si_cobranzas where id_pre=".$_GET['i']." order by id_cob asc";
$cn->ejecutar_sql(base64_encode($sql));
$row=$cn->cantidad_sql();

$cn1=new getConection();
$sql1="select id_pre, tip_pre, mnt_pre, int_pre, dia_pre, fra_pre, moroso,
p.id_cli, concat(c.nom_cli,' ',c.ape_cli) as 'nombres', c.cod_cli, p.cod_pre, date_format(fecha,'%d-%m-%Y') as 'fecha'
 from si_prestamos p, si_clientes c
 where p.id_cli=c.id_cli and id_pre=".$_GET['i'];
$cn1->ejecutar_sql(base64_encode($sql1));
$cel1=$cn1->resultado_sql();
?>
<!DOCTYPE html> 
<html class="ui-mobile-rendering"> 
	<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
	<title>SIPC - PORCENTAJE</title> 
	<link rel="stylesheet"  href="../demos/css/themes/default/jquery.mobile-1.1.0.css" />
	<link rel="stylesheet" href="../demos/docs/_assets/css/jqm-docs.css" />
	<script src="../demos/js/jquery.js"></script>
	<script src="../demos/docs/_assets/js/jqm-docs.js"></script>
	<script src="../demos/js/jquery.mobile-1.1.0.js"></script>
</head> 
<body> 
<div data-role="page" class="type-interior">

	<div data-role="header" data-theme="f">
		<h1>P. Porcentaje</h1>
		<a href="../index.php" data-icon="home" data-iconpos="notext" data-direction="reverse" class="ui-btn-right jqm-home">Inicio</a>
	</div><!-- /header -->

	<div data-role="content">		
		<div class="content-primary">
<?php
	echo "<h3>Cr&eacute;dito Consignaci&oacute;n por Mes (".$cel1['cod_pre'].")</h3>";
	echo "<hr />";
	echo "<strong>Fecha de pr&eacute;stamo:</strong> ".$cel1['fecha']."<br />";
	echo "<strong>Cliente:</strong> ".$cel1['nombres']."<br /> <strong>C&oacute;digo:</strong> ".$cel1['cod_cli']."<br />";
		$monto=$cel1['mnt_pre'];
		echo "<strong>Monto</strong>: ".number_format(round($monto),2,'.',',')." -
		<strong>D&iacute;as</strong>: ".$cel1['dia_pre']." -
		<strong>Interes</strong>: ".$cel1['int_pre']."%";	
		$diario=$monto/$cel1['dia_pre'];
		$interes=$monto*$cel1['int_pre']/$cel1['dia_pre'];
		
		$int_tot=$monto*$cel1['int_pre'];
		$dev_tot=$monto+$int_tot;
	echo "<hr />";
?>
<style type="text/css">
#tbl_res{ font-size:9px; border-collapse:collapse;}
</style>   
<table width="100%" border="1" id="tbl_res">
<thead style="background:#73b242; color:#FFF; text-shadow:none;">
  <tr>
    <th width="4%" align="center" valign="middle">#</th>
    <th width="13%" align="center" valign="middle">Fecha</th>    
    <th width="13%" align="center" valign="middle">Cuota</th>
    <th width="10%" align="center" valign="middle">Inter&eacute;s</th>
    <th width="10%" align="center" valign="middle">Total</th>
    <th width="20%" align="center" valign="middle">Pago</th>
    <th width="4%" rowspan="2" align="center" valign="middle" title="Eliminar">*</th>   
  </tr>
</thead>  
<tbody>
  <?php 
	$i=1;
	while($cel=$cn->resultado_sql()){ 
	$fecha=date("d-m-Y",strtotime($cel['fecha']));
	$total=$cn->redondeo($diario)+$cn->redondeo($interes);	
  ?>
  <tr bgcolor="<?php if($fecha==date("d-m-Y")){echo "#ffe87b";}else{ if($i%2==0){echo "#ffffff";}else{echo "#e2e4ff";} }?>" <?php if($cel['mnt_cob']==0){ echo "style='display:none;'";}?>>
    <td align="center" valign="middle"><?=$i?></th>
    <td align="center" valign="middle"><?=$fecha?></th>
    <td align="right" valign="middle"><?=$cn->redondeo($diario)?></td>
    <td align="right" valign="middle"><?=$cn->redondeo($interes)?></td>
    <td align="right" valign="middle"><?=$cn->redondeo($total)?></td>
    <td align="right" valign="middle"><?=$cn->redondeo($cel['mnt_cob'])?></td>   
    <td align="center" valign="middle">
		<?php
        if($cel['fecha']==$fecha_ayer || $cel['fecha']==$fecha_hoy){
            $theme_eli='data-theme="a"';
            $function_eli="eliminar('".$cel['id_cob']."')";
        }else{
            $theme_eli='';
            $function_eli="javascript:alert('Opcion no permitida.')";
        }
        ?>     
    <a onclick="<?=$function_eli?>" data-role="button" data-icon="minus" data-iconpos="notext" <?=$theme_eli?>>Eliminar cobranza</a>
    
    </td>            
  </tr>
  <?php 
  	$ult_adc=$cel['adicional'];
	$acum1+=$cel['mnt_cob'];
	$i++;
	}?>

</tbody>
<tfoot>
  <tr>
    <th align="center" colspan="5">Totales</th>
    <th align="center">Cuota</th><th align="center">-</th>
  </tr>
  <tr>
    <th align="left" colspan="5">Total Cobrado</th>
    <th align="right"><?=$cn->redondeo($acum1)?></th><th align="center">-</th>
  </tr>
  <tr>
    <th align="left" colspan="5">Total Pendiente</th>
    <th align="right"><?=$cn->redondeo($dev_tot-$acum1)?></th><th align="center">-</th>
  </tr>
  <tr>
    <th align="left" colspan="5">Total Prestado</th>
    <th align="right"><?=$cn->redondeo($dev_tot)?></th><th align="center">-</th>
  </tr>
  <tr>
  	<?php if(($dev_tot-$acum1)>0){?><th align="center" colspan="7" bgcolor="#ff0000"><h3 style="color:#ffffff;">Pr&eacute;stamo Pendiente</h3></th>
    <?php }else{?><th align="center" colspan="7" bgcolor="#116194"><h3 style="color:#ffffff;">Pr&eacute;stamo Cancelado</h3></th><?php }?>
  </tr>     
<tfoot>
</table>
		</div>

        <div class="content-secondary">
        <?php 	$p=($cel1['tip_pre']=="p")?"C/MES":"C/D&Iacute;A";
				$m=($cel1['moroso']=="m")?"MOROSO":"";	?>
        <a href='formulario.php?i=<?=$cel1['id_pre']?>&c=<?=$cel1['cod_cli']?>&p=<?=$cel1['tip_pre']?>' data-role='button' data-icon='grid' data-iconpos='top' data-theme='a'><?=$cel1['cod_pre']?> (<?=number_format($cel1['mnt_pre'],2,'.',',')?>) - <?=$p?> <?=$m?></a>
		<a href="detalle.php?c=<?=$_GET['c']?>" data-role='button' data-icon='arrow-l' data-iconpos='left' data-theme='a'>Regresar</a>
        </div>
		</div><!-- /content -->

        <div data-role="footer" class="footer-docs" data-theme="c">
                <p>&copy; <?=date("Y")?> SIPC: Sistema Integral de Pr&eacute;stamos y Cobranzas</p>
        </div>

						

	</div><!-- /content -->
	
</div><!-- /page -->
</body>
</html>