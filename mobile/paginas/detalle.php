<?php
require("../../poo/clases/getConection.php");
$idc = $_GET['i'];
$cod = $_GET['c'];
$cn=new getConection();

if($idc!=""){
	$sql="select p.id_pre, p.cod_pre, p.mnt_pre, p.id_cli, p.tip_pre, p.est_pre
	from si_clientes c, si_prestamos p 
	where c.id_cli=p.id_cli and est_pre='a' and
	c.id_cli=".$idc."";
}else if($cod!=""){
	$sql="select p.id_pre, p.cod_pre, p.mnt_pre, p.id_cli, p.tip_pre, p.est_pre
	from si_clientes c, si_prestamos p 
	where c.id_cli=p.id_cli and est_pre='a' and
	cod_cli='".$cod."'";
}	
$cn->ejecutar_sql(base64_encode($sql));
?>   
<!DOCTYPE html> 
<html class="ui-mobile-rendering"> 
	<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
	<title>SIPC - PR&Eacute;STAMOS/COBRANZAS</title> 
	<link rel="stylesheet"  href="../demos/css/themes/default/jquery.mobile-1.1.0.css" />
	<link rel="stylesheet" href="../demos/docs/_assets/css/jqm-docs.css" />
	<script src="../demos/js/jquery.js"></script>
	<script src="../demos/docs/_assets/js/jqm-docs.js"></script>
	<script src="../demos/js/jquery.mobile-1.1.0.js"></script>
</head> 
<body> 
<div data-role="page" class="type-interior">

	<div data-role="header" data-theme="f">
		<h1>Detalle de pr&eacute;stamos</h1>
		<a href="../index.php" data-icon="home" data-iconpos="notext" data-direction="reverse" class="ui-btn-right jqm-home">Inicio</a>
	</div><!-- /header -->

	<div data-role="content">		
		<div class="content-primary">
		<?php
        $cn2=new getConection();
		if($idc!=""){
			$sql2="select concat(nom_cli,' ',ape_cli) as 'nombres' from si_clientes where id_cli=".$idc."";
		}else if($cod!=""){
			$sql2="select concat(nom_cli,' ',ape_cli) as 'nombres' from si_clientes where cod_cli='".$cod."'";
		}
		$cn2->ejecutar_sql(base64_encode($sql2));
		$cel2=$cn2->resultado_sql();
		?>
        <strong>CLIENTE:</strong> <?=strtoupper($cel2['nombres'])?><br/>
		<strong>ID:</strong> <?=$idc?><br/>
		<strong>C&Oacute;DIGO:</strong> <?=strtoupper($cod)?><hr/>
        <!--br/>
        <a href="prestamo.php?c=<?=$cod?>" data-role='button' data-icon='plus' data-iconpos='top' data-theme='b'>NUEVO CR&Eacute;DITO PARA <?=strtoupper($cod)?></a-->
		<br/>
		<?php
            while($cel=$cn->resultado_sql()){
				$p=($cel['tip_pre']=="p")?"C/MES":"C/D&Iacute;A";
				$m=($cel['moroso']=="m")?"MOROSO":"";		
				$idp=$cel['id_pre'];
				if($cel['tip_pre']=="p"){
					$detalle_cobranza="porcentual.php?i=$idp&c=$cod";
					//$formulario_cobro="formulario.php?i=$idp&c=$cod&p=".$cel['tip_pre'];
					$formulario_cobro="visualizar.php?i=$idp&c=$cod&p=".$cel['tip_pre'];
				}else{
					$detalle_cobranza="arrebatir.php?i=$idp&c=$cod";
					//$formulario_cobro="formulario.php?i=$idp&c=$cod&p=".$cel['tip_pre'];
					$formulario_cobro="visualizar.php?i=$idp&c=$cod&p=".$cel['tip_pre'];
				}
			
                $buttons.="
                <a href='$formulario_cobro' data-role='button' data-icon='grid' data-iconpos='top' data-theme='a'>".$cel['cod_pre']." (".number_format($cel['mnt_pre'],2,'.',',').") - $p $m</a>
                <!--a href='$detalle_cobranza' data-role='button' data-icon='search' data-iconpos='left' data-theme='b'>Ver detalle</a-->
				<hr/>
                ";
            }
			echo $buttons;
        ?><br/>
		</div>
        
        <div class="content-secondary">
		<a href="modulos.php?c=<?=$cod?>" data-role='button' data-icon='arrow-l' data-iconpos='left' data-theme='a'>Regresar</a>
        </div>
		</div><!-- /content -->

        <div data-role="footer" class="footer-docs" data-theme="c">
                <p>&copy; <?=date("Y")?> SIPC: Sistema Integral de Pr&eacute;stamos y Cobranzas</p>
        </div>
	</div><!-- /content -->
	
</div><!-- /page -->
</body>
</html>