<?php
session_start();
unset($_SESSION['sicpri_idu']);
unset($_SESSION['sicpri_cod']);
unset($_SESSION['sicpri_nom']);
unset($_SESSION['sicpri_ape']);

unset($_SESSION['sicpri_eml']);
unset($_SESSION['sicpri_reg']);
unset($_SESSION['sicpri_mod']);
unset($_SESSION['sicpri_eli']);
unset($_SESSION['sicpri_bus']);
unset($_SESSION['sicpri_rep']);
unset($_SESSION['sicpri_vis']);
unset($_SESSION['sicpri_imp']);
unset($_SESSION['sicpri_exp']);
unset($_SESSION['sicpri_env']);
unset($_SESSION['sicpri_niv']);
unset($_SESSION['sicpri_nivel']);
unset($_SESSION['sicpri_men']);
		
session_destroy();

$data['msj']="";
$data['cls']="";
$data['std']="0";
echo json_encode($data);	
?>