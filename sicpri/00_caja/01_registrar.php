<script type="text/javascript">
$(document).ready(function(){
	$.datepicker.setDefaults($.datepicker.regional["es"]);
	$("#fec_dsd").datepicker({
		dateFormat: "dd-mm-yy",
		numberOfMonths: 3,		
		showButtonPanel: true/*, showOn: "button", buttonImage: "img/calendar.gif", buttonImageOnly: true */
	});
	
	$("#save").click(function(){
		var mnt=$("#monto").val();
		var fch=$("#fec_dsd").val();
		if(mnt==""){ alert("Complete el campo Monto."); $("#monto").focus(); }
		else if(fch==""){ alert("Complete el campo Fecha."); $("#fec_dsd").focus(); }
		else{
			  $.post("sicpri/00_caja/dao.php",{opt:"i",mnt:mnt,fch:fch},function(data){
				  if(data==1){ 
					  alert("Caja registrada correctamente.");
					  $.post("sicpri/00_caja/06_historial.php",function(data){
						  $("#contenido_sicpri").html(data);
						  $("#sicpri_tit").empty().text("HISTORIAL DE CAJA");
					  });	
				  }else{
					  alert("Vuelva a intentarlo por favor.");
				  }
			  });
		}
	});
	$("#hist").click(function(){
		$.post("sicpri/00_caja/06_historial.php",function(data){
			$("#contenido_sicpri").html(data);
			$("#sicpri_tit").empty().text("HISTORIAL DE CAJA");
		});
	});	
});
</script>
<div id="sql"></div>
<table width="100%" border="0" id="tbl_prp">
  <tr>
    <td colspan="2">
        <table width="100%" border="0">
          <tr>
            <td width="25%">Ingresar Caja</td>
            <td width="25%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
          </tr>
          <tr>
            <td align="left" valign="top"> Monto<br/><input type="text" id="monto" /><br/></td>
            <td align="left" valign="top"> Fecha<br/><input type="text" id="fec_dsd" value="<?=date("d-m-Y")?>" /></td>
            <td align="left" valign="top">&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
          </tr>
        </table>
    </td>
  </tr>
  <tr>
    <td>    
    </td>
  </tr>  
  <tr>
    <td>
	<button id="save" class="btn"><span class="ui-icon ui-icon-disk"></span>Guardar nuevo ingreso de caja</button>
	<button id="hist" class="btn"><span class="ui-icon ui-icon-folder-open"></span>Historial de caja</button>
    </td>
  </tr>
</table>