<script type="text/javascript">
$(document).ready(function(){
	$("#cod_cli").autocomplete({
		source: "sicpri/03_clientes/_buscar_codigo.php",
		minLength: 2/*, select: function(event,ui){ $("#id_con").val(ui.item.id); }*/
	});	
	$("#dele").click(function(){
		var c=$("#cod_cli").val();
		if(c!=""){
			if(confirm("Esta seguro de eliminar al usuario: "+c+" ?")){
				$.post("sicpri/03_clientes/dao.php", {opt:"d",c:c}, function(data){
					if(data==1){
						$.post("sicpri/03_clientes/06_historial.php",function(data){
							$("#contenido_sicpri").html(data);
							$("#sicpri_tit").empty().text("HISTORIAL DE CLIENTES");
						});
					}
				});				
			}
		}else{ alert("Ingrese el codigo de zona a eliminar."); $("#cod_cli").focus(); }
	});
	
	$("#hist").click(function(){
		$.post("sicpri/03_clientes/06_historial.php",function(data){
			$("#contenido_sicpri").html(data);
			$("#sicpri_tit").empty().text("HISTORIAL DE CLIENTES");
		});
	});		
});
</script>
<table width="100%" border="0" id="tbl_prp">
  <tr>
    <td colspan="2">   
        <table width="100%" border="0" id="tbl_find">
          <tr>
            <td width="25%"><strong>Buscar cliente a eliminar:</strong></td>
            <td width="25%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
          </tr>
          <tr>
            <td align="left" valign="top">
			C&oacute;digo de Cliente:<br />
			<input type="text" id="cod_cli" />
            </td>
            <td align="left" valign="top">
            </td>
            <td align="left" valign="top">
            </td>
            <td align="left" valign="top">
            </td>
          </tr>
        </table>
	</td>
  </tr> 
  <tr>
    <td>
	<button id="dele" class="btn"><span class="ui-icon ui-icon-trash"></span>Eliminar cliente</button>
    <button id="hist" class="btn"><span class="ui-icon ui-icon-disk"></span>Historial de clientes</button>
    </td>
  </tr>
</table>