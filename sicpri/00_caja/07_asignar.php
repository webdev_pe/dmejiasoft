<?php
require("../../poo/clases/getConection.php");
/*$cn=new getConection();

$sql="select id_usu, id_zon, cod_usu, concat(nom_usu,' ',ape_usu) as 'nombres', tipo_dni, num_dni, user, pswd, dir_usu, eml_usu, tlf_usu, cla_usu, mov_usu, nex_usu, rpc_usu, rpm_usu, nivel, estado from si_usuarios where nivel='o'";
$cn->ejecutar_sql(base64_encode($sql));
$cn->cantidad_sql();*/
$fsearch=($_POST['f']=="")?"fecha=curdate()":"fecha='".date("Y-m-d",strtotime($_POST['f']))."'";

$cn=new getConection();
$sql="select id_caja, ifnull(sum(inicial),0) as 'caja', 
ifnull((select sum(monto) from si_caja_sol where ".$fsearch."),0) as 'solicitado',
ifnull(sum(inicial),0) - ifnull((select sum(monto) from si_caja_sol where ".$fsearch."),0) as 'total'
from si_caja where ".$fsearch." group by id_caja";
$cn->ejecutar_sql(base64_encode($sql));
$cel=$cn->resultado_sql();
#echo $sql;
$cns=new getConection();
$sqls="select id_sol, date_format(fecha,'%d-%m-%Y') as 'fecha', hora, monto, c.id_usu, concat(u.nom_usu,' ',u.ape_usu) as 'nombres'
from si_caja_sol c, si_usuarios u
where c.id_usu=u.id_usu and ".$fsearch."
order by id_sol desc";
$cns->ejecutar_sql(base64_encode($sqls));
#echo $sqls;
?>
<script type="text/javascript">
$(document).ready(function(){
	$.datepicker.setDefaults($.datepicker.regional["es"]);
	$("#fec_dsd").datepicker({
		dateFormat: "dd-mm-yy",
		numberOfMonths: 3,		
		showButtonPanel: true,/*, showOn: "button", buttonImage: "img/calendar.gif", buttonImageOnly: true */
		   onSelect: function(textoFecha, objDatepicker){
			  //$("#mensaje").html("<p>Has seleccionado: " + textoFecha + "</p>");
				$.post("sicpri/00_caja/07_asignar.php",{f:textoFecha},function(data){
					$("#contenido_sicpri").html(data);
					$("#sicpri_tit").empty().text("ASIGNAR MONTO");
				});
		   }
	});	
	$('#list_cli').dataTable({
      /*"bProcessing": true,
      "bServerSide": true,
      "sAjaxSource": "content/scripts/propiedades.php",*/
	  'bJQueryUI': true,
	  'sPaginationType': 'full_numbers',	  
	  'fnDrawCallback': function(oSettings){
		  if(oSettings.bSorted || oSettings.bFiltered){
			  for(var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++){
				  $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i+1);
			  }
		  }
	  },'aoColumnDefs': [
		  {'bSortable': false, 'aTargets': [0]},
		  {'bSortable': false, 'aTargets': [1]}
	  ],"bAutoWidth" : false,	  
	  "bSortClasses": false, 
	  "aoColumns" : [
		  { sWidth : '18px'},
		  { sWidth : '18px'},
		  { sWidth : '100px'},	  
		  { sWidth : '100px'},
		  { sWidth : 'auto'},
		  { sWidth : 'auto'}
	  ],'aaSorting': [[0, 'asc']]
	}).columnFilter({aoColumns: [
		  null,
		  null,
		  { type: "text" },
		  { type: "text" },
		  { type: "text" },
		  { type: "text" }
	  ]
	});
		
	$("#operador").load("sicpri/02_prestamos/operadores.php",function(data){
		$(this).selectmenu();
	});	
	
	$("#btn_sol").click(function(){
	  var opr=$("#operador").val();
	  var mnt=parseFloat($("#mnt_sol").val());
	  var sal=parseFloat($("#saldo").val());
	  var fch=$("#fec_dsd").val();
	  //alert(mnt+"+"+sal);
	  if(opr=="nn"){
		  alert("Seleccione un operador"); $("#btn_sol").focus();
	  }else{
			if(mnt=="" || mnt<=0){ alert("Ingrese un monto a solicitar."); $("#mnt_sol").focus(); }
			else if(mnt>sal){ alert("El monto solicitado debe ser menor o igual al disponible."); $("#mnt_sol").focus(); }		
			else if(fch==""){ alert("Complete el campo Fecha."); $("#fec_dsd").focus(); }
			else{
				$.post("sicpri/00_caja/dao.php",{opt:"c",i:opr,mnt:mnt,fch:fch,dis:"sistema"},function(data){
					if(data==1){ 
						alert("Solicitud registrada correctamente.");
						$.post("sicpri/00_caja/07_asignar.php",function(data){
							$("#contenido_sicpri").html(data);
							$("#sicpri_tit").empty().text("ASIGNAR MONTO A OPERADOR");
						});
					}else{
						alert("Vuelva a intentarlo por favor.");
					}
				});				
			}
	  }
	});
	
});
</script>
<input type="hidden" id="saldo" value="<?=$cel['total']?>" >
<table width="100%" border="0" id="tbl_prp">
  <tr>
    <td colspan="2">
        <table width="100%" border="0">
          <tr>
            <td><strong>Disponible en caja</strong>: S/. <?=number_format($cel['total'],2,'.',',')?><br/></td>
            <td width="25%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="2" align="left" valign="top"><table width="100%" border="0">
              <tr>
                <td>Seleccione operador</td>
                <td>Monto</td>
                <td>Fecha</td>
                <td></td>
              </tr>
              <tr>
                <td><select name="operador" id="operador">
                </select></td>
                <td><input type="text" id="mnt_sol" /></td>
                <td><input type="text" id="fec_dsd" value="<?=($_POST['f']=="")?date("d-m-Y"):$_POST['f']?>" /></td>
                <td><button class="btn" id="btn_sol"><span class="ui-icon ui-icon-plus"></span>Asignar saldo</button></td>
              </tr>
            </table></td>
            <td align="left" valign="top">&nbsp;</td>
          </tr>
        </table>
    </td>
  </tr> 
  <tr>
    <td></td>
  </tr>
</table>

	<table id="list_cli" class="list_tbl" width="100%">
	<thead>
        <tr>    
	        <th>N&ordm;</th>
	        <th><input type="checkbox" id="todos" /></th>    
	        <th>FECHA</th>
	        <th>HORA</th>
            <th>OPERADOR</th>
	        <th>MONTO</th>
        </tr>   
	</thead>
	<tbody>
    <?php
    while($cels=$cns->resultado_sql()){
	?>
      <tr>    
        <td align="center"></td>
        <td align="center"><input type="checkbox" name="cods[]" id="cods" value="<?=$cels['id_sol']?>"  /></td>
        <td align="center"><?=$cels['fecha']?></td>    
        <td align="center"><?=$cels['hora']?></td>
        <td align="left"><?=$cels['nombres']?></td>
        <td align="right"><span>S/.</span><?=number_format($cels['monto'],2,".",",")?></td>
      </tr>
    <?php
	}
	$cn->limpiar_sql();
	$cn->cerrar_sql();
    ?>
    </tbody>
	<tfoot>
        <tr>    
	        <th>N&ordm;</th>
	        <th><input type="checkbox" id="todos" /></th>    
	        <th>FECHA</th>
	        <th>HORA</th>
            <th>OPERADOR</th>
	        <th>MONTO</th>                    
        </tr>  
	</tfoot>    
    </table>
