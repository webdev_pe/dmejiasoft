<?php
require("../../poo/clases/getConection.php");
$cn=new getConection();
$d=($_POST['d']!="")?date("Y-m-d",strtotime($_POST['d'])):"";
$h=($_POST['h']!="")?date("Y-m-d",strtotime($_POST['h'])):"";
if($d!="" && $h==""){
	$fsearch="fecha>='$d'";
}else if($d!="" && $h!=""){
	$fsearch="fecha between '$d' and '$h'";
}else if($d=="" && $h!=""){
	$fsearch="fecha<='$h'";
}else if($d=="" && $h==""){
	$fsearch="fecha=curdate()";
}

$sql="select id_caja, date_format(fecha,'%d-%m-%Y') as 'fecha', hora, inicial, disponible from si_caja where $fsearch order by fecha desc, hora desc";
$cn->ejecutar_sql(base64_encode($sql));
$cn->cantidad_sql();

#echo $sql;
?>
<script type="text/javascript">
$(document).ready(function(){
	$('#list_cli').dataTable({
      /*"bProcessing": true,
      "bServerSide": true,
      "sAjaxSource": "content/scripts/propiedades.php",*/
	  'bJQueryUI': true,
	  'sPaginationType': 'full_numbers',	  
	  'fnDrawCallback': function(oSettings){
		  if(oSettings.bSorted || oSettings.bFiltered){
			  for(var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++){
				  $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i+1);
			  }
		  }
	  },'aoColumnDefs': [
		  {'bSortable': false, 'aTargets': [0]},
		  {'bSortable': false, 'aTargets': [1]},
		  {'bSortable': false, 'aTargets': [5]},
		  {'bSortable': false, 'aTargets': [6]},
		  {'bSortable': false, 'aTargets': [7]}
	  ],"bAutoWidth" : false,	  
	  "bSortClasses": false, 
	  "aoColumns" : [
		  { sWidth : '18px'},
		  { sWidth : '18px'},
		  { sWidth : '100px'},	  
		  { sWidth : '100px'},
		  { sWidth : 'auto'},  	  	  
		  { sWidth : '20px'},
		  { sWidth : '20px'},		  
		  { sWidth : '20px'}
	  ],'aaSorting': [[0, 'asc']]
	}).columnFilter({aoColumns: [
		  null,
		  null,
		  { type: "text" },
		  { type: "text" },
		  { type: "text" },
		  null,					
		  null,
		  null
	  ]
	});
	
	$("#todos").live("click", function(){
		$checkboxes=$("#list_cli tbody td").find(":checkbox");
		if($(this).is(":checked")){ $checkboxes.attr("checked",1); }
		else{ $checkboxes.removeAttr("checked");}
	});
			
	$("#rept").click(function(){
	  var s=$("#s").val();
	  var new_tab=window.open("","_blank");		
	  new_tab.location="sicpri/00_caja/reporte.php?s="+s;
	});

	$.datepicker.setDefaults($.datepicker.regional["es"]);
	$("#fec_dsd").datepicker({
		dateFormat: "dd-mm-yy",
		numberOfMonths: 3,		
		showButtonPanel: true/*, showOn: "button", buttonImage: "img/calendar.gif", buttonImageOnly: true */
	});
	$("#fec_hst").datepicker({dateFormat: "dd-mm-yy", numberOfMonths: 3,	showButtonPanel: true});	

	$("#bsqf").click(function(){
	  var d=$("#fec_dsd").val();
	  var h=$("#fec_hst").val();
		$.post("sicpri/00_caja/06_historial.php",{d:d,h:h},function(data){
			$("#contenido_sicpri").html(data);
			$("#sicpri_tit").empty().text("REPORTE MIXTO");
		});
	});			
});
	function editar(i){
		$.post("sicpri/00_caja/02_modificar.php",{i:i},function(data){
			$("#contenido_sicpri").html(data);
			$("#sicpri_tit").empty().text("MODIFICAR CAJA");
		});
	}
	function verificar(i){
		$.post("sicpri/00_caja/02_modificar.php",{i:i,v:"v"},function(data){
			$("#contenido_sicpri").html(data);
			$("#sicpri_tit").empty().text("VERIFICAR CAJA");
		});
	}	
	function eliminar(i){
		var selecteds = new Array();
		var chekeados = $("#list_cli tbody input[@name='cods[]']:checked").size();

		if(chekeados>1){
			$("#list_cli tbody input[@name='cods[]']:checked").each(function(){
				selecteds.push($(this).val());
			});
			if(confirm("Esta seguro de eliminar estos "+chekeados+" registros?")){
				$.post("sicpri/00_caja/dao.php", {opt:"dm",s:selecteds}, function(data){
					if(data==1){
						$.post("sicpri/00_caja/06_historial.php",function(data){
							$("#contenido_sicpri").html(data);
							$("#sicpri_tit").empty().text("HISTORIAL DE CAJA");
						});
					}
				});
			}			
		}else{
			if(confirm("Esta seguro de eliminar este registro?")){
				$.post("sicpri/00_caja/dao.php", {opt:"d",i:i}, function(data){
					if(data==1){
						$.post("sicpri/00_caja/06_historial.php",function(data){
							$("#contenido_sicpri").html(data);
							$("#sicpri_tit").empty().text("HISTORIAL DE CAJA");
						});
					}
				});
			}
		}
	}
</script>
	<input type="hidden" id="s" value="<?=base64_encode(utf8_encode($sql))?>" />
<table width="100%" border="0">
  <tr>
    <td align="left" valign="middle"><!--button id="rept" class="btn"><span class="ui-icon ui-icon-clipboard"></span>Generar previo de resultados</button--></td>
    <td align="right" valign="middle">Fecha desde: <input type="text" id="fec_dsd" style="width:150px;" /> | Fecha hasta: <input type="text" id="fec_hst" style="width:150px;"  /> <button id="bsqf" class="btn"><span class="ui-icon ui-icon-search"></span>Filtrar por fechas</button></td>
  </tr>
</table>    
	<table id="list_cli" class="list_tbl" width="100%">
	<thead>
        <tr>    
	        <th>N&ordm;</th>
	        <th><input type="checkbox" id="todos" /></th>    
	        <th>FECHA</th>
	        <th>HORA</th>
	        <th>MONTO INCIAL</th>               
	        <th title="Eliminar registro"><div class="ui-icon ui-icon-trash custom_icon"></div></th>
	        <th title="Editar registro"><div class="ui-icon ui-icon-pencil custom_icon"></div></th>
            <th title="Visualizar registro"><div class="ui-icon ui-icon-zoomin custom_icon"></div></th>
        </tr>   
	</thead>
	<tbody>
    <?php
    while($cell=$cn->resultado_sql()){
	?>
      <tr>    
        <td align="center"></td>
        <td align="center"><input type="checkbox" name="cods[]" id="cods" value="<?=$cell['id_caja']?>"  /></td>
        <td align="center"><?=$cell['fecha']?></td>    
        <td align="center"><?=$cell['hora']?></td>
        <td align="right"><span>S/.</span><?=number_format($cell['inicial'],2,".",",")?></td>        
        <td align="center">
        <button id="eliminar" class="btn" onclick="eliminar('<?=$cell['id_caja']?>')" title="Eliminar registro"><span class="ui-icon ui-icon-trash"></span></button>
        </td>
        <td align="center"><button id="editar" class="btn" onclick="editar('<?=$cell['id_caja']?>')" title="Modificar registro"><span class="ui-icon ui-icon-pencil"></span></button></td>
        <td align="center"><button id="visualizar" class="btn" onclick="verificar('<?=$cell['id_caja']?>')" title="Visualizar registro"><span class="ui-icon ui-icon-zoomin"></span></button></td>
      </tr>
    <?php
	}
	$cn->limpiar_sql();
	$cn->cerrar_sql();
    ?>
    </tbody>
	<tfoot>
        <tr>    
	        <th>N&ordm;</th>
	        <th><input type="checkbox" id="todos" /></th>    
	        <th>FECHA</th>
	        <th>HORA</th>
	        <th>MONTO INCIAL</th>                    
	        <th title="Eliminar registro"><div class="ui-icon ui-icon-trash custom_icon"></div></th>
	        <th title="Editar registro"><div class="ui-icon ui-icon-pencil custom_icon"></div></th>
            <th title="Visualizar registro"><div class="ui-icon ui-icon-zoomin custom_icon"></div></th>
        </tr>  
	</tfoot>    
    </table>