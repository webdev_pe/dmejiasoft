<?php
require("../../poo/clases/getConection.php");
$id  = $_POST['i'];
$cod = $_POST['c'];
$ver = $_POST['v'];
$cn=new getConection();
if(isset($id)){
	$sql="select id_cli, cod_cli, nom_cli, ape_cli, tip_cli, doc_cli, dir_cli, nro_cli, tel_cli, cel_cli, eml_cli, mnt_cli, id_zon, id_gal from si_clientes where id_cli=".$id;		
	$cn->ejecutar_sql(base64_encode($sql));
	$cel=$cn->resultado_sql();
}else if(isset($cod)){
	$sql="select id_cli, cod_cli, nom_cli, ape_cli, tip_cli, doc_cli, dir_cli, nro_cli, tel_cli, cel_cli, eml_cli, mnt_cli, id_zon, id_gal from si_clientes where cod_cli='".$cod."'";	
	$cn->ejecutar_sql(base64_encode($sql));
	$cel=$cn->resultado_sql();
}
?>
<script type="text/javascript">
$(document).ready(function(){
	$("#cod_cli").autocomplete({
		source: "sicpri/03_clientes/_buscar_codigo.php",
		minLength: 2/*, select: function(event,ui){ $("#id_con").val(ui.item.id); }*/
	});
		
	$("#se_dni").selectmenu();
	$("#cbo_gal").selectmenu();	
	
	$("#checkbox_persona").buttonset();
	
	<?php if(!isset($id) && !isset($cod)){?>
	$("#cbo_zon").selectmenu();
	$("#cbo_gal").selectmenu();
	<?php }else{?>
	$("#cbo_zon").load("sicpri/funciones/getZonas.php",{i:<?=$cel['id_zon']?>},function(data){
		$(this).selectmenu();
	});	
	$("#cbo_gal").load("sicpri/funciones/getGalerias.php",{i:<?=$cel['id_zon']?>,g:<?=$cel['id_gal']?>},function(data){
		$(this).selectmenu();
	});		
	<?php }?>
	
	$("#cbo_zon").change(function(data){
		$("#cbo_zon option:selected").each(function(){
			var i=$(this).val();
			$("#cbo_gal").load("sicpri/funciones/getGalerias.php",{i:i},function(data){
				$(this).selectmenu();
			});	
		});
	});	
	
	$("#edit").click(function(){
		var i=$("#id").val();
		
		var per=$("input[name='persona']:radio:checked").val();
		var nom=$("#tx_nom").val();
		var ape=$("#tx_ape").val();
		var dni=$("#se_dni").val();
		var nro=$("#tx_dni").val();
		var dir=$("#tx_dir").val();
		var tel=$("#tx_tel").val();
		var cel=$("#tx_cel").val();
		var eml=$("#tx_eml").val();	
		var mnt=$("#tx_mnt").val();						
		var zon=$("#cbo_zon").val();
		var gal=$("#cbo_gal").val();

		//if(nom.length<1){ alert("Complete el campo Nombres."); $("#tx_nom").focus(); }
		//else if(zon=="nn"){ alert("Asigne una zona a la galeria."); $("#zona").focus(); }
		//else{
			  $.post("sicpri/03_clientes/dao.php",{opt:"u",per:per,nom:nom,ape:ape,dni:dni,nro:nro,dir:dir,tel:tel,cel:cel,eml:eml,mnt:mnt,zon:zon,gal:gal,i:i},function(data){
				  if(data==1){ 
					  alert("Cliente editado correctamente.");
					  $.post("sicpri/03_clientes/02_modificar.php",function(data){
						  $("#contenido_sicpri").html(data);
						  $("#sicpri_tit").empty().text("MODIFICAR CLIENTE");
					  });	
				  }else{
					  alert("Vuelva a intentarlo por favor.");
				  }
			  });
		//}
	});
	$("#find").click(function(){
		var c=$("#cod_cli").val();
		if(c!=""){
			$.post("sicpri/03_clientes/02_modificar.php",{c:c},function(data){
				$("#contenido_sicpri").html(data);
				$("#sicpri_tit").empty().text("MODIFICAR CLIENTE");
			});
		}else{ alert("Ingrese el codigo de zona a modificar."); $("#cod_zon").focus(); }
	});		
	$("#hist").click(function(){
		$.post("sicpri/03_clientes/06_historial.php",function(data){
			$("#contenido_sicpri").html(data);
			$("#sicpri_tit").empty().text("HISTORIAL DE CLIENTES");
		});
	});	
});
</script>
<input type="hidden" id="id" value="<?=$cel['id_cli']?>" />
<div id="sql"></div>
<table width="100%" border="0" id="tbl_prp">
  <?php if(!isset($id) && !isset($cod)){?>
  <tr>
    <td colspan="2">   
        <table width="100%" border="0" id="tbl_find">
          <tr>
            <td width="25%"><strong>Buscar cliente:</strong></td>
            <td width="25%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
          </tr>
          <tr>
            <td align="left" valign="top">
			C&oacute;digo de Cliente:<br />
			<input type="text" id="cod_cli" /><br />
            <button id="find" class="btn"><span class="ui-icon ui-icon-search"></span>Buscar cliente</button>
            </td>
            <td align="left" valign="top">
            </td>
            <td align="left" valign="top">
            </td>
            <td align="left" valign="top">
            </td>
          </tr>
        </table>
	</td>
  </tr> 
  <?php }?>  
  <tr>
    <td colspan="2">
        <table width="100%" border="0">
          <tr>
            <td width="25%"><strong>Datos Personales</strong></td>
            <td width="25%">&nbsp;</td>
            <td width="25%"><strong>Zona/Galer&iacute;a</strong></td>
            <td width="25%">&nbsp;</td>
          </tr>
          <tr>
            <td align="left" valign="top">
              Tipo de Persona<br/>
              <div id="checkbox_persona">
	            <input type="radio" name="persona" id="pr_nat" value="1" <?php if($cel['tip_cli']=="1"){?>checked="checked"<?php }?> /><label for="pr_nat"><span class="ui-icon ui-icon-person"></span>NATURAL</label>
    	        <input type="radio" name="persona" id="pr_jur" value="2" <?php if($cel['tip_cli']=="2"){?>checked="checked"<?php }?> /><label for="pr_jur"><span class="ui-icon ui-icon-person"></span>JUR&Iacute;DICA</label>
    	        <!--input type="radio" name="persona" id="pr_otr" value="3" /><label for="pr_otr"><span class="ui-icon ui-icon-person"></span>OTROS</label-->
              </div>            
              Nombres<br/><input type="text" id="tx_nom" value="<?=$cel['nom_cli']?>" /><br/>
              Apellidos<br/><input type="text" id="tx_ape" value="<?=$cel['ape_cli']?>" /><br/>
              Tipo de Documento<br/>
              <select id="se_dni">
				<?php if($cel['doc_cli']=="1"){?>
	              <option value="1" selected="selected">DNI</option>
	              <option value="2">Carnet de Extranjer&iacute;a</option>
	              <option value="3">Otros</option>
                <?php }?> 
				<?php if($cel['doc_cli']=="2"){?>
	              <option value="1">DNI</option>
	              <option value="2" selected="selected">Carnet de Extranjer&iacute;a</option>
	              <option value="3">Otros</option>
                <?php }?> 
				<?php if($cel['doc_cli']=="3"){?>
	              <option value="1">DNI</option>
	              <option value="2">Carnet de Extranjer&iacute;a</option>
	              <option value="3" selected="selected">Otros</option>
                <?php }?> 
              </select><br/>
              Nro. de Documento<br/><input type="text" id="tx_dni" value="<?=$cel['nro_cli']?>">
            </td>
            <td align="left" valign="top">Dirección<br/><input type="text" id="tx_dir" value="<?=$cel['dir_cli']?>" /><br />
              Tel&eacute;fono Fijo<br/><input type="text" id="tx_tel" value="<?=$cel['tel_cli']?>" /><br />
			  Celular<br/><input type="text" id="tx_cel" value="<?=$cel['cel_cli']?>" /><br />
              Email<br/><input type="text" id="tx_eml" value="<?=$cel['eml_cli']?>" /><br />
			  Monto L&iacute;mite<br /><input type="text" id="tx_mnt" value="<?=$cel['mnt_cli']?>" />
            </td>
            <td align="left" valign="top">
              Seleccione Zona<br/><select id="cbo_zon"><option>[-Seleccione-]</option></select>
              <br />
              Seleccione Galer&iacute;a<br/>
              <select name="cbo_gal" id="cbo_gal">
                <option>[-Sin galeria-]</option>
            </select></td>
            <td align="left" valign="top">&nbsp;</td>
          </tr>
        </table>
    </td>
  </tr>
  <tr>
    <td>    
    </td>
  </tr>  
  <tr>
    <td>
	<button id="edit" class="btn"><span class="ui-icon ui-icon-disk"></span>Editar cliente</button>
	<button id="hist" class="btn"><span class="ui-icon ui-icon-folder-open"></span>Historial de clientes</button>
    </td>
  </tr>
</table>