<?php session_start(); ?>
<?php include("../rpt_head.php"); ?>
	<div id="cabecera">
     <div id="logo"><img src="../../images/login/logo.png" width="106" height="106" /></div>
     <div id="descripcion">
     <strong>REPORTE DE CLIENTES</strong><br /><hr />
	  <strong>Fecha</strong>: <?=date("d-m-Y")?> - <strong>Hora</strong> <?=date("H:m:s")?><br />
     </div>
  </div>
  <div id="cuerpo">
  <table id="list_usu" class="list_tbl" width="100%">
	<thead>
        <tr>    
	        <th>N&ordm;</th> 
	        <th>CODIGO</th>
	        <th>NOMBRE</th>
	        <th>ZONA</th>
            <th>GALER&Iacute;A</th>
            <th>MONTO L&Iacute;MITE</th>   
        </tr>   
	</thead>
	<tbody>
    <?php 
	if(isset($_GET['s'])){
		while($cell=$cn->resultado_sql()){ 
	?>
      <tr>    
        <td align="center"><?=$i++?></td>        
        <td align="center"><?=$cell['cod_cli']?></td>    
        <td align="center"><?=($cell['nom_cli']." ".$cell['ape_cli'])?></td>
        <td align="center"><?=$cell['nom_zon']?></td>        
        <td align="center"><?=$cell['nom_gal']?></td>
        <td align="right"><span>S/.</span><?=number_format($cell['mnt_cli'],2,".",",")?></td>    
      </tr>
    <?php 
		} 
		$cn->limpiar_sql(); $cn->cerrar_sql();
	}
	?>
    </tbody>  
    </table>
    </div>
<?php include("../rpt_footer.php"); ?>