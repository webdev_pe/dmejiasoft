<?php session_start(); 
header("Content-type: text/plain");
header("Content-Disposition: attachment; filename=sicpri_reporte_propietarios.csv");

date_default_timezone_set("America/Lima");
$fecha = date("d-m-Y");
$hora  = date("H:i:s");		

require("../../poo/clases/getConection.php");
$cn=new getConection();
$sql=utf8_decode(base64_decode($_GET['s']));
$cn->ejecutar_sql(base64_encode($sql));

echo 'Nro;"CODIGO";"NOMBRES Y APELLIDOS";"DOCUMENTO";"Nro";"EMAIL";"EMPRESA"';
echo "\n";
$i=1; 
while($cell=$cn->resultado_sql()){ 
	echo $i++.';"'.$cell['cod_pro'].'";"'.$cell['nombres'].'";"'.$cell['tipo_doc'].'";"'.$cell['num_dni'].'";"'.$cell['eml_pro'].'";"'.$cell['nom_emp_pro'].'"';
	echo "\n";
} 
$cn->limpiar_sql(); 
$cn->cerrar_sql(); 
?>