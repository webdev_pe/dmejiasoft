<script type="text/javascript">
$(document).ready(function(){
	$("#cbo_gal").selectmenu();	
	
	$("#checkbox_persona").buttonset();
		
	$("#cbo_zon").load("sicpri/funciones/getZonas.php", function(data){
		$(this).selectmenu();
	});	

	$("#cbo_zon").change(function(data){
		$("#cbo_zon option:selected").each(function(){
			var i=$(this).val();
			$("#cbo_gal").load("sicpri/funciones/getGalerias.php",{i:i},function(data){
				$(this).selectmenu();
			});	
		});
	});	
	
	$("#cod_cli").autocomplete({
		source: "sicpri/03_clientes/_buscar_codigo.php",
		minLength: 2/*, select: function(event,ui){ $("#id_con").val(ui.item.id); }*/
	});	
	$("#btx_nom").autocomplete({
		source: "sicpri/03_clientes/_buscar_nombres.php",
		minLength: 2/*, select: function(event,ui){ $("#id_con").val(ui.item.id); }*/
	});		
			
	$("#genr").click(function(){
		var cod=$("#cod_cli").val();
		var nom=$("#btx_nom").val();
		var per=$("input[name='persona']:radio:checked").val();		
		var zon=$("#cbo_zon").val();
		var gal=$("#cbo_gal").val();		
		$.post("sicpri/03_clientes/06_historial.php",{cod:cod,nom:nom,per:per,zon:zon,gal:gal,r:"r"},function(data){
			$("#contenido_sicpri").html(data);
			$("#sicpri_tit").empty().text("RESULTADO DE BUSQUEDA");
		});		
	});	
	
	$("#hist").click(function(){
		$.post("sicpri/03_clientes/06_historial.php",function(data){
			$("#contenido_sicpri").html(data);
			$("#sicpri_tit").empty().text("HISTORIAL DE CLIENTES");
		});
	});	
});
</script>
<table width="100%" border="0" id="tbl_prp">
  <tr>
    <td colspan="2">
        <table width="100%" border="0">
          <tr>
            <td width="25%"><strong>Datos de b&uacute;squeda</strong></td>
            <td width="25%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
          </tr>
          <tr>
            <td align="left" valign="top">
              Codigo<br /><input type="text" id="cod_cli" /><br />
              Nombres<br/><input type="text" id="btx_nom" /><br />
              Tipo de Persona<br/>
<div id="checkbox_persona">
<input type="radio" name="persona" id="pr_all" value="0" checked="checked" /><label for="pr_all"><span class="ui-icon ui-icon-person"></span>TODOS</label>
<input type="radio" name="persona" id="pr_nat" value="1" /><label for="pr_nat"><span class="ui-icon ui-icon-person"></span>NATURAL</label>
<input type="radio" name="persona" id="pr_jur" value="2" /><label for="pr_jur"><span class="ui-icon ui-icon-person"></span>JUR&Iacute;DICA</label>
</div>
				Seleccione Zona<br/><select Id="cbo_zon"><option value="nn">[-Seleccione-]</option></select>
              <br />
              Seleccione Galer&iacute;a<br/>
              <select name="cbo_gal" id="cbo_gal">
                <option value="nn">[-Sin galeria-]</option>
            </select>                
			</td>
            <td align="left" valign="top">&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
          </tr>
        </table>
    </td>
  </tr> 
  <tr>
    <td>
	<button id="genr" class="btn"><span class="ui-icon ui-icon-search"></span>Generar resultados para reporte</button>
	<button id="hist" class="btn"><span class="ui-icon ui-icon-folder-open"></span>Historial de clientes</button>
    </td>
  </tr>
</table>