<?php
require("../../poo/clases/getConection.php");
$cn=new getConection();

$q = strtolower($_GET["term"]);
if (!$q) return;

$items = array();
$sql="select ape_pro from si_propietarios";
$cn->ejecutar_sql(base64_encode($sql));
$row=$cn->cantidad_sql();

while($cel=$cn->resultado_sql()){
	$items[utf8_decode($cel['ape_pro'])]=utf8_decode($cel['ape_pro']);
	//array_push($items,array($cel['id_dep']=>$cel['nom_dep']));
}

/*
$items = array(
"Great Bittern"=>"Botaurus stellaris",
"Little Grebe"=>"Tachybaptus ruficollis",
"Black-necked Grebe"=>"Podiceps nigricollis",
);
*/

function array_to_json( $array ){
    if(!is_array($array)){
        return false;
    }

    $associative = count( array_diff( array_keys($array), array_keys( array_keys( $array )) ));
    if($associative){
        $construct = array();
        foreach( $array as $key => $value ){
            if(is_numeric($key)){
                $key = "key_$key";
            }
            $key = "\"".addslashes($key)."\"";

            if(is_array($value)){
                $value = array_to_json( $value );
            }else if(!is_numeric($value) || is_string($value)){
                $value = "\"".addslashes($value)."\"";
            }
            $construct[] = "$key: $value";
        }
        $result = "{ " . implode( ", ", $construct ) . " }";
    }else{
        $construct = array();
        foreach($array as $value){
            if( is_array($value)){
                $value = array_to_json($value);
            } else if(!is_numeric($value) || is_string($value)){
                $value = "'".addslashes($value)."'";
            }
            $construct[] = $value;
        }
        $result = "[ " . implode( ", ", $construct ) . " ]";
    }
    return $result;
}

$result = array();
foreach ($items as $key=>$value) {
	if (strpos(strtolower($key), $q) !== false) {
		array_push($result, array("id"=>$value, "label"=>$key, "value" => strip_tags($key)));
	}
	if (count($result) > 11)
		break;
}
echo array_to_json($result);
?>