<?php
require("../../poo/clases/getConection.php");
$cn=new getConection();
$cod=$_POST['cod']; $nom=$_POST['nom']; $per=$_POST['per']; $zon=$_POST['zon']; $gal=$_POST['gal'];

if($cod!=""){	$add_search=" and cod_cli like '%$cod%' "; 
}else{
	$add_search =($nom!="")?" nom_cli like '%$nom%' and ":"";
	$add_search .=($per!="0" && $per!="")?" tip_cli='$per' and ":"";
	$add_search .=($zon!="nn" && $zon!="")?" c.id_zon=$zon and ":"";	
	$add_search .=($gal!="nn" && $gal!="")?" c.id_gal=$gal and ":"";		
	$add_search=($add_search!="")?" and ".$add_search:"";	
	$lenght=strlen($add_search);
	$add_search=substr($add_search,0,($lenght-4));
}

$sql="select id_cli, cod_cli, nom_cli, ape_cli, mnt_cli, z.nom_zon, g.nom_gal
from si_clientes c, si_zonas z, si_galerias g
where c.id_zon=z.id_zon and c.id_gal=g.id_gal ".$add_search." order by id_cli desc";
$cn->ejecutar_sql(base64_encode($sql));
$cn->cantidad_sql();

#echo $sql;
?>
<script type="text/javascript">
$(document).ready(function(){
	$('#list_cli').dataTable({
      /*"bProcessing": true,
      "bServerSide": true,
      "sAjaxSource": "content/scripts/propiedades.php",*/
	  'bJQueryUI': true,
	  'sPaginationType': 'full_numbers',	  
	  'fnDrawCallback': function(oSettings){
		  if(oSettings.bSorted || oSettings.bFiltered){
			  for(var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++){
				  $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i+1);
			  }
		  }
	  },'aoColumnDefs': [
		  {'bSortable': false, 'aTargets': [0]},
		  {'bSortable': false, 'aTargets': [1]},
		  {'bSortable': false, 'aTargets': [7]},
		  {'bSortable': false, 'aTargets': [8]},
		  {'bSortable': false, 'aTargets': [9]}
	  ],"bAutoWidth" : false,	  
	  "bSortClasses": false, 
	  "aoColumns" : [
		  { sWidth : '18px'},
		  { sWidth : '18px'},
		  { sWidth : '100px'},	  
		  { sWidth : 'auto'},
		  { sWidth : 'auto'},
		  { sWidth : 'auto'},
		  { sWidth : 'auto'},	   	  	  
		  { sWidth : '20px'},
		  { sWidth : '20px'},		  
		  { sWidth : '20px'}
	  ],'aaSorting': [[0, 'asc']]
	}).columnFilter({aoColumns: [
		  null,
		  null,
		  { type: "text" },
		  { type: "text" },
		  { type: "text" },
		  { type: "text" },
		  { type: "text" },
		  null,					
		  null,
		  null
	  ]
	});
	
	$("#todos").live("click", function(){
		$checkboxes=$("#list_cli tbody td").find(":checkbox");
		if($(this).is(":checked")){ $checkboxes.attr("checked",1); }
		else{ $checkboxes.removeAttr("checked");}
	});
			
	$("#rept").click(function(){
	  var s=$("#s").val();
	  var new_tab=window.open("","_blank");		
	  new_tab.location="sicpri/03_clientes/reporte.php?s="+s;
	});
});
	function editar(i){
		$.post("sicpri/03_clientes/02_modificar.php",{i:i},function(data){
			$("#contenido_sicpri").html(data);
			$("#sicpri_tit").empty().text("MODIFICAR CLIENTE");
		});
	}
	function verificar(i){
		$.post("sicpri/03_clientes/02_modificar.php",{i:i,v:"v"},function(data){
			$("#contenido_sicpri").html(data);
			$("#sicpri_tit").empty().text("VERIFICAR CLIENTE");
		});
	}	
	function eliminar(i){
		var selecteds = new Array();
		var chekeados = $("#list_cli tbody input[@name='cods[]']:checked").size();

		if(chekeados>1){
			$("#list_cli tbody input[@name='cods[]']:checked").each(function(){
				selecteds.push($(this).val());
			});
			if(confirm("Esta seguro de eliminar estos "+chekeados+" registros?")){
				$.post("sicpri/03_clientes/dao.php", {opt:"dm",s:selecteds}, function(data){
					if(data==1){
						$.post("sicpri/03_clientes/06_historial.php",function(data){
							$("#contenido_sicpri").html(data);
							$("#sicpri_tit").empty().text("HISTORIAL DE CLIENTES");
						});
					}
				});
			}			
		}else{
			if(confirm("Esta seguro de eliminar este registro?")){
				$.post("sicpri/03_clientes/dao.php", {opt:"d",i:i}, function(data){
					if(data==1){
						$.post("sicpri/03_clientes/06_historial.php",function(data){
							$("#contenido_sicpri").html(data);
							$("#sicpri_tit").empty().text("HISTORIAL DE CLIENTES");
						});
					}
				});
			}
		}
	}
</script>
	<input type="hidden" id="s" value="<?=base64_encode(utf8_encode($sql))?>" />
    <?php if(isset($_POST['r'])){ ?>
	<button id="rept" class="btn"><span class="ui-icon ui-icon-clipboard"></span>Generar previo de resultados</button>
	<?php }?>
	<table id="list_cli" class="list_tbl" width="100%">
	<thead>
        <tr>    
	        <th>N&ordm;</th>
	        <th><input type="checkbox" id="todos" /></th>    
	        <th>CODIGO</th>
	        <th>NOMBRE</th>
	        <th>ZONA</th>
            <th>GALER&Iacute;A</th>
            <th>MONTO L&Iacute;MITE</th>                     
	        <th title="Eliminar registro"><div class="ui-icon ui-icon-trash custom_icon"></div></th>
	        <th title="Editar registro"><div class="ui-icon ui-icon-pencil custom_icon"></div></th>
            <th title="Visualizar registro"><div class="ui-icon ui-icon-zoomin custom_icon"></div></th>
        </tr>   
	</thead>
	<tbody>
    <?php
    while($cell=$cn->resultado_sql()){
	?>
      <tr>    
        <td align="center"></td>
        <td align="center"><input type="checkbox" name="cods[]" id="cods" value="<?=$cell['id_cli']?>"  /></td>
        <td align="center"><?=$cell['cod_cli']?></td>    
        <td align="center"><?=($cell['nom_cli']." ".$cell['ape_cli'])?></td>
        <td align="center"><?=$cell['nom_zon']?></td>        
        <td align="center"><?=$cell['nom_gal']?></td>
        <td align="right"><span>S/.</span><?=number_format($cell['mnt_cli'],2,".",",")?></td> 
        <td align="center">
        <button id="eliminar" class="btn" onclick="eliminar('<?=$cell['id_cli']?>')" title="Eliminar registro"><span class="ui-icon ui-icon-trash"></span></button>
        </td>
        <td align="center"><button id="editar" class="btn" onclick="editar('<?=$cell['id_cli']?>')" title="Modificar registro"><span class="ui-icon ui-icon-pencil"></span></button></td>
        <td align="center"><button id="visualizar" class="btn" onclick="verificar('<?=$cell['id_cli']?>')" title="Visualizar registro"><span class="ui-icon ui-icon-zoomin"></span></button></td>
      </tr>
    <?php
	}
	$cn->limpiar_sql();
	$cn->cerrar_sql();
    ?>
    </tbody>
	<tfoot>
        <tr>    
	        <th>N&ordm;</th>
	        <th><input type="checkbox" id="todos" /></th>    
	        <th>CODIGO</th>
	        <th>NOMBRE</th>
	        <th>ZONA</th>  
            <th>GALER&Iacute;A</th> 
            <th>MONTO L&Iacute;MITE</th>                     
	        <th title="Eliminar registro"><div class="ui-icon ui-icon-trash custom_icon"></div></th>
	        <th title="Editar registro"><div class="ui-icon ui-icon-pencil custom_icon"></div></th>
            <th title="Visualizar registro"><div class="ui-icon ui-icon-zoomin custom_icon"></div></th>
        </tr>  
	</tfoot>    
    </table>