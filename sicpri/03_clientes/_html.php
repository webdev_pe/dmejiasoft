<?php session_start(); 
header("Content-type: text/html");
header("Content-Disposition: attachment; filename=sicpri_reporte_propietarios.html");

date_default_timezone_set("America/Lima");
$fecha = date("d-m-Y");
$hora  = date("H:i:s");		

require("../../poo/clases/getConection.php");
$cn=new getConection();
$sql=utf8_decode(base64_decode($_GET['s']));
$cn->ejecutar_sql(base64_encode($sql));
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>sicpri_reporte_usuarios</title>
<style type="text/css">
table, table th, table td{ margin:0 auto; font-size:11px; font-family:Verdana; border:1px solid #ededed; border-collapse:collapse;}
</style>
</head>
<body>
    <!-- ############################################## inicio ############################################## -->  
	<table width="900" align="center">
	<thead>
	   <tr bgcolor="#b9bdc4">    
	        <th style="width: 2%; text-align: center;">N&ordm;</th> 
	        <th style="width: 10%; text-align: center;">CODIGO</th>
	        <th style="width: 30%; text-align: center;">NOMBRES Y APELLIDOS</th>
	        <th style="width: 10%; text-align: center;">DOCUMENTO</th>
	        <th style="width: 5%; text-align: center;">N&ordm;</th>
            <th style="width: 20%; text-align: center;">EMAIL</th>
   	        <th style="width: 10%; text-align: center;">EMPRESA</th>
        </tr>     
	</thead>
	<tbody>
    <?php $i=1; while($cell=$cn->resultado_sql()){ ?>
      <tr bgcolor="<?=($i%2==0)?"#ffffff":"#e2e4ff"?>">    
        <td align="center"><?=$i++?></td>
        <td align="center"><?=$cell['cod_pro']?></td>    
        <td align="center"><?=$cell['nombres']?></td>
        <td align="center"><?=$cell['tipo_doc']?></td>
        <td align="center"><?=$cell['num_dni']?></td>
        <td align="center"><?=$cell['eml_pro']?></td>        
        <td align="center"><?=($cell['nom_emp_pro']!="")?strtoupper($cell['nom_emp_pro']):"----------"?></td>     
      </tr>
    <?php } $cn->limpiar_sql(); $cn->cerrar_sql(); ?>
    </tbody>
    </table>
    <!-- ############################################## inicio ############################################## -->    
</body>
</html>