<?php
require("../../poo/clases/getPeruUbicacion.php");
$cn=new getPeruUbicacion();

$opt=$_REQUEST['opt'];
$dep=$_REQUEST['dep'];
$prv=$_REQUEST['prv'];
$dst=$_REQUEST['dst'];

switch($opt){
	case "dn": $options=$cn->traer_departamentos(); break; /*departamentos normales*/
	case "pd": $options=$cn->traer_provincias($dep); break; /*provincias del departamento*/
	case "dp": $options=$cn->traer_distritos($dep,$prv); break; /*distritos de la provincia*/
	
	case "dnc": $options=$cn->traer_departamento_cod($dep); break; /*departamento por codigo*/
	case "pdc": $options=$cn->traer_provincia_cod($dep,$prv); break; /*provincias por codigo*/
	case "dpc": $options=$cn->traer_distrito_cod($dep,$prv,$dst); break; /*distritos por codigo*/		
}

echo $options;
?>