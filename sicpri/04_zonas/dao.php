<?php
require("../../poo/clases/daoZonas.php");
$cn=new daoZonas();

$opt=$_POST['opt'];

$idu=$_POST['i'];
$cod=$_POST['c'];
$arr=$_POST['s'];

$nom=utf8_encode($_POST['nom']);
$des=utf8_decode($_POST['des']);

switch($opt){
	case "i": $get=$cn->guardar_zonas($nom, $des); break; //insertar
	case "u": $get=$cn->modificar_zonas($idu, $nom, $des); break; //actualizar
	case "d": $get=$cn->eliminar_zona($idu,$cod); break; //eliminacion simple
	case "dm": $get=$cn->eliminar_zonas($arr); break; //eliminacion multiple
}

echo $get;
?>