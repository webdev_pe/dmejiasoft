<script type="text/javascript">
$(document).ready(function(){
	$("#save").click(function(){
		var nom=$("#tx_nom").val();
		var des=$("#tx_des").val();
	
		if(nom.length<1){ alert("Complete el campo Nombres."); $("#tx_nom").focus(); }
		else{
				$.post("sicpri/04_zonas/dao.php",{opt:"i", nom:nom, des:des},function(data){
					if(data==1){ 
						alert("Zona registrada correctamente.");
						$.post("sicpri/04_zonas/06_historial.php",function(data){
							$("#contenido_sicpri").html(data);
							$("#sicpri_tit").empty().text("HISTORIAL DE ZONAS");
						});	
					}else{
						alert("Vuelva a intentarlo por favor.");
					}
				});
		}
	});
	$("#hist").click(function(){
		$.post("sicpri/04_zonas/06_historial.php",function(data){
			$("#contenido_sicpri").html(data);
			$("#sicpri_tit").empty().text("HISTORIAL DE ZONAS");
		});
	});
});
</script>
<table width="100%" border="0" id="tbl_prp">
  <tr>
    <td colspan="2">
        <table width="100%" border="0">
          <tr>
            <td width="25%"><strong>Datos de la zona</strong></td>
            <td width="25%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
          </tr>
          <tr>
            <td align="left" valign="top">
            Nombre<br/><input type="text" id="tx_nom" /><br/>
            Descripci&oacute;n<br/><textarea id="tx_des"></textarea><br/>
            </td>
            <td align="left" valign="top">&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
          </tr>
        </table>
    </td>
  </tr>
  <tr>
    <td>    
    </td>
  </tr>  
  <tr>
    <td>
	<button id="save" class="btn"><span class="ui-icon ui-icon-disk"></span>Guardar nueva zona</button>
	<button id="hist" class="btn"><span class="ui-icon ui-icon-folder-open"></span>Historial de zonas</button>
    </td>
  </tr>
</table>