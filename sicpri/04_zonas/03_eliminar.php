<script type="text/javascript">
$(document).ready(function(){
	$("#cod_zon").autocomplete({
		source: "sicpri/04_zonas/_buscar_codigo.php",
		minLength: 2/*, select: function(event,ui){ $("#id_con").val(ui.item.id); }*/
	});	
	$("#dele").click(function(){
		var c=$("#cod_zon").val();
		if(c!=""){
			if(confirm("Esta seguro de eliminar al usuario: "+c+" ?")){
				$.post("sicpri/04_zonas/dao.php", {opt:"d",c:c}, function(data){
					if(data==1){
						$.post("sicpri/04_zonas/06_historial.php",function(data){
							$("#contenido_sicpri").html(data);
							$("#sicpri_tit").empty().text("HISTORIAL DE ZONAS");
						});
					}
				});				
			}
		}else{ alert("Ingrese el codigo de zona a eliminar."); $("#cod_zon").focus(); }
	});
	
	$("#hist").click(function(){
		$.post("sicpri/04_zonas/06_historial.php",function(data){
			$("#contenido_sicpri").html(data);
			$("#sicpri_tit").empty().text("HISTORIAL DE ZONAS");
		});
	});		
});
</script>
<table width="100%" border="0" id="tbl_prp">
  <tr>
    <td colspan="2">   
        <table width="100%" border="0" id="tbl_find">
          <tr>
            <td width="25%"><strong>Buscar usuario a eliminar:</strong></td>
            <td width="25%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
          </tr>
          <tr>
            <td align="left" valign="top">
			C&oacute;digo de Zona:<br />
			<input type="text" id="cod_zon" />
            </td>
            <td align="left" valign="top">
            </td>
            <td align="left" valign="top">
            </td>
            <td align="left" valign="top">
            </td>
          </tr>
        </table>
	</td>
  </tr> 
  <tr>
    <td>
	<button id="dele" class="btn"><span class="ui-icon ui-icon-trash"></span>Eliminar zona</button>
    <button id="hist" class="btn"><span class="ui-icon ui-icon-disk"></span>Historial de zonas</button>
    </td>
  </tr>
</table>