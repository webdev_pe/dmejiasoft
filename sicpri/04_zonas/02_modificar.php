<?php
require("../../poo/clases/getConection.php");
$id  = $_POST['i'];
$cod = $_POST['c'];
$ver = $_POST['v'];
$cn=new getConection();
if(isset($id)){
	$sql="select id_zon, cod_zon, nom_zon, des_zon from si_zonas where id_zon=".$id;		
	$cn->ejecutar_sql(base64_encode($sql));
	$cel=$cn->resultado_sql();
}else if(isset($cod)){
	$sql="select id_zon, cod_zon, nom_zon, des_zon from si_zonas where cod_zon='".$cod."'";	
	$cn->ejecutar_sql(base64_encode($sql));
	$cel=$cn->resultado_sql();
}
?>
<script type="text/javascript">
$(document).ready(function(){
	$("#cod_zon").autocomplete({
		source: "sicpri/04_zonas/_buscar_codigo.php",
		minLength: 2/*, select: function(event,ui){ $("#id_con").val(ui.item.id); }*/
	});
	$("#edit").click(function(){
		var i=$("#id").val();
		
		var nom=$("#tx_nom").val();
		var des=$("#tx_des").val();
	
		if(nom.length<1){ alert("Complete el campo Nombres."); $("#tx_nom").focus(); }
		else{
				$.post("sicpri/04_zonas/dao.php",{opt:"u", i:i, nom:nom, des:des},function(data){
					if(data==1){ 
						alert("Zona modificada correctamente.");
						$.post("sicpri/04_zonas/06_historial.php",function(data){
							$("#contenido_sicpri").html(data);
							$("#sicpri_tit").empty().text("HISTORIAL DE ZONAS");
						});	
					}else{
						alert("Vuelva a intentarlo por favor.");
					}
				});
		}
	});	
	$("#find").click(function(){
		var c=$("#cod_zon").val();
		if(c!=""){
			$.post("sicpri/04_zonas/02_modificar.php",{c:c},function(data){
				$("#contenido_sicpri").html(data);
				$("#sicpri_tit").empty().text("MODIFICAR ZONA");
			});
		}else{ alert("Ingrese el codigo de zona a modificar."); $("#cod_zon").focus(); }
	});	
	
	$("#hist").click(function(){
		$.post("sicpri/04_zonas/06_historial.php",function(data){
			$("#contenido_sicpri").html(data);
			$("#sicpri_tit").empty().text("HISTORIAL DE ZONAS");
		});
	});
});
</script>
<input type="hidden" id="id" value="<?=$cel['id_zon']?>" /> 
<table width="100%" border="0" id="tbl_prp">
  <?php if(!isset($id) && !isset($cod)){?>
  <tr>
    <td colspan="2">   
        <table width="100%" border="0" id="tbl_find">
          <tr>
            <td width="25%"><strong>Buscar zona:</strong></td>
            <td width="25%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
          </tr>
          <tr>
            <td align="left" valign="top">
			C&oacute;digo de Zona:<br />
			<input type="text" id="cod_zon" /><br />
            <button id="find" class="btn"><span class="ui-icon ui-icon-search"></span>Buscar zona</button>
            </td>
            <td align="left" valign="top">
            </td>
            <td align="left" valign="top">
            </td>
            <td align="left" valign="top">
            </td>
          </tr>
        </table>
	</td>
  </tr> 
  <?php }?>  
  <tr>
    <td colspan="2">
        <table width="100%" border="0">
          <tr>
            <td width="25%"><strong>Datos de la zona</strong></td>
            <td width="25%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
          </tr>
          <tr>
            <td align="left" valign="top">
            Nombre<br/><input type="text" id="tx_nom" value="<?=$cel['nom_zon']?>" /><br/>
            Descripci&oacute;n<br/><textarea id="tx_des"><?=$cel['des_zon']?></textarea><br/>
            </td>
            <td align="left" valign="top">&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
          </tr>
        </table>
    </td>
  </tr>
  <tr>
    <td>    
    </td>
  </tr>  
  <tr>
    <td>
    <?php if(isset($id) || isset($cod)){ if(!isset($ver)){?>
	<button id="edit" class="btn"><span class="ui-icon ui-icon-disk"></span>Modificar zona</button>
    <?php }}?>
	<button id="hist" class="btn"><span class="ui-icon ui-icon-folder-open"></span>Historial de zonas</button>
    </td>
  </tr>
</table>