<?php session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SICPRI : Presentaci&oacute;n General de Reporte</title>
	<link href="../../images/favicon.png" rel="shortcut icon" />
	<link rel="stylesheet" type="text/css" href="../../js/jquery-ui-1.8.16.custom/development-bundle/themes/custom-theme/jquery.ui.all.css">
    <script type="text/javascript" src="../../js/jquery_layout/jquery-latest.js"></script>
	<link type="text/css" href="../../js/jquery-ui-1.8.16.custom/css/custom-theme/jquery-ui-1.8.16.custom.css" rel="stylesheet" />	
	<script type="text/javascript" src="../../js/jquery-ui-1.8.16.custom/js/jquery-ui-1.8.16.custom.min.js"></script>    
    <script type="text/javascript" charset="utf-8" src="../../js/datatable/js/jquery-ui.js"></script>
	<script type="text/javascript" charset="utf-8" src="../../js/jquery_datatable/js/jquery.dataTables.js"></script>  
    <link rel="stylesheet" type="text/css" href="../../js/jquery_datatable/css/demo_table_jui.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="../../js/jquery_datatable/css/demo_table.css" media="screen" /> 
    <link rel="stylesheet" type="text/css" href="../../main.css" media="screen" /> 
    <link rel="stylesheet" type="text/css" href="../../css/print.css" media="print" />
    <link rel="stylesheet" type="text/css" href="../../css/screen.css" media="screen" />
	<script type="text/javascript">
    $(document).ready(function(){
		$('#list_usu').dataTable({ 'bJQueryUI': true, "bPaginate": false, "bSort": false, "bAutoWidth" : false, "bSortClasses": false });
	});
    </script>
</head>
<body>
<?php
if(isset($_GET['s'])){
$sql=utf8_decode(base64_decode($_GET['s']));
require("../../poo/clases/getConection.php");
$cn=new getConection();
$cn->ejecutar_sql(base64_encode($sql));
$cn->cantidad_sql(); $i=1;
}
#echo $sql;
?>
<!--div id="dialog" title="ENVIAR EMAIL <?=$_SESSION['sicpri_cod']?>">
<table width="100%" border="0" cellspacing="3" cellpadding="3">
  <tr>
    <td align="right" valign="middle">Nombre:</td>
    <td align="left" valign="middle"><input type="text" id="eml_nom" disabled="disabled" value="<?=($_SESSION['sicpri_nom']." ".$_SESSION['sicpri_ape'])?>" /></td>
  </tr>
  <tr>
    <td align="right" valign="middle">Desde:</td>
    <td align="left" valign="middle"><input type="text" id="eml_dsd" disabled="disabled" value="<?=$_SESSION['sicpri_eml'];?>" /></td>
  </tr>
  <tr>
    <td align="right" valign="middle">Para:</td>
    <td align="left" valign="middle"><input type="text" id="eml_for" /></td>
  </tr>
  <tr>
    <td align="right" valign="middle">CC:</td>
    <td align="left" valign="middle"><input type="text" id="eml_cc" /></td>
  </tr>
  <tr>
    <td align="right" valign="middle">CCO:</td>
    <td align="left" valign="middle"><input type="text" id="eml_cco" /></td>
  </tr>
  <tr>
    <td align="right" valign="middle">Asunto:</td>
    <td align="left" valign="middle"><input type="text" id="eml_ast" value="Reporte" /></td>
  </tr>
</table>
</div-->
<div id="opciones">
<button id="imp" class="btn" onclick="javascript:print()"><span class="ui-icon ui-icon-print"></span>Imprimir resultados</button>
<!--button id="pdf" class="btn"><span class="ui-icon ui-icon-arrowthickstop-1-s"></span>Exportar a PDF</button>
<button id="xls" class="btn"><span class="ui-icon ui-icon-arrowthickstop-1-s"></span>Exportar a XLS</button>
<button id="csv" class="btn"><span class="ui-icon ui-icon-arrowthickstop-1-s"></span>Exportar a CSV</button>
<button id="html" class="btn"><span class="ui-icon ui-icon-arrowthickstop-1-s"></span>Exportar a HTML</button>
<button id="eml" class="btn"><span class="ui-icon ui-icon-mail-closed"></span>Enviar por email</button-->
<input type="hidden" id="s" value="<?=$_GET['s']?>" />
</div>
<div id="contenedor">