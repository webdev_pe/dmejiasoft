<script type="text/javascript">
	$("#uploader").plupload({
		runtimes : 'gears,flash,silverlight,browserplus,html5',
		url : 'sicpri/01_propiedades/_subir_fotos.php',
		max_file_size : '10mb',
		chunk_size : '1mb',
		unique_names : true,
		resize : {width : 700, height : 700, quality : 90},
		filters : [
			{title : "Archivos de Imagen", extensions : "jpg,gif,png"}/*,{title : "Zip files", extensions : "zip"}*/
		],
		flash_swf_url : 'js/plupload/js/plupload.flash.swf',
		silverlight_xap_url : 'js/plupload/js/plupload.silverlight.xap'
	});
</script>
<div class="center_wrapper">
    <div id="main_content">
            <div id="uploader" style="height: 330px;">
                <p>Tu navegador no tiene soporte a Flash, Silverlight, Gears, BrowserPlus o HTML5.</p>
            </div>
            <br style="clear: both" />                    
    </div>
</div>