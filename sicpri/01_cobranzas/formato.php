<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Formato de cobro</title>
<style type="text/css" media="screen">
*{ font-size:7px;}
body{ margin:0px; padding:0px;}
#contenedor{ max-width:900px; min-height:700px; margin:0 auto; position:relative; border:1px solid #ededed; background:#ffffff;}
.format_tbl{ font-family:Verdana; font-size:7px; border:1px solid #ededed; border-collapse:collapse;}
#opciones{ text-align:center; margin:5px;}
</style>
<style type="text/css" media="print">
#opciones{ display:none;}
</style>
</head>
<body>
<?php
require("../../poo/clases/getConection.php");
$cn=new getConection();

$sql="select id_pre, p.id_cli, p.id_usu, z.id_zon, g.id_gal, concat(z.nom_zon,'/',g.nom_gal) as 'zongal', l.cod_cli, concat(l.nom_cli,' ',l.ape_cli) as 'nombres', cod_pre, tip_pre, 
mnt_pre, fra_pre, dia_pre, int_pre,
((mnt_pre*int_pre)+mnt_pre)-(select sum(mnt_cob) from si_cobranzas where id_pre=p.id_pre) as 'pendiente_p',
(mnt_pre+(select sum(agregar) from si_cobranzas where id_pre=p.id_pre)-(select sum(mnt_cob) from si_cobranzas where id_pre=p.id_pre)) as 'pendiente_a',

(select sum(real_int-interes) from si_cobranzas where id_pre=p.id_pre and estado='1' and cierre!='pa') +
(((mnt_pre+(select sum(agregar) from si_cobranzas where id_pre=p.id_pre)-(select sum(mnt_cob) from si_cobranzas where id_pre=p.id_pre))*fra_pre)/mnt_pre)
as 'interes_a'

from si_prestamos p, si_usuarios u, si_clientes l, si_zonas z, si_galerias g 
where p.id_usu=u.id_usu and p.id_cli=l.id_cli and l.id_zon=z.id_zon and l.id_gal=g.id_gal and u.id_usu=".$_GET['i']."
order by zongal asc, cod_cli asc 
";
$cn->ejecutar_sql(base64_encode($sql));
$cn->cantidad_sql();
?>
<div id="opciones">
<button id="imp" class="btn" onclick="javascript:print()"><span class="ui-icon ui-icon-print"></span>Imprimir resultados</button>
</div>
<div id="contenedor">
<table width="100%" border="1" class="format_tbl">
<thead>
	<tr>
    	<th width="60" rowspan="2">GALER&Iacute;A</th>
		<th width="40" rowspan="2">CODCLI</th>
	    <th width="160" rowspan="2">NOMBRES</th>
	    <th width="40" rowspan="2">CODCRE</th>
	    <th width="30" rowspan="2">TIPO</th>
	    <th width="150" colspan="2">PENDIENTE</th>
	    <th width="150" colspan="2">COBRO</th>
    </tr>
    <tr>
	    <th>CAPITAL</th>
	    <th>INTER&Eacute;S</th>
	    <th>CAPITAL</th>
    	<th>INTER&Eacute;S</th>
    </tr>    
</thead>
<?php while($cel=$cn->resultado_sql()){?>      
  <tr>
  	<td><?=$cel['zongal'];?></td>
	<td align="center"><?=$cel['cod_cli'];?></td>
    <td><?=$cel['nombres'];?></td>
    <td align="center"><?=$cel['cod_pre'];?></td>
    <td align="center"><?=($cel['tip_pre']=='p'?"C/MES":"C/D&Iacute;A")?></td>
    <td align="right"><?=($cel['tip_pre']=='p'?number_format($cel['pendiente_p'],2,'.',','):number_format($cel['pendiente_a'],2,'.',','))?></td>
    <td align="right"><?=($cel['tip_pre']=='p'?"---":number_format($cel['interes_a'],2,'.',','))?></td>
    <td></td>
    <td></td>
  </tr>
<?php }?>
</table>
</div>
</body>
</html>