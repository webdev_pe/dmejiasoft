<?php
require("../../poo/clases/getConection.php");
$cn=new getConection();
$cod=$_POST['cod']; $cdc=$_POST['cdc']; $nom=$_POST['nom']; 
$dsd=($_POST['dsd']!="")?date("Y-m-d",strtotime($_POST['dsd'])):"";
$hst=($_POST['hst']!="")?date("Y-m-d",strtotime($_POST['hst'])):"";
$tip=$_POST['tip'];

if($cod!=""){	$add_search=" and cod_pre='$cod' "; 
}else{
	$add_search =($cdc!="")?" cod_cli='$cdc' and ":"";
	$add_search.=($nom!="")?" concat(c.nom_cli,' ',c.ape_cli) like '%$nom%' and ":"";
	$add_search.=($dsd!="" && $hst=="")?" fecha>='$dsd' and ":"";
	$add_search.=($dsd=="" && $hst!="")?" fecha<='$hst' and ":"";
	$add_search.=($dsd!="" && $hst!="")?" fecha between '$dsd' and '$hst' and ":"";	
	$add_search.=($tip!="")?" tip_pre='$tip' and ":"";
	$add_search=($add_search!="")?" and ".$add_search:"";	
	$lenght=strlen($add_search);
	$add_search=substr($add_search,0,($lenght-4));
}

$sql="select id_pre, date_format(fecha,'%d-%m-%Y') as 'fecha', hora, cod_pre, c.cod_cli, u.cod_usu, mnt_pre,
case tip_pre
	when 'a' then 'C/D&iacute;a'
	when 'p' then 'C/Mes'
end as 'tipo', concat(c.nom_cli,' ',c.ape_cli) as 'nombres', est_pre, z.nom_zon, g.nom_gal
from si_prestamos p, si_clientes c, si_usuarios u, si_zonas z, si_galerias g
where p.id_cli=c.id_cli and p.id_usu=u.id_usu and c.id_zon=z.id_zon and c.id_gal=g.id_gal".$add_search." and p.est_pre='a' order by id_pre desc";
$cn->ejecutar_sql(base64_encode($sql));
$cn->cantidad_sql();
?>
<script type="text/javascript">
$(document).ready(function(){
	$('#list_pre').dataTable({
      /*"bProcessing": true,
      "bServerSide": true,
      "sAjaxSource": "content/scripts/propiedades.php",*/
	  'bJQueryUI': true,
	  'sPaginationType': 'full_numbers',	  
	  'fnDrawCallback': function(oSettings){
		  if(oSettings.bSorted || oSettings.bFiltered){
			  for(var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++){
				  $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i+1);
			  }
		  }
	  },'aoColumnDefs': [
		  {'bSortable': false, 'aTargets': [0]},
		  {'bSortable': false, 'aTargets': [1]},
		  {'bSortable': false, 'aTargets': [9]},
		  {'bSortable': false, 'aTargets': [10]}
	  ],"bAutoWidth" : false,	  
	  "bSortClasses": false, 
	  "aoColumns" : [
		  { sWidth : '18px'},
		  { sWidth : '18px'},
		  { sWidth : '80px'},	  
		  { sWidth : '70px'},
		  { sWidth : '70px'},	  
		  { sWidth : 'auto'},
		  { sWidth : '90px'},	  
		  { sWidth : 'auto'},		  		  	  	  		  	  
		  { sWidth : '80px'},		  		  	  	  
		  { sWidth : '20px'},	  
		  { sWidth : '20px'}
	  ],'aaSorting': [[0, 'asc']]
	}).columnFilter({aoColumns: [
		  null,
		  null,
		  { type: "text" },
		  { type: "text" },
		  { type: "text" },
		  { type: "select", values: ['DNI', 'Carnet de Extranjería', 'Otros'] },
		  { type: "text" },
		  { type: "text" },
		  { type: "text" },	  				
		  null,
		  null
	  ]
	});
	
	$("#todos").live("click", function(){
		$checkboxes=$("#list_pre tbody td").find(":checkbox");
		if($(this).is(":checked")){ $checkboxes.attr("checked",1); }
		else{ $checkboxes.removeAttr("checked");}
	});

	$("#rept").click(function(){
	  var s=$("#s").val();
	  var new_tab=window.open("","_blank");		
	  new_tab.location="sicpri/02_prestamos/reporte.php?s="+s;
	});

	  $('#dialog1').dialog({
		  autoOpen: false,
		  width: 250,
		  buttons: {
			  "CAMBIAR": function() { 
				  var i=$("#i").val();
				  var e=$("[name=est1]:checked").val();
				  $.post("sicpri/01_cobranzas/cambiar_estado.php",{i:i,e:e},function(data){
					  if(data=="1"){
						$('#dialog1').dialog("close");
						$.post("sicpri/01_cobranzas/06_historial.php",function(data){
							$("#contenido_sicpri").html(data);
							$("#sicpri_tit").empty().text("HISTORIAL DE COBRANZAS");
						});						
					  }else{
						alert("Inténtelo de nuevo por favor.");
					  }
				  });
			  }, 
			  "CANCELAR": function() { 
				  $(this).dialog("close");
			  } 
		  }
	  });	
});	
	function editar(i,est,cod){
			switch(est){
				case "a": $("#opc_a1").attr("checked",1); break;
				case "c": $("#opc_b1").attr("checked",1); break;
			}
			$("#i").val(i);
			$("#codpre").empty().text(cod);
			$("#dialog1").dialog("open");		
	}	
	
	function verificar(i){
	  /*var new_tab=window.open("","_blank");		
	  new_tab.location="sicpri/02_prestamos/visualizar.php?i="+i;*/

	  var width=700;
	  var height=590;
	  var posx=$(window).width()/2 - width/2;
	  var posy=$(window).height()/2 - height/2;
	  var opciones=("toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, fullscreen=no, width="+width+", height="+height+", top="+posy+", left="+posx); 
	  window.open("sicpri/01_cobranzas/visualizar.php?i="+i,"SIPC",opciones);	  
	}
</script>
<div id="dialog1" title="CAMBIA ESTADO DE PRESTAMO" style="display:none;">
    <p><strong>PR&Eacute;STAMO: <span id="codpre"></span></strong><br />
    <table id="opcs" width="100%" border="0">
      <tr>
        <td colspan="2">Seleccione estado:</td>
      </tr>
      <tr>
        <td width="15"><input type="radio" id="opc_a1" name="est1" value="a" /></td>
        <td><label for="opc_a1">Activo</label></td>
      </tr>
      <tr>
        <td><input type="radio" id="opc_b1" name="est1" value="c" /></td>
        <td><label for="opc_b1">Cancelado</label></td>
      </tr>
    </table>
    </p>
</div>
	<input type="hidden" id="i" value="" />
	<input type="hidden" id="s" value="<?=base64_encode(utf8_encode($sql))?>" />
    <?php if(isset($_POST['r'])){ ?>
	<button id="rept" class="btn"><span class="ui-icon ui-icon-clipboard"></span>Generar previo de resultados</button>
	<?php }?>
	<table id="list_pre" class="list_tbl" width="100%">
	<thead>
        <tr>    
	        <th>N&ordm;</th>
	        <th><input type="checkbox" id="todos" /></th>    
	        <th>FECHA</th>
	        <th>CODIGO</th>
	        <th>CLIENTE</th>
	        <th>NOMBRE DEL CLIENTE</th>
	        <th>OPERADOR</th>
            <th>MONTO</th>
            <th>TIPO</th>
	        <th title="Editar registro"><div class="ui-icon ui-icon-pencil custom_icon"></div></th>
            <th title="Visualizar registro"><div class="ui-icon ui-icon-zoomin custom_icon"></div></th>
        </tr>   
	</thead>
	<tbody>
    <?php
	$cn_c=new getConection();
    while($cell=$cn->resultado_sql()){	
	?>
      <tr>    
        <td align="center"></td>
        <td align="center"><input type="checkbox" name="cods[]" id="cods" value="<?=$cell['id_pre']?>"  /></td>
        
        <td align="center"><?=$cell['fecha']?></td>    
        <td align="center"><?=$cell['cod_pre']?></td>
        <td align="center"><?=$cell['cod_cli']?></td>
        <td align="left"><?=$cell['nombres']?> - (<?=$cell['nom_zon']?>-<?=$cell['nom_gal']?>)</td>      
        <td align="center"><?=$cell['cod_usu']?></td>
        <td align="right"><span>S/.</span><?=number_format($cell['mnt_pre'],2,'.',',')?></td>
        <td align="center"><?=$cell['tipo']?></td>        
        <td align="center"><button id="editar" class="btn" onclick="editar('<?=$cell['id_pre']?>','<?=$cell['est_pre']?>','<?=$cell['cod_pre']?>')" title="Modificar registro"><span class="ui-icon ui-icon-pencil"></span></button></td>
        <td align="center"><button id="visualizar" class="btn" onclick="verificar('<?=$cell['id_pre']?>')" title="Visualizar registro"><span class="ui-icon ui-icon-zoomin"></span></button></td>
      </tr>
    <?php
	}
	$cn->limpiar_sql();
	$cn->cerrar_sql();
    ?>
    </tbody>
	<tfoot>
        <tr>    
	        <th>N&ordm;</th>
	        <th><input type="checkbox" id="todos" /></th>    
	        <th>FECHA</th>
	        <th>CODIGO</th>
	        <th>CLIENTE</th>
	        <th>NOMBRE DEL CLIENTE</th>
	        <th>OPERADOR</th>
            <th>MONTO</th>                
            <th>TIPO</th>
	        <th title="Editar registro"><div class="ui-icon ui-icon-pencil custom_icon"></div></th>
            <th title="Visualizar registro"><div class="ui-icon ui-icon-zoomin custom_icon"></div></th>
        </tr>   
	</tfoot>    
    </table>