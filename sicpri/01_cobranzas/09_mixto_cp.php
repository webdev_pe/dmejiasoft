<?php
require("../../poo/clases/getConection.php");
$cn=new getConection();
$d=($_POST['d']!="")?date("Y-m-d",strtotime($_POST['d'])):"";
$h=($_POST['h']!="")?date("Y-m-d",strtotime($_POST['h'])):"";
if($d!="" && $h==""){
	$fsearch1="c.fecha>='$d'"; 					$fsearch2="fecha>='$d'";
}else if($d!="" && $h!=""){
	$fsearch1="c.fecha between '$d' and '$h'"; 	$fsearch2="fecha between '$d' and '$h'";
}else if($d=="" && $h!=""){
	$fsearch1="c.fecha<='$h'"; 					$fsearch2="fecha<='$h'";
}else if($d=="" && $h==""){
	$fsearch1="c.fecha=curdate()"; 				$fsearch2="fecha=curdate()";
}
$sql="
select p.id_pre, date_format(c.fecha,'%d-%m-%Y') as 'fecha', p.cod_pre, l.cod_cli, u.cod_usu, c.mnt_cob as 'Monto', ifnull(c.interes,0) as 'interes',
(c.mnt_cob+ifnull(c.interes,0)) as 'total',
case p.tip_pre
	when 'a' then 'Arrebatir'
	when 'p' then 'Porcentaje'
end as 'tipo', concat(l.nom_cli,' ',l.ape_cli) as 'nombres', 
case p.est_pre
	when 'a' then 'Vigente'
	when 'c' then 'Cancelado'
end as 'estado', 'Cobranza' as 'Modo', c.agregar as 'agregado', c.dispositivo as 'dispositivo', z.nom_zon, g.nom_gal
from si_cobranzas c, si_prestamos p, si_clientes l, si_usuarios u, si_zonas z, si_galerias g
where c.id_pre=p.id_pre and p.id_cli=l.id_cli and p.id_usu=u.id_usu and l.id_zon=z.id_zon and l.id_gal=g.id_gal and $fsearch1
union
select id_pre, date_format(fecha,'%d-%m-%Y') as 'fecha', cod_pre, c.cod_cli, u.cod_usu, mnt_pre as 'Monto', fra_pre as 'interes',
(mnt_pre+fra_pre) as 'total',
case tip_pre
	when 'a' then 'Arrebatir'
	when 'p' then 'Porcentaje'
end as 'tipo', concat(c.nom_cli,' ',c.ape_cli) as 'nombres', 
case est_pre
	when 'a' then 'Vigente'
	when 'c' then 'Cancelado'
end as 'estado', 'Pr&eacute;stamo' as 'Modo', 0 as 'agregado', p.dispositivo as 'dispositivo', z.nom_zon, g.nom_gal
from si_prestamos p, si_clientes c, si_usuarios u, si_zonas z, si_galerias g
where p.id_cli=c.id_cli and p.id_usu=u.id_usu and c.id_zon=z.id_zon and c.id_gal=g.id_gal and $fsearch2
";
#echo $sql;
$cn->ejecutar_sql(base64_encode($sql));
$cn->cantidad_sql();
?>
<script type="text/javascript">
$(document).ready(function(){
	$('#list_pre').dataTable({
      /*"bProcessing": true,
      "bServerSide": true,
      "sAjaxSource": "content/scripts/propiedades.php",*/
	  'bJQueryUI': true,
	  'sPaginationType': 'full_numbers',	  
	  'fnDrawCallback': function(oSettings){
		  if(oSettings.bSorted || oSettings.bFiltered){
			  for(var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++){
				  $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i+1);
			  }
		  }
	  },"bAutoWidth" : false,	  
	  "bSortClasses": false, 
	  "aoColumns" : [
		  { sWidth : '20px'},
		  { sWidth : '80px'},	  
		  { sWidth : '70px'},
		  { sWidth : 'auto'},	  
		  { sWidth : '90px'},
		  { sWidth : '90px'},	  
		  { sWidth : '70px'},		  		  	  	  		  	  
		  { sWidth : '70px'},		  		  	  	  
		  { sWidth : 'auto'},	  
		  { sWidth : 'auto'},
		  { sWidth : 'auto'},		  
		  { sWidth : 'auto'},	  
		  { sWidth : 'auto'}
	  ],'aaSorting': [[0, 'asc']]
	}).columnFilter({aoColumns: [
		  { type: "text" },
		  { type: "text" },
		  { type: "text" },
		  { type: "text" },
		  { type: "text" },
		  { type: "text" },
		  { type: "text" },
		  { type: "text" },	  				
		  { type: "text" },
		  { type: "text" },
		  { type: "text" },
		  { type: "text" },		  
		  { type: "text" }
	  ]
	});

	$("#rept").click(function(){
	  var s=$("#s").val();
	  var new_tab=window.open("","_blank");		
	  new_tab.location="sicpri/01_cobranzas/reporte_mixto.php?s="+s;
	});	
	
	$.datepicker.setDefaults($.datepicker.regional["es"]);
	$("#fec_dsd").datepicker({
		dateFormat: "dd-mm-yy",
		numberOfMonths: 3,		
		showButtonPanel: true/*, showOn: "button", buttonImage: "img/calendar.gif", buttonImageOnly: true */
	});
	$("#fec_hst").datepicker({dateFormat: "dd-mm-yy", numberOfMonths: 3,	showButtonPanel: true});	
	
	$("#bsqf").click(function(){
	  var d=$("#fec_dsd").val();
	  var h=$("#fec_hst").val();
		$.post("sicpri/01_cobranzas/09_mixto_cp.php",{d:d,h:h},function(data){
			$("#contenido_sicpri").html(data);
			$("#sicpri_tit").empty().text("REPORTE MIXTO");
		});
	});			
});
</script>
	<input type="hidden" id="i" value="" />
	<input type="hidden" id="s" value="<?=base64_encode(utf8_encode($sql))?>" />
<table width="100%" border="0">
  <tr>
    <td align="left" valign="middle"><button id="rept" class="btn"><span class="ui-icon ui-icon-clipboard"></span>Generar previo de resultados</button></td>
    <td align="right" valign="middle">Fecha desde: <input type="text" id="fec_dsd" style="width:150px;" /> | Fecha hasta: <input type="text" id="fec_hst" style="width:150px;"  /> <button id="bsqf" class="btn"><span class="ui-icon ui-icon-search"></span>Filtrar por fechas</button></td>
  </tr>
</table>
	<table id="list_pre" class="list_tbl" width="100%">
	<thead>
        <tr>    
	        <th>N&ordm;</th>
	        <th>FECHA</th>	        
	        <th>CLIENTE</th>
	        <th>NOMBRE DEL CLIENTE</th>
	        <th>OPERADOR</th>
            <th>PRESTAMO</th>
            <th>TIPO</th>
            <th>MODO</th>            
            <th>MONTO</th>
            <th>INTERES</th>
	        <th>TOTAL COB.</th>
            <th>TOTAL PRE.</th>
            <th>TOTAL AGR.</th>
        </tr>   
	</thead>
	<tbody>
    <?php
	$cn_c=new getConection();
    while($cell=$cn->resultado_sql()){	
	?>
      <tr>    
        <td align="center"></td>
        <td align="center"><?=$cell['fecha']?></td>    
        <td align="center"><?=$cell['cod_cli']?></td>
        <td align="left"><?=$cell['nombres']?> - (<?=$cell['nom_zon']?>-<?=$cell['nom_gal']?>)</td> 
        <td align="center"><?=$cell['cod_usu']?></td>
        <td align="center"><?=$cell['cod_pre']?></td>             
        <td align="center"><?=$cell['tipo']?></td>
        <td align="center"><?=$cell['Modo']?></td>    
        <td align="right"><span>S/.</span><?=number_format($cell['Monto'],2,'.',',')?></td>
        <td align="right"><span>S/.</span><?=number_format($cell['interes'],2,'.',',')?></td>
		<?php if($cell['Modo']=="Cobranza"){?> 
        <td align="right"><span>S/.</span><?=number_format($cell['total'],2,'.',',')?></td> 
        <td align="right">---</td>
        <?php }else{?>
        <td align="right">---</td>
        <td align="right"><span>S/.</span><?=number_format($cell['total'],2,'.',',')?></td> 
        <?php }?>               
        <td align="right"><span>S/.</span><?=number_format($cell['agregado'],2,'.',',')?></td>
      </tr>
    <?php
		if($cell['Modo']=="Cobranza"){
			$cob+=$cell['total'];
			$agr+=$cell['agregado'];
		}else{
			$pre+=$cell['total'];
		}
	}
	$cn->limpiar_sql();
	$cn->cerrar_sql();
    ?>
    </tbody>
	<tfoot> 
        <tr style="font-weight:bold; background:#e2e4ff;">    
	        <td colspan="8" align="left">TOTAL COBRADO: &raquo;</td>	        
            <td></td><td></td>
            <td align="right"><span>S/.</span><?=number_format($cob,2,'.',',')?></td>
            <td></td>
            <td></td>
        </tr>
        <tr style="font-weight:bold; background:#e2e4ff;">    
	        <td colspan="8" align="left">TOTAL PRESTADO: &raquo;</td>      
            <td></td><td></td>
            <td></td>
            <td align="right"><span>S/.</span><?=number_format($pre,2,'.',',')?></td>
            <td></td>
        </tr> 
        <tr style="font-weight:bold; background:#e2e4ff;">    
	        <td colspan="8" align="left">TOTAL AGREGADO: &raquo;</td>
	        <td></td><td></td>           
            <td></td>
            <td></td>
            <td align="right"><span>S/.</span><?=number_format($agr,2,'.',',')?></td>
        </tr>  
        <tr>    
	        <td colspan="13"><hr /></td>
        </tr>
        <tr style="font-weight:bold; background:#e2e4ff;">    
	        <td colspan="8"></td>
	        <td></td><td></td>            
            <td colspan="2">TOTAL ENTRANTE: &raquo;</td>
            <td align="right"><span>S/.</span><?=number_format($cob,2,'.',',')?></td>
        </tr>         
        <tr style="font-weight:bold; background:#e2e4ff;">    
	        <td colspan="8"></td>
	        <td></td><td></td>            
            <td colspan="2">TOTAL SALIENTE: &raquo;</td>
            <td align="right"><span>S/.</span><?=number_format(($pre+$agr),2,'.',',')?></td>
        </tr>                                                  
        <tr>    
	        <th>N&ordm;</th>
	        <th>FECHA</th>	        
	        <th>CLIENTE</th>
	        <th>NOMBRE DEL CLIENTE</th>
	        <th>OPERADOR</th>
            <th>PRESTAMO</th>
            <th>TIPO</th>
            <th>MODO</th>            
            <th>MONTO</th>
            <th>INTERES</th>
	        <th>TOTAL COB.</th>
            <th>TOTAL PRE.</th>
            <th>TOTAL AGR.</th>
        </tr>   
	</tfoot>    
    </table>