<?php
session_start();
unset($_SESSION['s']); unset($_SESSION['sh']);

date_default_timezone_set("America/Lima");
require("../../poo/clases/getConection.php");
$cn=new getConection();

$dias=-1;
$fecha_ayer=date("Y-m-d", strtotime("$dias days"));
$fecha_hoy=date("Y-m-d");

$sql="
select id_cob, id_pre, mnt_cob, dias, adicional, interes, agregar, fecha, cierre, estado, dscto 
from si_cobranzas where id_pre=".$_GET['i']." order by fecha asc";
$cn->ejecutar_sql(base64_encode($sql));
$row=$cn->cantidad_sql();
#echo $sql;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Detalle de pr&eacute;stamo</title>
<style type="text/css">
body{ font-family:Verdana; font-size:11px; color:#116194;}
#visual{ margin:0 auto; height:auto; width:850px; border:1px solid #ededed;}
#tbl_res{border-collapse:collapse; border:1px solid #cccccc;}
#tbl_res thead th{ background:#116194; color:#ffffff;}
#tbl_res tbody tr:hover{ cursor:pointer; background:#ffe87b; color:#116194;}
#imprimir{ position:fixed; top:0px; right:0px; background:#116194; color:#ffffff; text-align:center; cursor:pointer; width:80px; height:25px; line-height:23px; font-weight:bold;}
</style>
<script type="text/javascript" src="../../js/jquery_layout/jquery-latest.js"></script>
<script type="text/javascript">
function activar(idc,t){
	var chk=(t.is(":checked"))?1:0;
	$.post("omitir_cobranza.php", {idc:idc,chk:chk}, function(data){
		if(data==1){
			window.location.reload();
		}	
	});
}
function eliminar(idc){
	if(confirm("Esta seguro de eliminar este cobro?")){
		$.post("eliminar_cobro.php",{idc:idc},function(data){
			if(data=="1"){
			  alert("Cobro eliminado correctamente.");
			  window.location.reload();
			}else{
			  alert("Intentelo de nuevo por favor.");
			}			
		});
	}
}
</script>
</head>
<body>
<div id="imprimir" onclick="javascript:print();">Imprimir</div>
<div id="visual">
<?php
$cn1=new getConection();
$sql1="select id_pre, tip_pre, mnt_pre, int_pre, dia_pre, fra_pre,
p.id_cli, concat(c.nom_cli,' ',c.ape_cli) as 'nombres', c.cod_cli, p.cod_pre, date_format(fecha,'%d-%m-%Y') as 'fecha', interes_por, est_pre, observaciones
 from si_prestamos p, si_clientes c
 where p.id_cli=c.id_cli and id_pre=".$_GET['i'];
$cn1->ejecutar_sql(base64_encode($sql1));
$cel1=$cn1->resultado_sql();

if($cel1['tip_pre']=="p"){
	echo "<h3>Cr&eacute;dito Consignaci&oacute;n por Mes (".$cel1['cod_pre'].")</h3>";
	echo "<hr />";
	echo "<strong>Fecha de pr&eacute;stamo:</strong> ".$cel1['fecha']."<br />";
	echo "<strong>Cliente:</strong> ".$cel1['nombres']."<br /> <strong>C&oacute;digo:</strong> ".$cel1['cod_cli']."<br />";
		$monto=$cel1['mnt_pre'];
		echo "<strong>Monto</strong>: ".number_format(round($monto),2,'.',',')." -
		<strong>D&iacute;as</strong>: ".$cel1['dia_pre']." -
		<strong>Inter&eacute;s</strong>: ".$cel1['int_pre']."% - ";	
		$diario=$monto/$cel1['dia_pre'];
		$interes=$monto*$cel1['int_pre']/$cel1['dia_pre'];
		
		$int_tot=$monto*$cel1['int_pre'];
		#$dev_tot=$monto+$int_tot;

		$diario_a_pagar=$cn->redondeo($diario)+$cn->redondeo($interes);
		$dev_tot=$diario_a_pagar*$cel1['dia_pre'];
		echo "<strong>Cuota diaria</strong>: ".$cn->redondeo($diario_a_pagar);		
	echo "<hr />";
}else if($cel1['tip_pre']=="a"){
	echo "<h3>Cr&eacute;dito Consignaci&oacute;n por D&iacute;a (".$cel1['cod_pre'].")</h3>";
	echo "<hr />";
	echo "<strong>Fecha de pr&eacute;stamo:</strong> ".$cel1['fecha']."<br />";
	echo "<strong>Cliente:</strong> ".$cel1['nombres']."<br /> <strong>C&oacute;digo:</strong> ".$cel1['cod_cli']."<br />";
	echo "<strong>Capital</strong>: ".$cn->redondeo($cel1['mnt_pre'])." -
		 <strong>Inter&eacute;s</strong>: ".$cn->redondeo($cel1['fra_pre'])." - 
		 <strong>Inter&eacute;s por</strong>: ".$cn->redondeo($cel1['interes_por'])."";	
	echo "<hr />";
	$monto=$cn->redondeo($cel1['mnt_pre']);
	$inter=$cn->redondeo($cel1['fra_pre']);
	$dev_tot=$monto;
	
/*$n=700.99; 
$nn=700.21;
$r=($n-intval($n))*100; 
echo $r;;*/
}else{
	echo "<h1>No se ha iniciado ning&uacute;n pago para este pr&eacute;stamo</h1><hr />";
}

?>
<script type="text/javascript">
$(document).ready(function(){
	$("#estado").change(function(){
		$("#estado option:selected").each(function(){
			var e=$(this).val();
			if(confirm("Esta seguro de cambiar el estado de su prestamo?")){
				var m=prompt("Ingrese el motivo de la cancelacion:","Escriba su motivo...");
				if(m!=""){ //alert(m);
					$.post("cambiar_estado.php",{i:<?=$cel1['id_pre']?>,e:e,m:m},function(data){
						if(data=="1"){
						  alert("Prestamo cambiado de estado correctamente.");
						  window.close();
						}else{
						  alert("Intentelo de nuevo por favor.");
						}			
					});
				}				
			}
		});	
	});	
});
</script>
Estado del pr&eacute;stamo:
<select id="estado" style="font-size:11px; color:#116194;">
<option value="a" <?=($cel1['est_pre']=="a")?"selected='selected'":""?>>Activo</option>
<option value="c" <?=($cel1['est_pre']=="c")?"selected='selected'":""?>>Cancelado</option>
</select>
<?php if($cel1['tip_pre']=="p"){ ?>
<table width="100%" border="1" id="tbl_res">
<thead>
  <tr>
    <th width="4%" align="center" valign="middle">#</th>
    <th width="15%" align="center" valign="middle">Fecha</th>    
    <th width="15%" align="center" valign="middle">Cuota</th>
    <th width="10%" align="center" valign="middle">Inter&eacute;s</th>
    <th width="10%" align="center" valign="middle">Total</th>
    <th width="20%" align="center" valign="middle">Pago</th>
    <th width="4%" align="center" valign="middle" title="Eliminar">*</th>       
  </tr>
</thead>  
<tbody>
  <?php 
	$i=1;
	while($cel=$cn->resultado_sql()){ 
	$fecha=date("d-m-Y",strtotime($cel['fecha']));
	$total=$cn->redondeo($diario)+$cn->redondeo($interes);	
  ?>
  <tr bgcolor="<?php if($fecha==date("d-m-Y")){echo "#ffe87b";}else{ if($i%2==0){echo "#ffffff";}else{echo "#e2e4ff";} }?>" <?php if($cel['mnt_cob']==0){ echo "style='display:none;'";}?>  >
    <td align="center" valign="middle"><?=$i?></th>
    <td align="center" valign="middle"><?=$fecha?></th>
    <td align="right" valign="middle"><?=$cn->redondeo($diario)?></td>
    <td align="right" valign="middle"><?=$cn->redondeo($interes)?></td>
    <td align="right" valign="middle"><?=$cn->redondeo($total)?></td>
    <td align="right" valign="middle"><?=$cn->redondeo($cel['mnt_cob'])?> <br />
    <?php 
	$int_c=$cn->redondeo($cel['mnt_cob'])*$cn->redondeo($cel1['int_pre'])/(1+$cel1['int_pre']);
	$cap_c=$cn->redondeo($cel['mnt_cob'])-$int_c;
	echo $cn->redondeo($int_c)." - ".$cn->redondeo($cap_c);
	?> 
    </td>
    <?php
	if($cel['fecha']==$fecha_ayer || $cel['fecha']==$fecha_hoy){
		$checked_est='';
	}else{
		$checked_est='disabled="disabled"';
	}
    ?>    
    <td align="center" valign="middle">
    <input type="button" onclick="eliminar('<?=$cel['id_cob']?>')" value="E" <?=$checked_est?> />
    </td>       
  </tr>
  <?php 
  	$ult_adc=$cel['adicional'];
	$acum1+=$cel['mnt_cob'];
	$i++;
	}?>

</tbody>
<tfoot>
  <tr>
    <th align="center" colspan="5">Totales</th>
    <th align="center">Cuota</th>
    <th align="center">-</th>
  </tr>
  <tr>
    <th align="left" colspan="5">Total Cobrado</th>
    <th align="right"><?=$cn->redondeo($acum1)?></th><th align="center">-</th>
  </tr>
  <tr>
    <th align="left" colspan="5">Total Pendiente</th>
    <th align="right"><?=$cn->redondeo($dev_tot-$acum1)?></th><th align="center">-</th>
  </tr>
  <tr>
    <th align="left" colspan="5">Total Prestado</th>
    <th align="right"><?=$cn->redondeo($dev_tot)?></th><th align="center">-</th>
  </tr>
  <tr>
  	<?php if(($dev_tot-$acum1)>0){?><th align="center" colspan="7" bgcolor="#ff0000"><h3 style="color:#ffffff;">Pr&eacute;stamo Pendiente</h3></th>
    <?php }else{?><th align="center" colspan="7" bgcolor="#116194"><h3 style="color:#ffffff;">Pr&eacute;stamo Cancelado</h3></th><?php }?>
  </tr>      
<tfoot>
</table>
<?php }else if($cel1['tip_pre']=="a"){ ?>
<table width="100%" border="1" id="tbl_res">
<thead>
  <tr>
    <th width="3%" rowspan="2" align="center" valign="middle">#</th>
    <th width="12%" rowspan="2" align="center" valign="middle">Fecha de cobro</th>    
    <th width="8%" rowspan="2" align="center" valign="middle">Capítal</th>
    <th width="8%" rowspan="2" align="center" valign="middle">Cuota</th>
    <th width="10%" rowspan="2" align="center" valign="middle">Total</th>
    <th colspan="3" align="center" valign="middle">Pago arrebatir</th> 
    <th width="" rowspan="2" align="center" valign="middle">Pagado</th>
    <th width="" rowspan="2" align="center" valign="middle">Por pagar</th>
    <th width="5%" rowspan="2" align="center" valign="middle">Agregado</th>
    <th width="6%" rowspan="2" align="center" valign="middle">Dscto.</th>  
    <th width="4%" rowspan="2" align="center" valign="middle" title="Cobranza">*</th>
    <th width="4%" rowspan="2" align="center" valign="middle" title="Eliminar">*</th>         
    </tr>
  <tr>
    <th width="11%" align="center" valign="middle">Amortizaci&oacute;n de capital</th>
    <th width="11%" align="center" valign="middle">Intereses pagados</th> 
    <th width="11%"  align="center" valign="middle">Intereses por pagar</th>       
  </tr>  
</thead>  
<tbody>
  <?php  
	$i=1;
	$ult_int=$cel1['fra_pre'];
	$j=0;	
	$montos=$cel1['mnt_pre'];
	$intere=$cn->redondeo($cel1['fra_pre']);
	$totale=$montos+$intere;
	#echo "<br />".date("d-m-Y H:m:i")."<br />";
	$array_fechas=array();
	$array_montos=array();
	while($cel=$cn->resultado_sql()){ 
	$fecha=date("d-m-Y",strtotime($cel['fecha']));
	$pago_real=$montos;

		if($cel['estado']=="1"){
			$ultimo+=$cel['mnt_cob'];
			
			if($cel['cierre']=='pa'){ if(!in_array($fecha,$array_fechas)){$array_fechas[$fecha]=$cel['agregar']; }
			}else{ if(!in_array($fecha,$array_montos)){$array_montos[$fecha]=$cel['mnt_cob'];} }
			
			$ayer=date("d-m-Y", strtotime("$fecha -1 days"));
	
			if($ayer!=$_SESSION['s']){
				$ultimo_add1+=$array_fechas[$ayer];
				$ultimo_mnt1+=$array_montos[$ayer];
				$_SESSION['s']=$ayer;
			}else{
				unset($_SESSION['s']);
			}

			$montos=$cel1['mnt_pre']-$ultimo_mnt1+$ultimo_add1; #echo "<br />".$cel1['mnt_pre']."-$ultimo+$ultimo_add1 ======= $montos 1";

		$intere=$cn->redondeo(($montos*$cel1['fra_pre'])/$cel1['mnt_pre']);
		$totale=$montos+$intere;
		$acum1=$ultimo;
		$ult_mnt=$cel['mnt_cob'];
		}else{	
			$ayer=date("d-m-Y", strtotime("$fecha -1 days"));
	
			if($ayer!=$_SESSION['s']){
				$ultimo_add1+=$array_fechas[$ayer];
				$ultimo_mnt1+=$array_montos[$ayer];
				$_SESSION['s']=$ayer;
			}else{
				unset($_SESSION['s']);
			}

			$montos=$cel1['mnt_pre']-$ultimo_mnt1+$ultimo_add1; #echo "<br />".$cel1['mnt_pre']."-$ultimo+$ultimo_add1 ======= $montos 1";		
		}
  ?>
  <tr bgcolor="<?php if($fecha==date("d-m-Y")){echo "#ffe87b";}else{ if($cel['estado']=="0"){echo "#f67c7c";}else{ if($i%2==0){echo "#ffffff";}else{echo "#e2e4ff";} } }?>">
    <td align="center" valign="middle"><?=$i?></th>
    <td align="center" valign="middle"><?=$fecha?></th>
    <td align="right" valign="middle"><?=($cel['agregar']>0)?"0.00":$cn->redondeo($montos)?></td>
    <td align="right" valign="middle"><?=($cel['agregar']>0)?"0.00":$cn->redondeo($intere)?></td>
    <td align="right" valign="middle"><?=($cel['agregar']>0)?"0.00":$cn->redondeo($totale)?></td>
    <td align="right" valign="middle"><?=$cn->redondeo($cel['mnt_cob'])?></td>
    <td align="right" valign="middle"><?=$cn->redondeo($cel['interes'])?></td>   
    <td align="right" valign="middle">
	<?php 
	$porpagar=($cel['agregar']>0)?0:$intere-$cel['interes'];
	if($porpagar<=0){echo "0.00";
	}else{echo $cn->redondeo($porpagar);} 
	?><!-- br />< ? =$cn->redondeo($porpagar)? --></td>   
    <td align="right" valign="middle">
	<?php 
	$acum+=($cel['interes']); 
	echo ($cel['interes']<=0)?"0.00":$cn->redondeo($acum);
	?>
    </td>   
    <td align="right" valign="middle">
	<?php 
	if($cel['estado']=="1"){		
		if($cel['agregar']>0){
			$acum2+=0;
			echo "0.00";
		}else{
			$acum2+=$intere;
			echo $cn->redondeo($acum2-$acum);
		}			
	}else{
		$acum2+=0;
		echo "0.00";
	}	
	?>    
    </td>    
    <td align="right" valign="middle"><?php $ult_add+=$cel['agregar']; echo $cn->redondeo($cel['agregar']);?></td>
    <td align="right" valign="middle"><?php $dst_add+=$cel['dscto']; echo $cn->redondeo($cel['dscto']);?></td>            
    <td align="center" valign="middle">
      <?php
	if($cel['fecha']==$fecha_ayer || $cel['fecha']==$fecha_hoy){$checked_est='';
	}else{ /*$checked_est='disabled="disabled"';*/}
    ?>
      <input type="checkbox" onclick="activar('<?=$cel['id_cob']?>',$(this))" <?=(($cel['estado']=="1")?"checked":"")?> <?=$checked_est?> />
    </td>        
    <td align="center" valign="middle">
    <input type="button" onclick="eliminar('<?=$cel['id_cob']?>')" value="E" <?=$checked_est?> />
    </td>       
  </tr>
  <?php 
		if($cel['estado']=="1"){
		$acum_pen+=($cel['agregar']>0)?0:$intere;
		$acum_cob+=$cel['mnt_cob'];
		$acum_int+=$cel['interes'];
		$ppagar=($acum_pen-$acum_int);
			
		}
	$i++;	
	} 
	#echo "<br /><br />"; print_r($array_fechas);
	#echo "<br /><br />"; print_r($array_montos);
	$pendiente=$ult_add+$pago_real-$ult_mnt;
	?>

</tbody>
<tfoot>
  <tr>
    <th align="center">-</th>
    <th align="center">-</th>
    <th align="center">-</th>
    <th align="right"><?=number_format($acum_pen,2,'.',',')?></th>
    <th align="center">-</th>
    <th align="right"><?=number_format($acum_cob,2,'.',',')?></th>
    <th align="right"><?=number_format($acum_int,2,'.',',')?></th>
    <th align="right"><?=number_format($ppagar,2,'.',',')?></th>
    <th align="center">-</th>
    <th align="center">-</th>    
    <th align="right"><?=number_format($ult_add,2,'.',',') ?></th>
    <th align="right"><?=$cn->redondeo($dst_add)?></th>
    <th align="center">-</th>
    <th align="center">-</th>
  </tr>
  <tr>
    <th align="center" colspan="5">Totales</th>
    <th align="center" colspan="4">Cuota</th>
    <th align="center"></th>
    <th align="center"></th>
    <th align="center"></th>
    <th align="center"></th>    
    <th align="center"></th>    
  </tr>
  <tr>
    <th align="left" colspan="5">Total Capital Agregado</th>
    <th align="right">-</th>
    <th align="right">-</th>    
    <th align="right">-</th>        
    <th align="right">-</th>
    <th align="center"></th>
    <th align="right"><?=$cn->redondeo($ult_add)?></th>
    <th align="right">&nbsp;</th>    
    <th align="center"></th>
    <th align="center"></th>
  </tr>  
  <tr>
    <th align="left" colspan="5">Total Inter&eacute;s Pendiente</th>
    <th align="right">-</th>
    <th align="right">-</th>    
    <th align="right"><?=$cn->redondeo($ppagar)?></th>        
    <th align="right">-</th>
    <th align="center"></th>
    <th align="center"></th>
    <th align="center"></th>    
    <th align="center"></th>
    <th align="center"></th>
  </tr>    
  <tr>
    <th align="left" colspan="5">Total Capital e Inter&eacute;s Cobrado</th>
    <th align="right"><?=$cn->redondeo($acum1)?></th>
    <th align="right"><?=$cn->redondeo($acum_int)?></th>    
    <th align="right">-</th>        
    <th align="right">-</th>
    <th align="center"></th>
    <th align="center"></th>
    <th align="center"></th>
    <th align="center"></th>    
    <th align="center"></th>
  </tr>
  <tr style="background:#e2e4ff;">
    <th align="left" colspan="5">Total a Pagar a la Fecha de Cobro</th>
    <th align="right" style="border:1px solid #ff0000;">
	<?php
	/*if($fecha==date("d-m-Y")){
		$amort=end($array_montos);
		$amort=prev($array_montos);
		$pago_hoy=($montos+$amort)-$amort+$array_fechas[date("d-m-Y")];
		echo $cn->redondeo($pago_hoy);		
	}else{
		$pago_hoy=$montos-end($array_montos);
		echo $cn->redondeo($pago_hoy);
	}*/
	
		$amort=end($array_montos);
		$amort=prev($array_montos);
		
		$pago_hoy=$montos-$array_montos[$fecha]+$array_fechas[$fecha];
		$inte_hoy=($pago_hoy*$cel1['fra_pre'])/$cel1['mnt_pre'];
		$real_hoy=$inte_hoy+$ppagar;
		
		echo $cn->redondeo($pago_hoy);		
	?>
    </th>
    <th align="right"                                  ><?=$cn->redondeo($inte_hoy);?></th>    
    <th align="right" style="border:1px solid #ff0000;"><?=$cn->redondeo($real_hoy); ?></th>        
    <th align="right">-</th>
    <th align="center"></th>
    <th align="center"></th>
    <th align="center"></th>
    <th align="center"></th>
    <th align="center"></th>
  </tr>    
  <!-- tr>
    <th align="left" colspan="5">Total Pendiente Para Pr&oacute;ximo Pago</th>
    <th align="right" style="border:1px solid #ff0000;">
    <?php /*
	if($fecha==date("d-m-Y")){
		$pago_prx=$montos-$array_montos[date("d-m-Y")]+$array_fechas[date("d-m-Y")];
		echo $cn->redondeo($pago_prx);			
	}else{
		$pago_prx=$montos-end($array_montos);
		echo $cn->redondeo($pago_prx);	
	}
		#$pago_prx=$montos-$array_montos[date("d-m-Y")]+$array_fechas[date("d-m-Y")];
		#echo $cn->redondeo($pago_prx);		
	?>
    </th>
    <th align="right">									<?=$cn->redondeo(($pago_prx*$cel1['fra_pre'])/$cel1['mnt_pre']);?></th>
    <th align="right" style="border:1px solid #ff0000;"><?=$cn->redondeo(($pago_prx*$cel1['fra_pre'])/$cel1['mnt_pre']);*/ ?></th>
    <th align="right">-</th>
    <th align="center"></th>
    <th align="center"></th>
  </tr-->
  <tr>
  <?php if($row<=0){?>
	<th align="center" colspan="15" bgcolor="#ff0000"><h3 style="color:#ffffff;">Pr&eacute;stamo Pendiente</h3></th>  
  <?php }else{?>
  	<?php if($pago_hoy>0 || $inte_hoy>0 || $real_hoy>0){?>
    <th align="center" colspan="15" bgcolor="#ff0000"><h3 style="color:#ffffff;">Pr&eacute;stamo Pendiente</h3></th>
    <?php }else{?>
    <th align="center" colspan="15" bgcolor="#116194"><h3 style="color:#ffffff;">Pr&eacute;stamo Cancelado</h3></th>
	<?php }?>
  <?php }?>
  </tr>      
</tfoot>
</table>
<?php } ?>
<br />
<strong>Observaciones;</strong> <?=$cel1['observaciones']?>
</div>
</body>
</html>