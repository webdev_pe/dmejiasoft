<?php session_start(); 
/*header('Content-type: application/vnd.ms-excel');
header("Content-Disposition: attachment; filename=archivo.xls");
header("Pragma: no-cache");
header("Expires: 0");*/

header ("Content-type: application/x-msexcel");
header ("Content-Disposition: attachment; filename=sicpri_reporte_propiedades.xls" );	
header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");  
header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");  
header ("Cache-Control: no-cache, must-revalidate");  
header ("Pragma: no-cache");  

date_default_timezone_set("America/Lima");
$fecha = date("d-m-Y");
$hora  = date("H:i:s");		

require("../../poo/clases/getConection.php");
$cn=new getConection();
$sql=utf8_decode(base64_decode($_GET['s']));
$cn->ejecutar_sql(base64_encode($sql));
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>sicpri_reporte_usuarios</title>
</head>
<body>
    <!-- ############################################## inicio ############################################## -->  
	<table style="width: 100%;" align="center">
	<thead>
	   <tr bgcolor="#b9bdc4">    
	        <th style="width: 2%; text-align: center;">N&ordm;</th> 
	        <th style="width: 8%; text-align: center;">CODIGO</th>
	        <th style="width: 8%; text-align: center;">FECHA REG.</th>
	        <th style="width: 10%; text-align: center;">ESTADO</th>
            <th style="width: 5%; text-align: center;">TIPO</th>
   	        <th style="width: 10%; text-align: center;">UBICACI&Oacute;N</th>                   
   	        <th style="width: 10%; text-align: center;">OPERACI&Oacute;N</th>
   	        <th style="width: 10%; text-align: center;">PRECIO</th>
            <th style="width: 10%; text-align: center;">AREA TOTAL</th>
            <th style="width: 10%; text-align: center;">AREA CONST</th>   
        </tr>     
	</thead>
	<tbody>
    <?php $i=1; while($cell=$cn->resultado_sql()){ ?>
      <tr bgcolor="<?=($i%2==0)?"#ffffff":"#e2e4ff"?>">    
        <td align="center"><?=$i++?></td>        
        <td align="center"><?=$cell['cod_prp']?></td>
        <td align="center"><?=$cell['fecha']?></td>    
        <td align="center"><?=$cell['est']?></td>
        <td align="center">
		<?php 
		echo ($cell['residencial']==1)?" R":"";
		echo ($cell['comercial']==1)?" C":"";
		echo ($cell['industrial']==1)?" I":"";
		echo ($cell['terreno']==1)?" T":"";
		echo ($cell['proyecto']==1)?" P":"";
		?>
        </td>
        <td align="left"><?=$cell['nom_dep']." - ".$cell['nom_prv']." - ".$cell['nom_dst'];?></td>
        <td align="center"><?=$cell['opr']?></td>    
        <td align="right"><span><?=($cell['mon_prp']=="$")?$cell['mon_prp']:"S/."?></span><?=number_format($cell['prc_prp'],2,".",",")?></td>  
        <td align="right"><?=$cell['art_prp']?></td>  
        <td align="right"><?=$cell['act_prp']?></td>   
      </tr>
    <?php } $cn->limpiar_sql(); $cn->cerrar_sql(); ?>
    </tbody>
    </table>
    <!-- ############################################## inicio ############################################## -->    
</body>
</html>