<script type="text/javascript">
$(document).ready(function() {
	$.datepicker.setDefaults($.datepicker.regional["es"]);
	$("#pre_fch").datepicker({
		dateFormat: "dd-mm-yy",
		numberOfMonths: 3,		
		showButtonPanel: true/*, showOn: "button", buttonImage: "img/calendar.gif", buttonImageOnly: true */
	});
		
			
	$("#pre_cie").buttonset();
	$("#cod_cli").autocomplete({
		source: "sicpri/02_prestamos/_buscar_codigo.php",
		minLength: 2, 
		select: function(event,ui){ 
			var c=ui.item.value;
			$("#nom_cli").val(ui.item.id);
			$.post("sicpri/01_cobranzas/traer_prestamos.php",{c:c},function(data){
				$("#pre_vig").empty().html(data);
				$("#pre_tip").empty();
				$("#pre_pago").hide();
				$("#pre_cie").hide();
			});			
		}
	});
	
	$("#nom_cli").autocomplete({
		source: "sicpri/02_prestamos/_buscar_nombre.php",
		minLength: 2, 
		select: function(event,ui){
			var c=ui.item.id;
			$("#cod_cli").val(ui.item.id);
			$.post("sicpri/01_cobranzas/traer_prestamos.php",{c:c},function(data){
				$("#pre_vig").empty().html(data);
				$("#pre_tip").empty();
				$("#pre_pago").hide();
				$("#pre_cie").hide();
			});	
		}
	});	
	
	$("#save").click(function(){
		var idp=$("#idp").val();
		var dro=$("#dro").val();
		var mnt=$("#pre_mnt").val();
		var fra=$("#pre_int").val();
		var dto=($("#pre_dto").val()!="")?parseFloat($("#pre_dto").val()):0;
		var add=($("#pre_add").val()!="")?parseFloat($("#pre_add").val()):0;
		var saldo=($("#saldo").val()!="")?parseFloat($("#saldo").val()):0;
		var cie=$("input[name=modo]:radio:checked").val();
		var tip=$("#tip").val();
		var mto=$("#monto").val();
		var inr=$("#inter").val();
		var fch=$("#pre_fch").val();

		$.post("sicpri/01_cobranzas/dao.php",{opt:"i",idp:idp,dro:dro,mnt:mnt,fra:fra,add:add,cie:cie,tip:tip,mto:mto,inr:inr,dto:dto,cob:"sistema",fch:fch},function(data){
			//$("#jajaja").html(data);
			if(data==1){ 
				if(fch==""){
					alert("Ingrese una fecha de regitro de cobranza.");
					$("#pre_fch").focus();
				}else{
					alert("Cobranza registrada correctamente.");
					$.post("sicpri/01_cobranzas/01_registrar.php",function(data){
						$("#contenido_sicpri").html(data);
						$("#sicpri_tit").empty().text("REGISTRO DE COBRANZAS");
					});
				}
			}else{
				alert("Vuelva a intentarlo por favor.");
			}
		});

	});	
	$("#hist").click(function(){
		$.post("sicpri/01_cobranzas/06_historial.php",function(data){
			$("#contenido_sicpri").html(data);
			$("#sicpri_tit").empty().text("HISTORIAL DE COBRANZAS");
		});
	});	
	
	$("input[name=modo]:radio").click(function(){ 
		var t=$("#tip").val();
		switch($(this).attr("id")){
			case "md01":
				$("#pre_pago").hide();
				$("#pre_frac").hide();
				$("#pre_agre").hide();
				$("#pre_dsto").hide();
				$("#pre_mnt").val("0");
				$("#pre_int").val("0");
			break;
			case "md02": 
				$("#pre_pago").show(); 
				$("#pre_frac").hide();
				$("#pre_dsto").hide();
				$("#pre_agre").hide();
				$("#pre_mnt").val(""); 
				$("#pre_int").val("");
				$("#pre_add").val("0");				
			break;	
			case "md03": 
				$("#pre_pago").show(); 
				if(t=="a") $("#pre_frac").show();
				if(t=="a") $("#pre_dsto").show();
				$("#pre_agre").hide(); 	
				$("#pre_mnt").val(""); 
				$("#pre_int").val("0"); 
				$("#pre_add").val("0"); 
			break;
			case "md04": 
				$("#pre_pago").hide(); 
				if(t=="a") $("#pre_frac").hide();
				if(t=="a") $("#pre_dsto").hide();
				$("#pre_agre").show(); 	
				$("#pre_mnt").val("0"); 
				$("#pre_int").val("0"); 			
			break;
		}
	});
});
</script>
<div id="jajaja"></div>
<input type="hidden" id="idp" />
<input type="hidden" id="dro" />
<input type="hidden" id="tip" />
<input type="hidden" id="saldo" />
<input type="hidden" id="monto" />
<input type="hidden" id="inter" />
<table width="100%" border="0" id="tbl_prp">
  <tr>
    <td colspan="2">
    
<table width="100%" border="0">
  <tr>
    <td width="33%" align="left" valign="top">
	<strong>Cliente</strong><br />
    C&oacute;digo<br /><input type="text" id="cod_cli" /><br />
    Nombre<br /><input type="text" id="nom_cli" /><br />
    <div id="pre_vig"></div>        
	</td>
    <td width="33%" align="left" valign="top"><input type="hidden" id="pre_id" />
      <div id="pre_tip"></div> 
      <div id="pre_cie" style="display:none;">
		<input type="radio" value="np" name="modo" id="md01"/><label for="md01">No pago</label>
        <input type="radio" value="fi" name="modo" id="md02" /><label for="md02">Refinanciar</label>
        <input type="radio" value="pg" name="modo" id="md03" checked="checked" /><label for="md03">Pago</label>      
        <input type="radio" value="pa" name="modo" id="md04" /><label for="md04" id="lb04">Agregar</label>
      </div>
      <div id="pre_fecha" style="display:none;">
      	<strong>Fecha de registro</strong><br /><input type="text" id="pre_fch" value="<?=date("d-m-Y")?>" />
      </div>            
      <div id="pre_pago" style="display:none;">
      	<strong>Capital</strong><br /><input type="text" id="pre_mnt" value="0" />
      </div> 
      <div id="pre_frac" style="display:none;">
      	<strong>Inter&eacute;s</strong><br /><input type="text" id="pre_int" value="0" />
      </div>
      <div id="pre_dsto" style="display:none;">
      	<strong>Descuento</strong><br /><input type="text" id="pre_dto" value="0" />
      </div>      
      <div id="pre_agre" style="display:none;">
      	<strong>Agregar capital</strong><br /><input type="text" id="pre_add" value="0" />
      </div>       
    </td>
    <td width="34%" align="left" valign="top">
    </td>
    </tr>    
  </table>

    </td>
  </tr>
  <tr>
    <td>    
    </td>
  </tr>  
  <tr>
    <td>
<button id="save" class="btn"><span class="ui-icon ui-icon-disk"></span>Guardar nueva cobranza</button>
<button id="hist" class="btn"><span class="ui-icon ui-icon-disk"></span>Historial de cobranzas</button>
    </td>
  </tr>
</table>