<script type="text/javascript">
$(document).ready(function(){
	$("#radio_prestamo").buttonset();
	$("#operador").load("sicpri/02_prestamos/operadores.php",function(data){
		$(this).selectmenu();
	});
	$("#cod_pre").autocomplete({
		source: "sicpri/02_prestamos/_buscar_prestamo.php",
		minLength: 2
	});	
	
	$("#cod_cli").autocomplete({
		source: "sicpri/02_prestamos/_buscar_codigo.php",
		minLength: 2, 
		select: function(event,ui){ 
			var c=ui.item.value;
			$("#nom_cli").val(ui.item.id);		
		}
	});
	
	$("#nom_cli").autocomplete({
		source: "sicpri/02_prestamos/_buscar_nombre.php",
		minLength: 2, 
		select: function(event,ui){
			var c=ui.item.id;
			$("#cod_cli").val(ui.item.id);
		}
	});	

	$("#cbo_gal").selectmenu();	

	$("#cbo_zon").load("sicpri/funciones/getZonas.php", function(data){
		$(this).selectmenu();
	});	

	$("#operador").change(function(data){
		$("#operador option:selected").each(function(){
			var t=$(this).text();
			$("#otx").val(t);	
		});
	});

	$("#cbo_zon").change(function(data){
		$("#cbo_zon option:selected").each(function(){
			var i=$(this).val();
			var t=$("#cbo_zon option:selected").text();
			$("#ztx").val(t);
			$("#cbo_gal").load("sicpri/funciones/getGalerias.php",{i:i},function(data){
				$(this).selectmenu();
			});	
		});
	});

	$("#cbo_gal").change(function(data){
		$("#cbo_gal option:selected").each(function(){
			var t=$("#cbo_gal option:selected").text();
			$("#gtx").val(t);	
		});
	});	
		
	$.datepicker.setDefaults($.datepicker.regional["es"]);
	$("#fec_dsd").datepicker({
		dateFormat: "dd-mm-yy",
		numberOfMonths: 3,		
		showButtonPanel: true/*, showOn: "button", buttonImage: "img/calendar.gif", buttonImageOnly: true */
	});
	$("#fec_hst").datepicker({dateFormat: "dd-mm-yy", numberOfMonths: 3,	showButtonPanel: true});	

	/*$("#genr").click(function(){
		var cod=$("#cod_pre").val();
		var cdc=$("#cod_cli").val();
		var nom=$("#nom_cli").val();
		var dsd=$("#fec_dsd").val();
		var hst=$("#fec_hst").val();
		var tip=$("input[name=prest]:radio:checked").val();
		var opr=$("#operador").val();
		var otx=$("#operador option:selected").text();
		var zon=$("#cbo_zon").val();
		var ztx=$("#cbo_zon option:selected").text();
		var gal=$("#cbo_gal").val();		
		var gtx=$("#cbo_gal option:selected").text();
						
		$.post("sicpri/01_cobranzas/07_reportes.php",{cod:cod,cdc:cdc,nom:nom,dsd:dsd,hst:hst,tip:tip,opr:opr,otx:otx,zon:zon,gal:gal,ztx:ztx,gtx:gtx,r:"r"},function(data){
			$("#contenido_sicpri").html(data);
			$("#sicpri_tit").empty().text("RESULTADO DE BUSQUEDA");
		});		
	});*/	
	
	$("#hist").click(function(){
		$.post("sicpri/01_cobranzas/07_reportes.php",function(data){
			$("#contenido_sicpri").html(data);
			$("#sicpri_tit").empty().text("HISTORIAL DE PRESTAMOS");
		});
	});	
});
</script>
<form method="post" target="_blank" action="sicpri/01_cobranzas/reporte.php">
<table width="100%" border="0" id="tbl_prp">
  <tr>
    <td colspan="2">
        <table width="100%" border="0">
          <tr>
            <td width="35%"><strong>Datos de b&uacute;squeda</strong></td>
            <td width="25%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
          </tr>
          <tr>
            <td align="left" valign="top">
            Codigo de prestamo<br /><input type="text" id="cod_pre" name="cod" /><br />
            Codigo de cliente<br /><input type="text" id="cod_cli" name="cdc" /><br />
            Nombre de cliente<br/><input type="text" id="nom_cli" name="nom" /><br />

            Fecha desde<br/><input type="text"  id="fec_dsd" name="dsd"/><br />
            Fecha hasta<br/><input type="text" id="fec_hst" name="hst" /><br />
            Tipo de prestamo<br />
    		<div id="radio_prestamo">
	       	<input type="radio" value="t" name="tip" id="all" checked="checked" /><label for="all">Todos</label>
	       	<input type="radio" value="p" name="tip" id="opp" /><label for="opp">Consignaci&oacute;n por mes</label>
	        <input type="radio" value="a" name="tip" id="opa" /><label for="opa">Consignaci&oacute;n por d&iacute;a</label>
	        </div><br />
            Operador<br /><select id="operador" name="opr"></select><br />
            Seleccione Zona<br/><select id="cbo_zon" name="zon"><option value="nn">[-Seleccione-]</option></select><br />
            Seleccione Galer&iacute;a<br/><select id="cbo_gal" name="gal"><option value="nn">[-Sin galeria-]</option></select>

            <input type="hidden" value="" id="otx" name="otx">
            <input type="hidden" value="" id="ztx" name="ztx">
            <input type="hidden" value="" id="gtx" name="gtx">

            </td>
            <td align="left" valign="top">&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
          </tr>
        </table>
    </td>
  </tr> 
  <tr>
    <td>
	<button id="genr" class="btn" type="submit"><span class="ui-icon ui-icon-search"></span>Generar resultados para reporte</button>
	<button id="hist" class="btn"><span class="ui-icon ui-icon-folder-open"></span>Historial de prestamos</button>    
    </td>
  </tr>
</table>
</form>