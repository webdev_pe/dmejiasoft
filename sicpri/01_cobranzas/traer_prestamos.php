<script type="text/javascript">
$(document).ready(function(){
	$("#pre_vig").find(":button").each(function(){
		$(this).click(function(){
			var id=$(this).attr("id");
			if(id!=""){
				$("#pre_vig").find(":button").removeClass().addClass("btn");
				$(this).removeClass().addClass("menu_ac");
				$("#idp").val(id);
				$.post("sicpri/01_cobranzas/traer_detalle.php",{id:id},function(data){				
					if(data.tip=="p"){
						$("#pre_tip").empty().html("<strong>Cr&eacute;dito Consignaci&oacute;n por Mes ("+data.cod+")</strong>");
						$("#pre_cie").fadeIn("fast");	
						$("#dro").val(data.dro);
						$("#pre_pago").slideDown("fast");
						$("#pre_fecha").slideDown("fast");
						$("#pre_int").val("");
						$("#pre_frac").slideUp("fast");
						$("#tip").val("p");
						$("#md04").hide();
						$("#lb04").hide();
					}else{
						$("#pre_tip").empty().html("<strong>Cr&eacute;dito Consignaci&oacute;n por D&iacute;a ("+data.cod+")</strong>");
						$("#pre_cie").fadeIn("fast");
						$("#pre_pago").slideDown("fast");
						$("#pre_fecha").slideDown("fast");
						$("#pre_frac").slideDown("fast");
						$("#pre_dsto").slideDown("fast");
						$("#tip").val("a");
						$("#md04").show();
						$("#lb04").show();
						//$("#pre_add").val(data.saldo);	
						$("#saldo").val(data.saldo);
						$("#monto").val(data.monto);
						$("#inter").val(data.inter);						
					}
				},"json");
			}
		});
	});
	verificar=function(i){
	  var width=700;
	  var height=587;
	  var posx=$(window).width()/2 - width/2;
	  var posy=$(window).height()/2 - height/2;
	  var opciones=("toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, fullscreen=no, width="+width+", height="+height+", top="+posy+", left="+posx); 
	  window.open("sicpri/01_cobranzas/visualizar.php?i="+i,"SIPC",opciones);		
	}
});
</script>
<?php
require("../../poo/clases/getConection.php");
$cod = $_POST['c'];
$cn=new getConection();

$sql="select p.id_pre, p.cod_pre, p.mnt_pre, p.id_cli, p.tip_pre, p.est_pre, moroso
from si_clientes c, si_prestamos p 
where c.id_cli=p.id_cli and est_pre='a' and
cod_cli='".$cod."'";	
$cn->ejecutar_sql(base64_encode($sql));
$row=$cn->cantidad_sql();

if($row>0){
	$buttons="
	<br />
	<strong>Seleccione un cr&eacute;dito a cobrar</strong><br />
	<table border='0'>
	";
	while($cel=$cn->resultado_sql()){
	$p=($cel['tip_pre']=="p")?"C/MES":"C/D&Iacute;A";
	$m=($cel['moroso']=="m")?"MOROSO":"";		
		$buttons.="
		<tr>
		<td>
		<button id='".$cel['id_pre']."' class='btn' style='width:100%; text-align:left;'>".$cel['cod_pre']." (".number_format($cel['mnt_pre'],2,'.',',').") - $p $m</button>
		</td>
		<td>
		<button class='btn' onclick=\"verificar('".$cel['id_pre']."')\"><span class='ui-icon ui-icon-zoomin'></span></button>
		</td>
		</tr>
		";
	}
	$buttons.="</table>";	
}else{
	$buttons="<strong>No se encontraron cr&eacute;ditos vigentes asociados a este cliente.</strong>";
}
echo $buttons;
?>