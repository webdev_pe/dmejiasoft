<?php session_start(); set_time_limit(0); ?>
<?php include("../rpt_head.php"); #echo $sql; ?>
	<div id="cabecera">
     <div id="logo"><img src="../../images/login/logo.png" width="106" height="106" /></div>
     <div id="descripcion">
     <strong>REPORTE DE COBRANZAS</strong><br /><hr />
     <strong>Generado el:</strong> <?=date("d-m-Y")?> - <strong>a las</strong> <?=date("H:m:s")?><hr />
<?php if($_GET['cp']!=""){?><strong>C&oacute;digo de pr&eacute;stamo:</strong> <?=base64_decode($_GET['cp'])?><br /><?php }?>
<?php if($_GET['cc']!=""){?><strong>C&oacute;digo de cliente</strong> <?=base64_decode($_GET['cc'])?><br /><?php }?>
<?php if($_GET['nc']!=""){?><strong>Nombre del cliente:</strong> <?=base64_decode($_GET['nc'])?><br /><?php }?>
<?php if($_GET['fd']!=""){?><strong>Fecha de reporte desde:</strong> <?=date("d-m-Y",strtotime(base64_decode($_GET['fd'])))?><br /><?php }?>
<?php if($_GET['fh']!=""){?><strong>Fecha de reporte hasta:</strong> <?=date("d-m-Y",strtotime(base64_decode($_GET['fh'])))?><br /><?php }?>
<?php if(base64_decode($_GET['tp'])!=""){?><strong>Tipo de pr&eacute;stamo:</strong> 
	<?php
    switch(base64_decode($_GET['tp'])){
		case "a": echo "Arrebatir"; break;
		case "p": echo "Porcentual"; break;
		case "t": echo "Arrebatir/Porcentual"; break;
	}
	?><br /><?php }?>
<?php if(base64_decode($_GET['op'])!="[-Seleccione-]" && base64_decode($_GET['op'])!=""){?><strong>Operador:</strong> <?=base64_decode($_GET['op'])?><br /><?php }?>
<?php if(base64_decode($_GET['zn'])!="[-Seleccione-]" && base64_decode($_GET['zn'])!=""){?><strong>Zona:</strong> <?=base64_decode($_GET['zn'])?><br /><?php }?>
<?php if(base64_decode($_GET['gl'])!="[-Seleccione-]" && base64_decode($_GET['gl'])!="" && base64_decode($_GET['gl'])!="[-Sin galeria-]"){?><strong>Galer&iacute;a:</strong> <?=base64_decode($_GET['gl'])?><br /><?php }?>
     </div>
  </div>
  <div id="cuerpo">
<style type="text/css" media="screen">
th.ui-state-default{font-size:8px; color:#000000;}
.list_tbl{ font-size:8px; font-family:Verdana;}
.list_tbl tr,.list_tbl th,.list_tbl td{ font-size:8px; color:#000000;}
span{ float:left; font-size:8px; color:#116194;}
.cobro{ background:#acdd4a; color:#ffffff;}
.saldo{ background:#ffe87b; color:#116194;}
</style>  
<style type="text/css" media="print">
th.ui-state-default{font-size:8px; color:#000000;}
.list_tbl{ font-size:8px; color:#000000;}
.list_tbl th,.list_tbl td{ font-size:8px; color:#000000;}
span{ float:left; font-size:8px; color:#000000;}
.cobro, .saldo{ font-weight:normal; color:#000000;}
</style> 

  <table class="list_tbl" width="100%">
	<thead> 
        <tr>    
	        <th colspan="8" class="ui-state-default">MOVIMIENTOS REALIZADOS SOLICITADOS DE CAJA</th>
        </tr>      
        <tr>    
	        <th class="ui-state-default">N&ordm;</th>
          <th class="ui-state-default">NOMBRE DEL OPERADOR</th>
	        <th class="ui-state-default">CODOPR</th>
	        <th class="ui-state-default">FECHA</th>
	        <th class="ui-state-default">HORA</th>
	        <th class="ui-state-default">MONTO</th>
            <th class="ui-state-default">TOTAL</th>
        </tr>   
    </thead>
	<tbody>
	<?php 
	$sqlc=utf8_decode(base64_decode($_GET['sc']));
	$cnc=new getConection();
	$cnc->ejecutar_sql(base64_encode($sqlc));
	$row=$cnc->cantidad_sql();
	
	if($row>0){
		$i=1;
		while($cell=$cnc->resultado_sql()){	
		?>
		  <tr>    
			<td align="center"><?=$i?></td> 
			<td align="left"><?=$cell['nombres']?></td>
			<td align="center"><?=$cell['cod_usu']?></td>
			<td align="center"><?=$cell['fecha']?></td>
			<td align="center"><?=$cell['hora']?></td>
			<td align="right" class="cobro"><span>S/.</span><?=$cnc->redondeo($cell['monto'])?></td>      
			<td align="right" class="cobro"><span>S/.</span><?=$cnc->redondeo($cell['monto'])?></td>        
	
		  </tr>
		<?php
			$montosc+=$cell['monto'];
		}
		$cnc->limpiar_sql();
		$cnc->cerrar_sql();
	}else{
		$sqlcc=utf8_decode(base64_decode($_GET['scc']));
		$cncc=new getConection();
		$cncc->ejecutar_sql(base64_encode($sqlcc));	
		$i=1;
		while($cell=$cncc->resultado_sql()){	
		?>
		  <tr>    
			<td align="center"><?=$i?></td> 
			<td align="left"> <?=$cell['nombres']?></td>
			<td align="center"><?=$cell['cod_usu']?></td>
			<td align="center"><?=$cell['fecha']?></td>
			<td align="center"><?=$cell['hora']?></td>
			<td align="right" class="cobro"><span>S/.</span><?=$cnc->redondeo($cell['monto'])?></td>      
			<td align="right" class="cobro"><span>S/.</span><?=$cnc->redondeo($cell['monto'])?></td>        
	
		  </tr>
		<?php
			$montosc+=$cell['monto'];
		}
		$cncc->limpiar_sql();
		$cncc->cerrar_sql();		
	}
    ?>
    </tbody>
	<tfoot>    
        <tr bgcolor="#ededed">    
	        <td colspan="3" align="center" style="color:#116194;">TOTALES</td>
	        <td align="center" style="color:#116194;">---</td>
            <td align="center" style="color:#116194;">---</td>
            <td align="right" class="cobro" style="color:#116194;"><span>S/.</span><?=$cnc->redondeo($montosc)?></td>         
			<td align="right" class="cobro" style="color:#116194;"><span>S/.</span><?=$cnc->redondeo($montosc)?></td>
        </tr>           
	</tfoot>      
    </table>
<style type="text/css" media="screen">
.wind{ width:100%; text-align:center; cursor:pointer; color: #ff0000;}
</style>
<style type="text/css" media="print">
.wind{ display:none;}
</style>
<script type="text/javascript">
$(document).ready(function(){
	$(".wind").click(function(){
		var i=$(this).attr("id");
		var width=700;
		var height=590;
		var posx=$(window).width()/2 - width/2;
		var posy=$(window).height()/2 - height/2;
		var opciones=("toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, fullscreen=no, width="+width+", height="+height+", top="+posy+", left="+posx); 
		window.open("visualizar.php?i="+i,"SIPC",opciones);	  	
	});
});
</script>
  <table class="list_tbl" width="100%">
	<thead>
        <tr>    
	        <th colspan="12" class="ui-state-default">MOVIMIENTOS REALIZADOS POR COBRANZAS</th>
        </tr>     
        <tr>    
	        <th rowspan="2" class="ui-state-default">N&ordm;</th>
	        <th rowspan="2" class="ui-state-default">NOMBRE DEL CLIENTE - (ZONA-GALER&Iacute;A)<br />FECHA/CODCLI/CODPRE</th>
	        <th rowspan="2" class="ui-state-default">TIPO</th>
	        <th class="ui-state-default">PORCENTAJE</th>
            <th class="ui-state-default">PORCENTAJE</th>
            <th colspan="2" class="ui-state-default">PAGO ARREBATIR</th>
            <th colspan="2" class="ui-state-default">SALDO POR COBRAR</th>            
            <th rowspan="2" class="ui-state-default">DESCUENTO</th>
            <th rowspan="2" class="ui-state-default">TOTAL</th>
        </tr>   
        <tr>
          <th class="ui-state-default">CUOTA</th>
	        <th class="ui-state-default">SALDO</th>
            <th class="ui-state-default">CAPITAL</th>
            <th class="ui-state-default">INTER&Eacute;S</th>
            <th class="ui-state-default">CAPITAL</th>
            <th class="ui-state-default">INTER&Eacute;S</th>            
        </tr>             
	</thead>
	<tbody>
    <?php $i=1;
    while($cell=$cn->resultado_sql()){
		$interes_cobrar=($cell['a']-$cell['b']>0)?$cell['a']-$cell['b']:0;
		$interes_cobrar=$interes_cobrar+$cell['c'];
	?>
      <tr>    
        <td align="center"><?=$i?><br /><span class="wind" id="<?=$cell['id_pre']?>">ver</span></td> 
        <td align="left"> <?=$cell['nombres']?> - (<?=$cell['nom_zon']?>-<?=$cell['nom_gal']?>)<br />
        <?=date("d-m-y",strtotime($cell['fecha']))?> / <?=$cell['cod_cli']?> / <?=$cell['cod_pre']?></td>
        <td align="center"><?=(($cell['tip_pre']=='p')?"P":"A")?></td>
        <?php 
		if($cell['tip_pre']=='p'){
		$total=$cell['mnt_cob'];
		$acum1+=$total;
		$acum9+=$cell['saldo_porcetaje'];		
		?>
        <td align="right" class="cobro"><span>S/.</span><?=$cn->redondeo($cell['mnt_cob'])?></td>
        <td align="right" class="saldo"><span>S/.</span><?=$cn->redondeo($cell['saldo_porcetaje'])?></td>        
        <td align="right" class="cobro">---</td>
        <td align="right" class="cobro">---</td>        
        <td align="right" class="saldo">---</td>
        <td align="right" class="saldo">---</td>        
        <?php
		}else{
		$total=$cell['mnt_cob']+$cell['interes']-$cell['dscto'];
		$acum2+=$cell['mnt_cob'];
		$acum3+=$cell['interes'];
		?>
        <td align="right" class="cobro">---</td>
        <td align="right" class="saldo">---</td>
        <td align="right" class="cobro"><span>S/.</span><?=$cn->redondeo($cell['mnt_cob'])?></td>
        <td align="right" class="cobro"><span>S/.</span><?=$cn->redondeo($cell['interes'])?></td>  
        <td align="right" class="saldo"><span>S/.</span><?=$cn->redondeo(($cell['capital_cobrar']>0?$cell['capital_cobrar']:0))?></td>
        <td align="right" class="saldo"><span>S/.</span><?=$cn->redondeo(($interes_cobrar>0?$interes_cobrar:0))?></td>                 
		<?php
		$acum7+=$cell['capital_cobrar']>0?$cell['capital_cobrar']:0;
		$acum8+=$interes_cobrar>0?$interes_cobrar:0;			
		}
		$acum5+=$cell['agregar'];
		$acum6+=$cell['dscto'];
		?>
        <td align="right"><span>S/.</span><?=$cn->redondeo($cell['dscto'])?></td>
        <td align="right" class="cobro"><span>S/.</span><?=$cn->redondeo($total)?></td>
      </tr>
      <?php	$i++;
	}
	$cn->limpiar_sql();
	$cn->cerrar_sql();
	$acum4=$acum1+$acum2+$acum3-$acum6;
    ?>   
    </tbody>
	<tfoot>    
        <tr bgcolor="#ededed">    
	        <td colspan="3" align="center" style="color:#116194;">TOTALES</td>
	        <td align="right" class="cobro" style="color:#116194;"><span>S/.</span><?=$cn->redondeo($acum1)?></td>
            <td align="right" class="saldo" style="color:#116194;"><span>S/.</span><?=$cn->redondeo($acum9)?></td>
            <td align="right" class="cobro" style="color:#116194;"><span>S/.</span><?=$cn->redondeo($acum2)?></td>                
            <td align="right" class="cobro" style="color:#116194;"><span>S/.</span><?=$cn->redondeo($acum3)?></td>
			<td align="right" bgcolor="#ffe87b" style="color:#116194;"><span>S/.</span><?=$cn->redondeo($acum7)?></td>
            <td align="right" bgcolor="#ffe87b" style="color:#116194;"><span>S/.</span><?=$cn->redondeo($acum8)?></td>
			<td align="right" style="color:#116194;"><span>S/.</span><?=$cn->redondeo($acum6)?></td>            
            <td align="right" class="cobro" style="color:#116194;"><span>S/.</span><?=$cn->redondeo($acum4)?></td>
        </tr>          
	</tfoot>    
    </table>
    
  <table class="list_tbl" width="100%">
	<thead> 
        <tr>    
	        <th colspan="12" class="ui-state-default">MOVIMIENTOS REALIZADOS POR PR&Eacute;STAMOS</th>
        </tr>      
        <tr>    
	        <th rowspan="2" class="ui-state-default">N&ordm;</th>
	        <th rowspan="2" class="ui-state-default">NOMBRE DEL CLIENTE - (ZONA-GALER&Iacute;A)<br />FECHA/CODCLI/CODPRE</th>
	        <th rowspan="2" class="ui-state-default">TIPO</th>
	        <th colspan="3" class="ui-state-default">PORCENTAJE</th>
            <th colspan="2" class="ui-state-default">ARREBATIR</th>
            <th rowspan="2" class="ui-state-default">AGREGADO</th>            
            <th rowspan="2" class="ui-state-default">ORIGEN</th>
            <th rowspan="2" class="ui-state-default">TOTAL</th>
        </tr>   
        <tr>
          <th class="ui-state-default">MONTO</th>
	        <th class="ui-state-default">D&Iacute;AS</th>
            <th class="ui-state-default">INTER&Eacute;S</th>
            <th class="ui-state-default">MONTO</th>
            <th class="ui-state-default">CUOTA</th>
        </tr>              
	</thead>
	<tbody>
	<?php 
	$sqlp=utf8_decode(base64_decode($_GET['sp']));
	$cnp=new getConection();
	$cnp->ejecutar_sql(base64_encode($sqlp));
	$cnp->cantidad_sql();
	#echo $sqlp;
	$j=1;
    while($cell=$cnp->resultado_sql()){	
	?>   
      <tr>    
        <td align="center"><?=$j++?></td> 
        <td align="left"> <?=$cell['nombres']?> - (<?=$cell['nom_zon']?>-<?=$cell['nom_gal']?>)<br />
        <?=date("d-m-y",strtotime($cell['fecha']))?> / <?=$cell['cod_cli']?> / <?=$cell['cod_pre']?></td>
        <td align="center"><?=(($cell['tipo']=='Porcentaje')?"P":"A")?></td>

        <?php 
		if($cell['tipo']=='Porcentaje'){
		
		?>
        <td align="right" class="cobro"><span>S/.</span><?=number_format($cell['mnt_pre'],2,'.',',')?></td>
        <td align="right" class="saldo"><?=$cell['dia_pre']?></td>
        <td align="right" class="saldo"><?=$cell['int_pre']?></td>
        <td align="right" class="cobro">---</td>        
        <td align="right" class="saldo">---</td>    
        <?php
		$acum1p+=$cell['mnt_pre'];
		}else{
		
		?>
        <td align="right" class="cobro">---</td>
        <td align="right" class="saldo">---</td>
        <td align="right" class="saldo">---</td>
        <td align="right" class="cobro"><span>S/.</span><?=number_format($cell['mnt_pre'],2,'.',',')?></td>  
        <td align="right" class="saldo"><span>S/.</span><?=number_format($cell['fra_pre'],2,'.',',')?></td>                       
		<?php	
		$acum2p+=$cell['mnt_pre'];	
		}
		?>     

        <td align="right" class="cobro"><span>S/.</span><?=number_format($cell['agregar'],2,'.',',')?></td>
        <td align="center"><?=$cell['titulo']?></td>  
        <td align="right" class="cobro"><span>S/.</span><?=number_format(($cell['mnt_pre']<=0)?$cell['agregar']:$cell['mnt_pre'],2,'.',',')?></td>       
      </tr>
    <?php
		$montos+=($cell['mnt_pre']<=0)?$cell['agregar']:$cell['mnt_pre'];
		$agrega+=$cell['agregar'];
	}
	$acum3p=$acum1p+$acum2p+$agrega;
	$cnp->limpiar_sql();
	$cnp->cerrar_sql();
    ?>
    </tbody>
	<tfoot>    
        <tr bgcolor="#ededed">    
	        <td colspan="3" align="center" style="color:#116194;">TOTALES</td>
	        <td align="right" class="cobro" style="color:#116194;"><span>S/.</span><?=number_format($acum1p,2,'.',',')?></td>
            <td align="right" class="saldo" style="color:#116194;">--</td>
            <td align="right" class="saldo" style="color:#116194;">---</td>
            <td align="right" class="cobro" style="color:#116194;"><span>S/.</span><?=number_format($acum2p,2,'.',',')?></td>            
			<td align="right" bgcolor="#ffe87b" style="color:#116194;">---</td>
            <td align="right" class="cobro" style="color:#116194;"><span>S/.</span><?=number_format($agrega,2,'.',',')?></td>
			<td align="right" style="color:#116194;">---</td>            
            <td align="right" class="cobro" style="color:#116194;"><span>S/.</span><?=number_format($acum3p,2,'.',',')?></td>
        </tr>           
	</tfoot>      
    </table>

  <table class="list_tbl" width="100%">
	<thead> 
        <tr>    
	        <th colspan="4" class="ui-state-default">MOVIMIENTOS TOTALIZADOS</th>
        </tr>     
        <tr>    
	        <th class="ui-state-default">N&ordm;</th>
	        <th class="ui-state-default">NOMBRE TOTAL</th>
	        <th class="ui-state-default">INGRESOS</th>
	        <th class="ui-state-default">EGRESOS</th>
        </tr>            
    </thead>
	<tbody>
      <tr>    
        <td align="center">1</td> 
        <td align="left">TOTAL SOLICITADO DE CAJA</td>
        <td align="right"><span>S/.</span><?=number_format($montosc,2,'.',',')?></td>   
        <td align="right">---</td>     
      </tr>
      <tr>    
        <td align="center">2</td> 
        <td align="left">TOTAL COBRANZA</td>
        <td align="right"><span>S/.</span><?=number_format(($acum1+$acum2+$acum3),2,'.',',')?></td>
        <td align="right">---</td>         
      </tr>
      <tr>    
        <td align="center">3</td> 
        <td align="left">TOTAL DESCONTADO</td>
        <td align="right">---</td> 
        <td align="right"><span>S/.</span><?=number_format($acum6,2,'.',',')?></td>        
      </tr>
      <tr>    
        <td align="center">4</td> 
        <td align="left">TOTAL AGREGADO</td>
        <td align="right">---</td> 
        <td align="right"><span>S/.</span><?=number_format($agrega,2,'.',',')?></td>        
      </tr>
      <tr>    
        <td align="center">5</td> 
        <td align="left">TOTAL PRESTADO</td>
        <td align="right">---</td> 
        <td align="right"><span>S/.</span><?=number_format($acum1p+$acum2p,2,'.',',')?></td>        
      </tr>
     <tfoot> 
      <tr bgcolor="#ededed">    
        <td colspan="2" align="center" style="color:#116194;">TOTALES</td>
        <td align="right" style="color:#116194;"><?php $ingresos=$montosc+($acum1+$acum2+$acum3); ?><span>S/.</span><?=number_format($ingresos,2,'.',',')?></td> 
        <td align="right" style="color:#116194;"><?php $egresos=$acum6+$agrega+$acum1p+$acum2p; ?><span>S/.</span><?=number_format($egresos,2,'.',',')?></td>        
      </tr>
      <tr bgcolor="#ededed">    
        <td colspan="2" align="center" style="color:#116194;">TOTALES</td>
        <td colspan="2" align="right" style="color:#116194;"><span>S/.</span><?=number_format(($ingresos-$egresos),2,'.',',')?></td>        
      </tr>       
    </tfoot>   
    </table>

 <table class="list_tbl" width="100%">
	<thead> 
        <tr>    
	        <th colspan="5" class="ui-state-default">MOVIMIENTOS TOTALIZADOS POR TIPO DE CR&Eacute;DITO</th>
        </tr>     
        <tr>    
	        <th class="ui-state-default">N&ordm;</th>
	        <th class="ui-state-default">NOMBRE TOTAL</th>
	        <th class="ui-state-default">CR&Eacute;DITOS</th>
	        <th class="ui-state-default">COBRANZAS</th>
	        <th class="ui-state-default">PENDIENTES</th>
        </tr>            
    </thead>
	<tbody>
      <tr>    
        <td align="center">1</td> 
        <td align="left">TOTAL CONSIGNACI&Oacute;N MES</td>
        <td align="right"><span>S/.</span><?=number_format($acum1p,2,'.',',')?></td>   
        <td align="right"><span>S/.</span><?=number_format($acum1,2,'.',',')?></td>
        <td align="right"><span>S/.</span><?=number_format($acum1p-$acum1,2,'.',',')?></td>     
      </tr>
      <tr>    
        <td align="center">2</td> 
        <td align="left">TOTAL CONSIGNACI&Oacute;N D&iacute;A</td>
        <td align="right"><span>S/.</span><?=number_format(($acum2p),2,'.',',')?></td>
        <td align="right"><span>S/.</span><?=number_format(($acum2+$acum3),2,'.',',')?></td>
        <td align="right"><span>S/.</span><?=number_format(($acum2p)-($acum2+$acum3),2,'.',',')?></td>         
      </tr>
     <tfoot> 
      <tr bgcolor="#ededed">    
        <td colspan="2" align="center" style="color:#116194;">TOTALES</td>
        <td align="right" style="color:#116194;"><?php $cres=($acum1p+$acum2p); ?><span>S/.</span><?=number_format($cres,2,'.',',')?></td> 
        <td align="right" style="color:#116194;"><?php $cobs=($acum1+$acum2+$acum3); ?><span>S/.</span><?=number_format($cobs,2,'.',',')?></td>
        <td align="right" style="color:#116194;"><?php $cobs=($acum1+$acum2+$acum3); ?><span>S/.</span><?=number_format($cres-$cobs,2,'.',',')?></td>        
      </tr>       
    </tfoot>   
    </table>        
  </div>
<?php include("../rpt_footer.php"); ?>