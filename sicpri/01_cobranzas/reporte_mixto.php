<?php session_start(); ?>
<?php include("../rpt_head.php"); ?>
	<div id="cabecera">
     <div id="logo"><img src="../../images/login/logo.png" width="106" height="106" /></div>
     <div id="descripcion">
     <strong>REPORTE MIXTO COBRANZAS Y PRESTAMOS DEL DIA</strong><br /><hr />
     <strong>Generado el:</strong> <?=date("d-m-Y")?> - <strong>a las</strong> <?=date("H:m:s")?><hr />
     </div>
  </div>
  <div id="cuerpo">
<style type="text/css">
.cobro{ background:#acdd4a; color:#ffffff;}
.saldo{ background:#ffe87b; color:#116194;}
</style>	  
  <table id="list_usu" class="list_tbl" width="100%">
	<thead>
        <tr>    
	        <th>N&ordm;</th>
	        <th>FECHA</th>	        
	        <th>CLIENTE</th>
	        <th width="200">NOMBRE DEL CLIENTE</th>
	        <th>OPER.</th>
            <th>PRES.</th>
            <th>TIPO</th>
            <th>MODO</th>            
            <th>MONTO</th>
            <th>INTERES</th>
	        <th>TOTAL COB.</th>
            <th>TOTAL PRE.</th>
            <th>TOTAL AGR.</th>        
        </tr>   
	</thead>
	<tbody>
    <?php 
	if(isset($_GET['s'])){
		$i=1;
		while($cell=$cn->resultado_sql()){ 
	?>
       <tr>    
        <td align="center"><?=$i?></td>
        <td align="center"><?=date("d/m/y",strtotime($cell['fecha']))?></td>    
        <td align="center"><?=$cell['cod_cli']?></td>
        <td align="left"><?=$cell['nombres']?> - (<?=$cell['nom_zon']?>-<?=$cell['nom_gal']?>)</td> 
        <td align="center"><?=$cell['cod_usu']?></td>
        <td align="center"><?=$cell['cod_pre']?></td>             
        <td align="center"><?=$cell['tipo']=="Porcentaje"?"POR":"ARR"?></td>
        <td align="center"><?=$cell['Modo']=="Cobranza"?"COB":"PRE"?></td>    
        <td align="right"><span>S/.</span><?=number_format($cell['Monto'],2,'.',',')?></td>
        <td align="right"><span>S/.</span><?=number_format($cell['interes'],2,'.',',')?></td>
		<?php if($cell['Modo']=="Cobranza"){?> 
        <td align="right"><span>S/.</span><?=number_format($cell['total'],2,'.',',')?></td> 
        <td align="right">---</td>
        <?php }else{?>
        <td align="right">---</td>
        <td align="right"><span>S/.</span><?=number_format($cell['total'],2,'.',',')?></td> 
        <?php }?>               
        <td align="right"><span>S/.</span><?=number_format($cell['agregado'],2,'.',',')?></td>             
      </tr>
    <?php 
		if($cell['Modo']=="Cobranza"){
			$cob+=$cell['total']; $agr+=$cell['agregado'];
		}else{
			$pre+=$cell['total'];
		}	
		$i++;
		} 
		$cn->limpiar_sql(); $cn->cerrar_sql();
	}
	?>
    </tbody> 
	<tfoot>
        <tr style="font-weight:bold; background:#e2e4ff;">    
	        <td colspan="8" align="left">TOTAL COBRADO: &raquo;</td>	        
            <td></td><td></td>
            <td align="right"><span>S/.</span><?=number_format($cob,2,'.',',')?></td>
            <td></td>
            <td></td>
        </tr>
        <tr style="font-weight:bold; background:#e2e4ff;">    
	        <td colspan="8" align="left">TOTAL PRESTADO: &raquo;</td>      
            <td></td><td></td>
            <td></td>
            <td align="right"><span>S/.</span><?=number_format($pre,2,'.',',')?></td>
            <td></td>
        </tr> 
        <tr style="font-weight:bold; background:#e2e4ff;">    
	        <td colspan="8" align="left">TOTAL AGREGADO: &raquo;</td>
	        <td></td><td></td>           
            <td></td>
            <td></td>
            <td align="right"><span>S/.</span><?=number_format($agr,2,'.',',')?></td>
        </tr>  
        <tr>    
	        <td colspan="13"><hr /></td>
        </tr>
        <tr style="font-weight:bold; background:#e2e4ff;">    
	        <td colspan="8"></td>
	        <td></td><td></td>            
            <td colspan="2">TOTAL ENTRANTE: &raquo;</td>
            <td align="right"><span>S/.</span><?=number_format($cob,2,'.',',')?></td>
        </tr>         
        <tr style="font-weight:bold; background:#e2e4ff;">    
	        <td colspan="8"></td>
	        <td></td><td></td>            
            <td colspan="2">TOTAL SALIENTE: &raquo;</td>
            <td align="right"><span>S/.</span><?=number_format(($pre+$agr),2,'.',',')?></td>
        </tr>  
    </tfoot>         
    </table>
    </div>
<?php include("../rpt_footer.php"); ?>