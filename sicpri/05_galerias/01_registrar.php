<script type="text/javascript">
$(document).ready(function(){
	$("#zona").load("sicpri/funciones/getZonas.php", function(data){
		$(this).selectmenu();
	});	
	$("#save").click(function(){
		var nom=$("#tx_nom").val();
		var dir=$("#tx_dir").val();
		var zon=$("#zona").val();
	
		if(nom.length<1){ alert("Complete el campo Nombres."); $("#tx_nom").focus(); }
		else if(zon=="nn"){ alert("Asigne una zona a la galeria."); $("#zona").focus(); }
		else{
				$.post("sicpri/05_galerias/dao.php",{opt:"i", nom:nom, dir:dir, zon:zon},function(data){
					if(data==1){ 
						alert("Galeria registrada correctamente.");
						$.post("sicpri/05_galerias/06_historial.php",function(data){
							$("#contenido_sicpri").html(data);
							$("#sicpri_tit").empty().text("HISTORIAL DE GALERIAS");
						});	
					}else{
						alert("Vuelva a intentarlo por favor.");
					}
				});
		}
	});
	$("#hist").click(function(){
		$.post("sicpri/05_galerias/06_historial.php",function(data){
			$("#contenido_sicpri").html(data);
			$("#sicpri_tit").empty().text("HISTORIAL DE GALERIAS");
		});
	});
});
</script>
<table width="100%" border="0" id="tbl_prp">
  <tr>
    <td colspan="2">
        <table width="100%" border="0">
          <tr>
            <td width="25%"><strong>Datos de la galeria</strong></td>
            <td width="25%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
          </tr>
          <tr>
            <td align="left" valign="top">
            Nombre<br/><input type="text" id="tx_nom" /><br/>
            Zona:<br /><select id="zona" ></select><br />
            Direcci&oacute;n<br/><input type="text" id="tx_dir" /><br/>
            </td>
            <td align="left" valign="top">&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
          </tr>
        </table>
    </td>
  </tr>
  <tr>
    <td>    
    </td>
  </tr>  
  <tr>
    <td>
	<button id="save" class="btn"><span class="ui-icon ui-icon-disk"></span>Guardar nueva galeria</button>
	<button id="hist" class="btn"><span class="ui-icon ui-icon-folder-open"></span>Historial de galerias</button>
    </td>
  </tr>
</table>