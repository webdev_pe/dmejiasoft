<script type="text/javascript">
$(document).ready(function(){
	$("#zona").load("sicpri/funciones/getZonas.php", function(data){
		$(this).selectmenu();
	});	
		
	$("#cod_zon").autocomplete({
		source: "sicpri/05_galerias/_buscar_codigo.php",
		minLength: 2/*, select: function(event,ui){ $("#id_con").val(ui.item.id); }*/
	});	
	$("#btx_nom").autocomplete({
		source: "sicpri/05_galerias/_buscar_nombres.php",
		minLength: 2/*, select: function(event,ui){ $("#id_con").val(ui.item.id); }*/
	});		
	
	$("#genr").click(function(){
		var cod=$("#cod_gal").val();
		var nom=$("#btx_nom").val();
		var zon=$("#zona").val();
		
		$.post("sicpri/05_galerias/06_historial.php",{cod:cod,nom:nom,zon:zon,r:"r"},function(data){
			$("#contenido_sicpri").html(data);
			$("#sicpri_tit").empty().text("RESULTADO DE BUSQUEDA");
		});		
	});	
	
	$("#hist").click(function(){
		$.post("sicpri/05_galerias/06_historial.php",function(data){
			$("#contenido_sicpri").html(data);
			$("#sicpri_tit").empty().text("HISTORIAL DE ZONAS");
		});
	});		
});
</script>
<table width="100%" border="0" id="tbl_prp">
  <tr>
    <td colspan="2">
        <table width="100%" border="0">
          <tr>
            <td width="25%"><strong>Datos de b&uacute;squeda</strong></td>
            <td width="25%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
          </tr>
          <tr>
            <td align="left" valign="top">
              Codigo<br /><input type="text" id="cod_gal" /><br />
              Nombres<br/><input type="text" id="btx_nom" /><br />
			  Zona:<br /><select id="zona" ></select>
</td>
            <td align="left" valign="top">&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
          </tr>
        </table>
    </td>
  </tr> 
  <tr>
    <td>
	<button id="genr" class="btn"><span class="ui-icon ui-icon-search"></span>Generar resultados para reporte</button>
	<button id="hist" class="btn"><span class="ui-icon ui-icon-folder-open"></span>Historial de galer&iacute;as</button>
    </td>
  </tr>
</table>

