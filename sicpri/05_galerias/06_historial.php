<?php
set_time_limit(0); 
require("../../poo/clases/getConection.php");
$cn=new getConection();
$cod=$_POST['cod']; $nom=$_POST['nom']; $zon=$_POST['zon'];

if($cod!=""){	$add_search=" and cod_gal like '%$cod%' "; 
}else{
	$add_search =($nom!="")?" nom_gal like '%$nom%' and ":"";
	$add_search =($zon!="nn" && $zon!="")?" g.id_zon=$zon and ":"";	
	$add_search=($add_search!="")?" and ".$add_search:"";	
	$lenght=strlen($add_search);
	$add_search=substr($add_search,0,($lenght-4));
}

$sql="
select distinct g.id_gal, cod_gal, nom_gal, dir_gal, z.nom_zon/*,
(select sum(mnt_pre) from si_prestamos p, si_clientes c where c.id_cli=p.id_cli and c.id_zon=z.id_zon and c.id_gal=g.id_gal) as 'pre',
(select sum(mnt_cob) from si_cobranzas b, si_prestamos p, si_clientes c where b.id_pre=p.id_pre and c.id_cli=p.id_cli and c.id_zon=z.id_zon and c.id_gal=g.id_gal) as 'cob',
(select sum(mnt_pre) from si_prestamos p, si_clientes c where c.id_cli=p.id_cli and c.id_zon=z.id_zon and c.id_gal=g.id_gal) - 
(select sum(mnt_cob) from si_cobranzas b, si_prestamos p, si_clientes c where b.id_pre=p.id_pre and c.id_cli=p.id_cli and c.id_zon=z.id_zon and c.id_gal=g.id_gal) as 'pen'*/
from si_zonas z, si_galerias g, si_prestamos p, si_clientes c
where g.id_zon=z.id_zon and z.id_zon=c.id_zon and c.id_cli=p.id_cli and 
g.id_zon=z.id_zon and g.id_gal>1 ".$add_search." order by g.id_gal desc";
$cn->ejecutar_sql(base64_encode($sql));
$cn->cantidad_sql();
?>
<script type="text/javascript">
$(document).ready(function(){
	$('#list_gal').dataTable({
      /*"bProcessing": true,
      "bServerSide": true,
      "sAjaxSource": "content/scripts/propiedades.php",*/
	  'bJQueryUI': true,
	  'sPaginationType': 'full_numbers',	  
	  'fnDrawCallback': function(oSettings){
		  if(oSettings.bSorted || oSettings.bFiltered){
			  for(var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++){
				  $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i+1);
			  }
		  }
	  },'aoColumnDefs': [
		  {'bSortable': false, 'aTargets': [0]},
		  {'bSortable': false, 'aTargets': [1]},
		  {'bSortable': false, 'aTargets': [5]},
		  {'bSortable': false, 'aTargets': [6]},
		  {'bSortable': false, 'aTargets': [7]}
	  ],"bAutoWidth" : false,	  
	  "bSortClasses": false, 
	  "aoColumns" : [
		  { sWidth : '18px'},
		  { sWidth : '18px'},
		  { sWidth : '100px'},	  
		  { sWidth : 'auto'},
		  { sWidth : 'auto'},			  	   	  	  
		  { sWidth : '20px'},
		  { sWidth : '20px'},		  
		  { sWidth : '20px'}
	  ],'aaSorting': [[0, 'asc']]
	}).columnFilter({aoColumns: [
		  null,
		  null,
		  { type: "text" },
		  { type: "text" },
		  { type: "text" },	  
		  null,					
		  null,
		  null
	  ]
	});
	
	$("#todos").live("click", function(){
		$checkboxes=$("#list_gal tbody td").find(":checkbox");
		if($(this).is(":checked")){ $checkboxes.attr("checked",1); }
		else{ $checkboxes.removeAttr("checked");}
	});
		
	$("#rept").click(function(){
	  var s=$("#s").val();
	  var new_tab=window.open("","_blank");		
	  new_tab.location="sicpri/05_galerias/reporte.php?s="+s;
	});
});	
	function editar(i){
		$.post("sicpri/05_galerias/02_modificar.php",{i:i},function(data){
			$("#contenido_sicpri").html(data);
			$("#sicpri_tit").empty().text("MODIFICAR GALERIA");
		});
	}
	function verificar(i){
		$.post("sicpri/05_galerias/02_modificar.php",{i:i,v:"v"},function(data){
			$("#contenido_sicpri").html(data);
			$("#sicpri_tit").empty().text("VERIFICAR GALERIA");
		});
	}	
	function eliminar(i){
		var selecteds = new Array();
		var chekeados = $("#list_gal tbody input[@name='cods[]']:checked").size();

		if(chekeados>1){
			$("#list_gal tbody input[@name='cods[]']:checked").each(function(){
				selecteds.push($(this).val());
			});
			if(confirm("Esta seguro de eliminar estos "+chekeados+" registros?")){
				$.post("sicpri/05_galerias/dao.php", {opt:"dm",s:selecteds}, function(data){
					if(data==1){
						$.post("sicpri/05_galerias/06_historial.php",function(data){
							$("#contenido_sicpri").html(data);
							$("#sicpri_tit").empty().text("HISTORIAL DE GALERIAS");
						});
					}
				});
			}			
		}else{
			if(confirm("Esta seguro de eliminar este registro?")){
				$.post("sicpri/05_galerias/dao.php", {opt:"d",i:i}, function(data){
					if(data==1){
						$.post("sicpri/05_galerias/06_historial.php",function(data){
							$("#contenido_sicpri").html(data);
							$("#sicpri_tit").empty().text("HISTORIAL DE GALERIAS");
						});
					}
				});
			}
		}
	}

</script>
	<input type="hidden" id="s" value="<?=base64_encode(utf8_encode($sql))?>" />
    <?php if(isset($_POST['r'])){ ?>
	<button id="rept" class="btn"><span class="ui-icon ui-icon-clipboard"></span>Generar previo de resultados</button>
	<?php }?>
	<table id="list_gal" class="list_tbl" width="100%">
	<thead>
        <tr>    
	        <th>N&ordm;</th>
	        <th><input type="checkbox" id="todos" /></th>    
	        <th>CODIGO</th>
	        <th>NOMBRE</th>
	        <th>ZONA</th>
          
	        <th title="Eliminar registro"><div class="ui-icon ui-icon-trash custom_icon"></div></th>
	        <th title="Editar registro"><div class="ui-icon ui-icon-pencil custom_icon"></div></th>
            <th title="Visualizar registro"><div class="ui-icon ui-icon-zoomin custom_icon"></div></th>
        </tr>   
	</thead>
	<tbody>
    <?php
    while($cell=$cn->resultado_sql()){
	?>
      <tr>    
        <td align="center"></td>
        <td align="center"><input type="checkbox" name="cods[]" id="cods" value="<?=$cell['id_gal']?>"  /></td>
        <td align="center"><?=$cell['cod_gal']?></td>    
        <td align="center"><?=utf8_decode($cell['nom_gal'])?></td>
        <td align="center"><?=utf8_decode($cell['nom_zon'])?></td>        
   
        <td align="center">
        <button id="eliminar" class="btn" onclick="eliminar('<?=$cell['id_gal']?>')" title="Eliminar registro"><span class="ui-icon ui-icon-trash"></span></button>
        </td>
        <td align="center"><button id="editar" class="btn" onclick="editar('<?=$cell['id_gal']?>')" title="Modificar registro"><span class="ui-icon ui-icon-pencil"></span></button></td>
        <td align="center"><button id="visualizar" class="btn" onclick="verificar('<?=$cell['id_gal']?>')" title="Visualizar registro"><span class="ui-icon ui-icon-zoomin"></span></button></td>
      </tr>
    <?php
	}
	$cn->limpiar_sql();
	$cn->cerrar_sql();
    ?>
    </tbody>
	<tfoot>
        <tr>    
	        <th>N&ordm;</th>
	        <th><input type="checkbox" id="todos" /></th>    
	        <th>CODIGO</th>
	        <th>NOMBRE</th>
	        <th>ZONA</th>              
	        <th title="Eliminar registro"><div class="ui-icon ui-icon-trash custom_icon"></div></th>
	        <th title="Editar registro"><div class="ui-icon ui-icon-pencil custom_icon"></div></th>
            <th title="Visualizar registro"><div class="ui-icon ui-icon-zoomin custom_icon"></div></th>
        </tr>  
	</tfoot>    
    </table>