<?php
require("../../poo/clases/getConection.php");
$id  = $_POST['i'];
$cod = $_POST['c'];
$ver = $_POST['v'];
$cn=new getConection();
if(isset($id)){
	$sql="select id_gal, id_zon, cod_gal, nom_gal, dir_gal from si_galerias where id_gal=".$id;		
	$cn->ejecutar_sql(base64_encode($sql));
	$cel=$cn->resultado_sql();
}else if(isset($cod)){
	$sql="select id_gal, id_zon, cod_gal, nom_gal, dir_gal from si_galerias where cod_gal='".$cod."'";	
	$cn->ejecutar_sql(base64_encode($sql));
	$cel=$cn->resultado_sql();
}
?>
<script type="text/javascript">
$(document).ready(function(){
	$("#cod_gal").autocomplete({
		source: "sicpri/05_galerias/_buscar_codigo.php",
		minLength: 2/*, select: function(event,ui){ $("#id_con").val(ui.item.id); }*/
	});
	$("#zona").load("sicpri/funciones/getZonas.php",{i:"<?=$cel['id_zon']?>"},function(data){ $(this).selectmenu(); });		
	$("#edit").click(function(){
		var i=$("#id").val();
		
		var nom=$("#tx_nom").val();
		var zon=$("#zona").val();
		var dir=$("#tx_dir").val();
	
		if(nom.length<1){ alert("Complete el campo Nombres."); $("#tx_nom").focus(); }
		else if(zon=="nn"){ alert("Asigne una zona a la galeria."); $("#zona").focus(); }
		else{
				$.post("sicpri/05_galerias/dao.php",{opt:"u", i:i, nom:nom, dir:dir, zon:zon},function(data){
					if(data==1){ 
						alert("Galeria modificada correctamente.");
						$.post("sicpri/05_galerias/06_historial.php",function(data){
							$("#contenido_sicpri").html(data);
							$("#sicpri_tit").empty().text("HISTORIAL DE GALERIAS");
						});	
					}else{
						alert("Vuelva a intentarlo por favor.");
					}
				});
		}
	});	
	$("#find").click(function(){
		var c=$("#cod_gal").val();
		if(c!=""){
			$.post("sicpri/05_galerias/02_modificar.php",{c:c},function(data){
				$("#contenido_sicpri").html(data);
				$("#sicpri_tit").empty().text("MODIFICAR GALERIA");
			});
		}else{ alert("Ingrese el codigo de zona a modificar."); $("#cod_gal").focus(); }
	});	
	
	$("#hist").click(function(){
		$.post("sicpri/05_galerias/06_historial.php",function(data){
			$("#contenido_sicpri").html(data);
			$("#sicpri_tit").empty().text("HISTORIAL DE GALERIAS");
		});
	});
});
</script>
<input type="hidden" id="id" value="<?=$cel['id_gal']?>" /> 
<table width="100%" border="0" id="tbl_prp">
  <?php if(!isset($id) && !isset($cod)){?>
  <tr>
    <td colspan="2">   
        <table width="100%" border="0" id="tbl_find">
          <tr>
            <td width="25%"><strong>Buscar zona:</strong></td>
            <td width="25%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
          </tr>
          <tr>
            <td align="left" valign="top">
			C&oacute;digo de Galeria:<br />
			<input type="text" id="cod_gal" /><br />
            <button id="find" class="btn"><span class="ui-icon ui-icon-search"></span>Buscar galeria</button>
            </td>
            <td align="left" valign="top">
            </td>
            <td align="left" valign="top">
            </td>
            <td align="left" valign="top">
            </td>
          </tr>
        </table>
	</td>
  </tr> 
  <?php }?>  
  <tr>
    <td colspan="2">
        <table width="100%" border="0">
          <tr>
            <td width="25%"><strong>Datos de la galeria</strong></td>
            <td width="25%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
          </tr>
          <tr>
            <td align="left" valign="top">
            Nombre<br/><input type="text" id="tx_nom" value="<?=$cel['nom_gal']?>" /><br/>
            Zona:<br /><select id="zona" ></select><br />
            Direcci&oacute;n<br/><input type="text" id="tx_dir" value="<?=$cel['dir_gal']?>"/><br/>
            </td>
            <td align="left" valign="top">&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
          </tr>
        </table>
    </td>
  </tr>
  <tr>
    <td>    
    </td>
  </tr>  
  <tr>
    <td>
    <?php if(isset($id) || isset($cod)){ if(!isset($ver)){?>
	<button id="edit" class="btn"><span class="ui-icon ui-icon-disk"></span>Modificar galeria</button>
    <?php }}?>
	<button id="hist" class="btn"><span class="ui-icon ui-icon-folder-open"></span>Historial de galeria</button>
    </td>
  </tr>
</table>