<?php
session_start();
require("../../poo/clases/getConection.php");
$cn=new getConection();
$cod=$_POST['cod']; $nom=$_POST['nom']; $ape=$_POST['ape']; $tdc=$_POST['tdc'];
$ndc=$_POST['ndc']; $usu=$_POST['usu']; $eml=$_POST['eml']; $niv=$_POST['niv'];

if($cod!=""){	$add_search=" and cod_usu like '%$cod%' "; 
}else{
	$add_search =($nom!="")?" nom_usu like '%$nom%' and ":"";
	$add_search.=($ape!="")?" ape_usu like '%$ape%' and ":"";
	$add_search.=($tdc!="nn" && $tdc!="")?" tipo_dni=$tdc and ":"";
	$add_search.=($ndc!="")?" num_dni like '%$ndc%' and ":"";
	$add_search.=($eml!="")?" eml_usu like '%$eml%' and ":"";	
	$add_search.=($niv!="")?" nivel='$niv' and ":"";
	$add_search=($add_search!="")?" and ".$add_search:"";	
	$lenght=strlen($add_search);
	$add_search=substr($add_search,0,($lenght-4));
}

$sql="select id_usu, cod_usu, concat(nom_usu,' ',ape_usu) as 'nombres',
	case tipo_dni 
		when 1 then 'DNI'
		when 2 then 'Carnet de Extranjer&iacute;a'
		when 3 then 'Otros'
	end as 'tipo_doc',
	num_dni,user, pswd, eml_usu,
	case nivel
		when 'a' then 'Administrador'
		when 'o' then 'Operador'
	end as 'nivel', z.nom_zon, estado
	from si_usuarios u, si_zonas z where u.id_zon=z.id_zon and nivel='o' ".$add_search." order by id_usu desc";
$cn->ejecutar_sql(base64_encode($sql));
$cn->cantidad_sql();
?>
<script type="text/javascript">
$(document).ready(function(){
	$('#list_usu').dataTable({
      /*"bProcessing": true,
      "bServerSide": true,
      "sAjaxSource": "content/scripts/propiedades.php",*/
	  'bJQueryUI': true,
	  'sPaginationType': 'full_numbers',	  
	  'fnDrawCallback': function(oSettings){
		  if(oSettings.bSorted || oSettings.bFiltered){
			  for(var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++){
				  $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i+1);
			  }
		  }
	  },'aoColumnDefs': [
		  {'bSortable': false, 'aTargets': [0]},
		  {'bSortable': false, 'aTargets': [1]},
		  {'bSortable': false, 'aTargets': [9]},
		  {'bSortable': false, 'aTargets': [10]},
		  {'bSortable': false, 'aTargets': [11]},		  
		  {'bSortable': false, 'aTargets': [12]}
	  ],"bAutoWidth" : false,	  
	  "bSortClasses": false, 
	  "aoColumns" : [
		  { sWidth : '18px'},
		  { sWidth : '18px'},
		  { sWidth : '100px'},	  
		  { sWidth : '80px'},
		  { sWidth : 'auto'},	  
		  { sWidth : '100px'},
		  { sWidth : '100px'},	  
		  { sWidth : 'auto'},		  		  	  
		  { sWidth : 'auto'},		  		  	  	  
		  { sWidth : '20px'},
		  { sWidth : '20px'},
		  { sWidth : '20px'},		  
		  { sWidth : '20px'}
	  ],'aaSorting': [[0, 'asc']]
	}).columnFilter({aoColumns: [
		  null,
		  null,
		  { type: "text" },
		  { type: "text" },
		  { type: "text" },
		  { type: "select", values: ['DNI', 'Carnet de Extranjería', 'Otros'] },
		  { type: "text" },
		  { type: "text" },
		  { type: "text" },
		  null,				
		  null,					
		  null,
		  null
	  ]
	});
	
	$("#todos").live("click", function(){
		$checkboxes=$("#list_usu tbody td").find(":checkbox");
		if($(this).is(":checked")){ $checkboxes.attr("checked",1); }
		else{ $checkboxes.removeAttr("checked");}
	});
		
	$("#rept").click(function(){
	  var s=$("#s").val();
	  var new_tab=window.open("","_blank");		
	  new_tab.location="sicpri/12_usuarios/reporte.php?s="+s;
	});
	
	  $('#dialog').dialog({
		  autoOpen: false,
		  width: 250,
		  buttons: {
			  "CAMBIAR": function() { 
				  var i=$("#i").val();
				  var e=$("[name=est]:checked").val();
				  $.post("sicpri/12_usuarios/cambiar_estado.php",{i:i,e:e},function(data){
					  if(data=="1"){
						$('#dialog').dialog("close");
						$.post("sicpri/12_usuarios/06_historial.php",function(data){
							$("#contenido_sicpri").html(data);
							$("#sicpri_tit").empty().text("HISTORIAL DE USUARIOS");
						});						
					  }else{
						alert("Inténtelo de nuevo por favor.");
					  }
				  });
			  }, 
			  "CANCELAR": function() { 
				  $(this).dialog("close");
			  } 
		  }
	  });		
});
	function editar(i){
		$.post("sicpri/12_usuarios/02_modificar.php",{i:i},function(data){
			$("#contenido_sicpri").html(data);
			$("#sicpri_tit").empty().text("MODIFICAR USUARIO");
			
			if($("input[name=nivel]:radio:checked").attr("id")=="nv_col"){ $("#checkbox_menu").fadeIn("slow"); }
		});
	}
	function verificar(i){
		$.post("sicpri/12_usuarios/02_modificar.php",{i:i,v:"v"},function(data){
			$("#contenido_sicpri").html(data);
			$("#sicpri_tit").empty().text("VERIFICAR USUARIO");
			
			if($("input[name=nivel]:radio:checked").attr("id")=="nv_col"){ $("#checkbox_menu").fadeIn("slow"); }
		});
	}	
	function eliminar(i){
		var selecteds = new Array();
		var chekeados = $("#list_usu tbody input[@name='cods[]']:checked").size();

		if(chekeados>1){
			$("#list_usu tbody input[@name='cods[]']:checked").each(function(){
				selecteds.push($(this).val());
			});
			if(confirm("Esta seguro de eliminar estos "+chekeados+" registros?")){
				$.post("sicpri/12_usuarios/dao.php", {opt:"dm",s:selecteds}, function(data){
					if(data==1){
						$.post("sicpri/12_usuarios/06_historial.php",function(data){
							$("#contenido_sicpri").html(data);
							$("#sicpri_tit").empty().text("HISTORIAL DE USUARIOS");
						});
					}
				});
			}			
		}else{
			if(confirm("Esta seguro de eliminar este registro?")){
				$.post("sicpri/12_usuarios/dao.php", {opt:"d",i:i}, function(data){
					if(data==1){
						$.post("sicpri/12_usuarios/06_historial.php",function(data){
							$("#contenido_sicpri").html(data);
							$("#sicpri_tit").empty().text("HISTORIAL DE USUARIOS");
						});
					}
				});
			}
		}
	}
	function estado(i,est,cod){
			switch(est){
				case "a": $("#opc_a").attr("checked",1); break;
				case "d": $("#opc_b").attr("checked",1); break;
			}
			$("#i").val(i);
			$("#codusu").empty().text(cod);
			$("#dialog").dialog("open");		
	}
	
	function fx_hoja_cobro($id){
		var width=950;
		var height=$(window).height();
		var posx=$(window).width()/2 - width/2;
		var posy=$(window).height()/2 - height/2;
		var opciones=("toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, fullscreen=yes, width="+width+", height="+height+", top="+posy+", left="+posx);
		window.open("sicpri/12_usuarios/hoja_cobros.php?i="+$id,"HOJA DE COBROS",opciones);		
	}
</script>
<div id="dialog" title="CAMBIA ESTADO DE USUARIO" style="display:none;">
    <p><strong>USUARIO: <span id="codusu"></span></strong><br />
    <table id="opcs" width="100%" border="0">
      <tr>
        <td colspan="2">Seleccione estado:</td>
      </tr>
      <tr>
        <td width="15"><input type="radio" id="opc_a" name="est" value="a" /></td>
        <td><label for="opc_a">Activado</label></td>
      </tr>
      <tr>
        <td><input type="radio" id="opc_b" name="est" value="d" /></td>
        <td><label for="opc_b">Desactivado</label></td>
      </tr>
    </table>
    </p>
</div>
	<input type="hidden" id="i" value="" />
	<input type="hidden" id="s" value="<?=base64_encode(utf8_encode($sql))?>" />
    <?php if(isset($_POST['r'])){ ?>
	<button id="rept" class="btn"><span class="ui-icon ui-icon-clipboard"></span>Generar previo de resultados</button>
	<?php }?>
	<table id="list_usu" class="list_tbl" width="100%">
	<thead>
        <tr>    
	        <th>N&ordm;</th>
	        <th><input type="checkbox" id="todos" /></th>    
	        <th>NIVEL</th>
	        <th>CODIGO</th>
	        <th>NOMBRES Y APELLIDOS</th>
	        <th>DOCUMENTO</th>
	        <th>N&ordm;</th>
            <th>EMAIL</th>                      
            <th title="Zona">ZONA</th>
	        <th title="Estado de usuario"><div class="ui-icon ui-icon-key custom_icon"></div></th>
	        <th title="Eliminar registro"><div class="ui-icon ui-icon-trash custom_icon"></div></th>
	        <th title="Editar registro"><div class="ui-icon ui-icon-pencil custom_icon"></div></th>
            <th title="Visualizar registro"><div class="ui-icon ui-icon-zoomin custom_icon"></div></th>
        </tr>   
	</thead>
	<tbody>
    <?php
    while($cell=$cn->resultado_sql()){
	?>
      <tr>    
        <td align="center"></td>
        <td align="center"><input type="checkbox" name="cods[]" id="cods" value="<?=$cell['id_usu']?>"  /></td>
        
        <td align="center"><?=$cell['nivel']?></td>
        <td align="center"><?=$cell['cod_usu']?></td>    
        <td align="left"><?=utf8_decode($cell['nombres'])?><br />
        <a onclick="javascript:fx_hoja_cobro('<?=$cell['id_usu']?>');" style=" color:rgba(255,0,0,1); cursor:pointer; font-weight:bold; text-decoration:underline;">Ver hoja de cobros</a>
        </td>
        <td align="center"><?=$cell['tipo_doc']?></td>
        <td align="center"><?=$cell['num_dni']?></td>
        <td align="center"><?=utf8_decode($cell['eml_usu'])?></td>        
        <td align="center"><?=$cell['nom_zon']?></td>
        <td align="center">
<?php if($_SESSION['sicpri_nivel']!="SUPER ADMINISTRADOR"){ ?>
-----
<?php }else{ ?>
        <?php if($cell['estado']=="a"){?>
        <button id="estado" class="btn" onclick="estado('<?=$cell['id_usu']?>','a','<?=$cell['cod_usu']?>')" title="Activado"><span class="ui-icon ui-icon-circle-check "></span></button>
        <?php }else{?>
        <button id="estado" class="btn" onclick="estado('<?=$cell['id_usu']?>','d','<?=$cell['cod_usu']?>')" title="Desactivado"><span class="ui-icon ui-icon-circle-close"></span></button>
        <?php }?>
<?php }?>
        </td>             
        <td align="center">
<?php if($_SESSION['sicpri_nivel']!="SUPER ADMINISTRADOR"){ ?>
-----
<?php }else{ ?>
<button id="eliminar" class="btn" onclick="eliminar('<?=$cell['id_usu']?>')" title="Eliminar registro"><span class="ui-icon ui-icon-trash"></span></button>
<?php }?>
        </td>
        <td align="center">
<?php if($_SESSION['sicpri_nivel']!="SUPER ADMINISTRADOR"){ ?>
-----
<?php }else{ ?>
<button id="editar" class="btn" onclick="editar('<?=$cell['id_usu']?>')" title="Modificar registro"><span class="ui-icon ui-icon-pencil"></span></button>
<?php }?>
        </td>
        <td align="center">  
<?php if($_SESSION['sicpri_nivel']!="SUPER ADMINISTRADOR"){ ?>
-----
<?php }else{ ?>
<button id="visualizar" class="btn" onclick="verificar('<?=$cell['id_usu']?>')" title="Visualizar registro"><span class="ui-icon ui-icon-zoomin"></span></button>
<?php }?>
        </td>
      </tr>
    <?php
	}
	$cn->limpiar_sql();
	$cn->cerrar_sql();
    ?>
    </tbody>
	<tfoot>
        <tr>    
	        <th>N&ordm;</th>
	        <th><input type="checkbox" id="todos" /></th>
	        <th>NIVEL</th>
	        <th>CODIGO</th>
	        <th>NOMBRES Y APELLIDOS</th>
	        <th>DOCUMENTO</th>
	        <th>N&ordm;</th>  
            <th>EMAIL</th>          
            <th title="Zona">ZONA</th>
	        <th title="Estado de usuario"><div class="ui-icon ui-icon-key custom_icon"></div></th>            
	        <th title="Eliminar registro"><div class="ui-icon ui-icon-trash custom_icon"></div></th>
	        <th title="Editar registro"><div class="ui-icon ui-icon-pencil custom_icon"></div></th>
            <th title="Visualizar registro"><div class="ui-icon ui-icon-zoomin custom_icon"></div></th>
        </tr>   
	</tfoot>    
    </table>