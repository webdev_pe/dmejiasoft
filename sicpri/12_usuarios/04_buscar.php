<script type="text/javascript">
$(document).ready(function(){
	$("#checkbox_nivel").buttonset();

	$("#cod_usu").autocomplete({
		source: "sicpri/12_usuarios/_buscar_codigo.php",
		minLength: 2/*, select: function(event,ui){ $("#id_con").val(ui.item.id); }*/
	});	
	
	$("#btx_nom").autocomplete({
		source: "sicpri/12_usuarios/_buscar_nombres.php",
		minLength: 2/*, select: function(event,ui){ $("#id_con").val(ui.item.id); }*/
	});	
	
	$("#btx_ape").autocomplete({
		source: "sicpri/12_usuarios/_buscar_apellidos.php",
		minLength: 2/*, select: function(event,ui){ $("#id_con").val(ui.item.id); }*/
	});	
	
	$("#btx_dni").autocomplete({
		source: "sicpri/12_usuarios/_buscar_nro_dni.php",
		minLength: 2/*, select: function(event,ui){ $("#id_con").val(ui.item.id); }*/
	});	
	
	$("#btx_eml").autocomplete({
		source: "sicpri/12_usuarios/_buscar_email.php",
		minLength: 2/*, select: function(event,ui){ $("#id_con").val(ui.item.id); }*/
	});		
	
	$("#schr").click(function(){
		var cod=$("#cod_usu").val();
		var nom=$("#btx_nom").val();
		var ape=$("#btx_ape").val();
		var tdc=$("#bse_dni").val();
		var ndc=$("#btx_dni").val();
		var eml=$("#btx_eml").val();
		var niv=$("input[name=nivel]:radio:checked").val();
		
		$.post("sicpri/12_usuarios/06_historial.php",{cod:cod,nom:nom,ape:ape,tdc:tdc,ndc:ndc,eml:eml,niv:niv},function(data){
			$("#contenido_sicpri").html(data);
			$("#sicpri_tit").empty().text("RESULTADO DE BUSQUEDA");
		});		
	});	
	
	$("#hist").click(function(){
		$.post("sicpri/12_usuarios/06_historial.php",function(data){
			$("#contenido_sicpri").html(data);
			$("#sicpri_tit").empty().text("HISTORIAL DE USUARIOS");
		});
	});	
});
</script>
<table width="100%" border="0" id="tbl_prp">
  <tr>
    <td colspan="2">
        <table width="100%" border="0">
          <tr>
            <td width="25%"><strong>Datos de b&uacute;squeda</strong></td>
            <td width="25%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="2" align="left" valign="top">
              Codigo<br /><input type="text" id="cod_usu" /><br />
              Nombres<br/><input type="text" id="btx_nom" /><br/>
              Apellidos<br/><input type="text" id="btx_ape" /><br/>
              Tipo de Documento<br/>
              <select id="bse_dni">
	              <option value="nn" selected="selected">[-Seleccione-]</option>
	              <option value="1">DNI</option>
	              <option value="2">Carnet de Extranjer&iacute;a</option>
	              <option value="3">Otros</option>
              </select><br/>
              Nro. de Documento<br/><input type="text" id="btx_dni"><br />
			  Email<br/><input type="text" id="btx_eml" /><br />
              Nivel<br />
        	<div id="checkbox_nivel">
            <input type="radio" name="nivel" id="nv_mas" value="a" checked="checked" />
            <label for="nv_mas"><span class="ui-icon ui-icon-person"></span>ADMINISTRADOR</label>
            <input type="radio" name="nivel" id="nv_col" value="o" />
            <label for="nv_col"><span class="ui-icon ui-icon-person"></span>OPERADOR</label>
            </div>
            </td>
            <td align="left" valign="top">&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
          </tr>
        </table>
    </td>
  </tr> 
  <tr>
    <td>
	<button id="schr" class="btn"><span class="ui-icon ui-icon-search"></span>Buscar usuario</button>
	<button id="hist" class="btn"><span class="ui-icon ui-icon-folder-open"></span>Historial de usuarios</button>
    </td>
  </tr>
</table>