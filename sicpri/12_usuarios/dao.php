<?php
require("../../poo/clases/daoUsuarios.php");
$cn=new daoUsuarios();

$opt=$_POST['opt'];

$idu=$_POST['i'];
$cod=$_POST['c'];
$arr=$_POST['s'];

$nom=utf8_encode($_POST['nom']);
$ape=utf8_encode($_POST['ape']);
$tdc=$_POST['tdc']; 
$ndc=$_POST['ndc']; 
$usu=$_POST['usu'];
$pas=$_POST['pas']; 
$dir=$_POST['dir']; 
$eml=utf8_encode($_POST['eml']);
$tel=$_POST['tel']; 
$cla=$_POST['cla']; 
$mov=$_POST['mov']; 
$nex=$_POST['nex']; 
$rpc=$_POST['rpc']; 
$rpm=$_POST['rpm']; 
$niv=$_POST['niv'];
$zon=($niv=="o")?$_POST['zon']:1;

$p1=$_POST['p1']; 
$p2=$_POST['p2']; 
$p3=$_POST['p3']; 
$p4=$_POST['p4']; 
$p5=$_POST['p5']; 
$p6=$_POST['p6']; 
$p7=$_POST['p7'];

$d1=$_POST['d1']; 
$d2=$_POST['d2']; 
$d3=$_POST['d3']; 
$d4=$_POST['d4']; 
$d5=$_POST['d5']; 
$d6=$_POST['d6']; 
$d7=$_POST['d7'];
$ds=$_POST['ds']; 
$hs=$_POST['hs'];

//per:per,nom:nom,ape:ape,dni:dni,nro:nro,dir:dir,tel:tel,cel:cel,eml:eml,mnt:mnt,zon:zon,gal:gal,p1:p1,p2:p2,p3:p3,p4:p4,p5:p5,p6:p6,p7:p
#$fcn=date("Y-m-d",strtotime($_POST['fcn']));

switch($opt){
	case "v": $get=$cn->validar_usuario($usu); break; //validar
	case "i": $get=$cn->guardar_usuarios($nom, $ape, $tdc, $ndc, $usu, $pas, $dir, $eml, $tel, $cla, $mov, $nex, $rpc, $rpm, $niv, $zon, $p1, $p2, $p3, $p4, $p5, $p6, $p7, $d1, $d2, $d3, $d4, $d5, $d6, $d7, $ds, $hs); break; //insertar
	case "u": $get=$cn->modificar_usuarios($idu, $nom, $ape, $tdc, $ndc, $usu, $pas, $dir, $eml, $tel, $cla, $mov, $nex, $rpc, $rpm, $niv, $zon, $p1, $p2, $p3, $p4, $p5, $p6, $p7, $d1, $d2, $d3, $d4, $d5, $d6, $d7, $ds, $hs); break; //insertar	
	case "d": $get=$cn->eliminar_usuario($idu,$cod); break; //eliminacion simple
	
	case "dm": $get=$cn->eliminar_usuarios($arr); break; //eliminacion multiple
	case "r": $get=$cn->restaurar_usuario($idu); break;
	case "rm": $get=$cn->restaurar_usuarios($arr); break;
	case "e": $get=$cn->validar_estado($cod); break; //validar
}

echo $get;
?>