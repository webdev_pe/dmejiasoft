<?php
session_start();
set_time_limit(0);
//require("../../poo/clases/getConection.php");
require("../../poo/clases/daoCobranzas.php");
$cn=new getConection();
$cn2=new getConection();
$cn3=new getConection();
$cob=new daoCobranzas();
$sql="select z.nom_zon, g.id_gal, g.nom_gal, u.nom_usu, count(p.id_pre) as prestamos
from si_usuarios u
inner join si_prestamos p on u.id_usu=p.id_usu and p.est_pre='a'
inner join si_clientes cl on p.id_cli=cl.id_cli 
inner join si_zonas z on z.id_zon=u.id_zon
inner join si_galerias g on g.id_zon=z.id_zon and cl.id_gal=g.id_gal
where u.nivel='o' and u.id_usu=".$_GET['i']."
group by g.id_gal
order by g.nom_gal asc
";
$cn->ejecutar_sql(base64_encode($sql));
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Detalle de pr&eacute;stamo</title>
<script type="text/javascript" src="../../js/jquery_layout/jquery-latest.js"></script>
<style type="text/css">
body{ font-family:Verdana; font-size:11px; color:#116194;}
#encab{ text-align:center; font-weight:bold;}
#visual{ margin:0 auto; height:auto; width:100%;; border:1px solid #ededed; z-index:1;}
#tbl_res{border-collapse:collapse; border:1px solid #cccccc; margin-bottom:20px;}
#tbl_res thead th{ background:#116194; color:#ffffff; font-size:10px; font-weight:normal;}
#tbl_res tbody tr:hover{ cursor:pointer; background:#ffe87b; color:#116194;}
#tbl_res thead td{ background:#116194; color:#ffffff; font-size:10px;}
#imprimir{ position:fixed; top:7px; right:7px; background:#116194; color:#ffffff; text-align:center; cursor:pointer; width:100px; height:25px; line-height:23px; font-weight:bold; z-index:2;}
@media print{
#imprimir, #visual{ display:none;}
#tbl_res thead th{ background:#116194; color:#ffffff; font-size:10px; font-weight:normal;}
#tbl_res tbody tr:hover{ cursor:pointer; background:#ffe87b; color:#116194;}
#tbl_res thead td{ background:#116194; color:#ffffff; font-size:10px;}
}
</style>
</head>
<body>
<div id="imprimir">Imprimir</div>
<div id="visual">
<?php while($rset=$cn->resultado_sql()){ ?>
<div id="encab">COBRANZAS</div>
GALER&Iacute;A: <?=$rset['nom_gal']?><br />
OPERADOR: <?=$rset['nom_usu']?><br />
<table width="100%" border="1" id="tbl_res">
<thead>
  <tr>
    <th width="5%" rowspan="2" align="center" valign="middle">COD</th>
    <th width="28%" rowspan="2" align="center" valign="middle">NOMBRE</th>    
    <th colspan="4" align="center" valign="middle">COBRO</th>
    <th width="12%" rowspan="2" align="center" valign="middle">INT PEN DIA</th>
    <th width="15%" rowspan="2" align="center" valign="middle">CAPITAL</th>
  </tr>
  <tr>
    <th width="10%" align="center" valign="middle">MORO/MES</th>
    <th width="10%" align="center" valign="middle">CAP/DIA</th>
    <th width="10%" align="center" valign="middle">INT/DIA</th>
    <th width="10%" align="center" valign="middle">DSCTO</th>
    </tr>
</thead>  
<tbody>
  <?php 
$sql2="select z.nom_zon, g.nom_gal, p.id_pre, p.cod_pre, p.tip_pre, (substr(cl.cod_cli,4)*1) as cod, 
concat(cl.nom_cli,' ',cl.ape_cli) as nom_cli, p.mnt_pre, p.tip_pre, (p.mnt_pre*p.int_pre) as 'cuota'
from si_usuarios u
inner join si_prestamos p on u.id_usu=p.id_usu and p.est_pre='a'
inner join si_clientes cl on p.id_cli=cl.id_cli
inner join si_zonas z on z.id_zon=cl.id_zon
inner join si_galerias g on g.id_gal=cl.id_gal and g.id_gal=".$rset['id_gal']."
where u.nivel='o' and u.id_usu=".$_GET['i']."
order by g.nom_gal asc, cl.nom_cli asc";
$cn2->ejecutar_sql(base64_encode($sql2));
while($rset2=$cn2->resultado_sql()){
	$sql3="select sum(mnt_cob) as 'mnt_cob', sum(interes) as 'intereses', sum(agregar) as 'agregados' from si_cobranzas where id_pre=".$rset2['id_pre'];
	$cn3->ejecutar_sql(base64_encode($sql3));
	$rset3=$cn3->resultado_sql();
  ?>
  <tr>
    <td align="left" valign="middle"><?=$rset2['cod']?></th>
    <td align="left" valign="middle"><?=$rset2['nom_cli']?></td> 
    <td align="center" valign="middle"></td> 
    <td align="center" valign="middle"></td> 
    <td align="center" valign="middle"></td> 
    <td align="center" valign="middle"></td> 
    <td align="right" valign="middle"><span style="float:left;">S/. </span>
<?php if($rset2['tip_pre']=='a'){ ?>
<?php echo number_format(str_replace(',','',$cob->traer_mi_cobros($rset2['id_pre'],"",1,$rset2['tip_pre'])),2,'.',','); ?>
<?php }else{?>
<?php echo number_format(str_replace(',','',$rset2['cuota']-$cob->traer_mi_cobros($rset2['id_pre'],"",1,$rset2['tip_pre'])),2,'.',','); ?>
<?php }?>    
    </td> 
    <td align="right" valign="middle"><span style="float:left;">S/. </span>
<?php if($rset2['tip_pre']=='a'){ ?>
<?=number_format($rset2['mnt_pre']+$rset3['agregados']-$rset3['mnt_cob'],2,'.',',')?>
<!-- +str_replace(',','',$cob->traer_mi_cobros($rset2['id_pre'],"",1,$rset2['tip_pre'])) -->
<?php }else{?>
<?=number_format($rset2['mnt_pre']+$rset2['cuota']-$rset3['mnt_cob'],2,'.',',')?>
<?php }?>    
    </td> 
  </tr>
<?php }?>
</tbody>
</table>
<?php }?>
</div>
<script type="text/javascript">
$(document).ready(function(){ 
 $(this).keydown( function (e) {
        var keycode = null;
        if(window.event) { keycode = e.keyCode; }
		else if(e) { keycode = e.which; }
 		if(keycode == 13){ return false; }
		if(keycode == 17){ return false; }
		if(keycode == 44){ return false; }
 		if(keycode == 67){ return false; }
		if(keycode == 80){ return false; }
		if(keycode == 86){ return false; }
		if(keycode == 88){ return false; }
 		if(keycode == 116){ return false; }
 		if(keycode == 123){ return false; }
    });
});
$(document).bind("contextmenu",function(e){
	var fecha=new Date();		
	alert(" WebDevelopment ::: Todos los Derechos Reservados \n Lima - Peru "+fecha.getFullYear()+" ::: Webmaster ");
	return false;
});
</script>
</body>
</html>