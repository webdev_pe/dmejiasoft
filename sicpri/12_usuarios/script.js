// JavaScript Document
$(document).ready(function() {
	$.datepicker.setDefaults($.datepicker.regional["es"]);
	$("#tx_fcn").datepicker({
		dateFormat: "dd-mm-yy",
		numberOfMonths: 3,		
		showButtonPanel: true/*, showOn: "button", buttonImage: "img/calendar.gif", buttonImageOnly: true */
	});	
	
	$("#checkbox_permisos").buttonset();	
	$("#checkbox_nivel").buttonset();
	$("#checkbox_menu").hide().buttonset();
	
	$("input[name=nivel]:radio").click(function(){
		switch($(this).attr("id")){
			case "nv_mas": $("#msj_nivel").empty().html("Todos los privilegios del sistema."); $("#checkbox_menu").fadeOut("slow"); break;
			case "nv_col": $("#msj_nivel").empty().html("Usuario encargado de cobranzas."); $("#checkbox_menu").fadeIn("slow"); break;
		}
	});
		
	$("#save").click(function(){
		var nom=$("#tx_nom").val();
		var ape=$("#tx_ape").val();
		var tdc=$("#se_dni").val();
		var ndc=$("#tx_dni").val();
		var usu=$("#tx_user").val();
		var pas=$("#tx_pswd").val();
		var dir=$("#tx_dir").val();
		var eml=$("#tx_eml").val();
		var tel=$("#tx_tel").val();
		var cla=$("#tx_cla").val();
		var mov=$("#tx_mov").val();
		var nex=$("#tx_nex").val();
		var rpc=$("#tx_rpc").val();
		var rpm=$("#tx_rpm").val();
		var des=$("#tx_des").val();		
		var fcn=$("#tx_fcn").val();
		
		var niv=$("input[name=nivel]:radio:checked").val();
	
		if(nom=="" || nom.length<3){ alert("Complete el campo Nombres."); $("#tx_nom").focus(); }
		else if(ape=="" || ape.length<5){ alert("Complete el campo Apellidos."); $("#tx_ape").focus(); }
		else if(tdc=="nn"){ alert("Seleccione Tipo de Documento."); $("#se_dni").focus(); }
		else if(ndc=="" || ndc.length<8){ alert("Complete el campo Numero del Documento."); $("#tx_dni").focus(); }
		else if(usu=="" || usu.length<5){ alert("Complete el campo Nombre de Usuario."); $("#tx_user").focus(); }
		else if(pas=="" || pas.length<5){ alert("Complete el campo Clave de Acceso."); $("#tx_pswd").focus(); }
		else if(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(eml)==false || eml==""){ alert("Ingrese un email valido"); $("#tx_eml").focus(); }
		else{
			$.post("sicpri/12_usuarios/dao.php",{opt:"v", usu:usu}, function(data){
				if(data==1){
					alert("El Nombre de Usuario ingresado ya esta en uso, por favor cambielo.");
					$("#tx_user").focus();			
				}else{
					$.post("sicpri/12_usuarios/dao.php",{opt:"i", nom:nom, ape:ape, tdc:tdc, ndc:ndc, usu:usu, pas:pas, dir:dir, eml:eml, tel:tel, cla:cla, mov:mov, nex:nex, rpc:rpc, rpm:rpm, des:des, niv:niv, fcn:fcn},function(data){alert(data);
						if(data==1){ 
							alert("Usuario registrado correctamente.");
							$.post("sicpri/12_usuarios/06_historial.php",function(data){
								$("#contenido_sicpri").html(data);
								$("#sicpri_tit").empty().text("HISTORIAL DE USUARIOS");
							});	
						}else{
							alert("Vuelva a intentarlo por favor.");
						}
					});
				}
			});
		}
	});
	
	$("#edit").click(function(){
		var i=$("#id").val();
		
		var nom=$("#tx_nom").val();
		var ape=$("#tx_ape").val();
		var tdc=$("#se_dni").val();
		var ndc=$("#tx_dni").val();
		var usu=$("#tx_user").val();
		var pas=$("#tx_pswd").val();
		var dir=$("#tx_dir").val();
		var eml=$("#tx_eml").val();
		var tel=$("#tx_tel").val();
		var cla=$("#tx_cla").val();
		var mov=$("#tx_mov").val();
		var nex=$("#tx_nex").val();
		var rpc=$("#tx_rpc").val();
		var rpm=$("#tx_rpm").val();
		var des=$("#tx_des").val();		
		var fcn=$("#tx_fcn").val();
		
		var niv=$("input[name=nivel]:radio:checked").val();
		
		if(nom=="" || nom.length<3){ alert("Complete el campo Nombres."); $("#tx_nom").focus(); }
		else if(ape=="" || ape.length<5){ alert("Complete el campo Apellidos."); $("#tx_ape").focus(); }
		else if(tdc=="nn"){ alert("Seleccione Tipo de Documento."); $("#se_dni").focus(); }
		else if(ndc=="" || ndc.length<8){ alert("Complete el campo Numero del Documento."); $("#tx_dni").focus(); }
		else if(usu=="" || usu.length<5){ alert("Complete el campo Nombre de Usuario."); $("#tx_user").focus(); }
		else if(pas=="" || pas.length<5){ alert("Complete el campo Clave de Acceso."); $("#tx_pswd").focus(); }
		else if(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(eml)==false || eml==""){ alert("Ingrese un email valido"); $("#tx_eml").focus(); }
		else if(fcn==""){ alert("Complete el campo fecha de nacimiento."); $("#tx_fcn").focus(); }
		else{
			$.post("sicpri/12_usuarios/dao.php",{opt:"u", i:i, nom:nom, ape:ape, tdc:tdc, ndc:ndc, usu:usu, pas:pas, dir:dir, eml:eml, tel:tel, cla:cla, mov:mov, nex:nex, rpc:rpc, rpm:rpm, des:des, niv:niv, fcn:fcn},function(data){
				if(data==1){
					alert("Usuario modificado correctamente.");
					$.post("sicpri/12_usuarios/06_historial.php",function(data){
						$("#contenido_sicpri").html(data);
						$("#sicpri_tit").empty().text("HISTORIAL DE USUARIOS");
					});					
				}else{
					alert("Vuelva a intentarlo por favor.");
				}
			});
		}
	});
	
	$("#dele").click(function(){
		var c=$("#cod_usu").val();
		if(c!=""){
			$.post("sicpri/12_usuarios/dao.php",{opt:"e", c:c}, function(data){
				if(data==1){
					if(confirm("Esta seguro de eliminar al usuario: "+c+" ?")){
						$.post("sicpri/12_usuarios/dao.php", {opt:"d",c:c}, function(data){
							if(data==1){
								$.post("sicpri/12_usuarios/06_historial.php",function(data){
									$("#contenido_sicpri").html(data);
									$("#sicpri_tit").empty().text("HISTORIAL DE USUARIOS");
								});
							}
						});				
					}
				}else{
					alert("Este usuario ya ha sido eliminado.");
				}
			});
		}else{ alert("Ingrese el codigo de usuario a eliminar."); $("#cod_usu").focus(); }
	});
	
	$("#hist").click(function(){
		$.post("sicpri/12_usuarios/06_historial.php",function(data){
			$("#contenido_sicpri").html(data);
			$("#sicpri_tit").empty().text("HISTORIAL DE USUARIOS");
		});
	});
	
	$("#find").click(function(){
		var c=$("#cod_usu").val();
		if(c!=""){
			$.post("sicpri/12_usuarios/02_modificar.php",{c:c},function(data){
				$("#contenido_sicpri").html(data);
				$("#sicpri_tit").empty().text("MODIFICAR USUARIO");
				if($("input[name=nivel]:radio:checked").attr("id")=="nv_col"){ $("#checkbox_menu").fadeIn("slow"); }
			});
		}else{ alert("Ingrese el codigo de usuario a modificar."); $("#cod_usu").focus(); }
	});
	
	
	
	$("#cod_usu").autocomplete({
		source: "sicpri/12_usuarios/_buscar_codigo.php",
		minLength: 2/*, select: function(event,ui){ $("#id_con").val(ui.item.id); }*/
	});	
	
	$("#btx_nom").autocomplete({
		source: "sicpri/12_usuarios/_buscar_nombres.php",
		minLength: 2/*, select: function(event,ui){ $("#id_con").val(ui.item.id); }*/
	});	
	
	$("#btx_ape").autocomplete({
		source: "sicpri/12_usuarios/_buscar_apellidos.php",
		minLength: 2/*, select: function(event,ui){ $("#id_con").val(ui.item.id); }*/
	});	
	
	$("#btx_dni").autocomplete({
		source: "sicpri/12_usuarios/_buscar_nro_dni.php",
		minLength: 2/*, select: function(event,ui){ $("#id_con").val(ui.item.id); }*/
	});	
	
	$("#btx_user").autocomplete({
		source: "sicpri/12_usuarios/_buscar_usuario.php",
		minLength: 2/*, select: function(event,ui){ $("#id_con").val(ui.item.id); }*/
	});	
	
	$("#btx_eml").autocomplete({
		source: "sicpri/12_usuarios/_buscar_email.php",
		minLength: 2/*, select: function(event,ui){ $("#id_con").val(ui.item.id); }*/
	});						
	
	
	
	$("#schr").click(function(){
		var cod=$("#cod_usu").val();
		var nom=$("#btx_nom").val();
		var ape=$("#btx_ape").val();
		var tdc=$("#bse_dni").val();
		var ndc=$("#btx_dni").val();
		var usu=$("#btx_user").val();
		var eml=$("#btx_eml").val();
		var niv=$("#bse_niv").val();	
		$.post("sicpri/12_usuarios/06_historial.php",{cod:cod,nom:nom,ape:ape,tdc:tdc,ndc:ndc,usu:usu,eml:eml,niv:niv},function(data){
			$("#contenido_sicpri").html(data);
			$("#sicpri_tit").empty().text("RESULTADO DE BUSQUEDA");
		});		
	});
	
	$("#genr").click(function(){
		var cod=$("#cod_usu").val();
		var nom=$("#btx_nom").val();
		var ape=$("#btx_ape").val();
		var tdc=$("#bse_dni").val();
		var ndc=$("#btx_dni").val();
		var usu=$("#btx_user").val();
		var eml=$("#btx_eml").val();
		var niv=$("#bse_niv").val();	
		$.post("sicpri/12_usuarios/06_historial.php",{cod:cod,nom:nom,ape:ape,tdc:tdc,ndc:ndc,usu:usu,eml:eml,niv:niv,r:"r"},function(data){
			$("#contenido_sicpri").html(data);
			$("#sicpri_tit").empty().text("RESULTADO DE BUSQUEDA");
		});		
	});
	
	/*-------*/
	$('#list_usu').dataTable({
      /*"bProcessing": true,
      "bServerSide": true,
      "sAjaxSource": "content/scripts/propiedades.php",*/
	  'bJQueryUI': true,
	  'sPaginationType': 'full_numbers',	  
	  'fnDrawCallback': function(oSettings){
		  if(oSettings.bSorted || oSettings.bFiltered){
			  for(var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++){
				  $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i+1);
			  }
		  }
	  },'aoColumnDefs': [
		  {'bSortable': false, 'aTargets': [0]},
		  {'bSortable': false, 'aTargets': [1]},
		  {'bSortable': false, 'aTargets': [9]},
		  {'bSortable': false, 'aTargets': [10]},
		  {'bSortable': false, 'aTargets': [11]},
		  {'bSortable': false, 'aTargets': [12]},
		  {'bSortable': false, 'aTargets': [13]}
	  ],"bAutoWidth" : false,	  
	  "bSortClasses": false, 
	  "aoColumns" : [
		  { sWidth : '18px'},
		  { sWidth : '18px'},
		  { sWidth : '100px'},	  
		  { sWidth : '100px'},
		  { sWidth : 'auto'},	  
		  { sWidth : '100px'},
		  { sWidth : '100px'},	  
		  { sWidth : 'auto'},		  		  
		  { sWidth : 'auto'},
		  { sWidth : '20px'},	  
		  { sWidth : 'auto'},		  		  	  	  
		  { sWidth : '20px'},
		  { sWidth : '20px'},		  
		  { sWidth : '20px'}
	  ],'aaSorting': [[0, 'asc']]
	}).columnFilter({aoColumns: [
		  null,
		  null,
		  { type: "text" },
		  { type: "text" },
		  { type: "text" },
		  { type: "select", values: ['DNI', 'Carnet de Extranjería', 'Otros'] },
		  { type: "text" },
		  { type: "text" },
		  { type: "text" },
		  null,
		  null,
		  null,					
		  null,
		  null
	  ]
	});
	
	$("#todos").live("click", function(){
		$checkboxes=$("#list_usu tbody td").find(":checkbox");
		if($(this).is(":checked")){ $checkboxes.attr("checked",1); }
		else{ $checkboxes.removeAttr("checked");}
	});
	
	editar=function(i){
		$.post("sicpri/12_usuarios/02_modificar.php",{i:i},function(data){
			$("#contenido_sicpri").html(data);
			$("#sicpri_tit").empty().text("MODIFICAR USUARIO");
			
			if($("input[name=nivel]:radio:checked").attr("id")=="nv_col"){ $("#checkbox_menu").fadeIn("slow"); }
		});
	}
	verificar=function(i){
		$.post("sicpri/12_usuarios/02_modificar.php",{i:i,v:"v"},function(data){
			$("#contenido_sicpri").html(data);
			$("#sicpri_tit").empty().text("VERIFICAR USUARIO");
			
			if($("input[name=nivel]:radio:checked").attr("id")=="nv_col"){ $("#checkbox_menu").fadeIn("slow"); }
		});
	}	
	eliminar=function(i){
		var selecteds = new Array();
		var chekeados = $("#list_usu tbody input[@name='cods[]']:checked").size();

		if(chekeados>1){
			$("#list_usu tbody input[@name='cods[]']:checked").each(function(){
				selecteds.push($(this).val());
			});
			if(confirm("Esta seguro de eliminar estos "+chekeados+" registros?")){
				$.post("sicpri/12_usuarios/dao.php", {opt:"dm",s:selecteds}, function(data){
					if(data==1){
						$.post("sicpri/12_usuarios/06_historial.php",function(data){
							$("#contenido_sicpri").html(data);
							$("#sicpri_tit").empty().text("HISTORIAL DE USUARIOS");
						});
					}
				});
			}			
		}else{
			if(confirm("Esta seguro de eliminar este registro?")){
				$.post("sicpri/12_usuarios/dao.php", {opt:"d",i:i}, function(data){
					if(data==1){
						$.post("sicpri/12_usuarios/06_historial.php",function(data){
							$("#contenido_sicpri").html(data);
							$("#sicpri_tit").empty().text("HISTORIAL DE USUARIOS");
						});
					}
				});
			}
		}
	}
	restaurar=function(i){
		var selecteds = new Array();
		var chekeados = $("#list_usu tbody input[@name='cods[]']:checked").size();

		if(chekeados>1){
			$("#list_usu tbody input[@name='cods[]']:checked").each(function(){
				selecteds.push($(this).val());
			});
			if(confirm("Esta seguro de restaurar estos "+chekeados+" registros?")){
				$.post("sicpri/12_usuarios/dao.php", {opt:"rm",s:selecteds}, function(data){
					if(data==1){
						$.post("sicpri/12_usuarios/06_historial.php",function(data){
							$("#contenido_sicpri").html(data);
							$("#sicpri_tit").empty().text("HISTORIAL DE USUARIOS");
						});
					}
				});
			}			
		}else{
			if(confirm("Esta seguro de restaurar este registro?")){
				$.post("sicpri/12_usuarios/dao.php", {opt:"r",i:i}, function(data){
					if(data==1){
						$.post("sicpri/12_usuarios/06_historial.php",function(data){
							$("#contenido_sicpri").html(data);
							$("#sicpri_tit").empty().text("HISTORIAL DE USUARIOS");
						});
					}
				});
			}
		}
	}
		
	$("#rept").click(function(){
	  var s=$("#s").val();
	  var new_tab=window.open("","_blank");		
	  new_tab.location="sicpri/12_usuarios/reporte.php?s="+s;
	});	
	/*-------*/
});