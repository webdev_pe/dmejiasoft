<?php
session_start();
set_time_limit(0);
//require("../../poo/clases/getConection.php");
require("../../poo/clases/daoCobranzas.php");
$cn=new getConection();
$cn2=new getConection();
$cn3=new getConection();
$cob=new daoCobranzas();
$sql="select z.nom_zon, g.id_gal, g.nom_gal, u.nom_usu, count(p.id_pre) as prestamos
from si_usuarios u
inner join si_prestamos p on u.id_usu=p.id_usu and p.est_pre='a'
inner join si_clientes cl on p.id_cli=cl.id_cli 
inner join si_zonas z on z.id_zon=u.id_zon
inner join si_galerias g on g.id_zon=z.id_zon and cl.id_gal=g.id_gal
where u.nivel='o' and u.id_usu=".$_GET['i']."
group by g.id_gal
order by g.nom_gal asc
";
$cn->ejecutar_sql(base64_encode($sql));
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Detalle de pr&eacute;stamo</title>
<script type="text/javascript" src="../../js/jquery_layout/jquery-latest.js"></script>
<style type="text/css">
html{overflow-x:hidden;}
body{ font-family:Verdana; font-size:9px; color:#116194; margin:0px; padding:0px; overflow-x:hidden;}
.encab{ text-align:center; font-weight:bold;}
#visual{ margin:0 auto; height:auto; width:100%; z-index:1;
/*-moz-column-count:2;
-webkit-column-count:2;
column-count:2;*/
}
#tbl_res{border-collapse:collapse; border:1px solid #cccccc; margin-bottom:5px;}
#tbl_res thead th{ background:#116194; color:#ffffff; font-size:9px; font-weight:normal;}
#tbl_res tbody tr{ page-break-after:always;}
#tbl_res tbody tr:hover{ cursor:pointer; background:#ffe87b; color:#116194;}
#tbl_res thead td{ background:#116194; color:#ffffff; font-size:9px;}
#exportar{ position:fixed; top:0px; right:107px; background:#116194; color:#ffffff; text-align:center; cursor:pointer; width:100px; height:20px; line-height:20px; font-weight:bold; z-index:2; border:1px solid #ffffff;}
#imprimir{ position:fixed; top:0px; right:0px; background:#116194; color:#ffffff; text-align:center; cursor:pointer; width:100px; height:20px; line-height:20px; font-weight:bold; z-index:2; border:1px solid #ffffff;}
@media print{
#imprimir,#exportar{ display:none;}
#visual{
/*-moz-column-count:2;
-webkit-column-count:2;
column-count:2;*/
page-break-after:always;
}
#tbl_res thead th{ background:#116194; color:#ffffff; font-size:10px; font-weight:normal;}
#tbl_res tbody tr{ page-break-after:always;}
#tbl_res tbody tr:hover{ cursor:pointer; background:#ffe87b; color:#116194;}
#tbl_res thead td{ background:#116194; color:#ffffff; font-size:10px;}
}
</style>
</head>
<body>
<div id="exportar">Exportar</div>
<form id="frmData" name="frmData" method="post" action="excel.php">
<input type="hidden" id="data" name="data" />
</form>
<div id="imprimir" onclick="javascript:print();">Imprimir</div>
<div id="contenido">
<div class="encab">FORMATO DE HOJA DE COBRANZAS</div>
<div id="visual">
<?php while($rset=$cn->resultado_sql()){ ?>
<strong>OPERADOR: <?=$rset['nom_usu']?> | GALER&Iacute;A: <?=$rset['nom_gal']?> | Fecha: <?=date('d-m-Y')?> | Morosos: (*)</strong>
<table width="100%" border="1" id="tbl_res">
<thead>
  <tr>
    <th width="5%" rowspan="2" align="center" valign="middle">COD</th>
    <th width="28%" rowspan="2" align="center" valign="middle">NOMBRE</th>    
    <th colspan="4" align="center" valign="middle">COBRO</th>
    <th width="12%" rowspan="2" align="center" valign="middle">INT PEN DIA</th>
    <th width="15%" rowspan="2" align="center" valign="middle">CAPITAL</th>
  </tr>
  <tr>
    <th width="10%" align="center" valign="middle">MORO/MES</th>
    <th width="10%" align="center" valign="middle">CAP/DIA</th>
    <th width="10%" align="center" valign="middle">INT/DIA</th>
    <th width="10%" align="center" valign="middle">DSCTO</th>
    </tr>
</thead>  
<tbody>
  <?php 
$sql2="select z.nom_zon, g.nom_gal, p.id_pre, p.cod_pre, p.tip_pre, (substr(cl.cod_cli,4)*1) as cod, 
concat(cl.nom_cli,' ',cl.ape_cli) as nom_cli, p.mnt_pre, p.tip_pre, (p.mnt_pre*p.int_pre) as 'cuota',
datediff((select max(fecha) from si_cobranzas where id_pre=p.id_pre),p.fecha) as 'dias'
from si_usuarios u
inner join si_prestamos p on u.id_usu=p.id_usu and p.est_pre='a'
inner join si_clientes cl on p.id_cli=cl.id_cli
inner join si_zonas z on z.id_zon=cl.id_zon
inner join si_galerias g on g.id_gal=cl.id_gal and g.id_gal=".$rset['id_gal']."
where u.nivel='o' and u.id_usu=".$_GET['i']."
order by g.nom_gal asc, cl.nom_cli asc";
$cn2->ejecutar_sql(base64_encode($sql2));
$acum01=0;
$acum02=0;
while($rset2=$cn2->resultado_sql()){
  $sql3="select sum(mnt_cob) as 'mnt_cob', sum(interes) as 'intereses', sum(agregar) as 'agregados' from si_cobranzas where id_pre=".$rset2['id_pre'];
  $cn3->ejecutar_sql(base64_encode($sql3));
  $rset3=$cn3->resultado_sql();
  ?>
  <tr <?php if($rset2['tip_pre']=='p' && $rset2['dias']>=40){ echo 'style="background:#ededed; color:#000000;"';}  ?> >
    <td align="left" valign="middle"><?=$rset2['cod']?></th>
    <td align="left" valign="middle"><?=$rset2['nom_cli']?> <?php if($rset2['tip_pre']=='p' && $rset2['dias']>=40){ echo '(*)';} ?></td> 
    <td align="center" valign="middle"></td> 
    <td align="center" valign="middle"></td> 
    <td align="center" valign="middle"></td> 
    <td align="center" valign="middle"></td> 
    <td align="right" valign="middle"><!--span style="float:left;">S/. </span-->
	<?php 
    if($rset2['tip_pre']=='a'){	$num01=$cob->traer_mi_cobros($rset2['id_pre'],"",1,$rset2['tip_pre']);}
    else{$num01=$rset2['cuota']-$cob->traer_mi_cobros($rset2['id_pre'],"",1,$rset2['tip_pre']);}
	$numero_nuevo=str_replace(',','',$num01); $acum01+=$numero_nuevo;
    echo number_format($numero_nuevo,2,'.',','); 
    ?>    
    </td> 
    <td align="right" valign="middle"><!--span style="float:left;">S/. </span-->
	<?php 
    if($rset2['tip_pre']=='a'){ $num02=$rset2['mnt_pre']+$rset3['agregados']-$rset3['mnt_cob']; }
    else{ $num02=$rset2['mnt_pre']+$rset2['cuota']-$rset3['mnt_cob']; }
    echo number_format($num02,2,'.',','); $acum02+=$num02; 
    // #<!-- +str_replace(',','',$cob->traer_mi_cobros($rset2['id_pre'],"",1,$rset2['tip_pre'])) -->
    ?> 
    </td> 
  </tr>
<?php }?>
</tbody>
<tfoot>
  <tr>
    <td colspan="6" align="left" valign="middle"><strong>SUB TOTAL</strong></td>    
    <td align="right" valign="middle"><strong><?=number_format($acum01,2,'.',',');?></strong></td> 
    <td align="right" valign="middle">---</td> 
  </tr>
  <tr>
    <td colspan="6" align="left" valign="middle"><strong>TOTAL</strong></td>    
    <td align="right" valign="middle">---</td> 
    <td align="right" valign="middle"><strong><?=number_format($acum02,2,'.',',');?></strong></td> 
  </tr>
</tfoot>
</table>
<?php }?>
<div class="encab">SALIDA DE CREDITOS DE COBRANZAS</div>
<table width="100%" border="1" id="tbl_res">
<thead>
  <tr>
    <th width="5%" rowspan="2" align="center" valign="middle">COD</th>
    <th width="20%" rowspan="2" align="center" valign="middle">NOMBRE</th>    
    <th width="15%" rowspan="2" align="center" valign="middle">GALERIA</th>
    <th width="15%" rowspan="2" align="center" valign="middle">CREDITOS / AGREGADOS </th>
    <th colspan="2" align="center" valign="middle">NUEVO</th>
    <th width="15%" rowspan="2" align="center" valign="middle">POR</th>
    </tr>
  <tr>
    <th width="15%" align="center" valign="middle">CREDITO/DIA</th>
    <th width="15%" align="center" valign="middle">CREDITO/MES</th>
    </tr>
</thead>  
<tbody>
<?php $q=1; while($q<=7){?>
  <tr>
    <td align="left" valign="middle" style="color:#ffffff;">.</td> 
    <td align="right" valign="middle"></td> 
    <td align="right" valign="middle"></td> 
    <td align="right" valign="middle"></td>
    <td align="right" valign="middle"></td> 
    <td align="right" valign="middle"></td> 
    <td align="right" valign="middle"></td> 
  </tr>
<?php $q++; }?>
</tbody>
<tfoot>
  <tr>
    <td colspan="3" align="left" valign="middle"><strong>TOTAL</strong></td>    
    <td align="right" valign="middle"></td> 
    <td align="right" valign="middle"></td>
    <td align="right" valign="middle"></td> 
    <td align="right" valign="middle"></td> 
  </tr>
</tfoot>
</table>
<div class="encab">SALIDA DE CREDITOS DE CAJA</div>
<table width="100%" border="1" id="tbl_res">
<thead>
  <tr>
    <th width="5%" rowspan="2" align="center" valign="middle">COD</th>
    <th width="20%" rowspan="2" align="center" valign="middle">NOMBRE</th>    
    <th width="15%" rowspan="2" align="center" valign="middle">GALERIA</th>
    <th width="15%" rowspan="2" align="center" valign="middle">CREDITOS / AGREGADOS </th>
    <th colspan="2" align="center" valign="middle">NUEVO</th>
    <th width="15%" rowspan="2" align="center" valign="middle">POR</th>
    </tr>
  <tr>
    <th width="15%" align="center" valign="middle">CREDITO/DIA</th>
    <th width="15%" align="center" valign="middle">CREDITO/MES</th>
    </tr>
</thead>  
<tbody>
<?php $w=1; while($w<=7){?>
  <tr>
    <td align="left" valign="middle" style="color:#ffffff;">.</td> 
    <td align="right" valign="middle"></td> 
    <td align="right" valign="middle"></td> 
    <td align="right" valign="middle"></td>
    <td align="right" valign="middle"></td> 
    <td align="right" valign="middle"></td> 
    <td align="right" valign="middle"></td> 
  </tr>
<?php $w++; }?>
</tbody>
<tfoot>
  <tr>
    <td colspan="3" align="left" valign="middle"><strong>TOTAL</strong></td>    
    <td align="right" valign="middle"></td> 
    <td align="right" valign="middle"></td>
    <td align="right" valign="middle"></td> 
    <td align="right" valign="middle"></td> 
  </tr>
</tfoot>
</table>
<div class="encab">ARQUEO DE CAJA</div>
<table width="100%" border="1" id="tbl_res">
<tbody>
  <tr><td width="30%" align="left" valign="middle">TOTAL COBRANZAS</td><td width="89%" align="" valign="middle"></td></tr>
  <tr><td align="left" valign="middle">TOTAL SALIDA</td><td align="" valign="middle"></td></tr>
  <tr><td align="left" valign="middle">CAJA GRAU</td><td align="" valign="middle"></td></tr>
  <tr><td align="left" valign="middle">CORTE DE COBRANZAS</td><td align="" valign="middle"></td></tr>
  <tr><td align="left" valign="middle">SALDO DE COBRANZAS</td><td align="" valign="middle"></td></tr>
  <tr><td align="left" valign="middle">TOTAL EFECTIVO SOLES</td><td align="" valign="middle"></td></tr>
  <tr><td align="left" valign="middle">TOTAL EFECTIVO DOLARES</td><td align="" valign="middle"></td></tr>
  <tr><td align="left" valign="middle">FALTANTE</td><td align="" valign="middle"></td></tr>
  <tr><td align="left" valign="middle">SOBRANTE</td><td align="" valign="middle"></td></tr>
  <tr><td align="left" valign="middle">OBSERVACIONES:</td><td align="" valign="middle"></td></tr>
</tbody>
</table>
</div>
</div>
<script type="text/javascript">
$(document).ready(function(){ 
	$("#exportar").click(function(){
		var $h=$("#contenido").eq(0).html();
		$("#data").val($h);
		$("#frmData").submit();
		//alert($h);
		//$.post("excel.php",{data:$h});
	});
	/*$(this).keydown( function (e) {
        var keycode = null;
        if(window.event) { keycode = e.keyCode; }
		else if(e) { keycode = e.which; }
 		if(keycode == 13){ return false; }
		if(keycode == 17){ return false; }
		if(keycode == 44){ return false; }
 		if(keycode == 67){ return false; }
		if(keycode == 80){ return false; }
		if(keycode == 86){ return false; }
		if(keycode == 88){ return false; }
 		if(keycode == 116){ return false; }
 		if(keycode == 123){ return false; }
    });*/
});
$(document).bind("contextmenu",function(e){
	/*var fecha=new Date();		
	alert(" WebDevelopment ::: Todos los Derechos Reservados \n Lima - Peru "+fecha.getFullYear()+" ::: Webmaster ");
	return false;*/
});
</script>
</body>
</html>