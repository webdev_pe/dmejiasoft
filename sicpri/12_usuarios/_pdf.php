<?php 
session_start(); 
ob_start();
require("../../poo/clases/getConection.php");
$cn=new getConection();
$sql=utf8_decode(base64_decode($_GET['s']));
$cn->ejecutar_sql(base64_encode($sql));
?>
<style type="text/css">
<!--
    table.page_header {width: 100%; border: none; background-color: #ededed; color:#666666; border-bottom: solid 1mm #666666; padding: 2mm }
    table.page_footer {width: 100%; border: none; background-color: #ededed; color:#666666; border-top: solid 1mm #666666; padding: 2mm}
	#list_rpt{ margin:0 auto; font-size:10px;}
-->
</style>
<page backtop="15mm" backbottom="15mm" backleft="3mm" backright="3mm" style="font-size: 11pt">
    <page_header>
        <table class="page_header">
            <tr><td style="width: 100%; text-align: left">Sistema Integral de Control y Procesos de Racaudaci&oacute;n de Inmuebles</td></tr>
        </table>
    </page_header>
    <page_footer>
        <table class="page_footer">
            <tr><td style="width: 100%; text-align: right">p&aacute;gina [[page_cu]]/[[page_nb]]</td></tr>
        </table>
    </page_footer>
    <!-- ############################################## inicio ############################################## -->  
	<table id="list_rpt" style="width: 100%;" align="center">
	<thead>
	   <tr bgcolor="#b9bdc4">
	        <th style="width: 2%; text-align: center;">N&ordm;</th> 
	        <th style="width: 8%; text-align: center;">CODIGO</th>
	        <th style="width: 25%; text-align: center;">NOMBRES Y APELLIDOS</th>
	        <th style="width: 10%; text-align: center;">DOCUMENTO</th>
	        <th style="width: 10%; text-align: center;">N&ordm;</th>
            <th style="width: 20%; text-align: center;">EMAIL</th>
   	        <th style="width: 15%; text-align: center;">USUARIO</th>
        </tr>     
	</thead>
	<tbody>
    <?php $i=1; while($cell=$cn->resultado_sql()){ ?>
      <tr bgcolor="<?=($i%2==0)?"#ffffff":"#e2e4ff"?>">
        <td align="center"><?=$i++?></td>        
        <td align="center"><?=$cell['cod_usu']?></td>    
        <td align="center"><?=$cell['nombres']?></td>
        <td align="center"><?=$cell['tipo_doc']?></td>
        <td align="center"><?=$cell['num_dni']?></td>
        <td align="center"><?=$cell['eml_usu']?></td>        
        <td align="center"><?=$cell['user']?></td>     
      </tr>
    <?php } $cn->limpiar_sql(); $cn->cerrar_sql(); ?>
    </tbody>
    </table>
    <!-- ############################################## inicio ############################################## -->    
</page>
    <?php
	
    $content = ob_get_clean();
    $nombre="reporte.pdf";
	#$fuente=$cell['fuente'];

    require_once('../../poo/html2pdf_v4.03/html2pdf.class.php');
    try{
        #$html2pdf = new HTML2PDF('P', 'A4', 'fr');
		$html2pdf = new HTML2PDF('P', 'A4', 'es', true, 'UTF-8', 0);
        #$html2pdf->pdf->IncludeJS($script);
		#$html2pdf->setDefaultFont($fuente);
        $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
		#$html2pdf->createIndex('Índice', 25, 12, false, true, 1);
        $html2pdf->Output("sicpri_reporte_usuarios.pdf");
    }catch(HTML2PDF_exception $e) {
        echo $e;
        exit;
    }
?>