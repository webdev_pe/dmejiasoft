<script type="text/javascript">
$(document).ready(function() {
	$("#se_dni").selectmenu();
	$("#zona").load("sicpri/funciones/getZonas.php", function(data){
		$(this).selectmenu();
	});
	$("#lzona").fadeOut("slow");
	$("#checkbox_nivel").buttonset();
	$("input[name=nivel]:radio").click(function(){
		switch($(this).attr("id")){
			case "nv_mas": 
				$("#msj_nivel").empty().html("Todos los privilegios del sistema."); 
				$("#luser").fadeIn("slow");
				$("#lzona").fadeOut("slow");
			break;
			case "nv_col": 
				$("#msj_nivel").empty().html("Usuario operador.");
				$("#luser").fadeOut("slow");
				$("#lzona").fadeIn("slow");				
			break;
		}
	});
	$("#save").click(function(){
		var nom=$("#tx_nom").val();
		var ape=$("#tx_ape").val();
		var tdc=$("#se_dni").val();
		var ndc=$("#tx_dni").val();
		var usu=$("#user").val();
		var pas=$("#pswd").val();
		var dir=$("#tx_dir").val();
		var eml=$("#tx_eml").val();
		var tel=$("#tx_tel").val();
		var cla=$("#tx_cla").val();
		var mov=$("#tx_mov").val();
		var nex=$("#tx_nex").val();
		var rpc=$("#tx_rpc").val();
		var rpm=$("#tx_rpm").val();
		var niv=$("input[name=nivel]:radio:checked").val();
		var zon=$("#zona").val();
		var p1=$("#p1").val();
		var p2=$("#p2").val();
		var p3=$("#p3").val();
		var p4=$("#p4").val();
		var p5=$("#p5").val();
		var p6=$("#p6").val();
		var p7=$("#p7").val();		
	
		var d1=$("#d1").val();
		var d2=$("#d2").val();
		var d3=$("#d3").val();
		var d4=$("#d4").val();
		var d5=$("#d5").val();
		var d6=$("#d6").val();
		var d7=$("#d7").val();	
		var ds=$("#txt_dsd").val();
		var hs=$("#txt_hst").val();		
	
		if(nom.length<3){ alert("Complete el campo Nombres."); $("#tx_nom").focus(); }
		else if(ape.length<5){ alert("Complete el campo Apellidos."); $("#tx_ape").focus(); }
		else if(tdc=="nn"){ alert("Seleccione Tipo de Documento."); $("#se_dni").focus(); }
		else if(ndc.length<8){ alert("Complete el campo Numero del Documento."); $("#tx_dni").focus(); }
		else if(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(eml)==false || eml==""){ alert("Ingrese un email valido"); $("#tx_eml").focus(); }		
		else if(usu.length<5 && niv=="a"){ alert("Complete el campo Nombre de Usuario."); $("#user").focus(); }
		else if(pas.length<5){ alert("Complete el campo Clave de Acceso."); $("#pswd").focus(); }
		else if(zon=="nn" && niv=="o"){ alert("Seleccione una Zona para el Operador."); $("#zona").focus(); }		
		else{
			if(niv=="a"){
			$.post("sicpri/12_usuarios/dao.php",{opt:"v", usu:usu}, function(data){
				if(data==1){
					alert("El Nombre de Usuario ingresado ya esta en uso, por favor cambielo.");
					$("#tx_user").focus();			
				}else{
					$.post("sicpri/12_usuarios/dao.php",{opt:"i", nom:nom, ape:ape, tdc:tdc, ndc:ndc, usu:usu, pas:pas, dir:dir, eml:eml, tel:tel, cla:cla, mov:mov, nex:nex, rpc:rpc, rpm:rpm, niv:niv, zon:zon,p1:p1,p2:p2,p3:p3,p4:p4,p5:p5,p6:p6,p7:p7,d1:d1,d2:d2,d3:d3,d4:d4,d5:d5,d6:d6,d7:d7,ds:ds,hs:hs},function(data){
						if(data==1){ 
							alert("Usuario registrado correctamente.");
							$.post("sicpri/12_usuarios/06_historial.php",function(data){
								$("#contenido_sicpri").html(data);
								$("#sicpri_tit").empty().text("HISTORIAL DE USUARIOS");
							});	
						}else{
							alert("Vuelva a intentarlo por favor.");
						}
					});
				}
			});
			}else{
				$.post("sicpri/12_usuarios/dao.php",{opt:"i", nom:nom, ape:ape, tdc:tdc, ndc:ndc, usu:usu, pas:pas, dir:dir, eml:eml, tel:tel, cla:cla, mov:mov, nex:nex, rpc:rpc, rpm:rpm, niv:niv, zon:zon},function(data){
					if(data==1){ 
						alert("Usuario registrado correctamente.");
						$.post("sicpri/12_usuarios/06_historial.php",function(data){
							$("#contenido_sicpri").html(data);
							$("#sicpri_tit").empty().text("HISTORIAL DE USUARIOS");
						});	
					}else{
						alert("Vuelva a intentarlo por favor.");
					}
				});			
			}
		}
	});	
	$("#hist").click(function(){
		$.post("sicpri/12_usuarios/06_historial.php",function(data){
			$("#contenido_sicpri").html(data);
			$("#sicpri_tit").empty().text("HISTORIAL DE USUARIOS");
		});
	});
	$("input[name='pr[]']").click(function(){
		if($(this).is(":checked")){ $(this).val("1");
		}else{ $(this).val("0"); }
	});	
});
</script>
<table width="100%" border="0" id="tbl_prp">
  <tr>
    <td colspan="2">
        <table width="100%" border="0">
          <tr>
            <td width="25%"><strong>Datos Personales</strong></td>
            <td width="25%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
          </tr>
          <tr>
            <td align="left" valign="top">
              Nombres<br/><input type="text" id="tx_nom" /><br/>
              Apellidos<br/><input type="text" id="tx_ape" /><br/>
              Tipo de Documento<br/>
              <select id="se_dni" style="min-width:180px;">
                <option value="nn" selected="selected">[-Seleccione-]</option>
                <option value="1">DNI</option>
                <option value="2">Carnet de Extranjer&iacute;a</option>
                <option value="3">Otros</option>
              </select><br/>
              Nro. de Documento<br/><input type="text" id="tx_dni"><br />
              Email<br/><input type="text" id="tx_eml" /><br/>
			  Dirección<br/><input type="text" id="tx_dir" />
            </td>
            <td align="left" valign="top">
            Tel&eacute;fono Fijo<br/><input type="text" id="tx_tel" /><br/>
            Celular Claro<br/><input type="text" id="tx_cla" /><br/>
            Celular Movistar<br/><input type="text" id="tx_mov" /><br/>
            Nextel<br/><input type="text" id="tx_nex" /><br/>
            RPC<br/><input type="text" id="tx_rpc" /><br/>
            RPM<br/><input type="text" id="tx_rpm" />
			</td>
            <td colspan="2" align="left" valign="top">
    		<strong>Seleccionar el nivel de usuario</strong><br/>
        	<div id="checkbox_nivel">
            <input type="radio" name="nivel" id="nv_mas" value="a" checked="checked" />
            <label for="nv_mas"><span class="ui-icon ui-icon-person"></span>ADMINISTRADOR</label>
            <input type="radio" name="nivel" id="nv_col" value="o" />
            <label for="nv_col"><span class="ui-icon ui-icon-person"></span>OPERADOR</label>
            </div>
            <span id="msj_nivel">Todos los privilegios del sistema.</span><br />
             <fieldset><legend>Datos de acceso</legend>
             <div id="luser">Usuario:<br /><input type="text" id="user" /></div>
			 Contrase&ntilde;a:<br /><input type="text" id="pswd" />   
             <div id="lzona">Zona:<br /><select id="zona" style="width:180px;"></select></div>
             </fieldset>  
            </td>
          </tr>
          <tr>
            <td align="left" valign="top"><strong>Aplicar Permisos</strong></td>
            <td align="left" valign="top"><strong>Seleccionar Dias</strong></td>
            <td colspan="2" align="left" valign="top"><strong>Seleccionar Horario</strong></td>
          </tr>
          <tr>
            <td align="left" valign="top">
              <input type="checkbox" name="pr[]" id="p1" value="0" /><label for="p1"><strong>Generar Reporte de Creditos</strong></label><br />
              <input type="checkbox" name="pr[]" id="p2" value="0" /><label for="p2"><strong>Historial de Creditos</strong></label><br />
              <input type="checkbox" name="pr[]" id="p3" value="0" /><label for="p3"><strong>Creditos Activos</strong></label><br />
              <input type="checkbox" name="pr[]" id="p4" value="0" /><label for="p4"><strong>Creditos Cancelados</strong></label><br />
              <input type="checkbox" name="pr[]" id="p5" value="0" /><label for="p5"><strong>Creditos Vencidos</strong></label><br />
              <input type="checkbox" name="pr[]" id="p6" value="0" /><label for="p6"><strong>Creditos Morosos</strong></label><br />
              <input type="checkbox" name="pr[]" id="p7" value="0" /><label for="p7"><strong>Usuarios</strong></label>            
            </td>
            <td align="left" valign="top">
            <script type="text/javascript">
			$(document).ready(function(){
				$("#d0").click(function(){
					if($(this).is(":checked")){ $("input[name='dias[]']").attr("checked",1).val("1");
					}else{ $("input[name='dias[]']").removeAttr("checked").val("0"); }
				});						
				$("input[name='dias[]']").click(function(){
					if($(this).is(":checked")){ $(this).val("1");
					}else{ $(this).val("0"); }
				});		
							
			});
            </script>
              <input type="checkbox" name="d0" id="d0" value="0" /><label for="d0"><strong>Todos los dias</strong></label><br />
              <input type="checkbox" name="dias[]" id="d1" value="0" /><label for="d1"><strong>Lunes</strong></label><br />
              <input type="checkbox" name="dias[]" id="d2" value="0" /><label for="d2"><strong>Martes</strong></label><br />
              <input type="checkbox" name="dias[]" id="d3" value="0" /><label for="d3"><strong>Miercoles</strong></label><br />
              <input type="checkbox" name="dias[]" id="d4" value="0" /><label for="d4"><strong>Jueves</strong></label><br />
              <input type="checkbox" name="dias[]" id="d5" value="0" /><label for="d5"><strong>Viernes</strong></label><br />
              <input type="checkbox" name="dias[]" id="d6" value="0" /><label for="d6"><strong>Sabado</strong></label><br />
              <input type="checkbox" name="dias[]" id="d7" value="0" /><label for="d7"><strong>Domingo</strong></label>
            </td>
            <td align="left" valign="top">
            <script type="text/javascript">
			$(document).ready(function(){
				$("#d00").click(function(){
					if($(this).is(":checked")){ 
						$("#txt_dsd").empty().attr("disabled",1);
						$("#txt_hst").empty().attr("disabled",1);
					}else{ 
						$("#txt_dsd").empty().removeAttr("disabled"); 
						$("#txt_hst").empty().removeAttr("disabled"); 
					}
				});						
				$("input[name='dias[]']").click(function(){
					if($(this).is(":checked")){ $(this).val("1");
					}else{ $(this).val("0"); }
				});				
			});
            </script>            
              <input type="checkbox" name="d00" id="d00" value="0" /><label for="d00"><strong>Todo el dia</strong></label><br />
              <strong>Desde</strong> (Ejemplo: 08:00)<input type="text" id="txt_dsd" /><br />
              <strong>Hasta</strong> (Ejemplo: 18:00)<input type="text" id="txt_hst" />
            </td>
            <td align="left" valign="top"></td>
          </tr>          
        </table>
    </td>
  </tr>
  <tr>
    <td>
    	<table width="100%" border="0">
        <tr><td>

        
        </td></tr>
    	</table>        
    </td>
  </tr>  
  <tr>
    <td>
	<button id="save" class="btn"><span class="ui-icon ui-icon-disk"></span>Guardar nuevo usuario</button>
	<button id="hist" class="btn"><span class="ui-icon ui-icon-folder-open"></span>Historial de usuarios</button>
    </td>
  </tr>
</table>