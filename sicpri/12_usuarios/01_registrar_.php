<script type="text/javascript">
$(document).ready(function(){
	$("#se_dni").selectmenu();
	$("#cbo_gal").selectmenu();	
	
	$("#checkbox_persona").buttonset();
		
	$("#cbo_zon").load("sicpri/funciones/getZonas.php", function(data){
		$(this).selectmenu();
	});	

	$("#cbo_zon").change(function(data){
		$("#cbo_zon option:selected").each(function(){
			var i=$(this).val();
			$("#cbo_gal").load("sicpri/funciones/getGalerias.php",{i:i},function(data){
				$(this).selectmenu();
			});	
		});
	});	
	
	$("#save").click(function(){
		var per=$("input[name='persona']:radio:checked").val();
		var nom=$("#tx_nom").val();
		var ape=$("#tx_ape").val();
		var dni=$("#se_dni").val();
		var nro=$("#tx_dni").val();
		var dir=$("#tx_dir").val();
		var tel=$("#tx_tel").val();
		var cel=$("#tx_cel").val();
		var eml=$("#tx_eml").val();	
		var mnt=$("#tx_mnt").val();						
		var zon=$("#cbo_zon").val();
		var gal=$("#cbo_gal").val();
		
		var p1=$("#p1").val();
		var p2=$("#p2").val();
		var p3=$("#p3").val();
		var p4=$("#p4").val();
		var p5=$("#p5").val();
		var p6=$("#p6").val();
		var p7=$("#p7").val();

		if(nom==""){ alert("Complete el campo Nombres."); $("#tx_nom").focus(); }
		else if(ape==""){ alert("Complete el campo Apellidos."); $("#tx_ape").focus(); }
		else if(dni=="nn"){ alert("Seleccione un tipo de documento."); $("#se_dni").focus(); }
		else if(nro==""){ alert("Complete su numero de documento."); $("#tx_dni").focus(); }
		else if(mnt==""){ alert("Asigne un monto limite."); $("#tx_mnt").focus(); }		
		else if(zon=="nn"){ alert("Asigne una zona."); $("#cbo_zon").focus(); }
		else if(gal=="nn"){ alert("Asigne una galeria."); $("#cbo_gal").focus(); }		
		else{
			  $.post("sicpri/12_usuarios/dao.php",{opt:"i",per:per,nom:nom,ape:ape,dni:dni,nro:nro,dir:dir,tel:tel,cel:cel,eml:eml,mnt:mnt,zon:zon,gal:gal,p1:p1,p2:p2,p3:p3,p4:p4,p5:p5,p6:p6,p7:p7},function(data){ $("body").html(data);
				  if(data==1){ 
					  alert("Usuario registrado correctamente.");
					  $.post("sicpri/12_usuarios/01_registrar.php",function(data){
						  $("#contenido_sicpri").html(data);
						  $("#sicpri_tit").empty().text("REGISTRAR CLIENTE");
					  });	
				  }else{
					  alert("Vuelva a intentarlo por favor.");
				  }
			  });
		}
	});
	$("#hist").click(function(){
		$.post("sicpri/03_clientes/06_historial.php",function(data){
			$("#contenido_sicpri").html(data);
			$("#sicpri_tit").empty().text("HISTORIAL DE GALERIAS");
		});
	});	
	
	$("input[name='pr[]']").click(function(){
		if($(this).is(":checked")){ $(this).val("1");
		}else{ $(this).val("0"); }
	});	
});
</script>
<div id="sql"></div>
<table width="100%" border="0" id="tbl_prp">
  <tr>
    <td colspan="2">
        <table width="100%" border="0">
          <tr>
            <td width="25%"><strong>Datos Personales</strong></td>
            <td width="25%">&nbsp;</td>
            <td width="25%"><strong>Zona/Galer&iacute;a</strong></td>
            <td width="25%">&nbsp;</td>
          </tr>
          <tr>
            <td align="left" valign="top">
              Tipo de Persona<br/>
              <div id="checkbox_persona">
	            <input type="radio" name="persona" id="pr_nat" value="1" checked="checked" /><label for="pr_nat"><span class="ui-icon ui-icon-person"></span>NATURAL</label>
    	        <input type="radio" name="persona" id="pr_jur" value="2" /><label for="pr_jur"><span class="ui-icon ui-icon-person"></span>JUR&Iacute;DICA</label>
    	        <!--input type="radio" name="persona" id="pr_otr" value="3" /><label for="pr_otr"><span class="ui-icon ui-icon-person"></span>OTROS</label-->
              </div>            
              Nombres<br/><input type="text" id="tx_nom" /><br/>
              Apellidos<br/><input type="text" id="tx_ape" /><br/>
              Tipo de Documento<br/>
              <select id="se_dni">
	              <option value="nn" selected="selected">[-Seleccione-]</option>
	              <option value="1">DNI</option>
	              <option value="2">Carnet de Extranjer&iacute;a</option>
	              <option value="3">Otros</option>
              </select><br/>
              Nro. de Documento<br/><input type="text" id="tx_dni">
            </td>
            <td align="left" valign="top">Dirección<br/><input type="text" id="tx_dir" /><br />
              Tel&eacute;fono Fijo<br/><input type="text" id="tx_tel" /><br />
			  Celular<br/><input type="text" id="tx_cel" /><br />
              Email<br/><input type="text" id="tx_eml" /><br />
			  Monto L&iacute;mite<br /><input type="text" id="tx_mnt" />
            </td>
            <td align="left" valign="top">
              Seleccione Zona<br/><select id="cbo_zon"><option>[-Seleccione-]</option></select>
              <br />
              Seleccione Galer&iacute;a<br/>
              <select name="cbo_gal" id="cbo_gal">
                <option>[-Sin galeria-]</option>
            </select></td>
            <td align="left" valign="top">&nbsp;</td>
          </tr>
          <tr>
            <td align="left" valign="top">Aplicar Permisos</td>
            <td align="left" valign="top">&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
          </tr>
          <tr>
            <td align="left" valign="top">
              <input type="checkbox" name="pr[]" id="p1" value="0" /><label for="p1"><strong>Generar Reporte de Creditos</strong></label><br />
              <input type="checkbox" name="pr[]" id="p2" value="0" /><label for="p2"><strong>Historial de Creditos</strong></label><br />
              <input type="checkbox" name="pr[]" id="p3" value="0" /><label for="p3"><strong>Creditos Activos</strong></label><br />
              <input type="checkbox" name="pr[]" id="p4" value="0" /><label for="p4"><strong>Creditos Cancelados</strong></label><br />
              <input type="checkbox" name="pr[]" id="p5" value="0" /><label for="p5"><strong>Creditos Vencidos</strong></label><br />
              <input type="checkbox" name="pr[]" id="p6" value="0" /><label for="p6"><strong>Creditos Morosos</strong></label><br />
              <input type="checkbox" name="pr[]" id="p7" value="0" /><label for="p7"><strong>Usuarios</strong></label>
            </td>
            <td align="left" valign="top">&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
          </tr>
        </table>
    </td>
  </tr>
  <tr>
    <td>    
    </td>
  </tr>  
  <tr>
    <td>
	<button id="save" class="btn"><span class="ui-icon ui-icon-disk"></span>Guardar nuevo cliente</button>
	<button id="hist" class="btn"><span class="ui-icon ui-icon-folder-open"></span>Historial de clientes</button>
    </td>
  </tr>
</table>