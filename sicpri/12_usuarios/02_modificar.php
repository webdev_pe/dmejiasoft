<?php
require("../../poo/clases/getConection.php");
$id  = $_POST['i'];
$cod = $_POST['c'];
$ver = $_POST['v'];
$cn=new getConection();
if(isset($id)){
	$sql="select id_usu, id_zon, cod_usu, nom_usu, ape_usu, tipo_dni, num_dni, user, pswd, dir_usu, eml_usu, tlf_usu, cla_usu, mov_usu, nex_usu, rpc_usu, rpm_usu, nivel, p1,p2,p3,p4,p5,p6,p7,d1,d2,d3,d4,d5,d6,d7,ds,hs from si_usuarios where id_usu=".$id;		
	$cn->ejecutar_sql(base64_encode($sql));
	$cel=$cn->resultado_sql();
}else if(isset($cod)){
	$sql="select id_usu, id_zon, cod_usu, nom_usu, ape_usu, tipo_dni, num_dni, user, pswd, dir_usu, eml_usu, tlf_usu, cla_usu, mov_usu, nex_usu, rpc_usu, rpm_usu, nivel, p1,p2,p3,p4,p5,p6,p7,d1,d2,d3,d4,d5,d6,d7,ds,hs from si_usuarios where cod_usu='".$cod."'";	
	$cn->ejecutar_sql(base64_encode($sql));
	$cel=$cn->resultado_sql();
}
?>
<script type="text/javascript">
$(document).ready(function(){
	$("#se_dni").selectmenu();
	<?php if($cel['nivel']=="o"){?>
	$("#zona").load("sicpri/funciones/getZonas.php",{i:"<?=$cel['id_zon']?>"},function(data){ $(this).selectmenu(); });	
	<?php }else{?>
	$("#zona").load("sicpri/funciones/getZonas.php",function(data){ $(this).selectmenu(); });	
	<?php }?>
		
	$("#cod_usu").autocomplete({
		source: "sicpri/12_usuarios/_buscar_codigo.php",
		minLength: 2/*, select: function(event,ui){ $("#id_con").val(ui.item.id); }*/
	});
	$("#find").click(function(){
		var c=$("#cod_usu").val();
		if(c!=""){
			$.post("sicpri/12_usuarios/02_modificar.php",{c:c},function(data){
				$("#contenido_sicpri").html(data);
				$("#sicpri_tit").empty().text("MODIFICAR USUARIO");
			});
		}else{ alert("Ingrese el codigo de usuario a modificar."); $("#cod_usu").focus(); }
	});	
	$("#checkbox_nivel").buttonset();
	$("input[name=nivel]:radio").click(function(){
		switch($(this).attr("id")){
			case "nv_mas": 
				$("#msj_nivel").empty().html("Todos los privilegios del sistema."); 
				$("#luser").fadeIn("slow");
				$("#lzona").fadeOut("slow");
			break;
			case "nv_col": 
				$("#msj_nivel").empty().html("Usuario operador.");
				$("#luser").fadeOut("slow");
				$("#lzona").fadeIn("slow");				
			break;
		}
	});
	<?php if($cel['nivel']=="a"){?>$("#lzona").fadeOut("slow");<?php }?>
	<?php if($cel['nivel']=="o"){?>$("#luser").fadeOut("slow");<?php }?>	
	$("#edit").click(function(){
		var i=$("#id").val();
				
		var nom=$("#tx_nom").val();
		var ape=$("#tx_ape").val();
		var tdc=$("#se_dni").val();
		var ndc=$("#tx_dni").val();
		var usu=$("#user").val();
		var pas=$("#pswd").val();
		var dir=$("#tx_dir").val();
		var eml=$("#tx_eml").val();
		var tel=$("#tx_tel").val();
		var cla=$("#tx_cla").val();
		var mov=$("#tx_mov").val();
		var nex=$("#tx_nex").val();
		var rpc=$("#tx_rpc").val();
		var rpm=$("#tx_rpm").val();
		var niv=$("input[name=nivel]:radio:checked").val();
		var zon=$("#zona").val();

		var p1=$("#p1").val();
		var p2=$("#p2").val();
		var p3=$("#p3").val();
		var p4=$("#p4").val();
		var p5=$("#p5").val();
		var p6=$("#p6").val();
		var p7=$("#p7").val();
		
		var d1=$("#d1").val();
		var d2=$("#d2").val();
		var d3=$("#d3").val();
		var d4=$("#d4").val();
		var d5=$("#d5").val();
		var d6=$("#d6").val();
		var d7=$("#d7").val();	
		var ds=$("#txt_dsd").val();
		var hs=$("#txt_hst").val();		
	
		if(nom.length<3){ alert("Complete el campo Nombres."); $("#tx_nom").focus(); }
		else if(ape.length<5){ alert("Complete el campo Apellidos."); $("#tx_ape").focus(); }
		else if(tdc=="nn"){ alert("Seleccione Tipo de Documento."); $("#se_dni").focus(); }
		else if(ndc.length<8){ alert("Complete el campo Numero del Documento."); $("#tx_dni").focus(); }
		else if(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(eml)==false || eml==""){ alert("Ingrese un email valido"); $("#tx_eml").focus(); }		
		else if(usu.length<5 && niv=="a"){ alert("Complete el campo Nombre de Usuario."); $("#user").focus(); }
		else if(pas.length<5){ alert("Complete el campo Clave de Acceso."); $("#pswd").focus(); }
		else{
			if(niv=="a"){
				$.post("sicpri/12_usuarios/dao.php",{opt:"u", i:i, nom:nom, ape:ape, tdc:tdc, ndc:ndc, usu:usu, pas:pas, dir:dir, eml:eml, tel:tel, cla:cla, mov:mov, nex:nex, rpc:rpc, rpm:rpm, niv:niv, zon:zon,p1:p1,p2:p2,p3:p3,p4:p4,p5:p5,p6:p6,p7:p7,d1:d1,d2:d2,d3:d3,d4:d4,d5:d5,d6:d6,d7:d7,ds:ds,hs:hs},function(data){
					if(data==1){ 
						alert("Usuario modificado correctamente.");
						$.post("sicpri/12_usuarios/06_historial.php",function(data){
							$("#contenido_sicpri").html(data);
							$("#sicpri_tit").empty().text("HISTORIAL DE USUARIOS");
						});	
					}else{
						alert("Vuelva a intentarlo por favor.");
					}
				});
			}else{
				$.post("sicpri/12_usuarios/dao.php",{opt:"u", i:i, nom:nom, ape:ape, tdc:tdc, ndc:ndc, usu:usu, pas:pas, dir:dir, eml:eml, tel:tel, cla:cla, mov:mov, nex:nex, rpc:rpc, rpm:rpm, niv:niv, zon:zon},function(data){
					if(data==1){ 
						alert("Usuario modificado correctamente.");
						$.post("sicpri/12_usuarios/06_historial.php",function(data){
							$("#contenido_sicpri").html(data);
							$("#sicpri_tit").empty().text("HISTORIAL DE USUARIOS");
						});	
					}else{
						alert("Vuelva a intentarlo por favor.");
					}
				});			
			}
		}
	});	
	$("#hist").click(function(){
		$.post("sicpri/12_usuarios/06_historial.php",function(data){
			$("#contenido_sicpri").html(data);
			$("#sicpri_tit").empty().text("HISTORIAL DE USUARIOS");
		});
	});	
	$("input[name='pr[]']").click(function(){
		if($(this).is(":checked")){ $(this).val("1");
		}else{ $(this).val("0"); }
	});		
});

</script>
<input type="hidden" id="id" value="<?=$cel['id_usu']?>" />  
<table width="100%" border="0" id="tbl_prp">
  <?php if(!isset($id) && !isset($cod)){?>
  <tr>
    <td colspan="2">   
        <table width="100%" border="0" id="tbl_find">
          <tr>
            <td width="25%"><strong>Buscar usuario:</strong></td>
            <td width="25%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
          </tr>
          <tr>
            <td align="left" valign="top">
			C&oacute;digo de  Usuario:<br />
			<input type="text" id="cod_usu" /><br />
            <button id="find" class="btn"><span class="ui-icon ui-icon-search"></span>Buscar usuario</button>
            </td>
            <td align="left" valign="top">
            </td>
            <td align="left" valign="top">
            </td>
            <td align="left" valign="top">
            </td>
          </tr>
        </table>
	</td>
  </tr> 
  <?php }?>  
  <tr>
    <td colspan="2">
        <table width="100%" border="0">
          <tr>
            <td width="25%"><strong>Datos Personales: <?=$cel['cod_usu']?></strong></td>
            <td width="25%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
          </tr>
          <tr>
            <td align="left" valign="top">                          
              Nombres<br/><input type="text" id="tx_nom" value="<?=utf8_decode($cel['nom_usu'])?>" /><br/>
              Apellidos<br/><input type="text" id="tx_ape" value="<?=utf8_decode($cel['ape_usu'])?>" /><br/>
              Tipo de Documento<br/>
              <select id="se_dni">
              	  <?php switch($cel['tipo_dni']){
				  	case "1": ?>                      
                      <option value="1" selected="selected">DNI</option>
                      <option value="2">Carnet de Extranjer&iacute;a</option>
                      <option value="3">Otros</option>
                      <option value="nn">[-Seleccione-]</option>
                    <?php break;
					case "2": ?>                      
                      <option value="1">DNI</option>
                      <option value="2" selected="selected">Carnet de Extranjer&iacute;a</option>
                      <option value="3">Otros</option>
                      <option value="nn">[-Seleccione-]</option>
                    <?php break;
					case "3": ?>                      
                      <option value="1">DNI</option>
                      <option value="2">Carnet de Extranjer&iacute;a</option>
                      <option value="3" selected="selected">Otros</option>
                      <option value="nn">[-Seleccione-]</option>
                    <?php break;
				  }?>
              </select><br/>             
              Nro. de Documento<br/><input type="text" id="tx_dni" value="<?=$cel['num_dni']?>" /><br />
              Email<br/><input type="text" id="tx_eml" value="<?=$cel['eml_usu']?>" /><br/>
			  Dirección<br/><input type="text" id="tx_dir" value="<?=$cel['dir_usu']?>" />
            </td>
            <td align="left" valign="top">
              Tel&eacute;fono Fijo<br/><input type="text" id="tx_tel" value="<?=$cel['tlf_usu']?>" /><br/>
              Celular Claro<br/><input type="text" id="tx_cla" value="<?=$cel['cla_usu']?>" /><br/>
              Celular Movistar<br/><input type="text" id="tx_mov" value="<?=$cel['mov_usu']?>" /><br/>
              Nextel<br/><input type="text" id="tx_nex" value="<?=$cel['nex_usu']?>" /><br/>
              RPC<br/><input type="text" id="tx_rpc" value="<?=$cel['rpc_usu']?>" /><br/>
              RPM<br/><input type="text" id="tx_rpm" value="<?=$cel['rpm_usu']?>" />
			</td>
            <td colspan="2" align="left" valign="top">
    		<strong>Seleccionar el nivel de usuario</strong><br/>
        	<div id="checkbox_nivel">
            <input type="radio" name="nivel" id="nv_mas" value="a" <?php if($cel['nivel']=="a"){?>checked="checked"<?php }?> />
            <label for="nv_mas"><span class="ui-icon ui-icon-person"></span>ADMINISTRADOR</label>
            <input type="radio" name="nivel" id="nv_col" value="o" <?php if($cel['nivel']=="o"){?>checked="checked"<?php }?> />
            <label for="nv_col"><span class="ui-icon ui-icon-person"></span>OPERADOR</label>
            </div>
            <span id="msj_nivel">Todos los privilegios del sistema.</span><br />
             <fieldset><legend>Datos de acceso</legend>
             <div id="luser">Usuario:<input type="text" id="user" value="<?=$cel['user']?>" disabled="disabled" /></div>
			 Contrase&ntilde;a:<input type="text" id="pswd" value="<?=$cel['pswd']?>" />          
             <div id="lzona">Zona:<br /><select id="zona" style="width:180px;"></select></div>
             </fieldset>         
            </td>
          </tr>   
          <tr>
            <td align="left" valign="top"><strong>Aplicar Permisos</strong></td>
            <td align="left" valign="top"><strong>Seleccionar Dias</strong></td>
            <td colspan="2" align="left" valign="top"><strong>Seleccionar Horario</strong></td>
          </tr>
          <tr>
            <td align="left" valign="top">
<input type="checkbox" name="pr[]" id="p1" value="<?=$cel['p1']?>" <?=($cel['p1']=="1")?'checked="checked"':'';?> /><label for="p1"><strong>Generar Reporte de Creditos</strong></label><br />
<input type="checkbox" name="pr[]" id="p2" value="<?=$cel['p2']?>" <?=($cel['p2']=="1")?'checked="checked"':'';?> /><label for="p2"><strong>Historial de Creditos</strong></label><br />
<input type="checkbox" name="pr[]" id="p3" value="<?=$cel['p3']?>" <?=($cel['p3']=="1")?'checked="checked"':'';?> /><label for="p3"><strong>Creditos Activos</strong></label><br />
<input type="checkbox" name="pr[]" id="p4" value="<?=$cel['p4']?>" <?=($cel['p4']=="1")?'checked="checked"':'';?> /><label for="p4"><strong>Creditos Cancelados</strong></label><br />
<input type="checkbox" name="pr[]" id="p5" value="<?=$cel['p5']?>" <?=($cel['p5']=="1")?'checked="checked"':'';?> /><label for="p5"><strong>Creditos Vencidos</strong></label><br />
<input type="checkbox" name="pr[]" id="p6" value="<?=$cel['p6']?>" <?=($cel['p6']=="1")?'checked="checked"':'';?> /><label for="p6"><strong>Creditos Morosos</strong></label><br />
<input type="checkbox" name="pr[]" id="p7" value="<?=$cel['p7']?>" <?=($cel['p7']=="1")?'checked="checked"':'';?> /><label for="p7"><strong>Usuarios</strong></label>            
            </td>
            <td align="left" valign="top">
            <script type="text/javascript">
			$(document).ready(function(){
				$("#d0").click(function(){
					if($(this).is(":checked")){ $("input[name='dias[]']").attr("checked",1).val("1");
					}else{ $("input[name='dias[]']").removeAttr("checked").val("0"); }
				});						
				$("input[name='dias[]']").click(function(){
					if($(this).is(":checked")){ $(this).val("1");
					}else{ $(this).val("0"); }
				});		
							
			});
            </script>
<input type="checkbox" name="d0" id="d0" value="0" /><label for="d0"><strong>Todos los dias</strong></label><br />
<input type="checkbox" name="dias[]" id="d1" value="<?=$cel['d1']?>" <?=($cel['d1']=="1")?'checked="checked"':'';?> /><label for="d1"><strong>Lunes</strong></label><br />
<input type="checkbox" name="dias[]" id="d2" value="<?=$cel['d2']?>" <?=($cel['d2']=="1")?'checked="checked"':'';?> /><label for="d2"><strong>Martes</strong></label><br />
<input type="checkbox" name="dias[]" id="d3" value="<?=$cel['d3']?>" <?=($cel['d3']=="1")?'checked="checked"':'';?> /><label for="d3"><strong>Miercoles</strong></label><br />
<input type="checkbox" name="dias[]" id="d4" value="<?=$cel['d4']?>" <?=($cel['d4']=="1")?'checked="checked"':'';?> /><label for="d4"><strong>Jueves</strong></label><br />
<input type="checkbox" name="dias[]" id="d5" value="<?=$cel['d5']?>" <?=($cel['d5']=="1")?'checked="checked"':'';?> /><label for="d5"><strong>Viernes</strong></label><br />
<input type="checkbox" name="dias[]" id="d6" value="<?=$cel['d6']?>" <?=($cel['d6']=="1")?'checked="checked"':'';?> /><label for="d6"><strong>Sabado</strong></label><br />
<input type="checkbox" name="dias[]" id="d7" value="<?=$cel['d7']?>" <?=($cel['d7']=="1")?'checked="checked"':'';?> /><label for="d7"><strong>Domingo</strong></label>            
            </td>
            <td colspan="2" align="left" valign="top">
            <script type="text/javascript">
			$(document).ready(function(){
				$("#d00").click(function(){
					if($(this).is(":checked")){ 
						$("#txt_dsd").empty().attr("disabled",1);
						$("#txt_hst").empty().attr("disabled",1);
					}else{ 
						$("#txt_dsd").empty().removeAttr("disabled"); 
						$("#txt_hst").empty().removeAttr("disabled"); 
					}
				});						
				$("input[name='dias[]']").click(function(){
					if($(this).is(":checked")){ $(this).val("1");
					}else{ $(this).val("0"); }
				});				
			});
            </script>            
              <input type="checkbox" name="d00" id="d00" value="0" /><label for="d00"><strong>Todo el dia</strong></label><br />
              <strong>Desde</strong> (Ejemplo: 08:00)<input type="text" id="txt_dsd" value="<?=$cel['ds']?>" /><br />
              <strong>Hasta</strong> (Ejemplo: 18:00)<input type="text" id="txt_hst" value="<?=$cel['hs']?>" />
              </td>
          </tr>                 
        </table>
    </td>
  </tr>
  <tr>
    <td>
    	<table width="100%" border="0">
        <tr><td>

        
        </td></tr>
    	</table>        
    </td>
  </tr>  
  <tr>
    <td>
    <?php if(isset($id) || isset($cod)){ if(!isset($ver)){?>
	<button id="edit" class="btn"><span class="ui-icon ui-icon-disk"></span>Modificar usuario</button>
    <?php }}?>
    <button id="hist" class="btn"><span class="ui-icon ui-icon-folder-open"></span>Historial de usuarios</button>
    </td>
  </tr>
</table>    