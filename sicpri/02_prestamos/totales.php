<?php
session_start();
set_time_limit(0);
require("../../poo/clases/daoCobranzas.php");
$cob=new daoCobranzas();
$cnn=new getConection();
switch($_GET['tc']){
	case "cv":
		$cred="VIGENTES";
		$sqln="
		select distinct p.id_pre, cod_pre, l.cod_cli, concat(l.nom_cli,' ',l.ape_cli) as 'nombres', u.cod_usu, 
		case p.tip_pre 
		when 'a' then 'C/D&iacute;a' 
		when 'p' then 'C/Mes' 
		end as 'tipo', z.nom_zon, g.nom_gal, 
		mnt_pre,
		dia_pre, int_pre, (mnt_pre*int_pre) as 'cuota', p.tip_pre/*,
		date_format(p.fecha,'%d-%m-%Y') as 'fecha1', 
		date_format(adddate(p.fecha,dia_pre),'%d-%m-%Y') as 'final',
		date_format((select fecha from si_cobranzas where id_pre=p.id_pre order by id_cob desc limit 1),'%d-%m-%Y') as 'fecha2', 
		datediff((select fecha from si_cobranzas where id_pre=p.id_pre order by id_cob desc limit 1),p.fecha) as 'dias' */
		from si_prestamos p, si_cobranzas c, si_clientes l, si_usuarios u, si_zonas z, si_galerias g 
		where p.id_pre=c.id_pre and p.id_cli=l.id_cli and u.id_usu=p.id_usu and l.id_zon=z.id_zon and l.id_gal=g.id_gal and 
		est_pre='a' and moroso='n' 
		order by p.id_pre desc
		";
	break;
	case "cc":
		$cred="CANCELADOS";
		$sqln="
		select distinct p.id_pre, cod_pre, l.cod_cli, concat(l.nom_cli,' ',l.ape_cli) as 'nombres', u.cod_usu, 
		case p.tip_pre 
		when 'a' then 'C/D&iacute;a' 
		when 'p' then 'C/Mes' 
		end as 'tipo', z.nom_zon, g.nom_gal, 
		mnt_pre, 
		dia_pre, int_pre, (mnt_pre*int_pre) as 'cuota', p.tip_pre/*,
		date_format(p.fecha,'%d-%m-%Y') as 'fecha1', 
		date_format(adddate(p.fecha,dia_pre),'%d-%m-%Y') as 'final',
		date_format((select fecha from si_cobranzas where id_pre=p.id_pre order by id_cob desc limit 1),'%d-%m-%Y') as 'fecha2', 
		datediff((select fecha from si_cobranzas where id_pre=p.id_pre order by id_cob desc limit 1),p.fecha) as 'dias' */
		from si_prestamos p, si_cobranzas c, si_clientes l, si_usuarios u, si_zonas z, si_galerias g 
		where p.id_pre=c.id_pre and p.id_cli=l.id_cli and u.id_usu=p.id_usu and l.id_zon=z.id_zon and l.id_gal=g.id_gal and 
		est_pre='c' and moroso='n' 
		order by p.id_pre desc
		";
	break;	
	case "ce":
		$cred="VENCIDOS";
		$sqln="
		select distinct p.id_pre, cod_pre, l.cod_cli, concat(l.nom_cli,' ',l.ape_cli) as 'nombres', u.cod_usu, 
		case p.tip_pre 
		when 'a' then 'C/D&iacute;a' 
		when 'p' then 'C/Mes' 
		end as 'tipo', z.nom_zon, g.nom_gal, 
		mnt_pre, 
		dia_pre, int_pre, (mnt_pre*int_pre) as 'cuota', p.tip_pre,
		date_format(p.fecha,'%d-%m-%Y') as 'fecha1', 
		date_format(adddate(p.fecha,dia_pre),'%d-%m-%Y') as 'final',
		date_format((select fecha from si_cobranzas where id_pre=p.id_pre order by id_cob desc limit 1),'%d-%m-%Y') as 'fecha2', 
		datediff((select fecha from si_cobranzas where id_pre=p.id_pre order by id_cob desc limit 1),p.fecha) as 'dias' 
		from si_prestamos p, si_cobranzas c, si_clientes l, si_usuarios u, si_zonas z, si_galerias g 
		where p.id_pre=c.id_pre and p.id_cli=l.id_cli and u.id_usu=p.id_usu and l.id_zon=z.id_zon and l.id_gal=g.id_gal and 
		tip_pre='p' and est_pre='a' and moroso<>'m' and
		datediff((select fecha from si_cobranzas where id_pre=p.id_pre order by id_cob desc limit 1),p.fecha) >= 40 
		order by p.id_pre desc
		";
	break;
	case "cm":
		$cred="MOROSOS";
		$sqln="
		select distinct p.id_pre, cod_pre, l.cod_cli, concat(l.nom_cli,' ',l.ape_cli) as 'nombres', u.cod_usu, 
		case p.tip_pre 
		when 'a' then 'C/D&iacute;a' 
		when 'p' then 'C/Mes' 
		end as 'tipo', z.nom_zon, g.nom_gal, 
		mnt_pre, 
		dia_pre, int_pre, (mnt_pre*int_pre) as 'cuota', p.tip_pre,
		date_format(p.fecha,'%d-%m-%Y') as 'fecha1', 
		date_format(adddate(p.fecha,dia_pre),'%d-%m-%Y') as 'final',
		date_format((select fecha from si_cobranzas where id_pre=p.id_pre order by id_cob desc limit 1),'%d-%m-%Y') as 'fecha2', 
		datediff((select fecha from si_cobranzas where id_pre=p.id_pre order by id_cob desc limit 1),p.fecha) as 'dias' 
		from si_prestamos p, si_cobranzas c, si_clientes l, si_usuarios u, si_zonas z, si_galerias g 
		where p.id_pre=c.id_pre and p.id_cli=l.id_cli and u.id_usu=p.id_usu and l.id_zon=z.id_zon and l.id_gal=g.id_gal and 
		est_pre='a' and moroso='m' 
		order by p.id_pre desc
		";
	break;	
}
$cnn->ejecutar_sql(base64_encode($sqln));
while($celx=$cnn->resultado_sql()){
	$cnx=new getConection();
	$sqlx="select sum(mnt_cob) as 'mnt_cob', sum(interes) as 'intereses', sum(agregar) as 'agregados' from si_cobranzas where id_pre=".$celx['id_pre']."";
	$cnx->ejecutar_sql(base64_encode($sqlx));
	$cellx=$cnx->resultado_sql();		

	$cant01 += $celx['mnt_pre']+$cellx['agregados'];
	if($celx['tip_pre']=='a'){ 
		$cant02 += $cellx['mnt_cob']+str_replace(',','',$cob->traer_mi_cobros($celx['id_pre'],1,"",$celx['tip_pre']));
		$cant03 += $celx['mnt_pre']+$cellx['agregados']-$cellx['mnt_cob']+str_replace(',','',$cob->traer_mi_cobros($celx['id_pre'],"",1,$celx['tip_pre']));
	}else{ 
		$cant02 += $cellx['mnt_cob'];
		$cant03 += $celx['mnt_pre']+$celx['cuota']-$cellx['mnt_cob'];
	}
	$cant04 +=str_replace(',','',$cob->traer_mi_cobros($celx['id_pre'],1,"",$celx['tip_pre']));
	$cant05 +=str_replace(',','',$cob->traer_mi_cobros($celx['id_pre'],"",1,$celx['tip_pre']));	
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Totales SIPC</title>
<style type="text/css">
#tbl_cred{border-collapse:collapse; border:1px solid #666666; font-family:Verdana; font-size:11px; color:#666666;}
#tbl_cred thead{ background:#116194;}
#tbl_cred thead th{ color:#ffffff;}
#tbl_cred thead td{ border-collapse:collapse; border:1px solid #ffffff;}
#tbl_cred tbody td{ border-collapse:collapse; border:1px solid #666666;}
</style>
</head>
<body>
<table  width="100%;" id="tbl_cred">
<thead>	
    <tr>    
        <th align="center" colspan="6">TOTALES EN CREDITOS <?=$cred?></th>
    </tr>
    <tr>    
        <th align="left">TOTALES: &raquo;</th>
        <th align="center">MONTO</th>
        <th align="center">M. COBRADO C+I</th>
        <th align="center">M. PENDIENTE C+I</th>
        <th align="center">I. COBRADO</th>
        <th align="center">I. PENDIENTE</th>
    </tr>
</thead>
<tbody>
    <tr>    
        <td align="left">TOTALES: &raquo;</td>
        <td align="right"><span>S/.</span><?=number_format($cant01,2,'.',',')?></td>
        <td align="right"><span>S/.</span><?=number_format($cant02,2,'.',',')?></td>   
        <td align="right"><span>S/.</span><?=number_format($cant03,2,'.',',')?></td>
        <td align="right"><span>S/.</span><?=number_format($cant04,2,'.',',')?></td>
        <td align="right"><span>S/.</span><?=number_format($cant05,2,'.',',')?></td>
    </tr>
</tbody>
</table>
      
</body>
</html>