<?php
require("../../poo/clases/getConection.php");
$cn=new getConection();
#$sql="select id_pre, dia_pre, int_pre, fra_pre, mnt_pre, tip_pre, fecha from si_prestamos where id_pre=".$_GET['i'];
$sql="select id_pre, tip_pre, mnt_pre, int_pre, dia_pre, fra_pre, date_format(fecha,'%d-%m-%Y') as 'fecha', 
p.id_cli, concat(c.nom_cli,' ',c.ape_cli) as 'nombres', c.cod_cli, p.cod_pre, interes_por,
( select sum(mnt_cob) from si_cobranzas where id_pre=".$_GET['i'].") as 'cobrado'
 from si_prestamos p, si_clientes c
 where p.id_cli=c.id_cli and id_pre=".$_GET['i'];
$cn->ejecutar_sql(base64_encode($sql));
$cel=$cn->resultado_sql();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Detalle de pr&eacute;stamo</title>
<style type="text/css">
body{ font-family:Verdana; font-size:11px; color:#116194;}
#visual{ margin:0 auto; height:auto; width:640px; border:1px solid #ededed;}
#tbl_res{border-collapse:collapse; border:1px solid #cccccc;}
#tbl_res thead th{ background:#116194; color:#ffffff;}
#tbl_res tbody tr:hover{ cursor:pointer; background:#ffe87b; color:#116194;}
#imprimir{ position:fixed; top:0px; right:0px; background:#116194; color:#ffffff; text-align:center; cursor:pointer; width:100px; height:25px; line-height:23px; font-weight:bold;}
</style>
</head>
<body>
<div id="imprimir" onclick="javascript:print();">Imprimir</div>
<div id="visual">
<?php
/*if($cel['tip_pre']=="p"){
	echo "<h1>Pr&eacute;stamo Porcentaje</h1>";
	echo "<hr />";
		echo "<strong>Monto</strong>: ".number_format($cel['mnt_pre'],2,'.',',')." -
		<strong>D&iacute;as</strong>: ".$cel['dia_pre']." -
		<strong>Interes</strong>: ".$cel['int_pre']."%";	
		$diario=$cel['mnt_pre']/$cel['dia_pre'];
		$interes=$cel['mnt_pre']*$cel['int_pre']/$cel['dia_pre'];
	echo "<hr />";
}else{
	echo "<h1>Pr&eacute;stamo Arrebatir</h1>";
	echo "<hr />";
		echo "<strong>Monto</strong>: ".number_format($cel['mnt_pre'],2,'.',',')." -
		<strong>Cuota</strong>: ".$cel['fra_pre'];	
	echo "<hr />";
}*/

if($cel['tip_pre']=="p"){
	echo "<h3>Cr&eacute;dito Consignaci&oacute;n por Mes (".$cel['cod_pre'].")</h3>";
	echo "<hr />";
	echo "<strong>Fecha de pr&eacute;stamo:</strong> ".$cel['fecha']."<br />";	
	echo "<strong>Cliente:</strong> ".$cel['nombres']."<br /> <strong>C&oacute;digo:</strong> ".$cel['cod_cli']."<br />";
		$monto=$cel['mnt_pre'];
		echo "<strong>Monto</strong>: ".$monto." -
		<strong>D&iacute;as</strong>: ".$cel['dia_pre']." -
		<strong>Interes</strong>: ".$cel['int_pre']."% - ";
		$diario=$monto/$cel['dia_pre'];
		$interes=$monto*$cel['int_pre']/$cel['dia_pre'];
				
		$int_tot=$monto*$cel['int_pre'];
		$dev_tot=$monto+$int_tot;
		
		$diario_a_pagar=$cn->redondeo($diario)+$cn->redondeo($interes);
		echo "<strong>Cuota diaria</strong>: ".$cn->redondeo($diario_a_pagar);
	echo "<hr />";
}else if($cel['tip_pre']=="a"){
	echo "<h3>Cr&eacute;dito Consignaci&oacute;n por D&iacute;a (".$cel['cod_pre'].")</h3>";
	echo "<hr />";
	echo "<strong>Fecha de pr&eacute;stamo:</strong> ".$cel['fecha']."<br />";	
	echo "<strong>Cliente:</strong> ".$cel['nombres']."<br /> <strong>C&oacute;digo:</strong> ".$cel['cod_cli']."<br />";
		echo "<strong>Capital</strong>: ".number_format(round($cel['mnt_pre']),2,'.',',')." -
		<strong>Inter&eacute;s</strong>: ".number_format(round($cel['fra_pre']),2,'.',',')." - 
		<strong>Interes por</strong>: ".number_format(round($cel['interes_por']),2,'.',',')."";	
	echo "<hr />";
	$monto=$cel['mnt_pre'];
	$inter=$cel['fra_pre'];
	$dev_tot=$monto;
}
?>

<?php if($cel['tip_pre']=="p"){ ?>
<table width="100%" border="1" id="tbl_res">
<thead>
  <tr>
    <th width="7%" align="center" valign="middle"># D&iacute;a</th>
    <th width="18%" align="center" valign="middle">Fecha</th>    
    <th width="20%" align="center" valign="middle">Cuota</th>
    <th width="20%" align="center" valign="middle">Inter&eacute;s</th>
    <th width="20%" align="center" valign="middle">Total</th>
    <th width="15%" align="center" valign="middle">Estado</th>
  </tr>
</thead>  
<tbody>
  <?php for($i=1;$i<=$cel['dia_pre'];$i++){
	$total=$cn->redondeo($diario)+$cn->redondeo($interes);
	$fecha=date("d-m-Y",strtotime($cel['fecha']." + $i days"));
  ?>
  <tr bgcolor="<?php if($fecha==date("d-m-Y")){echo "#ffe87b";}else{ if($i%2==0){echo "#ffffff";}else{echo "#e2e4ff";} }?>">
    <td align="center" valign="middle"><?=$i?></th>
    <td align="center" valign="middle"><?=$fecha?></th>
    <td align="center" valign="middle"><?=$cn->redondeo($diario)?></td>
    <td align="center" valign="middle"><?=$cn->redondeo($interes)?></td>
    <td align="center" valign="middle"><?=$cn->redondeo($total)?></td>
    <td align="center" valign="middle">
    <?php
	$pagos=$cel['cobrado']/$total;
	echo ($i<=$pagos)?"<strong style='color:#116194'>Cancelado</strong>":"<strong style='color:#ff0000'>Pendiente</strong>";
	?>
    </td> 
  </tr>
  <?php 
	$acum1+=$cn->redondeo($diario);
	$acum2+=$cn->redondeo($interes);
	$acum3+=$cn->redondeo($total);
  }?>
</tbody>
<tfoot>
  <tr>
    <th colspan="2">Totales</th>
    <th><?=number_format($acum1,2,'.',',')?></th>
    <th><?=number_format($acum2,2,'.',',')?></th>
    <th><?=number_format($acum3,2,'.',',')?></th>
    <th><?=number_format($cel['cobrado'],2,'.',',')?></th>
  </tr>
<tfoot>
</table>
<?php }else{?>
<table width="100%" border="1" id="tbl_res">
<thead>
  <tr>
    <th width="10%" align="center" valign="middle">Dia</th>
    <th width="30%" align="center" valign="middle">Monto</th>
    <th width="30%" align="center" valign="middle">Cuota</th>
    <th width="30%" align="center" valign="middle">Cuota del d&iacute;a</th>
  </tr>
</thead>  
<tbody>
  <tr bgcolor="#ffffff">
    <td align="center" valign="middle">1</th>
    <td align="center" valign="middle"><?=number_format(round($cel['mnt_pre']),2,'.',',')?></td>
    <td align="center" valign="middle"><?=number_format(round($cel['fra_pre']),2,'.',',')?></td>
    <td align="center" valign="middle"><?=number_format(round(($cel['mnt_pre']+$cel['fra_pre'])),2,'.',',')?></td>
  </tr>
</tbody>
</table>
<?php }?>
</div>
</body>
</html>