<script type="text/javascript">
$(document).ready(function(){
	$("#cod_pre").autocomplete({
		source: "sicpri/02_prestamos/_buscar_prestamo.php",
		minLength: 2
	});	
	$("#dele").click(function(){
		var c=$("#cod_pre").val();
		if(c!=""){
			if(confirm("Esta seguro de eliminar al usuario: "+c+" ?")){
				$.post("sicpri/02_prestamos/dao.php", {opt:"d",c:c}, function(data){
					if(data==1){
						$.post("sicpri/02_prestamos/03_eliminar.php",function(data){
							$("#contenido_sicpri").html(data);
							$("#sicpri_tit").empty().text("ELIMINAR CREDITOS");
						});
					}
				});				
			}
		}else{ alert("Ingrese el codigo de zona a eliminar."); $("#cod_zon").focus(); }
	});
	
	$("#hist").click(function(){
		$.post("sicpri/02_prestamos/06_historial.php",function(data){
			$("#contenido_sicpri").html(data);
			$("#sicpri_tit").empty().text("HISTORIAL DE CREDITOS");
		});
	});			
});
</script>
<table width="100%" border="0" id="tbl_prp">
  <tr>
    <td colspan="2">   
        <table width="100%" border="0" id="tbl_find">
          <tr>
            <td width="25%"><strong>Buscar cr&eacute;dito:</strong></td>
            <td width="25%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
          </tr>
          <tr>
            <td align="left" valign="top">
			C&oacute;digo de Cr&eacute;dito:<br />
			<input type="text" id="cod_pre" />
            </td>
            <td align="left" valign="top">
            </td>
            <td align="left" valign="top">
            </td>
            <td align="left" valign="top">
            </td>
          </tr>
        </table>
	</td>
  </tr> 
  <tr>
    <td>
	<button id="dele" class="btn"><span class="ui-icon ui-icon-trash"></span>Eliminar Cr&eacute;dito</button>
    <button id="hist" class="btn"><span class="ui-icon ui-icon-disk"></span>Historial de Cr&eacute;dito</button>
    </td>
  </tr>
</table>