<script type="text/javascript">
$(document).ready(function() {
	$.datepicker.setDefaults($.datepicker.regional["es"]);
	$("#pre_fch").datepicker({
		dateFormat: "dd-mm-yy",
		numberOfMonths: 3,		
		showButtonPanel: true/*, showOn: "button", buttonImage: "img/calendar.gif", buttonImageOnly: true */
	});
		
	$("#radio_prestamo").buttonset();
	$("#radio_dias").buttonset();
	$("#radio_intr").buttonset();
	$("#radio_moroso").buttonset();
		
	$("#opr").selectmenu();

	$("input[name=prest]:radio").click(function(){
		switch($(this).attr("id")){
			case "opp": $(".porc").show(); $(".arre").hide(); break;
			case "opa": $(".arre").show(); $(".porc").hide(); break;			
		}
	});
	
	
	var dias=27;
	$("input[name=dias]:radio").click(function(){
		switch($(this).attr("id")){
			case "op27": dias=27; $("#odias").hide(); break;
			case "op30": dias=30; $("#odias").hide(); break;			
			case "op00": 
				$("#odias").show().focus();
			break;
		}
	});
	var intr=0.08;
	$("input[name=intr]:radio").click(function(){
		switch($(this).attr("id")){
			case "op08": intr=0.08; $("#ointr").hide(); break;
			case "op10": intr=0.10; $("#ointr").hide(); break;			
			case "opOI": 
				$("#ointr").show().focus();
			break;
		}
	});	

	$("#cod_cli").autocomplete({
		source: "sicpri/02_prestamos/_buscar_codigo.php",
		minLength: 2, 
		select: function(event,ui){ 
			var c=ui.item.value;
			$("#nom_cli").val(ui.item.id);
			$.post("sicpri/02_prestamos/traer_saldo.php",{c:c},function(data){
				$("#mnt_cli").val(data.dis);
				$("#id").val(data.id);
				$("#lim_cli").val(data.mnt);
				$("#pre_cli").val(data.pre);
				var i=data.zon;
					$("#opr").load("sicpri/02_prestamos/traer_operadores.php",{i:i},function(data){
						$(this).empty().html(data).selectmenu();
					});	
			},"json");			
		}
	});
	
	$("#nom_cli").autocomplete({
		source: "sicpri/02_prestamos/_buscar_nombre.php",
		minLength: 2, 
		select: function(event,ui){
			var c=ui.item.id;
			$("#cod_cli").val(ui.item.id);
			$.post("sicpri/02_prestamos/traer_saldo.php",{c:c},function(data){
				$("#mnt_cli").val(data.dis);
				$("#id").val(data.id);
				$("#lim_cli").val(data.mnt);
				$("#pre_cli").val(data.pre);				
				var i=data.zon;
				$.post("sicpri/02_prestamos/traer_operadores.php",{i:i},function(data){
					$("#opr").empty().html(data);
					$("#opr").selectmenu();
				});
			},"json");
		}
	});	
	
	$("#save").click(function(){
		var cli=$("#id").val();
		var usu=$("#opr").val();
		var tip=$("input[name=prest]:radio:checked").val();
		var est=$("input[name=mor]:radio:checked").val();
		var txd=$("#odias").val();
		var dia=(tip=="p")?((txd!="")?txd:dias):0;
		var txi=$("#ointr").val();
		var int=(tip=="p")?((txi!="")?(txi)/100:intr):0;
		var cuo=(tip=="a")?$("#cuota").val():0;
		var mnt=$("#monto").val();	
		var cod=$("#cod_cli").val();
		var nom=$("#nom_cli").val();
		var inp=($("#intp").val()=="")?0:$("#intp").val();
		
		var lim=$("#mnt_cli").val();
		var lmt=parseFloat(lim.replace(",",""));
		var vmt=parseFloat(mnt);	
		
		var fch=$("#pre_fch").val();
		
		if(cli=="" && nom==""){ alert("Asigne un cliente para generarun prestamo."); }
		else if(tip=="a" && cuo==""){ alert("Ingrese una cuota."); $("#cuota").focus(); }		
		else if(mnt==""){ alert("Ingrese un monto."); $("#monto").focus(); }
		else if(usu=="nn"){ alert("Seleccione un operador de cobranza."); $("#opr").focus(); }
		else if(lmt<vmt){ alert("El monto no debe superar el saldo disponible ("+lmt+")."); $("#monto").focus();}
		else{
			$.post("sicpri/02_prestamos/dao.php",{opt:"i",cli:cli,usu:usu,tip:tip,est:est,dia:dia,int:int,cuo:cuo,mnt:mnt,inp:inp,pre:"sistema",fch:fch},function(data){//alert(data);
				if(data==1){ 
					alert("Credito registrado correctamente.");
					$.post("sicpri/02_prestamos/01_registrar.php",function(data){
						$("#contenido_sicpri").html(data);
						$("#sicpri_tit").empty().text("REGISTRO DE CREDITOS");
					});	
				}else{
					alert("Vuelva a intentarlo por favor.");
				}
			});
		}
	});	
	$("#hist").click(function(){
		$.post("sicpri/02_prestamos/06_historial.php",function(data){
			$("#contenido_sicpri").html(data);
			$("#sicpri_tit").empty().text("HISTORIAL DE CREDITOS");
		});
	});	
	$("#monto").blur(function(){
		var lim=$("#mnt_cli").val();
		var mnt=parseFloat(lim.replace(",",""));
		var val=parseFloat($(this).val());
		if(mnt<val){
			alert("El monto no debe superar el saldo dispoinble ("+mnt+")."); $(this).focus();
		}
	});		
});
</script>
<table width="100%" border="0" id="tbl_prp">
  <tr>
    <td colspan="2">
    
<table width="100%" border="0">
  <tr>
    <td width="33%" align="left" valign="top">
	<strong>Cliente</strong><br /><input type="hidden" id="id" />
    C&oacute;digo<br /><input type="text" id="cod_cli" /><br />
    Nombre<br /><input type="text" id="nom_cli" /><br />
    Saldo disponible<br /><input type="text" id="mnt_cli" readonly="readonly" /><br />
    Monto L&iacute;mite<br /><input type="text" id="lim_cli" readonly="readonly" /><br />
    Cr&eacute;ditos (Pendientes)<br /><input type="text" id="pre_cli" readonly="readonly" /><br />        
	</td>
    <td width="33%" align="left" valign="top">
      <strong>Tipo de Cr&eacute;dito</strong>
      <div id="radio_prestamo">
        <input type="radio" value="p" name="prest" id="opp" checked="checked" /><label for="opp">Consignaci&oacute;n por mes</label>
        <input type="radio" value="a" name="prest" id="opa" /><label for="opa">Consignaci&oacute;n por d&iacute;a</label>
        </div>
      	<strong>Fecha de registro</strong><br /><input type="text" id="pre_fch" value="<?=date("d-m-Y")?>" />         
      <strong>Estado</strong>
      <div id="radio_moroso">
        <input type="radio" value="n" name="mor" id="morn" checked="checked" /><label for="morn">Normal</label>
        <input type="radio" value="m" name="mor" id="morm" /><label for="morm">Moroso</label>
        </div>        
	  <div class="porc">
          <strong>Cantidad de D&iacute;as</strong>
          <div id="radio_dias">
            <input type="radio" value="27" name="dias" id="op27" checked="checked" /><label for="op27">27</label>
            <input type="radio" value="30" name="dias" id="op30" /><label for="op30">30</label>
            <input type="radio" value="00" name="dias" id="op00" /><label for="op00">Otro #</label>
          </div>      
		  <input type="text" id="odias" style="display:none;" />
	  </div>
	  <div class="porc">
          <strong>Cantidad de Inter&eacute;s</strong>
          <div id="radio_intr">
            <input type="radio" value="0.08" name="intr" id="op08" checked="checked" /><label for="op08">8%</label>
            <input type="radio" value="0.10" name="intr" id="op10" /><label for="op10">10%</label>
            <input type="radio" value="00"   name="intr" id="opOI" /><label for="opOI">Otro %</label>
          </div>      
		  <input type="text" id="ointr" style="display:none;" />
	  </div>
      <div class="norm">
      <strong>Monto a prestar</strong><br />
      <input type="text" id="monto" />
      </div>
      <div class="arre" style="display:none;">
      <strong>Cuota a pagar</strong><br />
      <input type="text" id="cuota" />
      </div>    
      <div class="arre" style="display:none;">
      <strong>Inter&eacute;s por</strong><br />
      <input type="text" id="intp" />
      </div>           
    </td>
    <td width="34%" align="left" valign="top">
      <strong>Operador de cobranza</strong><br />
      <select id="opr"><option value="nn">[-Sin operador-]</option></select>
    </td>
    </tr>    
  </table>

    </td>
  </tr>
  <tr>
    <td>    
    </td>
  </tr>  
  <tr>
    <td>
<button id="save" class="btn"><span class="ui-icon ui-icon-disk"></span>Guardar nuevo cr&eacute;dito</button>
<button id="hist" class="btn"><span class="ui-icon ui-icon-disk"></span>Historial de cr&eacute;ditos</button>
    </td>
  </tr>
</table>