<script type="text/javascript">
$(document).ready(function(){
	$("#radio_prestamo").buttonset();
	$("#radio_tprestamo").buttonset();
	$("#operador").load("sicpri/02_prestamos/operadores.php",function(data){
		$(this).selectmenu();
	});
	$("#cod_pre").autocomplete({
		source: "sicpri/02_prestamos/_buscar_prestamo.php",
		minLength: 2
	});	
	
	$("#cod_cli").autocomplete({
		source: "sicpri/02_prestamos/_buscar_codigo.php",
		minLength: 2, 
		select: function(event,ui){ 
			var c=ui.item.value;
			$("#nom_cli").val(ui.item.id);		
		}
	});
	
	$("#nom_cli").autocomplete({
		source: "sicpri/02_prestamos/_buscar_nombre.php",
		minLength: 2, 
		select: function(event,ui){
			var c=ui.item.id;
			$("#cod_cli").val(ui.item.id);
		}
	});	
	
	$("#cbo_gal").selectmenu();	

	$("#cbo_zon").load("sicpri/funciones/getZonas.php", function(data){
		$(this).selectmenu();
	});	

	$("#cbo_zon").change(function(data){
		$("#cbo_zon option:selected").each(function(){
			var i=$(this).val();
			$("#cbo_gal").load("sicpri/funciones/getGalerias.php",{i:i},function(data){
				$(this).selectmenu();
			});	
		});
	});	

	$.datepicker.setDefaults($.datepicker.regional["es"]);
	$("#fec_dsd").datepicker({
		dateFormat: "dd-mm-yy",
		numberOfMonths: 3,		
		showButtonPanel: true/*, showOn: "button", buttonImage: "img/calendar.gif", buttonImageOnly: true */
	});
	$("#fec_hst").datepicker({dateFormat: "dd-mm-yy", numberOfMonths: 3,	showButtonPanel: true});	

	$("#genr").click(function(){
		var tcr=$("input[name=tprest]:radio:checked").val();
		var cod=$("#cod_pre").val();
		var cdc=$("#cod_cli").val();
		var nom=$("#nom_cli").val();
		var dsd=$("#fec_dsd").val();
		var hst=$("#fec_hst").val();
		var tip=$("input[name=prest]:radio:checked").val();
		var opr=$("#operador").val();
		var otx=$("#operador option:selected").text();	
		var zon=$("#cbo_zon").val();
		var ztx=$("#cbo_zon option:selected").text();
		var gal=$("#cbo_gal").val();		
		var gtx=$("#cbo_gal option:selected").text();
		var page="";
		
		switch(tcr){
			case "cre_his": page="06_historial.php"; break; 
			case "cre_vig": page="09_vigentes.php"; break; 
			case "cre_can": page="07_cancelados.php"; break; 
			case "cre_mor": page="08_hoy.php"; break; 
			case "cre_moa": page="11_morosos.php"; break;
		}
				
		$.post("sicpri/02_prestamos/"+page,{cod:cod,cdc:cdc,nom:nom,dsd:dsd,hst:hst,tip:tip,opr:opr,otx:otx,zon:zon,gal:gal,ztx:ztx,gtx:gtx,r:"r"},function(data){
			$("#contenido_sicpri").html(data);
			$("#sicpri_tit").empty().text("RESULTADO DE BUSQUEDA");
		});		
	});		
	
	$("#hist").click(function(){
		$.post("sicpri/02_prestamos/06_historial.php",function(data){
			$("#contenido_sicpri").html(data);
			$("#sicpri_tit").empty().text("HISTORIAL DE CREDITOS");
		});
	});	
});
</script>
<table width="100%" border="0" id="tbl_prp">
  <tr>
    <td colspan="2">
        <table width="100%" border="0">
          <tr>
            <td width="25%"><strong>Tipo de cr&eacute;dito</strong></td>
            <td width="25%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="2" align="left" valign="top">
    		<div id="radio_tprestamo">
	       	<input type="radio" value="cre_his" name="tprest" id="tcre1" checked="checked" /><label for="tcre1">Historial</label>
            <input type="radio" value="cre_vig" name="tprest" id="tcre2" /><label for="tcre2">Vigentes</label>
	       	<input type="radio" value="cre_can" name="tprest" id="tcre3" /><label for="tcre3">Cancelados</label>
	        <input type="radio" value="cre_mor" name="tprest" id="tcre4" /><label for="tcre4">Vencidos</label>
            <input type="radio" value="cre_moa" name="tprest" id="tcre5" /><label for="tcre5">Morosos</label>
	        </div>            
            <strong>Datos de b&uacute;squeda</strong><br />                       
            C&oacute;digo de cr&eacute;dito<br /><input type="text" id="cod_pre" /><br />
            C&oacute;digo de cliente<br /><input type="text" id="cod_cli" /><br />
            Nombre de cliente<br/><input type="text" id="nom_cli" /><br />
            Fecha desde<br/><input type="text" id="fec_dsd" /><br />
            Fecha hasta<br/><input type="text" id="fec_hst" /><br />
            Tipo de pr&eacute;stamo<br />
    		<div id="radio_prestamo">
	       	<input type="radio" value="t" name="prest" id="all" checked="checked" /><label for="all">Todos</label>
	       	<input type="radio" value="p" name="prest" id="opp" /><label for="opp">Consignaci&oacute;n por mes</label>
	        <input type="radio" value="a" name="prest" id="opa" /><label for="opa">Consignaci&oacute;n por d&iacute;a</label>
	        </div>
            Operador<br /><select id="operador"></select><br />
            Seleccione Zona<br/><select id="cbo_zon"><option value="nn">[-Seleccione-]</option></select><br />
            Seleccione Galer&iacute;a<br/><select name="cbo_gal" id="cbo_gal"><option value="nn">[-Sin galeria-]</option></select>
            </td>
            <td align="left" valign="top">&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
          </tr>
        </table>
    </td>
  </tr> 
  <tr>
    <td>
	<button id="genr" class="btn"><span class="ui-icon ui-icon-search"></span>Generar resultados para reporte</button>
	<button id="hist" class="btn"><span class="ui-icon ui-icon-folder-open"></span>Historial de cr&eacute;ditos</button>    
    </td>
  </tr>
</table>