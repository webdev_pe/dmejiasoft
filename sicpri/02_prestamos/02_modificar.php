<?php
require("../../poo/clases/getConection.php");
$id  = $_POST['i'];
$cod = $_POST['c'];
$ver = $_POST['v'];
$cn=new getConection();
if(isset($id)){
	$sql="select id_pre, cod_pre, c.cod_cli, u.cod_usu, u.id_usu, mnt_pre, p.dia_pre, p.int_pre, p.fra_pre,
 tip_pre, concat(c.nom_cli,' ',c.ape_cli) as 'nombres', c.id_zon, interes_por, moroso
from si_prestamos p, si_clientes c, si_usuarios u
where p.id_cli=c.id_cli and p.id_usu=u.id_usu and id_pre=".$id;		
	$cn->ejecutar_sql(base64_encode($sql));
	$cel=$cn->resultado_sql();
}else if(isset($cod)){
	$sql="select id_pre, cod_pre, c.cod_cli, u.cod_usu, u.id_usu, mnt_pre, p.dia_pre, p.int_pre, p.fra_pre,
 tip_pre, concat(c.nom_cli,' ',c.ape_cli) as 'nombres', c.id_zon, interes_por, moroso
from si_prestamos p, si_clientes c, si_usuarios u
where p.id_cli=c.id_cli and p.id_usu=u.id_usu and cod_pre='".$cod."'";	
	$cn->ejecutar_sql(base64_encode($sql));
	$cel=$cn->resultado_sql();
}
?>
<script type="text/javascript">
$(document).ready(function() {
	$("#radio_prestamo").buttonset();
	$("#radio_dias").buttonset();
	$("#radio_intr").buttonset();
	$("#radio_moroso").buttonset();	
		
	<?php if(isset($id) || isset($cod)){?>
	$("#opr").load("sicpri/02_prestamos/traer_operadores.php",{i:'<?=$cel['id_zon']?>',u:'<?=$cel['id_usu']?>'},function(data){
		$(this).empty().html(data).selectmenu();
	});	
	<?php }else{?>
	$("#opr").selectmenu();
	<?php }?>	

	<?php if($cel['tip_pre']=="p"){?>
		$(".porc").show(); $(".arre").hide(); 
	<?php }else{?>
		$(".arre").show(); $(".porc").hide();
	<?php }?>	

	$("input[name=prest]:radio").click(function(){
		switch($(this).attr("id")){
			case "opp": $(".porc").show(); $(".arre").hide(); break;
			case "opa": $(".arre").show(); $(".porc").hide(); break;			
		}
	});
	
	
	$("input[name=dias]:radio").click(function(){
		switch($(this).attr("id")){
			case "op27": dias=27; $("#odias").hide(); break;
			case "op30": dias=30; $("#odias").hide(); break;			
			case "op00": 
				$("#odias").show().focus();
			break;
		}
	});
	$("input[name=intr]:radio").click(function(){
		switch($(this).attr("id")){
			case "op08": intr=0.08; $("#ointr").hide(); break;
			case "op10": intr=0.10; $("#ointr").hide(); break;			
			case "opOI": 
				$("#ointr").show().focus();
			break;
		}
	});	
	
	$("#cod_pre").autocomplete({
		source: "sicpri/02_prestamos/_buscar_prestamo.php",
		minLength: 2
	});	

	$("#cod_cli").autocomplete({
		source: "sicpri/02_prestamos/_buscar_codigo.php",
		minLength: 2, 
		select: function(event,ui){ 
			var c=ui.item.value;
			$("#nom_cli").val(ui.item.id);		
		}
	});
	
	$("#nom_cli").autocomplete({
		source: "sicpri/02_prestamos/_buscar_nombre.php",
		minLength: 2, 
		select: function(event,ui){
			var c=ui.item.id;
			$("#cod_cli").val(ui.item.id);
		}
	});	
			
	$("#edit").click(function(){
		var idp=$("#idp").val();
		
		var cli=$("#id").val();
		var usu=$("#opr").val();
		var tip=$("input[name=prest]:radio:checked").val();
		var est=$("input[name=mor]:radio:checked").val();
		var dia;

		if(tip=="p"){
			var ddd=$("input[name=dias]:radio:checked").val();
			if(ddd==27 || ddd==30){ dia=ddd;}
			else{dia=$("#odias").val();}
			
			var iii=$("input[name=intr]:radio:checked").val();
			if(iii==0.08 || iii==0.10){ int=iii;}
			else{int=$("#ointr").val();}			
		}else{ 
			dia=0;
			int=0;
		}
		
		var cuo=(tip=="a")?$("#cuota").val():0;
		var mnt=$("#monto").val();	
		var inp=($("#intp").val()=="")?0:$("#intp").val();
		
		if(tip=="a" && cuo==""){ alert("Ingrese una cuota."); $("#cuota").focus(); }		
		else if(mnt==""){ alert("Ingrese un monto."); $("#monto").focus(); }
		else if(usu=="nn"){ alert("Seleccione un operador de cobranza."); $("#opr").focus(); }
		else{
				$.post("sicpri/02_prestamos/dao.php",{opt:"u",idp:idp,cli:cli,usu:usu,tip:tip,est:est,dia:dia,int:int,cuo:cuo,mnt:mnt,inp:inp},function(data){
					if(data==1){ 
						alert("Prestamo editado correctamente.");
						$.post("sicpri/02_prestamos/02_modificar.php",function(data){
							$("#contenido_sicpri").html(data);
							$("#sicpri_tit").empty().text("MODIFICAR CREDITO");
						});	
					}else{
						alert("Vuelva a intentarlo por favor.");
					}
				});
		}
	});	

	$("#find").click(function(){
		var c=$("#cod_pre").val();
		if(c!=""){
			$.post("sicpri/02_prestamos/02_modificar.php",{c:c},function(data){
				$("#contenido_sicpri").html(data);
				$("#sicpri_tit").empty().text("MODIFICAR PRESTAMO");
			});
		}else{ alert("Ingrese el codigo de prestamos a modificar."); $("#cod_pre").focus(); }
	});		
	$("#hist").click(function(){
		$.post("sicpri/02_prestamos/06_historial.php",function(data){
			$("#contenido_sicpri").html(data);
			$("#sicpri_tit").empty().text("HISTORIAL DE CREDITOS");
		});
	});		
});
</script>
<input type="hidden" id="idp" value="<?=$cel['id_pre']?>" /> 
<table width="100%" border="0" id="tbl_prp">
  <tr>
    <td colspan="2">
    
<table width="100%" border="0">
  <?php if(!isset($id) && !isset($cod)){?>
  <tr>
	<td><strong>Buscar cr&eacute;dito:</strong><br />
        C&oacute;digo de cr&eacute;dito:<br />
        <input type="text" id="cod_pre" /><br />
        <button id="find" class="btn"><span class="ui-icon ui-icon-search"></span>Buscar cr&eacute;dito</button>
	</td>
  </tr> 
  <tr>
	<td>
	</td>
  </tr>   
  <?php }?>  
  <tr>
    <td width="33%" align="left" valign="top">
    <strong>Cr&eacute;dito: <?=$cel['cod_pre']?></strong><br />
	<strong>Cliente</strong><br /><input type="hidden" id="id" value="<?=$cel['id_cli']?>" /> 
    C&oacute;digo<br /><input type="text" id="cod_cli" disabled="disabled" value="<?=$cel['cod_cli']?>" /><br />
    Nombre<br /><input type="text" id="nom_cli" disabled="disabled" value="<?=$cel['nombres']?>"  />     
	</td>
    <td width="33%" align="left" valign="top">
      <strong>Tipo de Cr&eacute;dito</strong>
      <div id="radio_prestamo">
        <input type="radio" value="p" name="prest" id="opp" <?php if($cel['tip_pre']=="p"){?>checked="checked"<?php }?> /><label for="opp">Consignaci&oacute;n por mes</label>
        <input type="radio" value="a" name="prest" id="opa" <?php if($cel['tip_pre']=="a"){?>checked="checked"<?php }?> /><label for="opa">Consignaci&oacute;n por d&iacute;a</label>
        </div>
      Estado
      <div id="radio_moroso">
        <input type="radio" value="n" name="mor" id="morn" <?php if($cel['moroso']=="n"){?>checked="checked"<?php }?>  /><label for="morn">Normal</label>
        <input type="radio" value="m" name="mor" id="morm" <?php if($cel['moroso']=="m"){?>checked="checked"<?php }?> /><label for="morm">Moroso</label>
        </div>            
	  <div class="porc">
          Cantidad de D&iacute;as
          <div id="radio_dias">
            <input type="radio" value="27" name="dias" id="op27" <?php if($cel['dia_pre']=="27"){?>checked="checked"<?php }?> /><label for="op27">27</label>
            <input type="radio" value="30" name="dias" id="op30" <?php if($cel['dia_pre']=="30"){?>checked="checked"<?php }?> /><label for="op30">30</label>
            <input type="radio" value="00" name="dias" id="op00" <?php if($cel['dia_pre']!="27" && $cel['dia_pre']!="30"){?>checked="checked"<?php }?> /><label for="op00">Otro #</label>
          </div>      
		  <input type="text" id="odias" <?php if($cel['dia_pre']=="27" || $cel['dia_pre']=="30"){?>style="display:none;"<?php }?>  value="<?=$cel['dia_pre']?>" />
	  </div>
	  <div class="porc">
          Cantidad de Inter&eacute;s
          <div id="radio_intr">
            <input type="radio" value="0.08" name="intr" id="op08" <?php if($cel['int_pre']=="0.08"){?>checked="checked"<?php }?> /><label for="op08">8%</label>
            <input type="radio" value="0.10" name="intr" id="op10" <?php if($cel['int_pre']=="0.10"){?>checked="checked"<?php }?> /><label for="op10">10%</label>
            <input type="radio" value="00"   name="intr" id="opOI" <?php if($cel['int_pre']!="0.08" && $cel['int_pre']!="0.10"){?>checked="checked"<?php }?> /><label for="opOI">Otro %</label>
          </div>      
		  <input type="text" id="ointr" <?php if($cel['int_pre']=="0.08" || $cel['int_pre']=="0.10"){?>style="display:none;"<?php }?>  value="<?=$cel['int_pre']?>"  />
	  </div>
      <div class="norm">
      Monto a prestar<br />
      <input type="text" id="monto"  value="<?=$cel['mnt_pre']?>" />
      </div>
      <div class="arre" style="display:none;">
      Cuota a pagar<br />
      <input type="text" id="cuota"  value="<?=$cel['fra_pre']?>" />
      </div>   
      <div class="arre" style="display:none;">
      Inter&eacute;s por<br />
      <input type="text" id="intp"  value="<?=$cel['interes_por']?>" />
      </div>            
    </td>
    <td width="34%" align="left" valign="top">
      <strong>Operador de cobranza</strong><br />
      <select id="opr"><option value="nn">[-Sin operador-]</option></select>
    </td>
    </tr>    
  </table>

    </td>
  </tr>
  <tr>
    <td>    
    </td>
  </tr>  
  <tr>
    <td>
    <?php if(isset($id) || isset($cod)){ if(!isset($ver)){?>
	<button id="edit" class="btn"><span class="ui-icon ui-icon-disk"></span>Modificar cr&eacute;dito</button>
    <?php }}?>
	<button id="hist" class="btn"><span class="ui-icon ui-icon-folder-open"></span>Historial de cr&eacute;ditos</button>
    </td>    
  </tr>
</table>