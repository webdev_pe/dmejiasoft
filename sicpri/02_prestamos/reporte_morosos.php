<?php session_start(); set_time_limit(0); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SICPRI : Presentaci&oacute;n General de Reporte</title>
	<link href="../../images/favicon.png" rel="shortcut icon" />
	<link rel="stylesheet" type="text/css" href="../../js/jquery-ui-1.8.16.custom/development-bundle/themes/custom-theme/jquery.ui.all.css">
    <script type="text/javascript" src="../../js/jquery_layout/jquery-latest.js"></script>
	<link type="text/css" href="../../js/jquery-ui-1.8.16.custom/css/custom-theme/jquery-ui-1.8.16.custom.css" rel="stylesheet" />	
	<script type="text/javascript" src="../../js/jquery-ui-1.8.16.custom/js/jquery-ui-1.8.16.custom.min.js"></script>    
    <script type="text/javascript" charset="utf-8" src="../../js/datatable/js/jquery-ui.js"></script>
	<script type="text/javascript" charset="utf-8" src="../../js/jquery_datatable/js/jquery.dataTables.js"></script>  
    <link rel="stylesheet" type="text/css" href="../../js/jquery_datatable/css/demo_table_jui.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="../../js/jquery_datatable/css/demo_table.css" media="screen" /> 
    <link rel="stylesheet" type="text/css" href="../../main.css" media="screen" /> 
    <link rel="stylesheet" type="text/css" href="../../css/print.css" media="print" />
    <link rel="stylesheet" type="text/css" href="../../css/screen.css" media="screen" />
	<script type="text/javascript">
    $(document).ready(function(){
		$('#list_usu').dataTable({ 'bJQueryUI': true, "bPaginate": false, "bSort": false, "bAutoWidth" : false, "bSortClasses": false });
	});
    </script>
</head>
<body>
<?php
$sql="
select distinct p.id_pre, cod_pre, l.cod_cli, concat(l.nom_cli,' ',l.ape_cli) as 'nombres', u.cod_usu, 
case p.tip_pre 
when 'a' then 'C/D&iacute;a' 
when 'p' then 'C/Mes' 
end as 'tipo', z.nom_zon, g.nom_gal, 
mnt_pre, (select sum(mnt_cob) from si_cobranzas where id_pre=p.id_pre) as 'mnt_cob', 
dia_pre, int_pre, (mnt_pre*int_pre) as 'cuota',
date_format(p.fecha,'%d-%m-%Y') as 'fecha1', 
date_format((select fecha from si_cobranzas where id_pre=p.id_pre order by id_cob desc limit 1),'%d-%m-%Y') as 'fecha2', 
datediff((select fecha from si_cobranzas where id_pre=p.id_pre order by id_cob desc limit 1),p.fecha) as 'dias' 
from si_prestamos p, si_cobranzas c, si_clientes l, si_usuarios u, si_zonas z, si_galerias g 
where p.id_pre=c.id_pre and p.id_cli=l.id_cli and u.id_usu=p.id_usu and l.id_zon=z.id_zon and l.id_gal=g.id_gal and 
tip_pre='p' and est_pre='a' and 
datediff((select fecha from si_cobranzas where id_pre=p.id_pre order by id_cob desc limit 1),p.fecha) >= 10 
order by p.id_pre desc
";
require("../../poo/clases/getConection.php");
$cn=new getConection();
$cn->ejecutar_sql(base64_encode($sql));
$cn->cantidad_sql();
?>
<div id="opciones">
<button id="imp" class="btn" onclick="javascript:print()"><span class="ui-icon ui-icon-print"></span>Imprimir resultados</button>
<input type="hidden" id="s" value="<?=$_GET['s']?>" />
</div>
<div id="contenedor">
	<div id="cabecera">
     <div id="logo"><img src="../../images/login/logo.png" width="106" height="106" /></div>
     <div id="descripcion">
     <strong>REPORTE DE PR&Eacute;STAMOS EN MORA</strong><br /><hr />
     <strong>Generado el:</strong> <?=date("d-m-Y")?> - <strong>a las</strong> <?=date("H:m:s")?><hr />
     </div>
  </div>
  <div id="cuerpo">
  <table id="list_usu" class="list_tbl" width="100%">
	<thead>
        <tr>    
	        <th>N&ordm;</th> 
	        <th>CODIGO</th>
	        <th>CLIENTE</th>
	        <th>NOMBRE DEL CLIENTE</th>
	        <th>OPERADOR</th>
            <th>MONTO</th>                      
            <th>M. COBRADO</th>
            <th>M. PENDIENTE</th>
            <th>TIPO</th>          
        	<th>ZONA/GALER&Iacute;A</th>            
            <th>FECHA</th>
	        <th>D&Iacute;A</th>       
        </tr>   
	</thead>
	<tbody>
    <?php 
	$i=1;
		while($cell=$cn->resultado_sql()){ 
	?>
      <tr>    
        <td align="center"><?=$i++?></td>       
        <td align="center"><?=$cell['cod_pre']?></td>
        <td align="center"><?=$cell['cod_cli']?></td>
        <td align="center"><?=$cell['nombres']?></td>        
        <td align="center"><?=$cell['cod_usu']?></td>
        <td align="right"><span>S/.</span><?=number_format($cell['mnt_pre'],2,'.',',')?><br />
		<span>S/.</span><?=number_format($cell['cuota'],2,'.',',')?>
        </td>           
        <td align="right"><span>S/.</span><?=number_format($cell['mnt_cob'],2,'.',',')?></td>
        <td align="right"><span>S/.</span><?=number_format($cell['mnt_pre']+$cell['cuota']-$cell['mnt_cob'],2,'.',',')?></td>     
        <td align="center"><?=$cell['tipo']?></td> 
        <td align="left"><?=$cell['nom_zon']?>/<?=$cell['nom_gal']?></td>           
        <td align="center"><?=$cell['fecha1']?><br /><?=$cell['fecha2']?></td>        
        <td align="center"><?=$cell['dias']?></td>          
      </tr>
    <?php
		$montos1+=$cell['mnt_pre']+$cell['cuota'];
		$montos2+=$cell['mnt_cob'];	
		}
		$montos3=$montos1-$montos2;
		$cn->limpiar_sql(); $cn->cerrar_sql();
	?>
    </tbody> 
	<tfoot>
      <tr>    
        <td colspan="5" align="left">TOTALES: &raquo;</td>
        <td align="right"><span>S/.</span><?=number_format($montos1,2,'.',',')?></td>
        <td align="right"><span>S/.</span><?=number_format($montos2,2,'.',',')?></td>   
        <td align="right"><span>S/.</span><?=number_format($montos3,2,'.',',')?></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>      
    </tfoot>     
    </table>
    </div>
    <div id="pie">
    <hr />SICPRI : Sistema Integral de Prestamos y Cobranzas <br /> &copy; <?=date("Y")?>
    </div>
</div>
</body>
</html>