<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SICPRI - DETALLE DE PROPIEDADES</title>
<style type="text/css" media="screen">
body{ margin:0px; padding:0px; color:#666666; font-size:11px; font-family:Verdana;}
#a4{ position:relative; width:780px; height:1000px; margin:0 auto; margin-top:10px; margin-bottom:10px; background:#ffffff; border:1px solid #ccc;}

#a4_cab{ position:absolute; width:760px; height:150px; border:1px solid #ccc; top:10px; left:10px;}
#a4_cab_logo{ position:absolute; width:85px; height:122px; top:14px; left:10px;}
#a4_cab_desc{ position:absolute; width:645px; height:122px; top:14px; left:105px;}
#a4_cab_desc_tit{ position:absolute; width:645px; height:30px; top:0px; left:0px; font-size:12px; font-weight:bold; text-align:center;}
#a4_cab_desc_cnt{ position:absolute; width:625px; height:72px; top:40px; left:10px; font-size:11px; font-weight:normal; line-height:20px;}

#a4_cue{ position:absolute; width:760px; height:760px; border:1px solid #ccc; top:170px; left:10px;}
#a4_cue_img{ position:absolute;	width:365px; height:365px; border:1px solid #ccc; top:10px; left:10px;}
#a4_cue_b01{ position:absolute;	width:365px; height:365px; top:10px; right:10px; line-height:20px; border:1px solid #ccc;}
#a4_cue_des{ word-wrap: break-word; width:360px; height:130px; overflow-x:hidden; overflow-y:auto; border:1px solid #ccc;}
#a4_cue_b02{ position:absolute;	width:365px; height:365px; bottom:10px; left:10px; line-height:20px; border:1px solid #ccc;}
#a4_cue_b03{ position:absolute;	width:365px; height:365px; bottom:10px; right:10px; line-height:20px; border:1px solid #ccc;}

#a4_pie{ position:absolute; width:760px; height:50px;  border:1px solid #ccc; bottom:10px; left:10px;}
</style>
<style type="text/css" media="print">
body{ margin:0px; padding:0px; color:#666666; font-size:11px; font-family:Verdana;}
#a4{ position:relative; width:780px; height:1000px; margin:0 auto; margin-top:10px; margin-bottom:10px; background:#ffffff; border:1px solid #ccc; page-break-after:always;}

#a4_cab{ position:absolute; width:760px; height:150px; border:1px solid #ccc; top:10px; left:10px;}
#a4_cab_logo{ position:absolute; width:85px; height:122px; top:14px; left:10px;}
#a4_cab_desc{ position:absolute; width:645px; height:122px; top:14px; left:105px;}
#a4_cab_desc_tit{ position:absolute; width:645px; height:30px; top:0px; left:0px; font-size:12px; font-weight:bold; text-align:center;}
#a4_cab_desc_cnt{ position:absolute; width:625px; height:72px; top:40px; left:10px; font-size:11px; font-weight:normal; line-height:20px;}

#a4_cue{ position:absolute; width:760px; height:760px; border:1px solid #ccc; top:170px; left:10px;}
#a4_cue_img{ position:absolute;	width:365px; height:365px; border:1px solid #ccc; top:10px; left:10px;}
#a4_cue_b01{ position:absolute;	width:365px; height:365px; border:1px solid #ccc; top:10px; right:10px; line-height:20px;}
#a4_cue_des{ width:360px; height:130px; border:1px solid #ccc; text-overflow:ellipsis;	overflow:hidden; white-space:nowrap;}

#a4_pie{ position:absolute; width:760px; height:50px;  border:1px solid #ccc; bottom:10px; left:10px;}
</style>
</head>
<body>
<?php
require("../../poo/clases/getConection.php");
$cn=new getConection();
$sql="
select id_prp, cod_prp, date_format(fecha_prp,'%d-%m-%Y') as 'fecha', fecha_prp, hora_prp, 
case estado
	when 'd' then 'Disponible'
	when 'b' then 'Bajo contrato'
	when 'v' then 'Vendido'
	when 'a' then 'Alquilado'
end as 'est',
residencial, comercial, industrial, terreno, proyecto, casas, departamento, dplaya, oficina,
sit_prp, dir_prp, ref_prp, des_prp, lat_prp, lon_prp, opr_prp, mon_prp, prc_prp, art_prp, act_prp, dor_prp, ban_prp, fot_prp, tfot_prp, vis_prp, vid_prp, fecha_cap_prp, fecha_cie_prp, prc_ape_prp, prc_cie_prp, com_prp, cap_ape_prp, cap_cie_prp, exc_prp, cmp_prp, fecha_ini_alquiler, fecha_fin_alquiler, nom_proy, nom_emp, dir_proy, nom_cont, tel_cont, estado_pro, frente, fondo, props_nom, props_tel, props_eml, 	
prp.id_dep, dep.nom_dep, prp.id_prv, prv.nom_prv, prp.id_dst, dst.nom_dst,
case opr_prp
	when 'v' then 'VENTA'
	when 'a' then 'ALQUILER'
end as 'opr', mon_prp, prc_prp, art_prp, act_prp, dor_prp, ban_prp, vis_prp, lat_prp, lon_prp, fot_prp
from si_propiedades prp, si_departamentos dep, si_provincias prv, si_distritos dst
where 
dep.id_dep=prp.id_dep and 
dep.id_dep=prv.id_dep and prp.id_prv=prv.id_prv and
dep.id_dep=dst.id_dep and prv.id_prv=dst.id_prv and prp.id_dst=dst.id_dst 
order by fecha_prp desc, hora_prp desc
";
$cn->ejecutar_sql(base64_encode($sql));
?>
<?php 
$i=1; 
while($cel=$cn->resultado_sql()){?>
<div id="a4">
	<div id="a4_cab">
    	<div id="a4_cab_logo"><img src="../../images/login/logo.png" width="85" height="122" /></div>
        <div id="a4_cab_desc">
	        <div id="a4_cab_desc_tit">SISTEMA INTEGRAL DE CONTROL Y PROCESOS DE RECAUDACI&Oacute;N DE INMUEBLES<br />[DETALLE DE PROPIEDADES]</div>
   	        <div id="a4_cab_desc_cnt">
                <table width="100%" border="0" cellspacing="2" cellpadding="2">
                  <tr>
                    <td width="40%"><strong>Usuario:</strong> Maycol Zambrano Nu&ntilde;ez</td>
                    <td width="60%"><strong>Tel&eacute;fonos:</strong> 99456895</td>
                  </tr>
                  <tr>
                    <td><strong>C&oacute;digo:</strong> USU0001</td>
                    <td><strong>Email:</strong> webmaster@sicpri.com</td>
                  </tr>
                  <tr>
                    <td><strong>Fecha:</strong> <?=date("d-m-Y")?></td>
                    <td>&nbsp;</td>
                  </tr>
                </table>
            </div>
        </div>
    </div>
	<div id="a4_cue">
	    <div id="a4_cue_img">
        <?php $array=explode(",",$cel['fot_prp']); $foto=$array[0];	?>
        <img src="../../poo/timthumb/timthumb.php?src=/sicpri/01_propiedades/fotos/<?=$foto?>&h=365&w=365&zc=1&a=c&q=100" width="365" height="365" />
        </div>
	    <div id="a4_cue_b01">
            <table width="100%" border="0" cellspacing="2" cellpadding="2">
              <tr>
                <td><strong>C&oacute;digo:</strong> <?=$cel['cod_prp']?></td>
              </tr>
              <tr>
				<td width="50%"><strong>Estado:</strong> <?=$cel['est']?></td>
              </tr>
              <tr>
                <td><strong>Tipo:</strong>
					<?php $tp=array();
					if($cel['residencial']==1) { array_push($tp,"Residencial"); }
                    if($cel['comercial']==1)   { array_push($tp,"Comercial"); }
                    if($cel['industrial']==1)  { array_push($tp,"Industrial"); }
                    if($cel['terreno']==1)     { array_push($tp,"Terreno"); }
                    if($cel['proyecto']==1)    { array_push($tp,"Proyecto"); }
					echo implode("-",$tp);?>
                </td>
              </tr>
              <tr>
                <td><strong>Sub-Tipo:</strong>
					<?php $stp=array();
					if($cel['casas']==1) 		{ array_push($stp,"Casa"); }
                    if($cel['departamento']==1)	{ array_push($stp,"Departamento"); }
                    if($cel['dplaya']==1)  		{ array_push($stp,"Dpto. de Playa"); }
                    if($cel['oficina']==1)		{ array_push($stp,"Oficina"); }
					echo implode("-",$stp);?>
                </td>
              </tr>              
              <tr>
                <td><strong>Ubicaci&oacute;n:</strong> <?=$cel['nom_dep']?> - <?=$cel['nom_prv']?> - <?=$cel['nom_dst']?></td>
              </tr>
              <tr>
				<td><strong>Sitio:</strong> <?=$cel['sit_prp']?></td>
              </tr>
              <tr>
				<td><strong>Direcci&oacute;n:</strong> <?=$cel['dir_prp']?></td>
              </tr>
              <tr>
				<td><strong>Referencia:</strong> <?=$cel['ref_prp']?></td>
              </tr>
              <tr>
				<td><strong>Descripci&oacute;n:</strong> <div id="a4_cue_des"><?=base64_decode($cel['des_prp'])?></div></td>
              </tr>                                                        
            </table>        
        </div>
        <div id="a4_cue_b02">
            <table width="100%" border="0" cellspacing="2" cellpadding="2">
              <tr>
                <td><strong>Tipo de Operaci&oacute;n:</strong> <?=$cel['cod_prp']?></td>
              </tr>
              <tr>
				<td><strong>Fecha de Inicio:</strong> <?=$cel['est']?></td>
              </tr>
              <tr>
                <td><strong>Fecha de Termino:</strong></td>
              </tr>
              <tr>
                <td><strong>Moneda:</strong></td>
              </tr>              
              <tr>
                <td><strong>Precio:</strong> <?=$cel['nom_dst']?></td>
              </tr>
              <tr>
				<td><strong>Exclusividad:</strong> <?=$cel['sit_prp']?></td>
              </tr>
              <tr>
				<td><strong>Compartido:</strong> <?=$cel['dir_prp']?></td>
              </tr>
              <tr>
				<td><strong>Referencia:</strong> <?=$cel['ref_prp']?></td>
              </tr>
              <tr>
				<td><strong>Descripci&oacute;n:</strong> <div id="a4_cue_des"><?=base64_decode($cel['des_prp'])?></div></td>
              </tr>                                                        
            </table>         
        </div>
        <div id="a4_cue_b03"></div>
  </div>    
  <div id="a4_pie"><?=$i?></div>
</div>
<?php $i++; } ?>
</body>
</html>