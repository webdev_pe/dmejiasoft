<?php session_start(); set_time_limit(0); ?>
<?php include("../rpt_head.php"); ?>
	<div id="cabecera">
     <div id="logo"><img src="../../images/login/logo.png" width="106" height="106" /></div>
     <div id="descripcion">
     <strong>REPORTE DE PR&Eacute;STAMOS</strong><br /><hr />
     <strong>Generado el:</strong> <?=date("d-m-Y")?> - <strong>a las</strong> <?=date("H:m:s")?><hr />
<?php if($_GET['cp']!=""){?><strong>C&oacute;digo de pr&eacute;stamo:</strong> <?=base64_decode($_GET['cp'])?><br /><?php }?>
<?php if($_GET['cc']!=""){?><strong>C&oacute;digo de cliente</strong> <?=base64_decode($_GET['cc'])?><br /><?php }?>
<?php if($_GET['nc']!=""){?><strong>Nombre del cliente:</strong> <?=base64_decode($_GET['nc'])?><br /><?php }?>
<?php if($_GET['fd']!=""){?><strong>Fecha de reporte desde:</strong> <?=date("d-m-Y",strtotime(base64_decode($_GET['fd'])))?><br /><?php }?>
<?php if($_GET['fh']!=""){?><strong>Fecha de reporte hasta:</strong> <?=date("d-m-Y",strtotime(base64_decode($_GET['fh'])))?><br /><?php }?>
<?php if(base64_decode($_GET['tp'])!=""){?><strong>Tipo de pr&eacute;stamo:</strong> 
	<?php
    switch(base64_decode($_GET['tp'])){
		case "a": echo "Arrebatir"; break;
		case "p": echo "Porcentual"; break;
		case "t": echo "Arrebatir/Porcentual"; break;
	}
	?><br /><?php }?>
<?php if(base64_decode($_GET['op'])!="[-Seleccione-]" && base64_decode($_GET['op'])!=""){?><strong>Operador:</strong> <?=base64_decode($_GET['op'])?><br /><?php }?>
<?php if(base64_decode($_GET['zn'])!="[-Seleccione-]" && base64_decode($_GET['zn'])!=""){?><strong>Zona:</strong> <?=base64_decode($_GET['zn'])?><br /><?php }?>
<?php if(base64_decode($_GET['gl'])!="[-Seleccione-]" && base64_decode($_GET['gl'])!="" && base64_decode($_GET['gl'])!="[-Sin galeria-]"){?><strong>Galer&iacute;a:</strong> <?=base64_decode($_GET['gl'])?><br /><?php }?>
     </div>
  </div>
  <div id="cuerpo">
  <table id="list_usu" class="list_tbl" width="100%">
	<thead>
        <tr>    
	        <th>N&ordm;</th> 
	        <th>FECHA</th>
	        <th>CODIGO</th>
	        <th>CLIENTE</th>
	        <th>NOMBRE DEL CLIENTE</th>
	        <th>OPERADOR</th>
            <th>MONTO</th>                      
            <th>TIPO</th>
            <th>ZONA/GALER&Iacute;A</th>            
            <th>ORIGEN</th>           
            <th>ESTADO</th>          
        </tr>   
	</thead>
	<tbody>
    <?php 
	if(isset($_GET['s'])){ $i=1;
		while($cell=$cn->resultado_sql()){ 
	?>
      <tr>    
        <td align="center"><?=$i++?></td>       
        <td align="center"><?=$cell['fecha']?></td>    
        <td align="center"><?=$cell['cod_pre']?></td>
        <td align="center"><?=$cell['cod_cli']?></td>
        <td align="center"><?=$cell['nombres']?></td>        
        <td align="center"><?=$cell['cod_usu']?></td>
        <td align="right"><span>S/.</span><?=number_format($cell['mnt_pre'],2,'.',',')?></td>        
        <td align="center"><?=$cell['tipo']?></td> 
        <td align="left"><?=$cell['nom_zon']?>/<?=$cell['nom_gal']?></td> 
        <td align="center"><?=$cell['titulo']?></td>                    
        <td align="center"><?=$cell['estado']?></td>             
      </tr>
    <?php  		$montos+=$cell['mnt_pre'];	
		} 
		$cn->limpiar_sql(); $cn->cerrar_sql();
	}
	?>
    </tbody> 
	<tfoot>
      <tr>    
        <td colspan="6" align="left">TOTAL PRESTADO: &raquo;</td>
        <td align="right"><span>S/.</span><?=number_format($montos,2,'.',',')?></td>   
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>      
    </tfoot>     
    </table>
    </div>
<?php include("../rpt_footer.php"); ?>