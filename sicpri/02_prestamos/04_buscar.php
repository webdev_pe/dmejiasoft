<script type="text/javascript">
$(document).ready(function(){
	$("#radio_prestamo").buttonset();
	$("#cod_pre").autocomplete({
		source: "sicpri/02_prestamos/_buscar_prestamo.php",
		minLength: 2
	});	
	
	$("#cod_cli").autocomplete({
		source: "sicpri/02_prestamos/_buscar_codigo.php",
		minLength: 2, 
		select: function(event,ui){ 
			var c=ui.item.value;
			$("#nom_cli").val(ui.item.id);		
		}
	});
	
	$("#nom_cli").autocomplete({
		source: "sicpri/02_prestamos/_buscar_nombre.php",
		minLength: 2, 
		select: function(event,ui){
			var c=ui.item.id;
			$("#cod_cli").val(ui.item.id);
		}
	});	

	$.datepicker.setDefaults($.datepicker.regional["es"]);
	$("#fec_dsd").datepicker({
		dateFormat: "dd-mm-yy",
		numberOfMonths: 3,		
		showButtonPanel: true/*, showOn: "button", buttonImage: "img/calendar.gif", buttonImageOnly: true */
	});
	$("#fec_hst").datepicker({dateFormat: "dd-mm-yy", numberOfMonths: 3,	showButtonPanel: true});	

	$("#schr").click(function(){
		var cod=$("#cod_pre").val();
		var cdc=$("#cod_cli").val();
		var nom=$("#nom_cli").val();
		var dsd=$("#fec_dsd").val();
		var hst=$("#fec_hst").val();
		var tip=$("input[name=prest]:radio:checked").val();
		
		$.post("sicpri/02_prestamos/06_historial.php",{cod:cod,cdc:cdc,nom:nom,dsd:dsd,hst:hst,tip:tip},function(data){
			$("#contenido_sicpri").html(data);
			$("#sicpri_tit").empty().text("RESULTADO DE BUSQUEDA");
		});		
	});	
	
	$("#hist").click(function(){
		$.post("sicpri/02_prestamos/06_historial.php",function(data){
			$("#contenido_sicpri").html(data);
			$("#sicpri_tit").empty().text("HISTORIAL DE PRESTAMOS");
		});
	});	
});
</script>
<table width="100%" border="0" id="tbl_prp">
  <tr>
    <td colspan="2">
        <table width="100%" border="0">
          <tr>
            <td width="25%"><strong>Datos de b&uacute;squeda</strong></td>
            <td width="25%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
          </tr>
          <tr>
            <td align="left" valign="top">
            Codigo de prestamo<br /><input type="text" id="cod_pre" /><br />
            Codigo de cliente<br /><input type="text" id="cod_cli" /><br />
            Nombre de cliente<br/><input type="text" id="nom_cli" />
            Fecha desde<br/><input type="text" id="fec_dsd" />
            Fecha hasta<br/><input type="text" id="fec_hst" />
            Tipo de prestamo<br />
    		<div id="radio_prestamo">
	       	<input type="radio" value="p" name="prest" id="opp" checked="checked" /><label for="opp">Porcentaje</label>
	        <input type="radio" value="a" name="prest" id="opa" /><label for="opa">Arrebatir</label>
	        </div>
            </td>
            <td align="left" valign="top">&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
          </tr>
        </table>
    </td>
  </tr> 
  <tr>
    <td>
	<button id="schr" class="btn"><span class="ui-icon ui-icon-search"></span>Buscar prestamo</button>
	<button id="hist" class="btn"><span class="ui-icon ui-icon-folder-open"></span>Historial de prestamos</button>
    </td>
  </tr>
</table>