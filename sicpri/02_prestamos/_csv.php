<?php session_start(); 
header("Content-type: text/plain");
header("Content-Disposition: attachment; filename=sicpri_reporte_propiedades.csv");

date_default_timezone_set("America/Lima");
$fecha = date("d-m-Y");
$hora  = date("H:i:s");		

require("../../poo/clases/getConection.php");
$cn=new getConection();
$sql=utf8_decode(base64_decode($_GET['s']));
$cn->ejecutar_sql(base64_encode($sql));

echo 'Nro;"CODIGO";"FECHA";"ESTADO";"TIPOS";"UBICACION";"PRECIO";"AREA TERRENO";"AREA CONSTRUIDA"';
echo "\n";
$i=1; 
while($cell=$cn->resultado_sql()){ 
	$tipos=(($cell['residencial']==1)?" R":"").(($cell['comercial']==1)?" C":"").(($cell['industrial']==1)?" I":"").(($cell['terreno']==1)?" T":"").(($cell['proyecto']==1)?" P":"");
	$ubicacion=$cell['nom_dep']." - ".$cell['nom_prv']." - ".$cell['nom_dst'];
	$precio=($cell['mon_prp']=="$")?"$ ":"S/. ";
	$precio.=number_format($cell['prc_prp'],2,".",",");
	echo $i++.';"'.$cell['cod_prp'].'";"'.$cell['fecha'].'";"'.$cell['est'].'";"'.$tipos.'";"'.$ubicacion.'";"'.$precio.'";"'.$cell['art_prp'].'";"'.$cell['act_prp'].'"';
	echo "\n";
} 
$cn->limpiar_sql(); 
$cn->cerrar_sql(); 
?>