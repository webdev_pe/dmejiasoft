<?php
require("../poo/clases/getConection.php");
require("../poo/zipfile/zipfile.php");

$idu=$_GET['i'];
$cam=$_GET['c'];
switch($cam){
	case "ex": $c="contrato_exclusividad"; $t="si_documentacion_propietarios"; $i="id_doc_pro"; $d="02_propietarios/documentos"; break;
	case "dni": $c="dni"; $t="si_documentacion_propietarios"; $i="id_doc_pro"; $d="02_propietarios/documentos"; break;
	case "pri": $c="partida_registral_inmueble"; $t="si_documentacion_propietarios"; $i="id_doc_pro"; $d="02_propietarios/documentos"; break;
	case "hr": $c="hr"; $t="si_documentacion_propietarios"; $i="id_doc_pro"; $d="02_propietarios/documentos"; break;
	case "pu": $c="pu"; $t="si_documentacion_propietarios"; $i="id_doc_pro"; $d="02_propietarios/documentos"; break;
	case "add": $c="acta_divorcio"; $t="si_documentacion_propietarios"; $i="id_doc_pro"; $d="02_propietarios/documentos"; break;
	case "cri": $c="cri"; $t="si_documentacion_propietarios"; $i="id_doc_pro"; $d="02_propietarios/documentos"; break;
	case "pr": $c="poderes_registrados"; $t="si_documentacion_propietarios"; $i="id_doc_pro"; $d="02_propietarios/documentos"; break;
	case "si": $c="sucesion_intestada"; $t="si_documentacion_propietarios"; $i="id_doc_pro"; $d="02_propietarios/documentos"; break;
	
	case "rep": $c="dni_representantes"; $t="si_documentacion_propietarios"; $i="id_doc_pro"; $d="02_propietarios/documentos"; break;
	case "ruc": $c="ficha_ruc_empresa"; $t="si_documentacion_propietarios"; $i="id_doc_pro"; $d="02_propietarios/documentos"; break;
	case "emp": $c="partida_registral_inscripcion_empresa"; $t="si_documentacion_propietarios"; $i="id_doc_pro"; $d="02_propietarios/documentos"; break;
	case "vig": $c="vigencia_poderes"; $t="si_documentacion_propietarios"; $i="id_doc_pro"; $d="02_propietarios/documentos"; break;	
	
	case "pl": $c="planos"; $t="si_documentacion_propietarios"; $i="id_doc_pro"; $d="02_propietarios/documentos"; break;	
	case "pll": $c="planos_localizacion"; $t="si_documentacion_propietarios"; $i="id_doc_pro"; $d="02_propietarios/documentos"; break;	
	case "par": $c="certificado_parametros"; $t="si_documentacion_propietarios"; $i="id_doc_pro"; $d="02_propietarios/documentos"; break;	

	case "doc": $c="dni"; $t="si_documentacion_compradores"; $i="id_doc_com"; $d="04_compradores/documentos"; break;		
}

$zipfile = new zipfile();
$zipfile->add_dir($c."/");

$cn=new getConection();
$sql="select $c from $t where $i=$idu";
$stm=$cn->ejecutar_sql(base64_encode($sql));
$row=$cn->cantidad_sql();
$cel=$cn->resultado_sql();
if($row>0){
	if($cel[$c]!=""){
		$array=explode(",",$cel[$c]);
		foreach($array as $a){
			$a=base64_decode($a);
			$zipfile->add_file(implode("",file($d."/".$a)), $c."/".$a);
		}
	}
}

/*$zipfile->add_file(implode("",file("img01.jpg")), "img/img1.jpg");
$zipfile->add_file(implode("",file("img02.jpg")), "img/img2.jpg");
$zipfile->add_file(implode("",file("img03.jpg")), "img/img3.jpg");*/

header("Content-type: application/octet-stream");
header("Content-disposition: attachment; filename=".$c.".zip");
echo $zipfile->file();
?>