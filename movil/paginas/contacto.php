<!DOCTYPE html> 
<html class="ui-mobile-rendering"> 
	<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
	<title>SIPC - Contacto</title> 
	<link rel="stylesheet"  href="../jquery_mobile/css/themes/default/jquery.mobile.css" /> 
	<link rel="stylesheet" href="../jquery_mobile/docs/_assets/css/jqm-docs.css"/>
	<script data-main="../jquery_mobile/js/jquery.mobile.docs" src="../jquery_mobile/external/requirejs/require.js"></script>
	<script src="../jquery_mobile/js/jquery.js"></script>
	<script src="../jquery-validation-1.9.0/jquery.validate.js" type="text/javascript"></script>    
	<style type='text/css'>
    label.error {
        color: red;
        font-size: 16px;
        font-weight: normal;
        line-height: 1.4;
        margin-top: 0.5em;
        width: 100%;
        float: none;
    }
    @media screen and (orientation: portrait){
        label.error { margin-left: 0; display: block; }
    }
    @media screen and (orientation: landscape){
        label.error { display: inline-block; margin-left: 22%; }
    }
    em { color: red; font-weight: bold; padding-right: .25em; }
    </style>
	<script type='text/javascript'>//<![CDATA[ 
    $(window).load(function(){
    $("#frmCtc").validate(/*{
        submitHandler: function( form ) {

        }
    }*/);
    });//]]>  
    </script>    
</head> 
<body> 

<div data-role="page" class="type-interior">

	<div data-role="header" data-theme="f">
		<h1>Contacto</h1>
		<a href="../index.php" data-icon="home" data-iconpos="notext" data-direction="reverse" class="ui-btn-right jqm-home">Inicio</a>
	</div><!-- /header -->

	<div data-role="content">
		
		<div class="content-primary">
                <p>Complete el siguiente formulario por favor:</p>
        <form id="frmCtc" class="validate" action="funciones/email.php" method="post">
        <fieldset>
        <div data-role="fieldcontain">
        <label for="name"><em>* </em>Nombres:</label>
        <input type="text" name="nom" id="nom" value="" placeholder="Nombres" class="required"  />
        </div>
        
        <div data-role="fieldcontain">
        <label for="name"><em>* </em>Apellidos:</label>
        <input type="text" name="ape" id="ape" value="" placeholder="Apellidos" class="required"  />
        </div>
        
        <div data-role="fieldcontain">
        <label for="tel">Tel&eacute;fono:</label>
        <input type="tel" name="tel" id="tel" value="" placeholder="Tel&eacute;fono"  />
        </div>
        
        <div data-role="fieldcontain">
        <label for="email"><em>* </em>Email:</label>
        <input type="email" name="email" id="email" value="" placeholder="Email" class="required email"  />
        </div>
        
        <div data-role="fieldcontain">
        <label for="textarea"><em>* </em>Mensaje:</label>
        <textarea name="msj" id="msj" placeholder="Mensaje" class="required" ></textarea>
        </div>
        
        <!--button type="submit" data-theme="b" name="submit" value="submit-value" id="btn_env">Enviar formulario</button-->

            <div class="ui-body ui-body-b">
                <button class="btnLogin" type="submit" 
                    data-theme="a">Enviar formulario</button>
            </div>        
        </fieldset>
        </form>
		</div>		
		
		<div class="content-secondary">
			<div data-role="collapsible" data-collapsed="true" data-theme="b" data-content-theme="d">
					<h3>M&aacute;s opciones</h3>
					<ul data-role="listview" data-theme="c" data-dividertheme="d">
						<li data-role="list-divider">Soporte On-Line</li>
						<li><a href="#">Ayuda</a></li>
						<li><a href="#">Documentaci&oacute;n</a></li>
						<li><a href="../paginas/compatibilidad.php" data-transition="flip" data-inline="true">Compatibilidad M&oacute;vil</a></li>
						<li data-theme="a"><a href="../paginas/contacto.php" rel="external">Contacto</a></li>					
					</ul>
			</div>
		</div>
				

		</div><!-- /content -->

        <div data-role="footer" class="footer-docs" data-theme="c">
                <p>&copy; <?=date("Y")?> SIPC: Sistema Integral de Pr&eacute;stamos y Cobranzas</p>
        </div>

						

	</div><!-- /content -->
	
</div><!-- /page -->

</body>
</html>