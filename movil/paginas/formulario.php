<!DOCTYPE html> 
<html class="ui-mobile-rendering"> 
	<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
	<title>SIPC - COBRANZA</title> 
	<link rel="stylesheet"  href="../jquery_mobile/css/themes/default/jquery.mobile.css" /> 
	<link rel="stylesheet" href="../jquery_mobile/docs/_assets/css/jqm-docs.css"/>

	<script data-main="../jquery_mobile/js/jquery.mobile.docs" src="../jquery_mobile/external/requirejs/require.js"></script>
	<script src="../jquery_mobile/js/jquery.js"></script>

</head> 
<body> 

<div data-role="page" class="type-interior">

	<div data-role="header" data-theme="f">
		<h1><?php if($_GET['p']=="a"){echo"Arrebatir";}else{echo"Porcentual";}?></h1>
		<a href="../index.php" data-icon="home" data-iconpos="notext" data-direction="reverse" class="ui-btn-right jqm-home">Inicio</a>
	</div><!-- /header -->

	<div data-role="content">		
		<div class="content-primary">
		<strong>CLIENTE: <?=strtoupper($_GET['c'])?></strong><br/>
        <div data-role="fieldcontain">
		<script type="text/javascript">
        $(document).ready(function() {
			$.post("../../sicpri/01_cobranzas/traer_detalle.php",{id:'<?=$_GET['i']?>'},function(data){ //alert(data.tip);
				if(data.tip=="p"){
					$("#dro").val(data.dro);
					$("#md04").hide();
					$("#lb04").hide();
				}else{
					//$("#pre_add").val(data.saldo);
					$("#saldo").val(data.saldo);
					$("#monto").val(data.monto);
					$("#inter").val(data.inter);
				}
			},"json");
						
            $("#pre_pago").slideDown("fast");
            <?php if($_GET['p']=="a"){ ?>
            $("#pre_frac").slideDown("fast");
			$("#pre_dsto").slideDown("fast");
            <?php }?>
			$("input[name=modo]:radio").click(function(){ 
				var t=$("#tip").val();
				var c=$(this).val();
				//alert(c);
				$("#cie").val(c);
				switch($(this).attr("id")){
					case "md01":
						$("#pre_pago").hide();
						$("#pre_frac").hide();
						$("#pre_agre").hide();
						$("#pre_dsto").hide();
						$("#pre_mnt").val("0");
						$("#pre_int").val("0");
					break;
					case "md02": 
						$("#pre_pago").show(); 
						$("#pre_frac").hide();
						$("#pre_dsto").hide();
						$("#pre_agre").hide();
						$("#pre_mnt").val(""); 
						$("#pre_int").val("");
						$("#pre_add").val("0");				
					break;	
					case "md03": 
						$("#pre_pago").show(); 
						if(t=="a") $("#pre_frac").show();
						if(t=="a") $("#pre_dsto").show();
						$("#pre_agre").hide(); 	
						$("#pre_mnt").val(""); 
						$("#pre_int").val("0"); 
						$("#pre_add").val("0"); 
					break;
					case "md04": 
						$("#pre_pago").hide(); 
						if(t=="a") $("#pre_frac").hide();
						if(t=="a") $("#pre_dsto").hide();
						$("#pre_agre").show(); 	
						$("#pre_mnt").val("0"); 
						$("#pre_int").val("0"); 			
					break;
				}
			});

			$("#preCob").click(function(){
				var idp=$("#idp").val();
				var dro=$("#dro").val();
				var mnt=$("#pre_mnt").val();
				var fra=$("#pre_int").val();		
				var dto=($("#pre_dto").val()!="")?parseFloat($("#pre_dto").val()):0;
				var add=($("#pre_add").val()!="")?parseFloat($("#pre_add").val()):0;
				var cie=$("#cie").val();
				var tip=$("#tip").val();
				var mto=$("#monto").val();
				var inr=$("#inter").val();
		
				$.mobile.showPageLoadingMsg("b", "Guardando, espere un momento porfavor...", true);
				$.post("../../sicpri/01_cobranzas/dao.php",{opt:"i",idp:idp,dro:dro,mnt:mnt,fra:fra,add:add,cie:cie,tip:tip,mto:mto,inr:inr,dto:dto,cob:"movil",fch:"<?=date("d-m-Y")?>"},function(data){
					if(data==1){ 
						alert("Cobranza registrada correctamente.");
						$.mobile.hidePageLoadingMsg();
						$.mobile.changePage("modulos.php?c=<?=$_GET['c']?>",{transition:"flip"});
					}else{
						alert("Vuelva a intentarlo por favor.");
						$.mobile.hidePageLoadingMsg();
					}
				});
			});				
        });
        </script>
        <input type="hidden" id="idp" value="<?=$_GET['i']?>" />
        <input type="hidden" id="dro" />
        <input type="hidden" id="tip" value="<?=$_GET['p']?>" />
        <input type="hidden" id="cie" value="pg" />

        <input type="hidden" id="saldo" />
        <input type="hidden" id="monto" />
        <input type="hidden" id="inter" />
        
        <fieldset data-role="controlgroup" data-type="horizontal" data-role="fieldcontain">
        <input type="radio" name="modo" id="md01" value="np" />
        <label for="md01">No pago</label>
        
        <input type="radio" name="modo" id="md02" value="fi" />
        <label for="md02">Refinanciar</label>
        
        <input type="radio" name="modo" id="md03" value="pg" checked="checked" />
        <label for="md03">Pago</label>

        <input type="radio" name="modo" id="md04" value="pa" />
        <label for="md04" id="lb04">Agregar</label>        
        </fieldset>
        </div>        
              <div id="pre_pago" style="display:none;">
                <strong>Capital</strong><br /><input type="text" id="pre_mnt" value="0" />
              </div> 
              <div id="pre_frac" style="display:none;">
                <strong>Inter&eacute;s</strong><br /><input type="text" id="pre_int" value="0" />
              </div>
              <div id="pre_dsto" style="display:none;">
                <strong>Descuento</strong><br /><input type="text" id="pre_dto" value="0" />
              </div>      
              <div id="pre_agre" style="display:none;">
                <strong>Agregar capital</strong><br /><input type="text" id="pre_add" value="0" />
              </div>          
		<br/><br/>


            <div class="ui-body ui-body-b">
                <button class="btnLogin" type="submit" data-icon='check' data-iconpos='top' 
                    data-theme="b" id="preCob">Guardar cobranza</button>
            </div>    
      
		</div>
        <?php
		  $i=$_GET['i'];
		  $c=$_GET['c'];
		  if($_GET['p']=="p"){
			  $page="porcentual.php?i=$i&c=$c";
		  }else{
			  $page="arrebatir.php?i=$i&c=$c";
		  }        
		?>
        <div class="content-secondary">
        <a href='<?=$page?>' data-role='button' data-icon='search' data-iconpos='left' data-theme='b'>Ver detalle</a>
		<a href="detalle.php?c=<?=$_GET['c']?>" data-role='button' data-icon='arrow-l' data-iconpos='left' data-theme='a'>Regresar</a>		
        </div>
		</div><!-- /content -->

        <div data-role="footer" class="footer-docs" data-theme="c">
                <p>&copy; <?=date("Y")?> SIPC: Sistema Integral de Pr&eacute;stamos y Cobranzas</p>
        </div>

						

	</div><!-- /content -->
	
</div><!-- /page -->

</body>
</html>