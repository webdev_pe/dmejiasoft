<?php
session_start();
unset($_SESSION['s']); unset($_SESSION['sh']);

date_default_timezone_set("America/Lima");
require("../../poo/clases/getConection.php");
$cn=new getConection();

$dias=-1;
$fecha_ayer=date("Y-m-d", strtotime("$dias days"));
$fecha_hoy=date("Y-m-d");

$sql="select id_cob, id_pre, mnt_cob, dias, adicional, interes, agregar, fecha, cierre, estado 
from si_cobranzas where id_pre=".$_GET['i']." order by fecha asc";

$cn->ejecutar_sql(base64_encode($sql));
$row=$cn->cantidad_sql();

$cn1=new getConection();
 $sql1="select id_pre, tip_pre, mnt_pre, int_pre, dia_pre, fra_pre,
p.id_cli, concat(c.nom_cli,' ',c.ape_cli) as 'nombres', c.cod_cli, p.cod_pre, date_format(fecha,'%d-%m-%Y') as 'fecha', interes_por, est_pre
 from si_prestamos p, si_clientes c
 where p.id_cli=c.id_cli and id_pre=".$_GET['i'];
$cn1->ejecutar_sql(base64_encode($sql1));
$cel1=$cn1->resultado_sql();
?>       
<!DOCTYPE html> 
<html class="ui-mobile-rendering"> 
	<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
	<title>SIPC - C/D&Iacute;A</title> 
	<link rel="stylesheet"  href="../jquery_mobile/css/themes/default/jquery.mobile.css" /> 
	<link rel="stylesheet" href="../jquery_mobile/docs/_assets/css/jqm-docs.css"/>

	<script data-main="../jquery_mobile/js/jquery.mobile.docs" src="../jquery_mobile/external/requirejs/require.js"></script>
	<script src="../jquery_mobile/js/jquery.js"></script>
<style type="text/css">
#tbl_res{ font-size:9px; border-collapse:collapse;}
#tbl_res thead th{ font-size:9px;}
#tbl_res tbody td{ font-size:9px;}
</style>    
</head> 
<body> 

<div data-role="page" class="type-interior">

	<div data-role="header" data-theme="f">
		<h1>P. Arrebatir</h1>
		<a href="../index.php" data-icon="home" data-iconpos="notext" data-direction="reverse" class="ui-btn-right jqm-home">Inicio</a>
	</div><!-- /header -->

	<div data-role="content">		
		<div class="content-primary">
<?php
	echo "<h3>Cr&eacute;dito Consignaci&oacute;n por D&iacute;a (".$cel1['cod_pre'].")</h3>";
	echo "<hr />";
	echo "<strong>Fecha de pr&eacute;stamo:</strong> ".$cel1['fecha']."<br />";
	echo "<strong>Cliente:</strong> ".$cel1['nombres']."<br /> <strong>C&oacute;digo:</strong> ".$cel1['cod_cli']."<br />";
	echo "<strong>Capital</strong>: ".$cn->redondeo($cel1['mnt_pre'])." -
		 <strong>Inter&eacute;s</strong>: ".$cn->redondeo($cel1['fra_pre'])." - 
		 <strong>Interes por</strong>: ".$cn->redondeo($cel1['interes_por'])."";	
	echo "<hr />";
	$monto=$cn->redondeo($cel1['mnt_pre']);
	$inter=$cn->redondeo($cel1['fra_pre']);
	$dev_tot=$monto;	
?>
<table width="100%" border="1" id="tbl_res" style="text-shadow:none; font-size:9px; border-collapse:collapse;">
<thead style="background:#73b242; color:#FFF; text-shadow:none; font-size:9px; border-collapse:collapse;">
  <tr>
    <th width="3%" rowspan="2" align="center" valign="middle">#</th>
    <th width="12%" rowspan="2" align="center" valign="middle">Fecha de cobro</th>    
    <th width="8%" rowspan="2" align="center" valign="middle">Capítal</th>
    <th width="8%" rowspan="2" align="center" valign="middle">Cuota</th>
    <th width="10%" rowspan="2" align="center" valign="middle">Total</th>
    <th colspan="3" align="center" valign="middle">Pago arrebatir</th> 
    <th width="11%" rowspan="2" align="center" valign="middle">Agregado</th>  
    <th width="4%" rowspan="2" align="center" valign="middle" title="Cobranza">*</th>
    <th width="4%" rowspan="2" align="center" valign="middle" title="Eliminar">*</th>         
    </tr>
  <tr>
    <th width="11%" align="center" valign="middle">Amortizaci&oacute;n de capital</th>
    <th width="11%" align="center" valign="middle">Intereses pagados</th> 
    <th width="11%"  align="center" valign="middle">Intereses por pagar</th>       
  </tr>  
</thead>  
<tbody style="text-shadow:none; font-size:9px;">
<script type="text/javascript">
function activar(idc,e,t){ 
	var chk=e;
	$.mobile.showPageLoadingMsg("b", "Guardando, espere un momento porfavor...", true);
	$.post("../../sicpri/01_cobranzas/omitir_cobranza.php", {idc:idc,chk:chk}, function(data){
		if(data==1){
			  $.mobile.hidePageLoadingMsg();
			  //$.mobile.changePage("arrebatir.php?i=<?=$_GET['i']?>&c=<?=$_GET['c']?>",{transition:"flip"});
			  window.location.reload();
		}	
	});
}
function eliminar(idc){
	if(confirm("Esta seguro de eliminar este cobro?")){
		$.post("../../sicpri/01_cobranzas/eliminar_cobro.php",{idc:idc},function(data){
			if(data=="1"){
			  alert("Cobro eliminado correctamente.");
			  window.location.reload();
			}else{
			  alert("Intentelo de nuevo por favor.");
			}			
		});
	}
}
</script>
  <?php 
	$i=1;
	$ult_int=$cel1['fra_pre'];
	$j=0;	
	$montos=$cel1['mnt_pre'];
	$intere=$cn->redondeo($cel1['fra_pre']);
	$totale=$montos+$intere;
	#echo "<br />".date("d-m-Y H:m:i")."<br />";
	$array_fechas=array();
	$array_montos=array();
	
	while($cel=$cn->resultado_sql()){ 
	$fecha=date("d-m-Y",strtotime($cel['fecha']));
	$pago_real=$montos;

		if($cel['estado']=="1"){
			$ultimo+=$cel['mnt_cob'];
			
			if($cel['cierre']=='pa'){ if(!in_array($fecha,$array_fechas)){$array_fechas[$fecha]=$cel['agregar']; }
			}else{ if(!in_array($fecha,$array_montos)){$array_montos[$fecha]=$cel['mnt_cob'];} }
			
			$ayer=date("d-m-Y", strtotime("$fecha -1 days"));
	
			if($ayer!=$_SESSION['s']){
				$ultimo_add1+=$array_fechas[$ayer];
				$ultimo_mnt1+=$array_montos[$ayer];
				$_SESSION['s']=$ayer;
			}else{
				unset($_SESSION['s']);
			}

			$montos=$cel1['mnt_pre']-$ultimo_mnt1+$ultimo_add1; #echo "<br />".$cel1['mnt_pre']."-$ultimo+$ultimo_add1 ======= $montos 1";

		$intere=$cn->redondeo(($montos*$cel1['fra_pre'])/$cel1['mnt_pre']);
		$totale=$montos+$intere;
		$acum1=$ultimo;
		$ult_mnt=$cel['mnt_cob'];
		}else{	
			$ayer=date("d-m-Y", strtotime("$fecha -1 days"));
	
			if($ayer!=$_SESSION['s']){
				$ultimo_add1+=$array_fechas[$ayer];
				$ultimo_mnt1+=$array_montos[$ayer];
				$_SESSION['s']=$ayer;
			}else{
				unset($_SESSION['s']);
			}

			$montos=$cel1['mnt_pre']-$ultimo_mnt1+$ultimo_add1; #echo "<br />".$cel1['mnt_pre']."-$ultimo+$ultimo_add1 ======= $montos 1";		
		}
  ?>
  <tr bgcolor="<?php if($fecha==date("d-m-Y")){echo "#ffe87b";}else{ if($cel['estado']=="0"){echo "#f67c7c";}else{ if($i%2==0){echo "#ffffff";}else{echo "#e2e4ff";} } }?>">
    <td align="center" valign="middle"><?=$i?></th>
    <td align="center" valign="middle"><?=$fecha?></th>
    <td align="right" valign="middle"><?=($cel['agregar']>0)?"0.00":$cn->redondeo($montos)?></td>
    <td align="right" valign="middle"><?=($cel['agregar']>0)?"0.00":$cn->redondeo($intere)?></td>
    <td align="right" valign="middle"><?=($cel['agregar']>0)?"0.00":$cn->redondeo($totale)?></td>
    <td align="right" valign="middle"><?=$cn->redondeo($cel['mnt_cob'])?></td>
    <td align="right" valign="middle"><?=$cn->redondeo($cel['interes'])?></td>   
    <td align="right" valign="middle">
	<?php 
	$porpagar=($cel['agregar']>0)?0:$intere-$cel['interes'];
	if($porpagar<=0){ echo "0.00";
	}else{echo $cn->redondeo($porpagar);} 
	?><!-- br />< ? =$cn->redondeo($porpagar)? --></td>       
    <td align="right" valign="middle"><?php $ult_add+=$cel['agregar']; echo $cn->redondeo($cel['agregar']);?></td>     
    <td>
    <?php if($cel['estado']=="1"){?>
		<?php
        if($cel['fecha']==$fecha_ayer || $cel['fecha']==$fecha_hoy){
            $theme_est='data-theme="a"';
            $function_est="activar('".$cel['id_cob']."','0',$(this))";
        }else{
            $theme_est='';
            $function_est="javascript:alert('Opcion no permitida.')";
        }
        ?> 
        <a onclick="<?=$function_est?>" data-role="button" data-icon="check" data-iconpos="notext" <?=$theme_est?>>Cobranza aprobada</a>
    <?php }else{?>
		<?php
        if($cel['fecha']==$fecha_ayer || $cel['fecha']==$fecha_hoy){
            $theme_est='data-theme="a"';
            $function_est="activar('".$cel['id_cob']."','1',$(this))";
        }else{
            $theme_est='';
            $function_est="javascript:alert('Opcion no permitida.')";
        }
        ?>     
	<a onclick="<?=$function_est?>" data-role="button" data-icon="delete" data-iconpos="notext" <?=$theme_est?>>Cobranza omitido</a>
    <?php }?>
    </td>
    <td align="center" valign="middle">
		<?php
        if($cel['fecha']==$fecha_ayer || $cel['fecha']==$fecha_hoy){
            $theme_eli='data-theme="a"';
            $function_eli="eliminar('".$cel['id_cob']."')";
        }else{
            $theme_eli='';
            $function_eli="javascript:alert('Opcion no permitida.')";
        }
        ?>     
    <a onclick="<?=$function_eli?>" data-role="button" data-icon="minus" data-iconpos="notext" <?=$theme_eli?>>Eliminar cobranza</a>
    
    </td>       
  </tr>
  <?php 
		if($cel['estado']=="1"){
		$acum_pen+=($cel['agregar']>0)?0:$intere;
		$acum_cob+=$cel['mnt_cob'];
		$acum_int+=$cel['interes'];
		$ppagar=($acum_pen-$acum_int);
			
		}
	$i++;	
	} 
	#echo "<br /><br />"; print_r($array_fechas);
	#echo "<br /><br />"; print_r($array_montos);
	$pendiente=$ult_add+$pago_real-$ult_mnt;
	?>

</tbody>
<tfoot style="text-shadow:none; font-size:9px;">
  <tr>
    <th align="center" colspan="5">Totales</th>
    <th align="center" colspan="4">Cuota</th>
    <th align="center"></th>
    <th align="center"></th>
  </tr>
  <tr>
    <th align="left" colspan="5">Total Capital Agregado</th>
    <th align="right">-</th>
    <th align="right">-</th>    
    <th align="right">-</th>        
    <th align="right"><?=$cn->redondeo($ult_add)?></th>
    <th align="center"></th>
    <th align="center"></th>
  </tr>  
  <tr>
    <th align="left" colspan="5">Total Inter&eacute;s Pendiente</th>
    <th align="right">-</th>
    <th align="right">-</th>    
    <th align="right"><?=$cn->redondeo($ppagar)?></th>        
    <th align="right">-</th>
    <th align="center"></th>
    <th align="center"></th>
  </tr>    
  <tr>
    <th align="left" colspan="5">Total Capital e Inter&eacute;s Cobrado</th>
    <th align="right"><?=$cn->redondeo($acum1)?></th>
    <th align="right"><?=$cn->redondeo($acum_int)?></th>    
    <th align="right">-</th>        
    <th align="right">-</th>
    <th align="center"></th>
    <th align="center"></th>
  </tr>
  <tr style="background:#e2e4ff;">
    <th align="left" colspan="5">Total a Pagar a la Fecha de Cobro</th>
    <th align="right" style="border:1px solid #ff0000;">
	<?php
		$amort=end($array_montos);
		$amort=prev($array_montos);
		
		$pago_hoy=$montos-$array_montos[$fecha]+$array_fechas[$fecha];
		$inte_hoy=($pago_hoy*$cel1['fra_pre'])/$cel1['mnt_pre'];
		$real_hoy=$inte_hoy+$ppagar;
		
		echo $cn->redondeo($pago_hoy);		
	?>
    </th>
    <th align="right"                                  ><?=$cn->redondeo($inte_hoy);?></th>    
    <th align="right" style="border:1px solid #ff0000;"><?=$cn->redondeo($real_hoy); ?></th>        
    <th align="right">-</th>
    <th align="center"></th>
    <th align="center"></th>
  </tr>  
  <tr>
  	<?php if($pago_hoy>0 || $inte_hoy>0 || $real_hoy>0){?>
    <th align="center" colspan="11" bgcolor="#ff0000"><h3 style="color:#ffffff;">Pr&eacute;stamo Pendiente</h3></th>
    <?php }else{?>
    <th align="center" colspan="11" bgcolor="#116194"><h3 style="color:#ffffff;">Pr&eacute;stamo Cancelado</h3></th>
	<?php }?>
  </tr>      
</tfoot>
</table>
		</div>
        
        <div class="content-secondary">
        <?php 	$p=($cel1['tip_pre']=="p")?"C/MES":"C/D&Iacute;A";
				$m=($cel1['moroso']=="m")?"MOROSO":"";	?>
        <a href='formulario.php?i=<?=$cel1['id_pre']?>&c=<?=$cel1['cod_cli']?>&p=<?=$cel1['tip_pre']?>' data-role='button' data-icon='grid' data-iconpos='top' data-theme='a'><?=$cel1['cod_pre']?> (<?=number_format($cel1['mnt_pre'],2,'.',',')?>) - <?=$p?> <?=$m?></a>
		<a href="detalle.php?c=<?=$_GET['c']?>" data-role='button' data-icon='arrow-l' data-iconpos='left' data-theme='a'>Regresar</a>
        </div>
        
	</div><!-- /content -->

        <div data-role="footer" class="footer-docs" data-theme="c">
                <p>&copy; <?=date("Y")?> SIPC: Sistema Integral de Pr&eacute;stamos y Cobranzas</p>
        </div>

						

	</div><!-- /content -->
	
</div><!-- /page -->

</body>
</html>