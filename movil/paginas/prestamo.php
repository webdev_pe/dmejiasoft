<?php
session_start();
require("../../poo/clases/getConection.php");
$cn=new getConection();
$sql="select id_cli from si_clientes where cod_cli='".$_GET['c']."'";
$cn->ejecutar_sql(base64_encode($sql));
$cel=$cn->resultado_sql();
?>       
<!DOCTYPE html> 
<html class="ui-mobile-rendering"> 
	<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
	<title>SIPC - Pr&eacute;stamos</title> 
	<link rel="stylesheet"  href="../jquery_mobile/css/themes/default/jquery.mobile.css" /> 
	<link rel="stylesheet" href="../jquery_mobile/docs/_assets/css/jqm-docs.css"/>

	<script data-main="../jquery_mobile/js/jquery.mobile.docs" src="../jquery_mobile/external/requirejs/require.js"></script>
	<script src="../jquery_mobile/js/jquery.js"></script>
</head> 
<body> 
<div data-role="page" class="type-interior">

	<div data-role="header" data-theme="f">
		<h1>Nuevo Cr&eacute;dito</h1>
		<a href="../index.php" data-icon="home" data-iconpos="notext" data-direction="reverse" class="ui-btn-right jqm-home">Inicio</a>
	</div><!-- /header -->

	<div data-role="content">		
		<div class="content-primary">
	  <script type="text/javascript">
      $(document).ready(function(){
		$("input[name=prest]").click(function(){ 
			$("#tip").val($(this).val());
			switch($(this).attr("id")){
				case "opp": $(".porc").show(); $(".arre").hide(); break;
				case "opa": $(".arre").show(); $(".porc").hide(); break;			
			}
		});
			  
		var dias=27;
		$("input[name=dias]").click(function(){
			switch($(this).attr("id")){
				case "op27": dias=27; $("#odias").hide(); break;
				case "op30": dias=30; $("#odias").hide(); break;			
				case "op00": 
					$("#odias").show().focus();
				break;
			}
		});
		var intr=0.08;
		$("input[name=intr]").click(function(){
			switch($(this).attr("id")){
				case "op08": intr=0.08; $("#ointr").hide(); break;
				case "op10": intr=0.10; $("#ointr").hide(); break;			
				case "opOI": 
					$("#ointr").show().focus();
				break;
			}
		});

		$.post("../../sicpri/02_prestamos/traer_saldo.php",{c:'<?=$_GET['c']?>'},function(data){
			$("#mnt_lmt").text(data.dis);
			$("#mnt_cli").val(data.dis);
		},"json");			
		
		$("#prePre").click(function(){
			var cli=<?=$cel['id_cli']?>;
			var usu=<?=$_SESSION['idUsu']?>;
			var tip=$("#tip").val();
			var txd=$("#odias").val();
			var dia=(tip=="p")?((txd!="")?txd:dias):0;
			var txi=$("#ointr").val();
			var int=(tip=="p")?((txi!="")?(txi)/100:intr):0;
			var cuo=(tip=="a")?$("#cuota").val():0;
			var mnt=$("#monto").val();
			var inp=($("#intp").val()=="")?0:$("#intp").val();
			//alert(tip);
			var lim=$("#mnt_cli").val();
			var lmt=parseFloat(lim.replace(",",""));
			var vmt=parseFloat(mnt);
			
			if(tip=="a" && cuo==""){ alert("Ingrese una cuota."); $("#cuota").focus(); }		
			else if(mnt==""){ alert("Ingrese un monto."); $("#monto").focus(); }
			else if(lmt<vmt){ alert("El monto no debe superar el saldo disponible ("+lmt+")."); $("#monto").focus();}
			else{
				$.mobile.showPageLoadingMsg("b", "Guardando, espere un momento porfavor...", true);
				$.post("../../sicpri/02_prestamos/dao.php",{opt:"i",cli:cli,usu:usu,tip:tip,dia:dia,int:int,cuo:cuo,mnt:mnt,inp:inp,pre:"movil"},function(data){
					if(data==1){ 
						alert("Prestamo registrado correctamente.");
						$.mobile.hidePageLoadingMsg();
						$.mobile.changePage("modulos.php?c=<?=$_GET['c']?>",{transition:"flip"});
					}else{
						alert("Vuelva a intentarlo por favor.");
						$.mobile.hidePageLoadingMsg();
					}
				});
			}
		});				 
	  });
      </script>
      <strong>Cliente: </strong> <span><?=$_GET['c']?></span>  <br/>
      <strong>Saldo:</strong> <span id="mnt_lmt"></span><br/>
      <input type="hidden" id="mnt_cli" />
      <fieldset data-role="controlgroup" data-type="horizontal" data-role="fieldcontain">
        <legend>Tipo de Cr&eacute;dito:</legend>
        <input type="radio" value="p" name="prest" id="opp" checked="checked" />
        <label for="opp">Consig./Mes</label>
        <input type="radio" value="a" name="prest" id="opa" />
        <label for="opa">Consig./D&iacute;a</label>
      </fieldset>
      <input type="hidden" id="tip" value="p" />
	  <div class="porc">
          <fieldset data-role="controlgroup" data-type="horizontal" data-role="fieldcontain">
            <legend>Cantidad de D&iacute;as:</legend>
            <input type="radio" value="27" name="dias" id="op27" checked="checked" /><label for="op27">27</label>
            <input type="radio" value="30" name="dias" id="op30" /><label for="op30">30</label>
            <input type="radio" value="00" name="dias" id="op00" /><label for="op00">Otro #</label>
          </fieldset>
		  <input type="text" id="odias" style="display:none;" />
	  </div>
	  <div class="porc"> 
          <fieldset data-role="controlgroup" data-type="horizontal" data-role="fieldcontain">
          <legend>Cantidad de Inter&eacute;s:</legend>
            <input type="radio" value="0.08" name="intr" id="op08" checked="checked" /><label for="op08">8%</label>
            <input type="radio" value="0.10" name="intr" id="op10" /><label for="op10">10%</label>
            <input type="radio" value="00"   name="intr" id="opOI" /><label for="opOI">Otro %</label>
          </fieldset>    
		  <input type="text" id="ointr" style="display:none;" />
	  </div>
      <div class="norm">
      Monto a prestar<br />
      <input type="text" id="monto" />
      </div>
      <div class="arre" style="display:none;">
      Cuota a pagar<br />
      <input type="text" id="cuota" />
      </div>
      <div class="arre" style="display:none;">
      Inter&eacute;s por<br />
      <input type="text" id="intp" />
      </div>         

            <div class="ui-body ui-body-b">
                <button class="btnLogin" type="submit" data-icon='check' data-iconpos='top' 
                    data-theme="b" id="prePre">Registrar cr&eacute;dito</button>
            </div>  
		</div>


		<div class="content-secondary">
        <?php if($_GET['d']=="i"){?>
        <a href="modulos.php?c=<?=$_GET['c']?>" data-role='button' data-icon='arrow-l' data-iconpos='left' data-theme='a'>Regresar</a>
        <?php }else{?>
		<a href="detalle.php?c=<?=$_GET['c']?>" data-role='button' data-icon='arrow-l' data-iconpos='left' data-theme='a'>Regresar</a>		   
        <?php }?>
        </div>
		</div><!-- /content -->

        <div data-role="footer" class="footer-docs" data-theme="c">
                <p>&copy; <?=date("Y")?> SIPC: Sistema Integral de Pr&eacute;stamos y Cobranzas</p>
        </div>

						

	</div><!-- /content -->
	
</div><!-- /page -->

</body>
</html>