<!DOCTYPE html> 
<html class="ui-mobile-rendering"> 
	<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
	<title>SIPC - ACCESO SIPC</title> 
	<link rel="stylesheet"  href="../jquery_mobile/css/themes/default/jquery.mobile.css" /> 
	<link rel="stylesheet" href="../jquery_mobile/docs/_assets/css/jqm-docs.css"/>
	<script data-main="../jquery_mobile/js/jquery.mobile.docs" src="../jquery_mobile/external/requirejs/require.js"></script>
	<script src="../jquery_mobile/js/jquery.js"></script>  
</head> 
<body> 

<div data-role="page" class="type-interior">

	<div data-role="header" data-theme="f">
		<h1>ACCESO AL SISTEMA</h1>
		<a href="../index.php" data-icon="home" data-iconpos="notext" data-direction="reverse" class="ui-btn-right jqm-home">Inicio</a>
	</div><!-- /header -->

	<div data-role="content">
		<div class="content-primary">
        <strong>Ingrese sus datos de usuario:</strong>
		<script type='text/javascript'>//<![CDATA[ 
        $(document).ready(function(){ 		
			$("#btnEnter").click(function(){
				var u=$("#usu").val();
				var p=$("#psw").val();
				if(u==""){alert("Ingrese su codigo de usuario");$("#usu").focus();}
				else if(p==""){alert("Ingrese su clave de acceso");$("#psw").focus();}
				else{
					$.mobile.showPageLoadingMsg("b", "Enviando datos, espere un momento porfavor...", true);
					$.post("funciones/login.php",{u:u,p:p},function(data){
						$.mobile.hidePageLoadingMsg();
						switch(data){
							case "1": $.mobile.changePage("modulos.php",{transition:"flip"}); break
    	                    case "2": $("#result").empty().html("C&oacute;digo de usuario y/o contrase&ntilde;a incorrectos."); break;
	                        case "3": $("#result").empty().html("Su cuenta no se reconoce como cuenta de operador."); break;	
							case "4": $("#result").empty().html("Cuenta desactivada, comun&iacute;quese con el administrador del sistema."); break;							
						}
					})
				}
			});
        });//]]>  
        </script> 		
        <div data-role="fieldcontain">
        <label for="name">C&oacute;digo de Usuario:</label><br/>
        <input type="text" name="usu" id="usu" value="" placeholder="Usuario" class="required"  />
        </div>
        <div data-role="fieldcontain">
        <label for="name">Contrase&ntilde;a:</label><br/>
        <input type="password" name="psw" id="psw" value="" placeholder="Contrase&ntilde;a" class="required"  />
        </div>
        <div class="ui-body ui-body-b">
        <button class="btnLogin" type="submit" data-icon='check' data-iconpos='top' data-theme="a" id="btnEnter">Ingresar</button>
        </div>        
        <br/>
		<div id="result" style="text-align:center; font-weight:bold; color:<?php if($_GET['m']=="m"){echo "#116194;";}else{echo "#ff0000;";}?>">
        <?php if($_GET['m']=="m"){?>
        Sesi&oacute;n cerrada correctamente.
		<?php }else{?>
        Por favor ingrese sus datos de acceso.
		<?php }?>
        </div>  
        <br/>   
		</div>		
        
        <div class="content-secondary">
		<a href="../index.php" data-role='button' data-icon='arrow-l' data-iconpos='left' data-theme='a'>Regresar</a>
        </div>        
	</div><!-- /content -->
    <div data-role="footer" class="footer-docs" data-theme="c">
            <p>&copy; <?=date("Y")?> SIPC: Sistema Integral de Pr&eacute;stamos y Cobranzas</p>
    </div>
	</div><!-- /content -->
	
</div><!-- /page -->

</body>
</html>