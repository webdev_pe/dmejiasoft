<!DOCTYPE html>
<html class="ui-mobile-rendering">
<head>
	<meta charset="utf-8">
	<!--meta name="viewport" content="width=device-width, initial-scale=1"-->
	<title>SIPC: Sistema Integral de Pr&eacute;stamos y Cobranzas</title>
	<link href="icon.png" rel="shortcut icon" />    
	<link rel="stylesheet"  href="jquery_mobile/css/themes/default/jquery.mobile.css" />
	<link rel="stylesheet" href="jquery_mobile/docs/_assets/css/jqm-docs.css" />
	<script data-main="jquery_mobile/js/jquery.mobile.docs" src="jquery_mobile/external/requirejs/require.js"></script>
	<script src="jquery_mobile/js/jquery.js"></script>
    <script src="scripts.js"></script>
</head>
<body onload="initPage()">
<div data-role="page" class="type-home">
	<div data-role="content">
		<!--p id="jqm-version">1.0 Final Release</p-->


		<div class="content-secondary">

			<div id="jqm-homeheader">
				<h1 id="jqm-logo"><img src="images/logo.png" alt="SISTEMA DE COBRANZA" /></h1>
			</div>


			<p class="intro"><strong>Bienvenido.</strong> Seleccione una de las opciones a continuaci&oacute;n:</p>

			<ul data-role="listview" data-inset="true" data-theme="c" data-dividertheme="f">
				<li data-role="list-divider">Acceso</li>
				<li><a href="paginas/acceso.php" data-transition="flip" data-inline="true">Ingreso al sistema</a></li>
			</ul>

		</div><!--/content-primary-->

		<div class="content-primary">
			<nav>

				<ul data-role="listview" data-inset="true" data-theme="c" data-dividertheme="b">
					<li data-role="list-divider">Soporte On-Line</li>
					<li><a href="paginas/compatibilidad.php" data-transition="flip" data-inline="true">Compatibilidad M&oacute;vil</a></li>
					<li><a href="paginas/plataformas.php" data-transition="flip" data-inline="true">Soporte de Plataforma</a></li>
				</ul>
			</nav>
		</div>

	</div>

	<div data-role="footer" class="footer-docs" data-theme="c">
			<p>&copy; <?=date("Y")?> SIPC: Sistema Integral de Pr&eacute;stamos y Cobranzas</p>
	</div>

</div>
</body>
</html>
