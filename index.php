<?php
include("detectmobilebrowser.php");
session_start(); 
require_once("poo/clases/getConection.php");
if(isset($_COOKIE['sicprii']) && $_COOKIE['sicprii']!=""){
	$cn=new getConection();
	$sql=base64_encode("select user, pswd from si_usuarios where id_usu=".base64_decode($_COOKIE['sicprii']));
	$cn->ejecutar_sql($sql);
	$row=$cn->cantidad_sql();
	$cel=$cn->resultado_sql();
	$cn->limpiar_sql();
	$cn->cerrar_sql();
	
	$script.=" $(\"#chk\").trigger(\"click\"); ";	
}

if(isset($_GET['c'])){
	switch(urldecode(base64_decode($_GET['c']))){
	case "error":
		$script.=" $(\"#msj\").removeClass().addClass(\"ui-state-error ui-corner-all \").html(\" ".urldecode(base64_decode($_GET['m']))." \")
				 .prepend(\"<span class='ui-icon ui-icon-alert'></span>\").wrap(\"<div class='ui-widget'></div>\"); ";
		break;
	case "logout":
		$script.=" $(\"#msj\").removeClass().addClass(\"ui-state-highlight ui-corner-all \").html(\"".urldecode(base64_decode($_GET['m']))."\")
				 .prepend(\"<span class='ui-icon ui-icon-info'></span>\").wrap(\"<div class='ui-widget'></div>\"); ";
		break;
	}
}
$script="<script type=\"text/javascript\">$(document).ready(function(){ $script });</script>";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SIPC - SISTEMA INTEGRAL DE PR&Eacute;STAMOS Y COBRANZAS</title>
<link href="images/favicon.png" rel="shortcut icon" />
<link href="css/login.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery_layout/jquery-latest.js"></script>
<link type="text/css" href="js/jquery-ui-1.8.18.custom/css/custom-theme/jquery-ui-1.8.18.custom.css" rel="stylesheet" />	
<script type="text/javascript" src="js/jquery-ui-1.8.18.custom/js/jquery-ui-1.8.18.custom.min.js"></script>
<script type="text/javascript" src="js/login.js"></script>
<?=$script;?> 
</head>
<body>
<div id="cabecera"></div>
<div id="cuerpo">
	<div id="logo"></div>
	<div id="franja"><div id="titulo"></div></div>
	<div id="fondo"></div>    
	<div id="contenedor">
		<div id="tag_user"></div>
		<div id="input_text"><input type="text" id="user" value="<?=($cel['user'])?$cel['user']:"usuario"?>" /></div>
		<div id="tag_pswd"></div>
		<div id="input_pswd"><input type="password" id="pswd" value="<?=($cel['pswd'])?$cel['pswd']:"contraseña"?>" /></div>
	  	<div id="chk"></div>
	    <div id="tag_recordar"><input type="hidden" id="rmbr" value="0" /></div>
		<div id="msj"></div>
  </div>  
  <div id="ingresar"></div>    
</div>
<div id="pie"></div>
</body>
</html>